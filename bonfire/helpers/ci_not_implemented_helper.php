<?php

/**
 * Send an email
 *
 * @access  public
 * @return  bool
 */
if ( ! function_exists('send_email'))
{
    function send_email($recipient, $subject = 'Test email', $message = 'Hello World')
    {
        throw new Exception("send_email() is not implemented. Use Emailer class instead.");
    }
}
