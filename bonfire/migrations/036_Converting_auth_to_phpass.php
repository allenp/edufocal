<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
	In 0.7, we are moving from the salted password generation that we had
	to use the phpass 0.3 Password Hasing algorithm.
	
	This does make it impossible to convert the passwords but we will 
	make it so that the users must change their password on next login.
*/
class Migration_Converting_auth_to_phpass extends Migration
{

	//--------------------------------------------------------------------

	public function up()
	{
		$this->load->dbforge();

        $this->db->query("ALTER TABLE  `users` CHANGE  password_hash  password_hash VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");

        $this->db->query("Alter TABLE `users` ADD COLUMN force_password_reset TINYINT(1) NOT NULL DEFAULT 0 AFTER password_hash;");

        $this->db->query("Alter TABLE `users` ADD COLUMN password_iterations TINYINT(4) NOT NULL DEFAULT 8 AFTER force_password_reset ;");
        $this->db->query("INSERT INTO `permissions` (permission_id, name, description, status) VALUES (NULL, 'Raffles.Content.View', 'View raffles', 'active');");

        $this->db->query("INSERT INTO `permissions` (permission_id, name, description, status) VALUES (NULL, 'Teachers.Content.View', 'View teachers', 'active');");

        $this->db->query("INSERT INTO `permissions` (permission_id, name, description, status) VALUES (NULL, 'Pages.Content.View', 'View pages', 'active');");

        $this->db->query("INSERT INTO `permissions` (permission_id, name, description, status) VALUES (NULL, 'Dashboard.Settings.View', 'View student dashboard settings', 'active');");

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->load->dbforge();
		
		// Drop the password_iterations column
		$this->dbforge->drop_column('users', 'password_iterations');
		
		// Reshape the password_hash column
		$fields = array(
			'password_hash'	=> array(
				'type'			=> 'varchar',
				'constraint'	=> 40
			),
		);
		$this->dbforge->modify_column('users', $fields);
		
		// Remove the force_password_reset column
		$this->dbforge->drop_column('users', 'force_password_reset');
		
		// Remove the password_iterations setting
        //$this->load->library('settings/settings_lib');
        //$this->settings_lib->delete('password_iterations', 'users');

        $this->db->query("DELETE FROM `permissions` WHERE name = 'Raffles.Content.View' ");
        $this->db->query("DELETE FROM `permissions` WHERE name = 'Teachers.Content.View' ");
        $this->db->query("DELETE FROM `permissions` WHERE name = 'Pages.Content.View' ");
        $this->db->query("DELETE FROM `permissions` WHERE name = 'Dashboard.Settings.View' ");
	}

	//--------------------------------------------------------------------

}
