<?php

namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class AcceptanceHelper extends \Codeception\Module\PhpBrowser
{
    public function _initialize()
    {
        parent::_initialize();
        exec("mysql -uroot -p12desciples edufocal_test < ./tests/_data/test.sql");
    }

    public function answerQuestionsOnPage()
    {
        $nodes = $this->match('.choices li:first-child input');
        foreach ($nodes as $el) {
            $radios = $this->match($el->getAttribute('name'));
            $this->selectOption($el->getAttribute('name'), rand(1, count($radios)));
        }
    }
}
