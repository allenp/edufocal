<?php

namespace Codeception\Module;

class QuizCest extends \Codeception\Module\WebDriver
{
    public function _answerQuestionsOnPage()
    {
        $nodes = $this->match($this->webDriver, '.choices');
        codecept_debug($nodes->count());
        foreach ($nodes as $el) {
            if ($radios = $el->filter('input[@type="radio"]')->count()) {
                $radios->first()->select(1);
            }
        }
    }
}
