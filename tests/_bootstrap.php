<?php
// This is global bootstrap for autoloading
//

if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50300)
	die('PHP ActiveRecord requires PHP 5.3 or higher');

define('PHP_ACTIVERECORD_VERSION_ID','1.0');

define('APATH', dirname(__FILE__) . '/../application/libraries/');

require APATH. 'ActiveRecord/Singleton.php';
require APATH. 'ActiveRecord/Config.php';
require APATH. 'ActiveRecord/Utils.php';
require APATH. 'ActiveRecord/DateTime.php';
require APATH. 'ActiveRecord/Model.php';
require APATH. 'ActiveRecord/Table.php';
require APATH. 'ActiveRecord/ConnectionManager.php';
require APATH. 'ActiveRecord/Connection.php';
require APATH. 'ActiveRecord/SQLBuilder.php';
require APATH. 'ActiveRecord/Reflections.php';
require APATH. 'ActiveRecord/Inflector.php';
require APATH. 'ActiveRecord/CallBack.php';
require APATH. 'ActiveRecord/Exceptions.php';
require APATH. 'ActiveRecord/Cache.php';

try {

	ActiveRecord\Config::initialize(
		function($cfg) {
			$cfg->set_model_directory(APATH.'../activerecord');
			//$cfg->set_cache('file://localhost', array('expire' => 3600));
			$cfg->set_logging(true);
			$cfg->set_logger(new EFLogger());
			$cfg->set_connections(array('development' => 'mysql://root:12desciples@localhost/edufocal_test?charset=utf8'));
		}
	);

} catch ( Exception $e ){
	exit($e->getMessage());
}

class EFLogger
{
	public function log ( $log )
	{
		static $counter = 0;
		$counter++;
	}
}

spl_autoload_register('activerecord_autoload');

function activerecord_autoload($class_name)
{
	$path = ActiveRecord\Config::instance()->get_model_directory();
	$root = realpath(isset($path) ? $path : '.');

	if (($namespaces = ActiveRecord\get_namespaces($class_name)))
	{
		$class_name = array_pop($namespaces);
		$directories = array();

		foreach ($namespaces as $directory)
			$directories[] = $directory;

		$root .= '/'. implode($directories, '/');
	}

	$file = "$root/$class_name.php";

	if (file_exists($file))
		require $file;
}

