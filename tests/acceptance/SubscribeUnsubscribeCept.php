<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('Subscribe and then unsubscribe');
$I->wantTo('Log in as a regular user');
$I->amOnPage('/');
$I->fillField('email', 'paul@paulallen.org');
$I->fillField('password', 'tuudhfho');
$I->click('submit');
$I->see('Hello, Paul');
$I->amOnPage('/account/payment');

$subscribed = $I->grabTextFrom('.pull-right > h4');

if ($subscribed == 'Subscription Enabled') {
	$I->click('Unsubscribe');
	$I->see('Setup Auto-renew Subscription');
	$I->click('input[name=subscribe_option]');
	$I->see('Subscription Enabled');
} else {
	$I->see('Setup Auto-renew Subscription');
	$I->click('input[name=subscribe_option]');
	$I->see('Subscription Enabled');
}
