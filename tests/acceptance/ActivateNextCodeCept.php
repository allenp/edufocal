<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('Activate next user code if current one is expired.');
$I->amOnPage('/signup');
$I->fillField('first_name', 'Activate');
$I->fillField('last_name', 'Next');
$I->selectOption('input[name="sex"]', 'f');
$I->fillField('phone', '3353910');
$I->fillField('td input[name="email"]', 'next+code@paulallen.org');
$I->fillField('table input[name="password"]', 'tuudhfho');
$I->fillField('table input[name="password_confirmation"]', 'tuudhfho');
$I->click('table button');
$I->see('Welcome Activate');
$I->selectOption('#card_option', 'card');
$I->selectOption('#plan', 'monthly');
$I->selectOption('#currency', 'JMD');
$I->fillField("card_number", '4111111111111111');
$I->fillField('card_cvc', '143');
$I->fillField('card_name', 'Activate Next');
$I->selectOption('card_expiry_month', 05);
$I->selectOption('card_expiry_year', '18');
$I->fillField('address1', '9 Druesdale Avenue');
$I->fillField('address2', 'Kingston 19');
$I->fillField('city', 'Kingston');
$I->fillField('state', '');
$I->selectOption('country', 'JM');
$I->click('td button');
$I->see('Review and confirm your purchase');
$I->click('submit');
$I->see('You\'re finally a member of the EduFocal family');

$I->amOnPage('/account/card');
$I->selectOption('#plan', 'monthly');
$I->selectOption('#currency', 'JMD');
$I->fillField("card_number", '4111111111111111');
$I->fillField('card_cvc', '143');
$I->fillField('card_name', 'Trever Noah');
$I->selectOption('card_expiry_month', 05);
$I->selectOption('card_expiry_year', '18');
$I->fillField('address1', '9 Druesdale Avenue');
$I->fillField('address2', 'Kingston 19');
$I->fillField('city', 'Kingston');
$I->fillField('state', '');
$I->selectOption('country', 'JM');
$I->click('input[name=card_option]');
$I->see('Review and confirm your purchase');
$I->click('submit');
$I->see('Thank you! You can view your current and past receipts');

$user = User::find_by_email('next+code@paulallen.org');
$user->code->expiry_date = 0;
$user->code->save();

$I->amOnPage('/dashboard');
$I->see('Hello, Activate');

