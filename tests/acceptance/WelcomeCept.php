<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Ensure the front page loads properly');
$I->amOnPage('/');
$I->see('The future of learning is here');
