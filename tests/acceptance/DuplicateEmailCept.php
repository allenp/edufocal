<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Create two accounts with same email address to get duplicate error');


$I->amOnPage('/signup');
$I->fillField('first_name', 'Trevor');
$I->fillField('last_name', 'Noah');
$I->selectOption('input[name="sex"]', 'm');
$I->fillField('phone', '3353910');
$I->fillField('td input[name="email"]', 'paul+trevor@paulallen.org');
$I->fillField('table input[name="password"]', 'tuudhfho');
$I->fillField('table input[name="password_confirmation"]', 'tuudhfho');
$I->click('table button');
$I->see('Welcome Trevor');
$I->selectOption('#card_option', 'card');
$I->selectOption('#plan', 'monthly');
$I->selectOption('#currency', 'JMD');
$I->fillField("card_number", '4111111111111111');
$I->fillField('card_cvc', '143');
$I->fillField('card_name', 'Trever Noah');
$I->selectOption('card_expiry_month', 05);
$I->selectOption('card_expiry_year', '18');
$I->fillField('address1', '9 Druesdale Avenue');
$I->fillField('address2', 'Kingston 19');
$I->fillField('city', 'Kingston');
$I->fillField('state', '');
$I->selectOption('country', 'JM');
$I->click('td button');
$I->see('Review and confirm your purchase');
$I->click('submit');
$I->see('You\'re finally a member of the EduFocal family');

$I->amOnPage('/logout');

$I->amOnPage('/signup');
$I->fillField('first_name', 'Trevor');
$I->fillField('last_name', 'Noah');
$I->selectOption('input[name="sex"]', 'm');
$I->fillField('phone', '3353910');
$I->fillField('td input[name="email"]', 'paul+trevor@paulallen.org');
$I->fillField('table input[name="password"]', 'tuudhfho');
$I->fillField('table input[name="password_confirmation"]', 'tuudhfho');
$I->click('table button');
$I->see('The Email you have selected is already in use.');

