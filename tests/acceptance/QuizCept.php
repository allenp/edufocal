<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Take a test');

//Log in
$I->amOnPage('/');
$I->fillField('email', 'paul@paulallen.org');
$I->fillField('password', 'tuudhfho');
$I->click('submit');
$I->see('Hello, Paul');

//Find a test
$I->amOnPage('dashboard/subjects');
$I->see('Select a Subject');
$I->click('.subject li a:first-child');
$I->seeElement('.topic');

//$I->click('//a[text()=\'Probability\']');
//$I->click('Probability');
//$I->click('.topic li:last-child a');
//$I->click(['link', 'Probability']);
//$I->click('.topic li:last-child a:first-child');


////This became necessary because I couldnt get the selector to work
$I->amOnPage('/dashboard/subjects/by_subject/mathematics/probability');
$I->see('You will have twenty minutes to complete this test.');
//
////Start test
$I->click('input[name="timed"]');
$I->see('Mathematics: Probability');
$I->dontSeeElement('.previous');
//
do
{
	$I->answerQuestionsOnPage();
	$next = $I->grabValueFrom(['name' => 'next']);
	codecept_debug('The value of the next button is ' . $next);
	if ($next == 'Next Page') {
		$I->click('input[name=next]');
		$I->see('Previous Page');
	} else {
		$I->seeElement(['class' => 'finish']);
		$I->click('input[name=next]');
		break;
	}
//
}while(true);
//
//
$I->see('Test Completed!');
