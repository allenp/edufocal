<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('logout and return to the home page');

$I = new AcceptanceTester($scenario);
$I->wantTo('Log in as a regular user');
$I->amOnPage('/');
$I->fillField('email', 'paul@paulallen.org');
$I->fillField('password', 'tuudhfho');
$I->click('submit');
$I->see('Hello, Paul');
$I->amOnPage('/logout');
$I->see('The future of learning is here');
