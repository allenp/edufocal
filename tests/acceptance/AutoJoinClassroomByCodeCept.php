<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('Join Classroom Using Signup Code');

$I->amOnPage('/signup');
$I->fillField('first_name', 'Trevor');
$I->fillField('last_name', 'Noah');
$I->selectOption('input[name="sex"]', 'm');
$I->fillField('phone', '3353910');
$I->fillField('td input[name="email"]', 'trevor+classroomcode@paulallen.org');
$I->fillField('table input[name="password"]', 'tuudhfho');
$I->fillField('table input[name="password_confirmation"]', 'tuudhfho');
$I->click('table button');
$I->see('Welcome Trevor');
$I->selectOption('#code_option', 'code');
$I->fillField('table input[name="educode"]', '5698794523285');
$I->click('submit');
$I->see('You\'re finally a member of the EduFocal family');
$I->seeInDatabase('classroom_students', array('classroom_id' => '1'));

