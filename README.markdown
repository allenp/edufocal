## EduFocal

Welcome to EduFocal source. This project is primarily built on CI-Bonfire (which is a mod of CodeIgniter 2.XX).
We have also added composer support to the project since.

## Setup Environment
Once you have cloned the repository please also run
```
composer install
```
In order to use the proper configuration files
```
./setup development
```
Use these values where appropriate.
- development
- production
- testing

Ensure that your configuration settings are setup per the environment you are using
```
application/config/<<ENVIRONMENT NAME>>/database.php
```

##To Run Tests
There are a couple things that need to be set (I'm going to make these simpler soon)

* Create an empty database named ```edufocal_test```
* Setup your application/config/testing/database.php settings to point to this database
* Edit ```./tests/_support/AcceptanceHelper.php``` to have your database configuration
* Edit ```./codeception.yml``` to have your database configuration
* Edit ```./tests/acceptance.yml``` to have your test site location
* Run ```./setup testing``` from your root directory to activate testing configuration

From your root directory, run:
```
./vendor/bin/codecept run
```

You can also add these aliases to your .bashrc file
which allows you to type ```cept``` from anywhere in your codebase to run all tests

```
alias ccd='cd "$(git rev-parse --show-toplevel)"'
alias cept='ccd && ./vendor/bin/codecept run'
```

Run ```source ~/.bashrc``` to make the aliases active right away


## Road Map
- Code quality checks on git commit on client


## Grunt commands
Setup grunt and all the package dependencies
```
npm install
```

Tell Grunt to watch for css + js file changes. Grunt will also compile and minify the associated files.
```
grunt watch &
```
