<?php

class Front extends Front_Controller
{
	public function __construct ()
	{
		parent::__construct();

        Template::set_theme('public');

		
	}

	public function index ()
	{
        if ($this->auth->is_logged_in()) {
			if($this->auth->profile()->code_id == 0) {
				redirect('signup/payment');
			} else {
				redirect('dashboard');
			}
		}

        // TODO: put this into memcache or local cache store
        // This data doesn't change and can be stored in memcache
        // Update this "as needed"

        $cxcs = Subject::find_all_by_level("CXC", array("order" => "RAND()"));
        $gsats = Subject::find_all_by_level("GSAT", array("order" => "RAND()"));

        Template::set('cxcs', $cxcs);
        Template::set('gsats', $gsats);

        // TODO: put this into memcache or local cache store
        // Update this "daily"
        $join = "INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1";
        $condition = array("users.exam_level = ? and student_scores.track_id = ?", 'GSAT', 2);
        $top_gsat_students = StudentScore::find('all', array('limit' => 5, 'order' => 'score desc', 'joins' => $join, 'conditions' => $condition));
        $condition = array("users.exam_level = ? and student_scores.track_id = ?", 'CXC', 3);
        $top_cxc_students = StudentScore::find('all', array('limit' => 5, 'order' => 'score desc', 'joins' => $join, 'conditions' => $condition));

        Template::set('top_gsat_students', $top_gsat_students);
        Template::set('top_cxc_students', $top_cxc_students);

        Template::set_view('public/index');
        Template::render();
	}

    public function flow()
    {
        Template::set_theme('flow');
        Template::set_view('flow/index');
        Template::render();
    }
}
