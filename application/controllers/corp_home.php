<?php

class Corp_Home extends Front_Controller
{
	public function __construct ()
	{
		parent::__construct();

        Template::set_theme('public');

		if ($this->auth->is_logged_in()) {
			if($this->auth->profile()->code_id == 0) {
				redirect('signup/payment');
			} else {
				redirect('dashboard');
			}
        }
	}

	public function index ()
	{
        Template::set_view('public/corp_index');
        Template::render();
	}
}
