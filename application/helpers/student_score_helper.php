<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*@author: Paul Allen
 *Description: calculates student's subject score
 *
 */

if(!function_exists('ef_calculate_exp_points'))
{
    function ef_calculate_exp_points( $answers, $recalculate = false )
    {
        $exp = 0;
        $total_exp = 0;
        $result = array(
            'gained_points'        => 0,
            'lost_points'          => 0,
            'net_points'           => 0,
            'total_topic_points'   => 0,
            'is_new_topic_level'   => FALSE,
            'is_new_subject_level' => FALSE
        );


        if(count($answers) == 0)
            return $result;

        $track = StudentTrack::find_by_user_id($answers[0]->user_id);

        $test_exp = StudentExpTestPoints::find_by_test_id_and_user_id(
            $answers[0]->test_id, $answers[0]->user_id);
        $current_points = StudentExpPoint::find_by_user_id_and_topic_id_and_track_id(
            $answers[0]->user_id, $answers[0]->topic_id, $track->track_id);

        if($test_exp != null && $current_points != null && !$recalculate)
        {
            $exp_schedules = ExpPointsSchedule::find(
                $current_points->level);

            $result['net_points'] = $test_exp->net_points;
            $result['gained_points'] = $test_exp->gained_points;
            $result['lost_points'] = $test_exp->lost_points;
            $result['total_topic_points'] = $current_points->points;
            $result['current_topic_level'] = $current_points->level;
            $result['max_level_topic_points'] = $exp_schedules->points_required;
            if(is_production())
                return $result;
        }

        if($current_points == null)
        {
           $current_points = new StudentExpPoint( array(
              'user_id' => $answers[0]->user_id,
              'track_id' => $track->track_id,
              'topic_id' => $answers[0]->topic_id,
              'level' => is_numeric($answers[0]->question->level) ?
                             $answers[0]->question->level : 1,
              'points' => 0 )
            );
        }

        if($test_exp == null)
        {
            $r = array(
                'test_id' => $answers[0]->test_id,
                'user_id' => $answers[0]->user_id,
                'track_id' => $track->track_id,
                'net_points' => 0,
            );
            $test_exp = new StudentExpTestPoints($r);
        }

        $exp_schedules = ExpPointsSchedule::find(
            $current_points->level, $current_points->level + 1 );

        $questions = array();
        foreach($answers as $answer)
            $questions[] = $answer->question_id;


        $ans_count = Answer::find('all',
            array('select' => 'question_id, count(*) times',
                  'group' => 'question_id',
                  'conditions' => array(
                      'user_id = ? and question_id in (?) AND test_type = ?',
                      $answers[0]->user_id, $questions, 'timed')
                  )
              );

        foreach($answers as $answer) 
        {
            if($answer->question->type == 1) //multiple choice
            {
                //calc exp points
                $diff = $answer->question->difficulty ?
                    $answer->question->difficulty : 0.5;
                $level = $answer->question->level ?
                    $answer->question->level : 1;
                $diff = $level + $diff;

                if($answer->answer == $answer->question->accepted_answers)
                {
                    $times = 1;
                    foreach($ans_count as $key => $ques)
                    {
                        if($ques->question_id === $answer->question_id)
                        {
                            $times = $ques->times > 0 ? $ques->times : 1;
                            unset($ans_count[$key]);
                            break;
                        }
                    }
                    $exp = ef_experience($diff, $level, 1.3) / $times;
                    $total_exp += $exp;
                    $result['gained_points'] += $exp;
                }
                else
                {
                    /*subtract a fraction of the points that would
                     * have been awarded the fraction is 1 / number
                     * of choices for the question
                     * */

                    $count = count((array)json_decode(
                                            $answer->question->choices));

                    $exp = 1 / $count * ef_experience($diff, $level, 1.3);

                    $total_exp -= $exp;
                    $result['lost_points'] -= $exp;
                }
            }
        }

        $current_points->points += $total_exp;
        $test_exp->lost_points = $result['lost_points'];
        $test_exp->gained_points = $result['gained_points'];
        $test_exp->net_points = $total_exp;

        if($exp_schedules[0]->points_required < $current_points->points)
        {
            $result['is_new_topic_level'] = TRUE;
            $result['new_topic_level'] = $current_points->level + 1;
            $result['current_topic_level'] = $current_points->level;
            $result['max_level_topic_points'] = round(
                                    $exp_schedules[1]->points_required);
            //an inherent assumption here is that we're jumping from a consecutive level
            //and not multiple levels.
            //or this would def be wrong. (referring to the line below)
            $result['level_topic_progress'] = round($exp_schedules[1]->points_required - ($current_points->points - $exp_schedules[0]->points_required));
            $current_points->level += 1;
        }
        else
        {
            $result['current_topic_level'] = $current_points->level;
            $result['max_level_topic_points'] = round($exp_schedules[0]->points_required);
        }

        $current_points->save();
        $test_exp->save();

        $result['net_points'] = $total_exp; //how much user just earned on this test
        $result['total_topic_points'] = $current_points->points; //how much user has earned on topic overall

        $subject_result = ef_calculate_subject_score($answers[0]->user_id, Topic::find($answers[0]->topic_id)->subject_id, $answers[0]->topic_id, $total_exp, $track->track_id);

        if($subject_result !== FALSE && is_array($subject_result))
            $result = array_merge($result, $subject_result);

        return $result;
    }
}


if(!function_exists('ef_get_topic_weights'))
{
    function ef_get_topic_weights($subject_id, $user_id)
    {
        $topic_scores = StudentExpPoint::find_by_sql(
        "SELECT t.id, exp.user_id, ifnull(exp.points,0.0) points, exp.level, w.weight
         FROM topics t
         left join student_exp_points exp on t.id = exp.topic_id and exp.user_id = {$user_id}
        left join topic_weights w on w.topic_id = t.id
         where t.subject_id = {$subject_id}");

         $topic_count = count($topic_scores);

         if($topic_count > 0)
         {
             $weighted_topics = array_filter($topic_scores, "ef_filter_weighted_topics");
             $unweighted_topics = array_filter($topic_scores, "ef_filter_unweighted_topics");
             $wt = array_reduce($weighted_topics, "ef_reduce_add_weight");
             $wt = empty($wt) ? 0.0 : $wt;

             $w =  array(
                        "weighted_topics"   => $weighted_topics,
                        "unweighted_topics" => $unweighted_topics,
                        "weight_total"      => $wt,
                        "topic_count"       => $topic_count,
                   );
             return $w;
         }
         else
         {
             return FALSE;
         }
    }
}

if(!function_exists('ef_calculate_subject_score'))
{
    function ef_calculate_subject_score($user_id, $subject_id, $topic_id, $points, $track_id)
    {
        if(!is_numeric($user_id) || !is_numeric($topic_id) || !is_numeric($subject_id) || !is_numeric($points))
            return FALSE;

        $topic_weights = ef_get_topic_weights($subject_id, $user_id);

        if($topic_weights === FALSE)
            return FALSE;

        $wt = $topic_weights['weight_total'];
        $topic_count = $topic_weights['topic_count'];
        $unweighted_topics = $topic_weights['unweighted_topics'];
        $weighted_topics = $topic_weights['weighted_topics'];

        $subject_score = 0;

        $result = array(
            'is_new_subject_level' => FALSE 
        );

        if($wt > $topic_count)
        {
         //invalid weights, everything should get a single weight
         $subject_score = $points;
        }
        else
        {
            $count_uwt = count($unweighted_topics);
            $count_uwt = $count_uwt ? $count_uwt : 1;

            $r_weight = ($topic_count - $wt) / $count_uwt;

            $subject_score = 0.0;
            $found = false;

            foreach($unweighted_topics as $topic)
            {
                if($topic->id == $topic_id)
                {
                    $subject_score = $points * $r_weight;
                    break;
                }
            }

            if(!$found)
            {
                foreach($weighted_topics as $topic)
                {
                    if($topic->id == $topic_id)
                    {
                        $subject_score = $points * $topic->weight;
                        $found = true;
                        break;
                    }
                }
            }
        }

        $subject_score = $subject_score * 1.0 / $topic_count;

        $studentss = StudentSubjectScore::find_or_create_by_user_id_and_subject_id_and_track_id(
            $user_id, $subject_id, $track_id);
        $studentss->level = is_numeric($studentss->level) && $studentss->level > 0 ? $studentss->level : 1;

        if(!is_numeric($studentss->score))
            $studentss->score = 0.0;

        $studentss->score += $subject_score;

        $exp_schedules = ExpPointsSchedule::first(array(
                                'conditions' => 'points_required > ' . $studentss->score,
                                'order' => 'points_required asc')
                         );

        if($studentss->level < $exp_schedules->level)
        {
            $result['is_new_subject_level'] = TRUE;
            $result['current_subject_level'] = $studentss->level;
            $result['new_subject_level'] = $exp_schedules->level;
            $result['max_level_subject_points'] = round($exp_schedules->points_required);
            $result['total_subject_points'] = $studentss->score;
            $studentss->level = $exp_schedules->level;
        }

        $studentss->save();

        ef_calculate_score($studentss->user_id, $track_id);

        return $result;
    }
}

if(!function_exists('ef_recalculate_subject_score'))
{
    function ef_recalculate_subject_score($user_id, $subject_id)
    {
        if(!is_numeric($user_id) || !is_numeric($subject_id)) return;

        $track = StudentTracks::find_by_user_id($user_id);
        if(null == $track)
            return;
        $topic_scores = StudentExpPoint::find_by_sql(
        "SELECT t.id topic_id, exp.user_id, ifnull(exp.points,0.0) points, exp.level, w.weight
         FROM topics t
         left join student_exp_points exp on t.id = exp.topic_id and exp.user_id = {$user_id}
         left join student_tracks tr on exp.track_id = tr.id and exp.user_id = tr.user_id
         left join topic_weights w on w.topic_id = t.id
         where t.subject_id = {$subject_id}");

         $topic_count = count($topic_scores);

         if($topic_count > 0)
         {
             $weighted_topics = array_filter($topic_scores, "ef_filter_weighted_topics");
             $unweighted_topics = array_filter($topic_scores, "ef_filter_unweighted_topics");
             $wt = array_reduce($weighted_topics, "ef_reduce_add_weight");

             $wt = empty($wt) ? 0.0 : $wt;

             if($wt > $topic_count)
             {
                 //invalid weights, everything should get a single weight
                 $subject_score = 0.0;
                 foreach($topic_scores as $topic)
                     $subject_score += $topic->points;
             }
             else
             {
                $count_uwt = count($unweighted_topics);
                $count_uwt = $count_uwt ? $count_uwt : 1;

                $r_weight = ($topic_count - $wt) / $count_uwt;

                $subject_score = 0.0;

                foreach($weighted_topics as $topic)
                    $subject_score += $topic->points * $topic->weight;

                foreach($unweighted_topics as $topic)
                    $subject_score += $topic->points * $r_weight;
             }

             $subject_score = $subject_score * 1.0 / $topic_count;

             $studentss = StudentSubjectScore::find_or_create_by_user_id_and_subject_id_and_track_id(
                 $user_id, $subject_id, $track->track_id);
             $level = $studentss->level ? $studentss->level : 1;
             $exp_schedules = ExpPointsSchedule::first(
                 array(
                     'conditions' => "points_required > {$subject_score}",
                     'order' => 'points_required asc'
                 ));
             $studentss->score = $subject_score;
             $studentss->level = $exp_schedules->level;

             $studentss->save();

             ef_calculate_score($studentss->user_id, $track->track_id);
         }
    }
}

if(!function_exists('ef_calculate_score'))
{
    function ef_calculate_score($user_id, $track_id)
    {
        if(!is_numeric($user_id) && !is_numeric($track_id)) return;

        $subject_scores = StudentSubjectScore::find_all_by_user_id_and_track_id($user_id, $track_id);

        if(!is_null($subject_scores) && count($subject_scores) > 0)
        {
            $options = array('user_id' => $user_id, 'track_id' => $track_id);
            $score = StudentScore::find_by_user_id_and_track_id($user_id, $track_id);
            if(is_null($score))
                $score = StudentScore::create($options);
            $total = 0.0;
            foreach($subject_scores as $subject)
                $total += $subject->score;
            //should add the other forms of points
            //once we decide here.
            $score->score = $total;
            $score->save();

            return $score->score;
        }

        return 0;
    }
}

if(!function_exists('ef_experience'))
{
    function ef_experience($diff, $level, $k)
    {
        return $diff * exp($k * $level);
    }
}

if(!function_exists('ef_level_up'))
{
    function ef_level_up($j)
    {
        return round(25 * ef_experience(1, 1+$j/10, 1.3),2);
    }
}

if(!function_exists('ef_reduce_add_weight'))
{
    function ef_reduce_add_weight($total, $topic)
    {
        $total += $topic->weight;
        return $total;
    }
}

if(!function_exists('ef_reduce_add_weighted_scores'))
{
    function ef_reduce_add_weighted_scores($total, $topic)
    {
        $total += $topic->weight * $topic->points;
        return $total;
    }
}

if(!function_exists('ef_filter_unweighted_topics'))
{
    function ef_filter_unweighted_topics($topic)
    {
        return is_null($topic->weight);
    }
}

if(!function_exists('ef_filter_weighted_topics'))
{
    function ef_filter_weighted_topics($topic)
    {
        return !is_null($topic->weight) && is_numeric($topic->weight);
    }
}
