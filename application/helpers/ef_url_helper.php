<?php

if (! function_exists('by_subject_url')) {
	function by_subject_url($subject, $topic=null)
	{
		$args = ['dashboard', 'subjects', 'by_subject', $subject->permalink];
		if (null != $topic) {
			$args[] = $topic->permalink;
		}
		return site_url($args);
	}
}

if (! function_exists('video_url')) {
	function video_url($video, $subject=null, $topic=null)
	{
		if (null == $topic) {
			$topic = $video->topic;
		}

		if (null == $subject) {
			$subject = $video->topic->subject;
		}

		$args = ['dashboard', 'videos', $subject->permalink, $topic->permalink, $video->permalink];

		return site_url($args);
	}
}

if (! function_exists('active_url')) {
	function active_url($segment, $url)
	{
		return $segment == $url ? ' active ' : '';
	}
}
