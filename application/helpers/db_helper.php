<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('ef_where'))
{
    function ef_where($conditions)
    {
        $first = array();
        for($i=0; $i < count($conditions); $i++)
            $first[] = array_shift($conditions[$i]);

        $where = implode(' AND ', $first);

        $cond = array();

        foreach($conditions as $condition)
            foreach($condition as $param)
                $cond[] = $param;

        array_unshift($cond, $where);
        return $cond;
    }
}
