<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('get_menu'))
{
    function get_menu($name='')
    {
        global $menus;
        if( ! is_array($menus))
        {
            if(defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/menu.php'))
            {
                include(APPPATH.'config/'.ENVIRONMENT.'/menu.php');
            }
            elseif(is_file(APPPATH.'config/menu.php'))
            {
                include(APPPATH.'config/menu.php');
            }

            if( ! is_array($menus))
            {
                return FALSE;
            }
        }

        if(strlen($name) > 0 && array_key_exists($name, $menus))
        {
            return $menus[$name];
        }

        if(strlen($name) == 0)
        {
            return $menus;
        }
        else
        {
            return FALSE;
        }
    }
}
