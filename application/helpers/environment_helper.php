<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*@author: Jay Hall
*Description: Verifies current environment
*
*/

if(!function_exists('is_development'))
{
    function is_development()
    {
        return ENVIRONMENT != 'production';
    }
}

if( !function_exists('is_production') )
{
    function is_production()
    {
       return ENVIRONMENT == 'production';
    }
}
if( !function_exists('is_testing'))
{
    function is_testing()
    {
        return ENVIRONMENT == 'testing';
    }
}
