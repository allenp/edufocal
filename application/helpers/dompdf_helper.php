<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('pdf_create')) 
{
    function pdf_create($html, $filename='', $stream=TRUE)
    {
        require_once(dirname(__FILE__) . '/../libraries/dompdf/dompdf_config.inc.php');

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        if($stream) {
            $dompdf->stream($filename . '.pdf',array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }
}
