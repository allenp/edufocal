<?php

if(!function_exists('country_dropdown'))
{
    function country_dropdown( $name = 'country', $top_countries=array(), $selection=NULL, $show_all=TRUE)
    {
        $CI =& get_instance();

        $CI->load->config('countries');

        $countries = $CI->config->item('country_list');

        $html = "<select name='{$name}' class='form-control'>";
        $selected = NULL;
        if(in_array($selection, $top_countries)) {
            $top_selection = $selection;
            $all_selection = NULL;
        }
        else
        {
            $top_selection = NULL;
            $all_selection = $selection;
        }

        if(!empty($top_countries)) {
            foreach($top_countries as $value) {
                if(array_key_exists($value, $countries)) {
                    if($value === $top_selection) {
                        $slected = "selected";
                    }
                        $html .= "<option value='{$value}' {$selected}>{$countries[$value]}</option";
                        $selected = NULL;
                }
                $html .= "<option> </option>";
            }

            if($show_all) {
                foreach($countries as $key => $country) {
                    if($key === $all_selection) {
                        $selected = "selected";
                    }
                    $html .= "<option value='{$key}' {$selected}>{$country}</option>";
                    $selected = NULL;
                }
            }
            $html .= "</select>";
            return $html;
        }
    }
}
