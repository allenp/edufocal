<?php

namespace EduFocal\Code;

use Sabre\Event;

class JoinClassroomByCode implements CodeServiceInterface
{
    protected $code = null;
    protected $eventDispatcher = null;

    public function __construct(\Code $code, Event\EventEmitterInterface $eventDispatcher=null)
    {
        $this->code = $code;
        $this->eventDispatcher = $eventDispatcher;
    }

    /*
     * If this code is a "classroom code" then
     * auto-enroll the user into the classroom the
     * code belongs to.
     *
     * Classroom codes make the student automatically active
     * in the classroom.
     */
    public function apply()
    {
        $cc = \ClassroomCode::find_by_code_id($this->code->id);


        if (null == $cc) {
            return;
        }

		$classroom = \Classroom::find($cc->classroom_id);
        $cs = \ClassroomStudent::find_by_classroom_id_and_user_id($cc->classroom_id, $this->code->user_id);

        if ($cs != null) {
			$cs->manager_id = $classroom->manager_id;
            $cs->active = 1;
            $cs->save();
            return;
        }

        \ClassroomStudent::create(
            array(
                'user_id' => $this->code->user_id,
                'classroom_id' => $classroom->id,
				'manager_id' => $classroom->manager_id,
				'user_confirmed_dt' => new \ActiveRecord\DateTime(),
				'manager_confirmed_dt' => new \ActiveRecord\DateTime(),
                'active' => 1
            )
        );

		$user = \User::find($this->code->user_id);
        if (null != $this->eventDispatcher) {
            $this->eventDispatcher->emit(
                Provider::CLASSROOM_JOINED, array($classroom, $user));
        }
    }
}
