<?php

namespace EduFocal\Code;

use Sabre\Event;

/*
 * Gets a \Code instance for the user based on code serial provided.
 * Simple.
 */
class CodeRedemption implements CodeAccessInterface
{

	protected static $ci = null;
	protected $code = '';
	protected $user = null;
	protected $eventDispatcher = null;

	public function __construct($code, $user, Event\EventEmitter $eventDispatcher)
	{
		$this->code = preg_replace('/\s+/', '', $code);
		$this->user = $user;
		$this->eventDispatcher = $eventDispatcher;
		if (null == self::$ci) {
			self::$ci =& get_instance();
		}
	}

	public function process()
	{
		$result = new \stdClass();
		$result->code = \Code::find_by_code($this->code);
		$result->success = false;

		//TODO: More validation checks on the code inside the business
		//logic here and not rely on the ef_code input validators
		if (null != $result->code) {
			$accept = new AcceptUserCodeService(
				self::$ci->config->item('educodes'),
				$result->code,
				$this->user,
				$this->eventDispatcher
			);

			$result->success = $accept->apply();
		}

		return $result;
	}
}
