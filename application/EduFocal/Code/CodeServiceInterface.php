<?php

namespace EduFocal\Code;

interface CodeServiceInterface
{
    function apply();
}
