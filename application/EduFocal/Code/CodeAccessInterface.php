<?php

namespace EduFocal\Code;

/*
 * Author: Paul Allen
 *
 * Description:
 * Interface into the command classes that deal with getting verification
 * and authorization (represented in a \Code entity) to a user
 * Implementations of this interface should always return a \Code
 * created for a given user.
 */
interface CodeAccessInterface
{
	/*
	 * @returns \Code
	 */
	function process();
}
