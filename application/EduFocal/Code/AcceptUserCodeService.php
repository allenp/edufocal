<?php

namespace EduFocal\Code;

use Sabre\Event;
use EduFocal\User;

/*
 * Author: Paul Allen
 *
 * Scenario: user has a new code to be added to their account.
 *
 * What needs to happen:
 * ---------------------
 * Change the user_id field on the code to the user's id.
 * If the user's account has currently expired, then set the code
 * to be their active code.
 *
 */
class AcceptUserCodeService implements CodeServiceInterface
{
    protected static $config = null;

    protected $code = null;
    protected $user = null;
    protected $eventDispatcher = null;

    public function __construct($config=null, $code, $user, Event\EventEmitterInterface $eventDispatcher)
    {
		self::$config = $config;
        $this->code = $code;
        $this->eventDispatcher = $eventDispatcher;

        if (! $user instanceof User\UserSecurityProfile) {
            $this->user = new User\UserSecurityProfile($user);
        } else {
            $this->user = $user;
        }
    }

    public function apply()
    {
        if (!isset(self::$config[$this->code->type])) {
            throw new \Exception("Code type ". $this->code->type ." unkown in configuration.");
        }

         $this->code->user_id = $this->user->id;

		//TODO: only fire CODE_ACCEPTED event when successful
         if ($this->user->isExpired()) {
             $cfg = self::$config[$this->code->type];
             $this->code->accepted_on = time();

             if ($this->code->expiry_date == 0) {
                 $this->code->expiry_date = strtotime('+ ' . $cfg['valid_for'], time());
             }

			 $this->code->save();
             $this->user->code_id = $this->code->id;
			 $this->user->save();

			 $this->eventDispatcher->emit(Provider::CODE_ACTIVATED, [$this->code]);

			 return true;

		 } else {
			 $this->eventDispatcher->emit(Provider::CODE_ADDED, [$this->code]);
			 $this->code->save();
			 return false;
		 }
    }
}
