<?php

namespace EduFocal\Code;

class GenerateCodeService
{
	protected static $ci = null;
	protected $options = null;

	public function __construct($options=array())
	{
		$this->options = $options;
		self::$ci =& get_instance();
	}

	public function apply()
	{
		self::$ci->load->helper('string');
		$found = false;
		$iter = 1;

		$options = $this->options;

		if (!options) {
			$options = array();
		}

		do {
			$serial = random_string('numeric', 13);
			if (isset($options['sponsor']) && strlen($options['sponsor']) <= 7) {
				$serial .= $options['sponsor'];
			}

			try {
				$options['code'] = $serial;
				$code = \Code::create($options);
				if (is_numeric($code->id)) {
					$found = true;
					return $code;
				}
			} catch(Exception $e) {
				$found = false;
			}
		} while(!$found && ++$iter < 10);
		return FALSE;
	}
}
