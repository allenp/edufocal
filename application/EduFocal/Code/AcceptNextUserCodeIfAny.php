<?php

namespace EduFocal\Code;

use EduFocal\User;
use Sabre\Event;

class AcceptNextUserCodeIfAny
{
	protected static $ci = null;
	protected $eventDispatcher = null;

	public function __construct($user, Event\EventEmitterInterface $eventDispatcher=null)
	{
		if (! $user instanceof User\UserSecurityProfile) {
			$this->user = new User\UserSecurityProfile(clone $user);
		} else {
			$this->user = $user;
		}

		self::$ci =& get_instance();
		$this->eventDispatcher = $eventDispatcher;
	}

	public function apply()
	{
		if ( ! $this->user->isExpired()) {
			return true;
		}

		$code = \Code::find(
			'first',
			array(
				'conditions' => array(
					'user_id' => $this->user->id,
					'accepted_on' => 0
				)
			)
		);

		if($code == null) return false;

		$accept = new AcceptUserCodeService(
			self::$ci->config->item('educodes'),
			$code,
			$this->user,
			$this->eventDispatcher
		);

		$accept->apply();

		return true;
	}
}
