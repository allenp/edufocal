<?php

namespace EduFocal\Code;

use EduFocal\Validation;
use EduFocal\Payment;

class CodePurchaseValidation extends Validation\InputValidation
{
	protected static $rules = array(
		array(
			'field' => 'card_name',
			'label' => 'Card Holder',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'card_number',
			'label' => 'Card Number',
			'rules' => 'trim|required|callback__valid_card'
		),
		array(
			'field' => 'card_cvc',
			'label' => 'Verifcation Number',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'card_expiry_month',
			'label' => 'Expiration Month',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'card_expiry_year',
			'label' => 'Expiration Year',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'plan',
			'label' => 'Payment Plan',
			'rules' => 'trim|required',
		),
		array(
			'field' => 'address1',
			'label' => 'Address 1',
			'rules' => 'trim|required',
		),
		array(
			'field' => 'address2',
			'label' => 'Address 2',
			'rules' => 'trim|required',
		),
		array(
			'field' => 'country',
			'label' => 'Country',
			'rules' => 'trim|required',
		),
		array(
			'field' => 'city',
			'label' => 'City',
			'rules' => 'trim',
		),
		array(
			'field' => 'state',
			'label' => 'State',
			'rules' => 'trim',
		)
	);

	protected $user = null;
	protected $source = '';

	public function __construct($user, $source)
	{
		parent::__construct();
		$this->user = $user;
		$this->source = $source;
	}

	public function getRules()
	{
		return self::$rules;
	}

	public function getUserInput()
	{
		$configs = ['JMD' => 'educodes', 'USD' => 'educodes_usd'];

		$options = new Payment\PaymentOptions();
		$input = parent::$ci->input;

		$plan = $input->post('plan');
		$currency = $input->post('currency');
		$config_key = $configs[$currency];
		$charges = parent::$ci->config->item($config_key);


		$cardExp = $input->post('card_expiry_month');
		$cardExp .= '/';
		$cardExp .= $input->post('card_expiry_year');
		$amount = $charges[strtolower($plan)]['charge'];

		$options->cardNumber($input->post('card_number'))
			->userId($this->user->id)
			->shipName($this->user->name())
			->email($this->user->email)
			->cardName($input->post('card_name'))
			->cardCVV($input->post('card_cvc'))
			->cardAmount($amount)
			->currency($currency)
			->cardExp($cardExp)
			->address1($input->post('address1'))
			->address2($input->post('address2'))
			->city($input->post('city'))
			->state($input->post('state'))
			->country($input->post('country'))
			->source($this->source)
			->plan($plan);

		return $options;
	}

	public function valid_card($number)
	{
		$CI = parent::$ci;

		$number = preg_replace('/\s+/', '', $number);
		$card = '';
		$cards = array(
			'visa' => '/^4\d{12}(\d\d\d){0,1}$/',
			'mastercard' => '/^5[12345]\d{14}$/',
			'keycard' => '/^77/',
		);

		foreach($cards as $type => $regex) {
			if(preg_match($regex, $number)) {
				$card = $type;
				break;
			}
		}

		if (!$card) {
			$CI->form_validation->set_message('_valid_card', 'The %s is not a recognized VISA, MASTERCARD or KEYCARD number');
			return FALSE;
		}

		if ($card == 'keycard') {
			return TRUE;
		}

		$reverse = strrev($number);
		$checksum = 0;

		for ($i = 0; $i < strlen($reverse); $i++) {
			$current_num = intval($reverse[$i]);
			if($i & 1)
				$current_num *= 2;

			$checksum += $current_num % 10;
			if($current_num > 9)
				$checksum += 1;
		}

		if ($checksum % 10 == 0) {
			return TRUE;
		} else {
			$CI->form_validation->set_message('_valid_card', 'The %s is invalid');
			return FALSE;
		}
	}
}
