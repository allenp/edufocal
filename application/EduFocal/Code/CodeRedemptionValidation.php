<?php

namespace EduFocal\Code;

use EduFocal\Validation;

class CodeRedemptionValidation extends Validation\InputValidation
{
	protected static $rules = array(
        array(
            'field' => 'educode',
            'label' => 'EduFocal Code',
            'rules' => 'trim|required|callback__valid_efcode'
        )
    );

	public function __construct()
	{
		parent::__construct();
	}

	public function getRules()
	{
		return self::$rules;
	}

	public function getUserInput()
	{
		return parent::$ci->input->post('educode');
	}
}
