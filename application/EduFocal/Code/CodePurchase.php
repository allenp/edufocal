<?php

namespace EduFocal\Code;

use Sabre\Event;
use EduFocal\Payment;

/*
 * Gets a code for the user based on money collected in payment gateway.
 * Simple.
 */
class CodePurchase implements CodeAccessInterface
{
	protected static $ci = null;
	protected $options = null;
	protected $user = null;
	protected $eventDispatcher = null;

	public function __construct(Payment\PaymentOptions $options, $user, Event\EventEmitterInterface $eventDispatcher)
	{
		$this->options = $options;
		$this->user = $user;
		if (null == self::$ci) {
			self::$ci =& get_instance();
		}
		$this->eventDispatcher = $eventDispatcher;
	}

	public function process()
	{
		$card_processor = new Payment\CardPayment($this->eventDispatcher);
		$result = $card_processor->pay($this->options);

		if ($result->success == true) {
			$result->code = \Code::Generate( array(
				'type' => $this->options->plan,
				'user_id' => $this->options->userId,
				'source_ref' => $result->receipt->id,
				'value' => $this->options->cardAmount,
				'currency' => $this->options->currency,
			));

			$this->eventDispatcher->emit(Provider::CODE_PURCHASED, $result);

		} else {
			//log_message('error', "{$result->response->FinalStatus} {$result->response->MErrMsg}");
		}

		if (null != $result->code) {
			$accept = new AcceptUserCodeService(
				self::$ci->config->item('educodes'),
				$result->code,
				$this->user,
				$this->eventDispatcher
			);

			$accept->apply();
		}

		return $result;
	}
}
