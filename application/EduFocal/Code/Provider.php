<?php

namespace EduFocal\Code;

use EduFocal\ProviderInterface;
use EduFocal\Support\Listener;

class Provider implements \EduFocal\ProviderInterface
{
	/*
	 * CODE_PURCHASED: a code has been purchased!
	 */
	const CODE_PURCHASED = 'code_purchased';

	/*
	 * CODE_ACCEPTED code has been activated on user's account
	 * */
	const CODE_ACTIVATED = 'code_accepted';

	/*
	 * Code has been added to the user's account.
	 * but not activated
	 */
	const CODE_ADDED = 'code_added';

	/*
	 * TODO: MOVE THIS TO SOME PLACE ELSE.
	 * CLASSROOM_JOINED represents the moment when a user is enrolled
	 * in a classroom.
	 */
	const CLASSROOM_JOINED = 'classroom_joined';


	public function bootstrap()
	{
		$e = Listener::get();

		$self = $this;
		$e->on(self::CODE_ADDED, function($code) use($self) {
			$self->joinClass($code);
		});
		$e->on(self::CODE_ACTIVATED, function($code) use($self) {
			$self->joinClass($code);
		});
	}

	public function joinClass($code)
	{
		$addToClass = new JoinClassroomByCode(
			$code,
			Listener::get()
		);
		$addToClass->apply();
	}
}
