<?php

namespace EduFocal\User;

interface UserServiceInterface
{
	function createUser($options);
	function login($username, $password);
}
