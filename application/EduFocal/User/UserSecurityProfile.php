<?php

namespace EduFocal\User;

/*
 * Author: Paul Allen
 *
 * Decorator class for the User model to handle security issues
 * particularly with code subscription.
 *
 */
class UserSecurityProfile implements UserSecurityProfileInterface
{
	protected static $ci = null;
	protected $user = null;

	public function __construct(\User $user)
	{
		self::$ci =& get_instance();
		$this->user = $user;
	}

	public function __get($name)
	{
		return $this->user->{$name};
	}

	public function __set($name, $value)
	{
		$this->user->{$name} = $value;
	}

	public function __call($method, $arguments)
	{
		switch(count($arguments)) {
		case 0:
			return $this->user->{$method}();
			break;
		case 1:
			return $this->user->{$method}($arguments[0]);
			break;
		case 2:
			return $this->user->{method}($arguments[0], $arguments[1]);
			break;
		case 3:
			return $this->user->{$method}($arguments[0], $arguments[1], $arguments[2]);
		default:
			return call_user_func_array(array($this->user, $method),  $arguments);
		}
	}

	public function isPaidMember()
	{
		if (self::$ci->session->userdata('paid.member') === false) {
			$code = \Code::find('first',
				array(
					'conditions' => array(
						'user_id = ? and (accepted_on = 0 or expiry_date > unix_timestamp()) and type <> ?',
						$this->user->id,
						'weekly'
					)
				)
			);

			self::$ci->session->set_userdata('paid.member', $code == NULL ? 'weekly' : $code->type);
		}

		return self::$ci->session->userdata('paid.member') !== 'weekly';
	}

	public function isExpired()
	{
		switch($this->roleId())
		{
		case STUDENT_ROLE:
			//student doesn't have a code
			if($this->user->code_id == 0) {
				return TRUE;
			}

			if(!isset($this->user->code->expiry_date)) {
				return TRUE;
			} else {
				return $this->user->code->expiry_date < time();
			}
			break;
		case TEACHER_ROLE:
		case REVIEWER_ROLE:
			return ($this->user->code_id <= 0);
			break;
		default:
			return FALSE;
		}
	}

	public function roleId()
	{
		return $this->user->role_id;
	}

	/*
	 * @returns \ActiveRecord\DateTime()
	 */
	function calculateExpiryDate()
	{
		try {
			$expiry_date = $this->code->expiry_date;
		} catch(Exception $e) {
			return $expiry_date = 0;
		}

		$unused = \Code::all(array('conditions' => array('user_id' => $this->id, 'accepted_on' => 0)));

		if (count($unused) > 0) {

			$charges = self::$ci->config->item('educodes');

			foreach ($unused as $c) {
				//if the expiry date on the code isn't set then we need to find
				//how long from now it's valid for
				if ($c->expiry_date <= 0) {
					if(!isset($charges[$c->type]) && !isset($charges[$c->type]['valid_for'])) {
						continue;
					}
					$valid_for = $charges[$c->type]['valid_for'];
					$expiry_date = strtotime('+ ' . $valid_for, $expiry_date);
				} elseif ($c->expiry_date > $expiry_date) {
					$expiry_date = $c->expiry_date;
				}
			}
		}

		$date = new \ActiveRecord\DateTime();
		$date->setTimestamp($expiry_date);

		return $date;
	}
}
