<?php

namespace EduFocal\User;

use EduFocal\Support\Listener;

class Provider implements \EduFocal\ProviderInterface
{
	const USER_CREATED = 'user_created';
    static $ci = null;

    public function __construct()
    {
        if (null == self::$ci) {
            self::$ci =& get_instance();
        }
    }

	public function bootstrap()
	{
		$e = Listener::get();

		$e->on(self::USER_CREATED, function($user) {

			self::$ci->load->library('EFMCAPI', null, 'mailchimp');

			$school = \School::find($user->school_id);

			$merge_vars = array(
				'FNAME' => ucwords($user->first_name),
				'LNAME' => ucwords($user->last_name),
				'PHONE' => $user->phone,
				'SCHOOL' => $school->name,
				'GENDER' => $user->sex
			);

			$list = self::$ci->mailchimp
				->get_list_id(\EFMCAPI::REGISTRATION_START);

			self::$ci->mailchimp
				->listSubscribe($list, $user->email, $merge_vars);
		});
	}
}
