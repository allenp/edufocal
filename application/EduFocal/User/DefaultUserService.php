<?php

namespace EduFocal\User;

class DefaultUserService implements UserServiceInterface
{
	protected static $ci = null;
	protected $eventDispatcher = null;

	public function __construct(\Sabre\Event\EventEmitter $dispatcher)
	{
		self::$ci =& get_instance();
		if (!class_exists('User_model')) {
			self::$ci->load->model('users/User_model', 'user_model');
		}

		$this->eventDispatcher = $dispatcher;
	}

	public function createUser($options=[])
	{
		$user_id = self::$ci->user_model->insert($options);
		$user = \User::find($user_id);
		$this->eventDispatcher->emit(Provider::USER_CREATED, [$user]);
		return $user;
	}

	public function login($username, $password)
	{
		return self::$ci->auth->login(
			$username,
			$password,
			true
		);
	}
}
