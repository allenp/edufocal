<?php

namespace EduFocal\User;

use EduFocal\Validation;

class DefaultUserValidation extends Validation\InputValidation
{
	protected static $signup_rules = array(
		array(
			'field' => 'first_name',
			'label' => 'First Name',
			'rules' => 'trim|required|min_length[2]'
		),
		array(
			'field' => 'last_name',
			'label' => 'Last Name',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'sex',
			'label' => 'Gender',
			'rules' => 'trim|required|callback_check_gender'
		),
		array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email|callback_unique_email'
		),
		array(
			'field' => 'phone',
			'label' => 'Phone',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'exam_level',
			'label' => 'Exam level',
			'rules' => 'trim|required|callback_valid_exam'
		),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|matches[password_confirmation]|min_length[8]|max_length[32]'
		),
		array(
			'field' => 'password_confirmation',
			'label' => 'Confirm Password',
			'rules' => 'trim'
		)
	);

	public function __construct()
	{
		parent::__construct();
	}

	public function getRules()
	{
		return self::$signup_rules;
	}

	public function getUserInput()
	{
		$input = parent::$ci->input;
		$additional = array(
			'gender' => $input->post('sex'),
			'phone' => $input->post('phone')
		);
        $school_id = \School::first()->id;
		$data = array(
			'email'		=> $input->post('email'),
			'username'	=> $input->post('username'),
			'password'	=> $input->post('password'),
			'salt' => '', //legacy field
			'first_name' => ucwords($input->post('first_name')),
			'last_name' => ucwords($input->post('last_name')),
			'exam_level' => $input->post('exam_level'),
			'school_id' => $school_id,
			'sex' => $input->post('sex'),
			'phone' => $input->post('phone'),
			'profile' => json_encode($additional),
			'referral_code' =>  $this->get_unique_referral_code()
		);
		return $data;
	}

	private function get_unique_referral_code ()
	{
		parent::$ci->load->helper('string');

		$code = random_string('alnum', 10);

		if (\User::exists(array('referral_code', $code))) {
			return $this->get_unique_referral_code();
		}

		return $code;
	}

	//TODO: Make these methods work somehow.
	//and move them out of the controllers
	public function unique_email($email)
	{
		$user = User::find_by_email($email);
		if(count($user) > 0) {
			$this->form_validation->set_message(
				'unique_email',
				'The Email you have selected is already in use.'
			);
			return FALSE;
		}
		return TRUE;
	}
}
