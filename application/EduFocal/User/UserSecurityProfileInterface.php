<?php

namespace EduFocal\User;

interface UserSecurityProfileInterface
{
    public function isPaidMember();
    public function isExpired();
    public function roleId();
}
