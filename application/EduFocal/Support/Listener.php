<?php

namespace EduFocal\Support;

use Sabre\Event;

class Listener
{
	protected static $dispatcher = null;

	public static function get()
	{
		if (null == self::$dispatcher) {
			self::$dispatcher = new \Sabre\Event\EventEmitter();
		}
		return self::$dispatcher;
	}
}
