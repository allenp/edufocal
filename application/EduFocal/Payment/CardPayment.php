<?php

namespace EduFocal\Payment;

use Sabre\Event;


/*
 * TODO: Refactor this to general purpose payments interface
 */
class CardPayment
{
	protected $eventDispatcher = null;

	public function __construct(Event\EventEmitterInterface $eventEmitter)
	{
		$this->eventDispatcher = $eventEmitter;
	}

	/**
	 * Details of $options param:
	 * card-number
	 * card-name
	 * card-amount
	 * card-cvv
	 * currency
	 * ship-name
	 * card-exp
	 * email
	 * orderID (only used when doing authprev)
	 * address1
	 * address2
	 * city
	 * state
	 * country
	 * user_id
	 */
	public function pay(PaymentOptions $options)
	{
		$pnp = new \PnP();

		$result = new PaymentResult;
		$result->source = $options->source;

		if (isset($options->prevOrderID)) {
			$response = $pnp->authprev($options->toArray());
		} else {
			$response = $pnp->auth($options->toArray());
		}

		if ($response === false ||
			strtolower($response->FinalStatus) !== 'success') {

				if ($options->fireSourceEvent == true && count($options->source) > 0) {
					$this->eventDispatcher->emit(Provider::PAYMENT_FAIL,
						[$response, $options->source]);
				}

				$this->eventDispatcher->emit(Provider::PAYMENT_FAIL, [$response]);

				error_log($response->FinalStatus . ' ' . $response->MErrMsg);

				$result->success = false;
				$result->response = $response;

				return $result;
        }

		$redacted = str_repeat('X', strlen($options->cardNumber) - 5);
		$redacted .= substr($options->cardNumber,
			strlen($options->cardNumber) - 5);

		$receipt = \Receipt::create(
			array(
				'payment_ref' => $response->orderID,
				'user_id' => $options->userId,
				'amount' => $options->cardAmount,
				'currency' => $options->currency,
				'card_holder' => $options->cardName,
				'email' => $options->email,
				'paymethod' => $response->paymethod,
				'auth_code' => $response->auth_code,
				'card_type' => $response->card_type,
				'card' =>   $redacted,
				'address1' => $options->address1,
				'address2' => $options->address2,
				'city' => $options->city,
				'state' => $options->state,
				'country' => $options->country,
			)
		);

		$result->receipt = $receipt;

		//TODO: remove some of this fodder down here.
		$user = \User::find_by_id($options->userId);

		if ($user !== null) {
			$result->name = $user->name();
		}
		//END of fodder

		$result->success = true;

		if ($options->fireSourceEvent == true) {
			$this->eventDispatcher->emit(Provider::PAYMENT_SUCCESS.$result->source, [$result]);
		}

		$this->eventDispatcher->emit(Provider::PAYMENT_SUCCESS, [$result]);

		//TODO: move this card saving feature to its
		//Own name space. Activate it based on PAYMENT_SUCCESS

		$card = \Card::find_by_user_id($options->userId);

		if (null == $card) {
			$card = new \Card;
			$card->user_id = $options->userId;
		}

		$card->card_no = $options->cardNumber;
		$card->card_name = $options->cardName;
		$card->address1 = $options->address1;
		$card->address2 = $options->address2;
		$card->city = $options->city;
		$card->country = $options->country;
		$card->state = $options->state;
		$card->cvc = $options->cardCVV;
		$card->expiry_date = $options->cardExp;
		$card->save();

		$result->card = $card;

		return $result;
	}
}
