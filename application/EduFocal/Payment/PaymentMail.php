<?php
//TODO: Move this class to the Code Namespace or to a mailers namespace

namespace EduFocal\Payment;

use EduFocal\Support\Listener;

class PaymentMail implements \EduFocal\ProviderInterface
{
    protected static $ci;

    public function __construct()
    {
		if (null == self::$ci) {
			self::$ci =& get_instance();
		}
    }

    public function bootstrap()
    {
		$e = Listener::get();

		$e->on(\EduFocal\Code\Provider::CODE_PURCHASED, function(&$payload) {


			if ($payload->source == 'subscription') return;

			$receipt = $payload->receipt;

			self::$ci->load->library('emailer/emailer');

			$info = array(
				'to' => $receipt->email,
				'subject' => 'EduFocal.com: Payment Receipt #' . $receipt->id,
				'message' => self::$ci->load->view('account/email', array('name' => $payload->name, 'receipt' => $receipt), TRUE)
			);

			self::$ci->emailer->queue_emails = true;
			$result = self::$ci->emailer->send($info);

			if($result['success']) {
				$receipt->email_sent = 1;
			}

			$receipt->save();
		});
	}
}
