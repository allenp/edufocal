<?php

namespace EduFocal\Payment;

class PaymentResult
{
	public $success = false;
	public $response = null;
	public $receipt = null;
	public $code = null;
	public $name = null;
	public $card = null;
	public $source = '';
}
