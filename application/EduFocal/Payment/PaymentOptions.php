<?php

namespace EduFocal\Payment;

class PaymentOptions
{
	public $cardNumber = '';
	public $cardName = '';
	public $cardAmount = 0;
	public $cardCVV = '';
	public $currency = '';
	public $shipName = '';
	public $cardExp = '';
	public $email = '';
	public $address1 = '';
	public $address2 = '';
	public $city = '';
	public $state = '';
	public $country = '';
	public $plan = '';

	public $source = '';
	public $fireSourceEvent = false;

	public $userId;
	public $prevOrderID;

	public function __call($name, $args)
	{
		if(count($args) == 1) {
			if (isset($this->{$name}) || property_exists($this, $name)) {
				$this->{$name} = $args[0];
				return $this;
			} else {
				throw new \Exception('Property not found: '.$name);
			}
		} else {
			throw new \Exception('Invalid number of arguments. Only 1 accepted: ' . $name);
		}
	}

	/*
	 * Array with keys that PnP expects
	 *
	 */
	public function toArray()
	{
		return array(
			'card-number' => $this->cardNumber,
			'card-name' => $this->cardName,
			'card-amount' => $this->cardAmount,
			'card-cvv' => $this->cardCVV,
			'currency' => $this->currency,
			'ship-name' => $this->shipName,
			'card-exp' => $this->cardExp,
			'email' => $this->email,
			'prevorderid' => $this->prevOrderID,
			'address1' => $this->address1,
			'address2' => $this->address2,
			'city' => $this->city,
			'state' => $this->state,
			'country' => $this->country,
			'source' => $this->source,
			'user_id' => $this->userId
		);
	}
}
