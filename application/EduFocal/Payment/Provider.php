<?php

namespace EduFocal\Payment;

use EduFocal\ProviderInterface;
use EduFocal\Support\Listener;

class Provider implements ProviderInterface
{
	const PAYMENT_SUCCESS = 'payment_success';
	const PAYMENT_FAIL = 'payment_fail';

	public function bootstrap()
	{
		$e = Listener::get();
		//TODO: Implement email sending on PAYMENT_SUCCESS
	}
}
