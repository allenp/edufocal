<?php

namespace EduFocal\Subscription;

use EduFocal\User;

class Subscriber
{
	protected $user;

	public function __construct($user)
	{
		if (! $user instanceof User\UserSecurityProfile) {
			$this->user = new User\UserSecurityProfile($user);
		} else {
			$this->user = $user;
		}

	}

	public function subscribe($plan = null, $preferred_card = null, $currency = null, $cost = null)
	{
		$subscriber = \Subscription::find_by_user_id($this->user->id);

		$sub_date = $this->user->calculateExpiryDate();

		if ($subscriber === null) {
			\Subscription::Create(
				array(
					'user_id' => $this->user->id,
					'type' => $plan,
					'currency' => $currency,
					'subscription_end_dt' => $sub_date,
					'preferred_card' => $preferred_card,
					'cost' => $cost,
				)
			);
		} else {
			$subscriber->update_attributes(
				array(
					'type' => $plan,
					'currency' => $currency,
					'subscription_end_dt' => $sub_date,
					'preferred_card' => $preferred_card
				)
			);
		}
	}

	public function unsubscribe()
	{
		$subscriber = \Subscription::find_by_user_id($this->user->id);
		if ($subscriber !== null) {
			$subscriber->delete();
		}
	}
}
