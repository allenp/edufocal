<?php

namespace EduFocal\Subscription;

use EduFocal\Payment;
use EduFocal\Support\Listener;
use EduFocal\User;

class Provider implements \EduFocal\ProviderInterface
{
	public function bootstrap()
	{
		$e = Listener::get();

		//When a new code is purchased, extend their
		//subscription date.
		$e->on(Payment\Provider::PAYMENT_SUCCESS, function($pay) {

			$id = $pay->receipt->user_id;

			$user = new User\UserSecurityProfile(\User::find($id));

			$sub = \Subscription::find_by_user_id($id);

			if (null == $sub) {
				return;
			}

			$dt = $user->calculateExpiryDate();

			if($dt) {
				$sub->subscription_end_dt = $dt;
				$sub->failed_attempts = 0;
				$sub->save();
			}

		});
	}
}
