<?php

namespace EduFocal\Validation;

abstract class InputValidation implements InputValidationInterface
{
	protected static $ci = null;

	public function __construct()
	{
		if (null == self::$ci) {
			self::$ci =& get_instance();
		}
	}

	public function validate($context=null)
	{
		if (null == $context) {
			$context = self::$ci;
		}

		$rules = $this->getRules();

		foreach($rules as $rule) {
			self::$ci->form_validation->set_rules(
				$rule['field'], $rule['label'], $rule['rules']);
		}

		return self::$ci->form_validation->run($context);
	}
}
