<?php

namespace EduFocal\Validation;

interface InputValidationInterface
{
	function getRules();
	function getUserInput();
	function validate($context);
}
