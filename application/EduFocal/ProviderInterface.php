<?php

namespace EduFocal;

interface ProviderInterface
{
	function bootstrap();
}
