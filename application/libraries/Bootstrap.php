<?php

class Bootstrap
{
	public function __construct()
	{
		$this->boot();
	}

	public function boot()
	{
		//TODO: create a new config type called providers.php
		//To store all the providers to activate at run time
		$providers = [
			'EduFocal\Code\Provider',
			'EduFocal\User\Provider',
			'EduFocal\Payment\Provider',
			'EduFocal\Payment\PaymentMail',
			'EduFocal\Subscription\Provider',
			'EduFocal\Support\Parrot',
			];

		foreach($providers as $provider) {
			(new $provider())->bootstrap();
		}
	}
}
