<?php

namespace ActiveRecord;

/**
 * File cache adapter for PHP-ActiveRecord and CodeIgniter.
 *
 * @package EduFocal
 * @version July.16.2011
 * @author Jamie Chung
 */
class File
{
	/**
	 * Codeigniter instance.
	 *
	 * @var object
	 */
	private $CI;
	
	/**
	 * Constructs the File adapter.
	 * 
	 * @param array $options Needed by cache adapter, not really used.
	 * @return void
	 * @author Jamie Chung
	 **/
	public function __construct ( array $options )
	{
		$this->CI =& get_instance();
	}
	
	/**
	 * Reads a value from the filestore.
	 * 
	 * @param string $id Cache ID of the file to read.
	 * @return Object from the datastore, unserialized.
	 * @author Jamie Chung
	 */
	public function read ( $id )
	{
		return $this->CI->cache->get($id);
	}
	
	/**
	 * Writes a value to the filestore. 
	 *
	 * @param string $id Cache ID of the file to write.
	 * @param mixed $value Data to be written.
	 * @param int $expire Time to expire in seconds.
	 * @author Jamie Chung
	 */
	public function write ( $id, $value, $expire )
	{
		$this->CI->cache->save($id, $value, $expire);
	}
}

?>