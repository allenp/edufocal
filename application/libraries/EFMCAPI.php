<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class EFMCAPI extends MCAPI
{

    private $ci;

    const REGISTRATION_START = 'registration_start_list';
    const REGISTRATION_COMPLETE = 'registration_complete_list';

    public function __construct()
    {
       $this->ci =& get_instance();
       $this->ci->load->config('mailchimp');
       $config = array(
           'apikey' => $this->ci->config->item('mc_apikey'),
           'secure' => $this->ci->config->item('mc_secure')
       );
       parent::__construct($config);
    }

    public function get_list_id($key='')
    {
        error_log('LIST ' . $this->ci->config->item($key));
        return $this->ci->config->item($key);
    }
}
