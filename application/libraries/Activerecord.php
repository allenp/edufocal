<?php

if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50300)
	die('PHP ActiveRecord requires PHP 5.3 or higher');

define('PHP_ACTIVERECORD_VERSION_ID','1.0');

require 'ActiveRecord/Singleton.php';
require 'ActiveRecord/Config.php';
require 'ActiveRecord/Utils.php';
require 'ActiveRecord/DateTime.php';
require 'ActiveRecord/Model.php';
require 'ActiveRecord/Table.php';
require 'ActiveRecord/ConnectionManager.php';
require 'ActiveRecord/Connection.php';
require 'ActiveRecord/SQLBuilder.php';
require 'ActiveRecord/Reflections.php';
require 'ActiveRecord/Inflector.php';
require 'ActiveRecord/CallBack.php';
require 'ActiveRecord/Exceptions.php';
require 'ActiveRecord/Cache.php';

class CI_Activerecord
{
	public function __construct ()
	{
		try {
			ActiveRecord\Config::initialize(function($cfg)
			{
	
				$CI =& get_instance();
				$db = $CI->db;
				
				$cfg->set_model_directory(APPPATH.'activerecord');
				  //$cfg->set_cache("memcache://localhost:11211");
				  //$cfg->set_date_format('Y-m-d H:i:s');
				
				$cfg->set_cache('file://localhost', array('expire' => 3600));
				
				$cfg->set_logging(true);
				$cfg->set_logger(new EduFocal_logger());
				
				//ActiveRecord\Serialization::$DATETIME_FORMAT = 'Y';
				
				$cfg->set_connections(array('development' =>
				    'mysql://'.$db->username.':'.$db->password.'@'.$db->hostname.'/'.$db->database.'?charset=utf8'));
			});
		} catch ( Exception $e ){
			exit($e->getMessage());
		}
	}	
}

class EduFocal_logger 
{
	public function log ( $log )
	{
		static $counter = 0;
		
		$CI =& get_instance();
		$db = $CI->db;
		
		if ( is_array($log[1]) && count($log[1]) > 0)
		{
			$db->queries[] = $db->compile_binds($log[0], array_values($log[1]));
		}
		else
		{
			$db->queries[] = $log[0];	
		}
		
		$db->query_times[] = $counter++;
	}
}

spl_autoload_register('activerecord_autoload');

function activerecord_autoload($class_name)
{
	$path = ActiveRecord\Config::instance()->get_model_directory();	
	$root = realpath(isset($path) ? $path : '.');
	
	if (($namespaces = ActiveRecord\get_namespaces($class_name)))
	{
		$class_name = array_pop($namespaces);
		$directories = array();

		foreach ($namespaces as $directory)
			$directories[] = $directory;

		$root .= DIRECTORY_SEPARATOR . implode($directories, DIRECTORY_SEPARATOR);
	}

	$file = "$root/$class_name.php";

	if (file_exists($file))
		require $file;
}

