<?php
/**
 *
 * @package default
 * @author Paul Allen
 */
class ReportRequest extends ActiveRecord\Model
{
    static $has_one = array(
        array('report', 'readonly' => true),
    );
}
