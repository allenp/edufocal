<?php
/**
 *
 * @package default
 * @author Paul Allen
 */
class ReportSubject extends ActiveRecord\Model
{
    static $belongs_to = array(
        array('report', 'readonly' => true),
    );
}
