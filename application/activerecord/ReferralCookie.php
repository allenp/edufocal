<?php

/**
 * Represents a referral cookie object that is used to verify referrals.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class ReferralCookie extends ActiveRecord\Model
{
	
	/**
	 * Generates the hash of the cookie based on its credentials.
	 * 
	 * @return string 32 length string representing the cookie hash.
	 * @author Jamie Chung
	 */
	public function hash ()
	{
		return md5($this->code.$this->session.$this->salt);
	}
	
}