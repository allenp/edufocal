<?php

/**
 * Represents a user referral in a contest.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class UserReferral extends ActiveRecord\Model
{
	static $before_create = array('before_create');
	
	public function before_create ()
	{
		//$this->contest_id = Contest::find('last')->id;
		$this->status = 'pending';
		
		$CI =& get_instance();
		$this->user_ip = $CI->input->ip_address();
		
		$this->created_on = new ActiveRecord\DateTime();
	}
	
	public function complete ()
	{
		$this->status = 'completed';
		$this->save();
	}
}
