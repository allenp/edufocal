<?php

/**
 *
 * @package default
 * @author Paul Allen
 */
class Receipt extends ActiveRecord\Model
{
	static $before_create = array(
		'before_create'
	);

    static $has_many = array(
                            array(
                                'codes', 
                                'foreign_key' => 'source_ref',
                                'conditions' => array('(source = 0 or source = 2)'),
                                'readonly' => true,
                            ),
                       );
	
	/**
	 * Callback method before the code object is created in the database.
	 *
	 * @author Jamie Chung
	 */
	public function before_create ()
	{
		$this->created_on = time();
	}
}
