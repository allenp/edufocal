<?php

class Rating extends ActiveRecord\Model
{
	static $before_create = array ('create_stamp');
	
	public function create_stamp ()
	{
		$this->timestamp = time();
	}
}
