<?php

class StudentScore extends ActiveRecord\Model
{
    static $before_save = array('update_timestamp');
	static $belongs_to = array(
		array('user'),
		array('school', 'through' => 'user')
	);

    public function update_timestamp()
    {
        $this->timestamp = time();
    }
}
