<?php

class Answer extends ActiveRecord\Model
{
	static $belongs_to = array(
		array('question'),
		array('test'),
		array('user'),
		array('teacher'),
		array('subject'),
		array('topic')
	);
	
	static $has_many = array(
		array('responses')
	);
}
