<?php

/**
 * Represents a message entity in the system. A converstaion between multiple users.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class Message extends ActiveRecord\Model
{
	static $has_many = array(
		array('message_replies', 'order' => 'stamp asc'),
		array('message_users'),
		array('users', 'through' => 'message_users')
	);
	
	static $before_create = array ('create_stamp');
	
	static $alias_attribute = array (
		'replies' => 'message_replies',
	);	
	
	/**
	 * Callback method set the timestamps of the object.
	 *
	 * @author Jamie Chung
	 */
	public function create_stamp ()
	{
		// TODO: Make a migration to change int(11) timestamps to datetime in sql
		// TODO: Use ActiveRecord\DateTime to handle all times.
		$this->stamp = time();
		$this->last_stamp = time();
	}
	
	/**
	 * Generates a permalink to that specific message.
	 *
	 * @author Jamie Chung
	 */
	public function permalink ()
	{
		return site_url('messages/view/'.$this->id);
	}
}
