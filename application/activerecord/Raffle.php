<?php

class Raffle extends ActiveRecord\Model
{
    static $has_many = array(
        array('raffle_baskets', 'readonly' => true),
    );
}
