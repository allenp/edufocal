<?php

/**
 * Represents a school object in the database.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class School extends ActiveRecord\Model
{
	static $belongs_to = array ('parish');
}
