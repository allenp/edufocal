<?php
/**
 *
 * @package default
 * @author Paul Allen
 */
class ReportTopic extends ActiveRecord\Model
{
    static $belongs_to = array(
        array('report', 'readonly' => true),
    );
}
