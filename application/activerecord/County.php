<?php 

/**
 * Represents a county, region for schools and parishes.
 *
 * @package default
 * @author Jamie Chung
 */
class County extends ActiveRecord\Model
{
	static $has_many = array('parishes');

	static $before_save = array ('create_permalink');

	/**
	 * Callback method in order to generate a permalink based on the county name.
	 *
	 * @author Jamie Chung
	 */
	public function create_permalink ()
	{
		$this->permalink = create_permalink($this->name);
	}

}
