<?php

/**
 * Represents card information for a customer
 * Uses RIJNDAEL_256 cipher and MCRYPT_MODE_CBC to encrypt data
 *
 * @package default
 * @author Paul Allen
 */
class Card extends ActiveRecord\Model
{
    public static $encryption_key = '';
    public static $decryption_key = '';

    private static $ci;

    public function __construct(
        $attributes=array(),
        $guard_attributes=true,
        $instantiating_via_find=false,
        $new_record=true)
    {
        parent::__construct(
            $attributes,
            $guard_attributes,
            $instantiating_via_find,
            $new_record);

        self::$ci =& get_instance();
        self::$ci->load->library('encrypt');
        self::$ci->encrypt->set_mode(MCRYPT_MODE_CBC);
        self::$ci->encrypt->set_cipher(MCRYPT_RIJNDAEL_256);

        self::$ci->load->config('card');

        self::$encryption_key = self::$ci->config->item('card_encrypt_secret');
        self::$decryption_key = self::$ci->config->item('card_decrypt_secret');
    }

    public function set_card_no($card)
    {
        $this->assign_attribute('card_no', $this->encode($card));
        $redacted = str_repeat('X', strlen($card) - 5);
        $redacted .= substr($card, strlen($card) - 5);
        $this->assign_attribute('redacted', $redacted);
    }

    public function get_card_no()
    {
        return $this->decode($this->read_attribute('card_no'));
    }
    public function set_expiry_date($expiry_date)
    {
        $this->assign_attribute('expiry_date', $this->encode($expiry_date));
    }

    public function get_expiry_date()
    {
        return $this->decode($this->read_attribute('expiry_date'));
    }

    public function set_cvc($cvc)
    {
        $this->assign_attribute('cvc', $this->encode($cvc));
    }

    public function get_cvc()
    {
        return $this->decode($this->read_attribute('cvc'));
    }

    public function set_card_name($name)
    {
        $this->assign_attribute('card_name', $this->encode($name));
    }

    public function get_card_name()
    {
        return $this->decode($this->read_attribute('card_name'));
    }

    public function set_address1($address1)
    {
        $this->assign_attribute('address1', $this->encode($address1));
    }

    public function get_address1()
    {
        return $this->decode($this->read_attribute('address1'));
    }

    public function set_address2($address2)
    {
        $this->assign_attribute('address2', $this->encode($address2));
    }

    public function get_address2()
    {
        return $this->decode($this->read_attribute('address2'));
    }

    public function set_city($city)
    {
        $this->assign_attribute('city', $this->encode($city));
    }

    public function get_city()
    {
        return $this->decode($this->read_attribute('city'));
    }

    public function set_state($state)
    {
        $this->assign_attribute('state', $this->encode($state));
    }

    public function get_state()
    {
        return $this->decode($this->read_attribute('state'));
    }

    public function set_country($country)
    {
        $this->assign_attribute('country', $this->encode($country));
    }

    public function get_country()
    {
        return $this->decode($this->read_attribute('country'));
    }

    /*
     * Encodes text using secret key.
     */
    private function encode ($text)
    {
        return self::$ci->encrypt->encode($text, self::$encryption_key);
    }

    /*
     * Decodes text using secret key.
     */
    private function decode ($cipher)
    {
        return self::$ci->encrypt->decode($cipher, self::$decryption_key);
    }
}
