<?php

class VideoSource extends ActiveRecord\Model
{
	static $belongs_to = array('video');

	public function url()
	{
		return site_url('/videos/'. $this->path);
	}
}
