<?php

class Video extends ActiveRecord\Model
{
	static $belongs_to = array('topic');
	static $has_many = array('video_sources');
	static $before_destroy = array('delete_sources');

	private static $ci;

	public function __construct($attributes=array(), $guard_attributes=true,
	$instantiating_via_find=false,
        $new_record=true)
    {
        parent::__construct(
            $attributes,
            $guard_attributes,
            $instantiating_via_find,
            $new_record);

		if ( !function_exists('url_title')) {
			if (null == self::$ci) {
				self::$ci =& get_instance();
				self::$ci->load->helper('url');
			}
		}
    }

	public function set_title($title)
	{
		$this->assign_attribute('title', $title);
		$this->set_permalink($title);
	}

	public function set_permalink($title)
	{
		$this->assign_attribute('permalink', url_title($title, 'underscore', true));
	}

	public function get_free()
	{
		return $this->read_attribute('free') == 1;
	}

	public function seen()
	{
		return rand(0,1) == 1;
	}

	/*
	 * Sort with a preference for mp4 sources.
	 * Mostly because mp4 is transcoded with faststart enabled
	 * And is most widely supported too, for now.
	 */
	public function sources()
	{
		$sources = $this->video_sources;
		if (null !== $sources && count($sources) > 0) {
			usort($sources, function($a, $b) {
				if (strpos('mp4', $a->path) >= 0) {
					return 1;
				} else if(strpos('mp4', $b->path) >= 0) {
					return -1;
				} else {
					return 0;
				}
			});
		}
		return $sources;
	}

	public function delete_sources()
	{
		VideoSource::delete_all(['conditions' => ['video_id = ?', $this->id]]);
	}
}
