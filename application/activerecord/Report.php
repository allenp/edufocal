<?php

/**
 *
 * @package default
 * @author Paul Allen
 */
class Report extends ActiveRecord\Model
{
    static $has_many = array(
        array('report_subjects', 'readonly' => true, 'order' => 'avg_percent desc'),
        array('report_topics', 'readonly' => true, 'order' => 'avg_percent desc'),
    );

    public function before_destroy()
    {
        ReportSubject::delete_all(array('conditions' => array(
            'report_id' => $this->id)));
        ReportTopic::delete_all(array('conditions' => array(
            'report_id' => $this->id)));
    }
}
