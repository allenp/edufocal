<?php

class TeachersTopic extends ActiveRecord\Model
{
	static $belongs_to = array(
		array('subject'),
		array('topic')
	);
}

