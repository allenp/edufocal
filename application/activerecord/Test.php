<?php

class Test extends ActiveRecord\Model
{
	static $before_create = array ('create_stamp');
    static $has_many = array(
        array('answers', 'order' => 'id'),
        array(
            'questions',
            'through' => 'answers',
            'order' => 'answers.id'
        ),
    );
	
	public function create_stamp ()
	{
		$this->started = time();
	}

    public function before_destroy()
    {
        Answer::delete_all(array('conditions' => array(
            'test_id' => $this->id)));
    }

    public function get_weights()
    {
        if(!is_array($this->read_attribute('weight')))
            return (array)json_decode($this->read_attribute('weight'));
        else
            return $this->read_attribute('weight');
    }


    public function add_weight($weight)
    {
        $weights = $this->get_weights();
        $weights = is_array($weights) ? $weights : array();
        $weights[] = $weight;
        $this->weight = json_encode($weights);
    }

    /**
	 * @return boolean|integer False on short answer | Final grade
	 */
	public function mark()
	{
        if($this->marked == 1)
            return $this->percentage;

        $answers = $this->answers;
		$correct = 0;
        
		$short_answer = false;
        
        foreach($answers as $answer)
        {
            if($answer->question->type == 1)
            { //multiple choice    
                if($answer->answer == $answer->question->accepted_answers)
                    $correct++;
            }
            else if($answer->question->type == 2)
            {
                $short_answer = true;
            }
		}
		
        $percentage = round($correct / count($answers) * 100);

        if($this->percentage != $percentage || $this->marked == 0)
        {
            $this->percentage = $percentage;
            $this->marked = 1;
            $this->save();
        }

        if($short_answer)
            return false;
        else
            return $percentage;
	}

    public function complete()
    {
        Answer::update_all(
            array(
                'set' => array('completed' => 'yes'),
                'conditions' => array('test_id = ?', $this->id)
            )
        );
    }
}
