<?php

/**
 * Represents a single entity or item in a message.
 *
 * @package default
 * @author Jamie Chung
 */
class MessageReply extends ActiveRecord\Model
{
	static $belongs_to = array (
		array('message'),
		array('user')
	);
	
	static $before_create = array ('create_stamp');
	
	
	/**
	 * Callback method to generate the stamp.
	 *
	 * @author Jamie Chung
	 */
	public function create_stamp ()
	{
		// TODO: Use ActiveRecord\DateTime::now();
		$this->stamp = time();
	}
	
	/**
	 * Generates the permalink of the message so that it is easily linked.
	 *
	 * @author Jamie Chung
	 */
	public function permalink ()
	{
		return 'messages/view/'.$this->message_id.'#reply-'.$this->id;
	}
	
}

