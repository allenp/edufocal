<?php

/**
 * Contest ActiveRecord Model.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class Contest extends ActiveRecord\Model
{
	/**
	 * Generates a full site URL to a contest page.
	 *
	 * @author Jamie Chung
	 */
	function permalink ()
	{
		return site_url('contests/'.$this->id.'-'.$this->permalink);
	}
	
	/**
	 * Calculates the days remaining in a contest. 
	 *
	 * @author Jamie Chung
	 */
	function days_remaining ()
	{
		$stamp = time();
		$diff = strtotime($this->end_date) - time();
				
		$days = floor($diff / (60*60*24));
		
		if ( $days <= 0 )
		{
			return '0 days';
		}
		else if ( $days == 1 )
		{
			return '1 day';
		}
		
		return $days . ' days';;		
	}
	
	/**
	 * Produces a full site URL to the contest image. 
	 *
	 * @author Jamie Chung
	 */
	function image ()
	{
		return site_url('uploads/contests/'.$this->image);
	}

	/**
	 * Deactivates a contest. 
	 *
	 * @author Samuel Folkes
	 */
	public function deactivate() {
		$this->status = 0;
		$this->save();
	}
}

?>
