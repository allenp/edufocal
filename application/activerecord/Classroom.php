<?php

/**
 * Represents a grouping of students by a class
 * manager.
 *
 */
class Classroom extends ActiveRecord\Model
{
    static $has_many = array('classroom_students');
}
