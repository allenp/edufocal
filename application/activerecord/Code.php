<?php

/**
 * Represents an E-Pin/EF Code for user's registration and payment on the site.
 *
 * @package default
 * @author Jamie Chung
 */
class Code extends ActiveRecord\Model
{
	static $before_create = array(
		'before_create'
	);

	static $belongs_to = array(
		array('user')
	);

	/**
	 * Callback method before the code object is created in the database.
	 *
	 * @author Jamie Chung
	 */
	public function before_create ()
	{
		// TODO: Change to ActiveRecord\DateTime
		$this->created_on = time();
		$this->code = strtoupper($this->code);
	}

	/**
	 * Accepts a code from a user and sets expiry date.
	 *
	 * @author Paul Allen
	 */
	public function accept (& $code_charges = null)
	{
        if ($code_charges == null) {
            return;
        }

        if (isset($code_charges[$this->type])) {
            $code_charge = $chode_charges[$this->type];
            $valid_for = $code_charge['valid_for'];
            $this->accepted_on = time();

            if ($this->expiry_date == 0) { //expiry date can be pre-set.
                $this->expiry_date = strtotime('+ ' . $valid_for, time());
            }

            $this->save();
        }
	}

    public static function Generate($options = array())
    {
        $CI =& get_instance();
        $CI->load->helper('string');
        $found = FALSE;
        $iter = 1;

        if (!$options) {
            $options = array();
        }

        do {
            $serial = random_string('numeric', 13);
            if (isset($options['sponsor']) && strlen($options['sponsor']) <= 7) {
                $serial .= $options['sponsor'];
                unset($options['sponsor']);
            }

            try {
                $options['code'] = $serial;
                $code = Code::create($options);
                if (is_numeric($code->id)) {
                   $found = true;
                   return $code;
                }
            } catch(Exception $e) {
                $found = false;
            }
        } while(!$found && ++$iter < 10);
        return FALSE;
    }
}
