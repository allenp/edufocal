<?php

class User extends ActiveRecord\Model
{
	static $has_many = array(
		array('message_users')
	);

    static $has_one = array(
        array('code', 'order' => 'expiry_date desc')
    );

	static $validates_uniqueness_of = array (
		array('email')
	);


    static $belongs_to = array (
        array('school')
    );

	public function public_name ()
	{
		return $this->first_name.' '.ucwords(substr($this->last_name, 0, 1)).'.';
	}

	public function name ()
	{
		return implode(' ', array($this->first_name, $this->last_name));
	}

	public function get_profile ()
	{
		if ( !is_array($this->read_attribute('profile')) )
		{
			return (array) json_decode($this->read_attribute('profile'));
		}

		return $this->read_attribute('profile');
	}

	public function field ($field, $value)
	{
		if (isset($this->$field))
			$this->{$field} = $value;
        else
        {
            if($profile = $this->get_profile())
                $profile[$field] = $value;
            $this->profile = json_encode($profile);
        }
	}

	public function profile ( $field )
	{
        if ( $profile = $this->get_profile()  )
			return isset($profile[$field]) ? $profile[$field] : NULL;
		else
			return NULL;
	}

	public function testing_level ()
	{
        return $this->exam_level;
	}

	/**
	 * Callback method before the User object is created in the database.
	 *
	 * @author Jamie Chung
	 */
	public function before_create ()
	{
		$this->created_on = date('Y-m-d H:i:s');
	}
}
