<?php

/**
 * Repersents a parish/region for schools to be placed in.
 *
 * @package default
 * @author Jamie Chung
 */
class Parish extends ActiveRecord\Model
{
	static $has_many = array ('schools');
	static $belongs_to = array('county');

	static $before_save = array ('create_permalink');

	/**
	 * Generates the permalink of a parish based on its name.
	 *
	 * @return Slug version of the parish's name.
	 * @author Jamie Chung
	 */
	public function create_permalink ()
	{
		$this->permalink = create_permalink($this->name);
	}
}
