<?php

/**
 * Represents the association between a message and multiple users.
 *
 * @package default
 * @author Jamie Chung
 */
class MessageUser extends ActiveRecord\Model
{
	static $belongs_to = array (
		array('message', 'readyonly' => true),
		array('user', 'readyonly' => true)
	);
}
