<?php

class Response extends ActiveRecord\Model
{
	static $belongs_to = array(
		array('answer'),
		array('user')
	);
	
	static $has_many = array(
		array('responses')
	);
}
