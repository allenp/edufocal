<?php

class StudentSubjectScore extends ActiveRecord\Model
{
   static $before_save = array('update_timestamp');

   public function update_timestamp()
   {
       $this->timestamp = time();
   }
}
