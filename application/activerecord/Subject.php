<?php

class Subject extends ActiveRecord\Model
{
	static $has_many = array ('topics');

	private static $ci;

	public function __construct($attributes=array(), $guard_attributes=true,
	$instantiating_via_find=false,
        $new_record=true)
    {
        parent::__construct(
            $attributes,
            $guard_attributes,
            $instantiating_via_find,
            $new_record);

		if ( !function_exists('url_title')) {
			if (null == self::$ci) {
				self::$ci =& get_instance();
				self::$ci->load->helper('url');
			}
		}
    }

	public function set_name($title)
	{
		$this->assign_attribute('name', $title);
		$this->set_permalink($title);
	}

	public function set_permalink($title)
	{
		$this->assign_attribute('permalink', url_title($title, 'underscore', true));
	}

	public function get_permalink()
	{
		return trim($this->read_attribute('permalink'));
	}
}
