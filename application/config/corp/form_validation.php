<?php

$config = array(
    'enrollment' => array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim|required|min_length[2]'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|callback__unqiue_email'
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone Number',
            'rules' => 'trim|required|xss_clean'
        )
    ),

);