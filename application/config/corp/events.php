<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['before_controller'][] = array(
    'module' => 'ef_corp',
    'filepath' => 'controllers',
    'filename' => 'Base_Controller_Hooks.php',
    'class' => 'Base_Controller_Hooks',
    'method' => 'before_controller'
);


$config['after_controller_constructor'][] = array(
    'module' => 'ef_corp',
    'filepath' => 'controllers',
    'filename' => 'Base_Controller_Hooks.php',
    'class' => 'Base_Controller_Hooks',
    'method' => 'after_controller_constructor'
);
