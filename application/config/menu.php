<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
    |------------------------------------------------------------------------
    | MENUS
    |------------------------------------------------------------------------

    This file stores a logical grouping of the menus used in the dashboard area.
    It was originally conceived as a way to facilitate responsive menus.

    This acts as a central logical store for the main and sub navigations
    that enables their reuse to be generated in piece or in full where needed.
*/

$menus = array();
$menus['main'] = array(
    'dashboard' => array('title' => 'Dashboard', 'url' => 'dashboard'),
    'subjects' => array('title' => 'Subjects', 'url' => 'dashboard/subjects'),
    //'question_queue' => array('title' => 'Question Queue', 'url' => 'dashboard/question_queue'),
    'report' => array('title' => 'My Report Card', 'url' => 'dashboard/reports'),
);

$menus['student_personal'] = array(
    'profile' => array('title' => 'My Profile', 'url' => 'dashboard/profile'),
    'rankings' => array('title' => 'My Rankings', 'url' => 'rankings'),
    'messages' => array('title' => 'Messages', 'url' => 'messages'),
);

$menus['user_info'] = array(
    'account' => array('title' => 'Account Settings', 'url' => 'account'),
    'logout' => array('title' => 'Logout', 'url' => 'logout')
);
