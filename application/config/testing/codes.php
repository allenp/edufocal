<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');

/*
|-------------------------------------------------------------------------
| The price for each payment option and the duration of the valid time
|------------------------------------------------------------------------
|*/

$config['educodes'] = array();
$config['educodes_usd'] = array();

$config['educodes']['monthly'] = array( 'charge' => 1.00, 'valid_for' => '30 days' );
$config['educodes']['quarterly'] = array('charge' => 90.00, 'valid_for' => '90 days' );
$config['educodes']['yearly'] =  array( 'charge' => 2000.00, 'valid_for' => '1 year', 'show_publicly' => false );
$config['educodes']['weekly'] = array( 'charge' => 1.00, 'valid_for' => '1 week', 'show_publicly' => false );

$config['educodes_usd']['monthly'] = array( 'charge' => 1.00,'valid_for' => '30 days' );
$config['educodes_usd']['quarterly'] = array( 'charge' => 6.00,'valid_for' => '90 days' );
$config['educodes_usd']['yearly'] =  array( 'charge' => 24.00, 'valid_for' => '1 year', 'show_publicly' => false );

