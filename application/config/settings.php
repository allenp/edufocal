<?php

$config['site.status'] = 1;
$config['site.list_limit'] = 25;
$config['site.show_profiler'] = 1;
$config['site.show_front_profiler'] = 1;
$config['updates.do_check'] = 1;
$config['updates.bleeding_edge'] = 1;
$config['auth.allow_register'] = 1;
$config['auth.login_type'] = 'email';
$config['auth.use_usernames'] = 1;
$config['auth.allow_remember'] = 1;
$config['auth.remember_length'] = 1209600;
$config['auth.do_login_redirect'] = 0;
$config['auth.use_extended_profile'] = 0;
$config['auth.allw_name_change'] = 1;
$config['auth.name_change_frequency'] = 1;
$config['auth.name_change_limit'] = 1;
$config['auth.password_min_length'] = 8;
$config['auth.password_force_numbers'] = 0;
$config['auth.password_force_symbols'] = 0;
$config['auth.password_force_mixed_case'] = 0;
