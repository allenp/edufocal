<?php

$config = array(
	'signup_1' => array(
		array(
			'field' => 'first_name',
			'label' => 'First Name',
			'rules' => 'trim|required|min_length[2]'
		),
		array(
			'field' => 'last_name',
			'label' => 'Last Name',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'sex',
			'label' => 'Gender',
			'rules' => 'trim|required|check_gender'
		),
		array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|valid_email|callback_check_unique_email'
		),
        array(
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'trim|xss_clean'
        ),
		array(
			'field' => 'exam_level',
			'label' => 'Exam level',
			'rules' => 'trim|required|callback_valid_exam'
		),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|matches[password_confirmation]|min_length[8]|max_length[32]'
		),
		array(
			'field' => 'password_confirmation',
			'label' => 'Confirm Password',
			'rules' => 'trim'
		)
	),
	'messages/compose' => array (
		array (
			'field' => 'body',
			'label' => 'Message',
			'rules' => 'trim|required|htmlspecialchars'
		)
	),
	'signup/forgot' => array (
		array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|valid_email'
		)

	),
	'messages/reply' => array(
		array (
			'field' => 'body',
			'label' => 'Message',
			'rules' => 'trim|required|htmlspecialchars'
		)
	),
    'edufocal_code' => array(
        array(
            'field' => 'educode',
            'label' => 'EduFocal Code',
            'rules' => 'trim|required|callback__valid_efcode'
        )
    ),
    'card_payment' => array(
        array(
            'field' => 'card_name',
            'label' => 'Card Holder',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'card_number',
            'label' => 'Card Number',
            'rules' => 'trim|required|callback__valid_card'
        ),
        array(
            'field' => 'card_cvc',
            'label' => 'Verifcation Number',
            'rules' => 'trim|required|numeric'
        ),
        array(
            'field' => 'card_expiry_month',
            'label' => 'Expiration Month',
            'rules' => 'trim|required|numeric'
        ),
        array(
            'field' => 'card_expiry_year',
            'label' => 'Expiration Year',
            'rules' => 'trim|required|numeric'
        ),
        array(
            'field' => 'plan',
            'label' => 'Payment Plan',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'address1',
            'label' => 'Address 1',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'address2',
            'label' => 'Address 2',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'country',
            'label' => 'Country',
            'rules' => 'trim|required',
        ),
        array(
            'field' => 'city',
            'label' => 'City',
            'rules' => 'trim',
        ),
        array(
            'field' => 'state',
            'label' => 'State',
            'rules' => 'trim',
        )
    )
);

if(!function_exists('valid_card'))
{
    function valid_card(&$number, &$CI)
    {
        $number = preg_replace('/\s+/', '', $number);
        $card = '';
        $cards = array(
            'visa' => '/^4\d{12}(\d\d\d){0,1}$/',
            'mastercard' => '/^5[12345]\d{14}$/',
            'keycard' => '/^77/',
        );

        foreach($cards as $type => $regex)
        {
            if(preg_match($regex, $number))
            {
                $card = $type;
                break;
            }
        }

        if(!$card)
        {
            $CI->form_validation->set_message('_valid_card', 'The %s is not a recognized VISA, MASTERCARD or KEYCARD number');
            return FALSE;
        }

        if($card == 'keycard')
        {
            return TRUE;
        }

        $reverse = strrev($number);
        $checksum = 0;

        for($i = 0; $i < strlen($reverse); $i++)
        {
            $current_num = intval($reverse[$i]);
            if($i & 1)
                $current_num *= 2;

           $checksum += $current_num % 10;
           if($current_num > 9)
               $checksum += 1;
        }

        if($checksum % 10 == 0)
            return TRUE;
        else
        {
            $CI->form_validation->set_message('_valid_card', 'The %s is invalid');
            return FALSE;
        }
    }
}

if(!function_exists('valid_efcode'))
{
    function valid_efcode ( &$input, &$CI )
    {
        $input = preg_replace('/\s+/', '', $input);

        $code = Code::find_by_code_and_active($input, 1);

        if ( !$code )
        {
            $CI->form_validation->set_message(
                    '_valid_efcode',
                    'The EF Code cannot be found.');
            return FALSE;
        } else if ( $code->user_id == $CI->auth->user_id()) {
            $CI->form_validation->set_message(
                '_valid_efcode',
                'This code was added to your account. See <a href="/account/receipts">Payment History</a> for details.');
            return true;
        } else if ((!empty($code->expiry_date) || $code->expiry_date != 0) && $code->expiry_date < time()) {
            $CI->form_validation->set_message(
                '_valid_efcode',
                'This code expired on ' . date('F j, Y', $code->expiry_date) . ' and can no longer be used.');
            return FALSE;
        } else if ( $code->user_id != 0) {
            $CI->form_validation->set_message(
                    '_valid_efcode',
                    'The EF Code has already been processed.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}

if(!function_exists('unique_email'))
{
    function unique_email(&$CI) {
        $user = User::find_by_email($CI->input->post('email'));
        if(count($user) > 0) {
            $CI->form_validation->set_message(
            'check_unique_email',
            'The Email you have selected is already in use.');
            return false;
        }
        return true;
    }
}
