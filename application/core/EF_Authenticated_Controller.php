<?php

class EF_Authenticated_Controller extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ( !$this->auth->is_logged_in() ) {
            redirect('/');
        }
    }

    protected function ensure_role ( $role_id = array() )
    {
        if(!is_array($role_id))
            $role_id = array($role_id);

        switch ($this->auth->role_id()) {

        case TEACHER_ROLE:
        case EDITOR_ROLE:
            $url = 'teachers';
            break;

        case REVIEWER_ROLE:
            $url = 'reviewer';
            break;

        case STUDENT_ROLE:
            $url = 'dashboard';
            break;

        case MANAGER_ROLE:
            $url = 'managers';
            break;

        case ADMIN_ROLE:
            $url = 'admin';
            break;

        case SALES_ROLE:
            $url = 'admin';
            break;
        }

        //Admin role can access everything
        $role_id[] = ADMIN_ROLE;

        if ( !in_array($this->auth->role_id(), $role_id) ) {
            redirect($url);
        }
    }
}
