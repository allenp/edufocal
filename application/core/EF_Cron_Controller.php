<?php

class EF_Cron_Controller extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();

        Template::set_theme('cron');
        if ( !$this->input->is_cli_request() && is_production()) {
            die('unauthorised invocation');
        }
    }
}
