<?php

class EF_Manager_Controller extends EF_Authenticated_Controller
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensure_role(array(MANAGER_ROLE, TEACHER_ROLE));
        if ($this->auth->is_expired()) {
            Template::set_message('You need to verify your account with an EduFocal Code.', 'danger');
            redirect('/account/payment');
        }
        Template::set_block('main_navigation', 'partials/main_navigation');
        Template::set_block('personal_header_links', 'partials/manager_header_links');
        Template::set_theme('dashboard');
    }
}
