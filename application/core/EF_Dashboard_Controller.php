<?php

class EF_Dashboard_Controller extends EF_Authenticated_Controller
{
    public function __construct ()
    {
        parent::__construct();

        if ($this->auth->profile()->code_id == 0) {
           redirect('/');
        }

        //if their code has expired and they don't have any pre-ordered codes
        if($this->auth->is_expired() && ! $this->auth->use_saved_code_if_any()) {
           redirect('/account/payment');
        }

        $this->ensure_role(array(STUDENT_ROLE, EMPLOYEE_ROLE, MANAGER_ROLE));

        Template::set_theme('dashboard');
    }
}
