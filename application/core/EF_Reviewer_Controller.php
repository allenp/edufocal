<?php

class EF_Reviewer_Controller extends EF_Authenticated_Controller
{
    public function __construct ()
    {
        parent::__construct();

        if ( $this->auth->is_expired() ) {
            Template::set_message('You need to verify your account with an EduFocal Code.', 'danger');
            redirect('/account/payment');
        }

        Template::set_theme('reviewer');

        $this->ensure_role(REVIEWER_ROLE);
    }
}
