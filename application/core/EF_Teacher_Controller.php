<?php

class EF_Teacher_Controller extends EF_Authenticated_Controller
{
    public function __construct ()
    {
        parent::__construct();

        if ($this->auth->is_expired()) {
            Template::set_message('You need to verify your account with an EduFocal Code.', 'danger');
            redirect('/account/payment');
        }

		Assets::add_css(
        array(
            )
        );

		Assets::add_js(
        array(
            site_url('assets/teachers/js/jquery-ui-1.11.1.custom/jquery-ui.min.js'),
            site_url('assets/tiny_mce/jquery.tinymce.js'),
            site_url('assets/teachers/js/script.js'),
            )
        );

        Template::set_theme('dashboard');
        $this->ensure_role(array(TEACHER_ROLE, EDITOR_ROLE));
    }
}
