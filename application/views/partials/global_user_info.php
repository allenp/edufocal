
            	<div class="img">
            		<?php $avatar = $this->auth->profile()->avatar;
					if(empty($avatar)) : ?>
						<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" width="41" />
					<?php else : ?>
						<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" width="41"/>
					<?php endif; ?>
                </div>
                <p>Welcome, <?php echo $this->auth->name(); ?></p>
                <p class="small">
                	<?php echo anchor('account', 'Account Settings'); ?> | 
                	<?php echo anchor('logout', 'Logout') ;?>
                </p>
