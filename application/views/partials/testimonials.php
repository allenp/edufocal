                   <li><img src="<?php echo site_url(); ?>/assets/img/testimonials/ronald.jpg" alt="User" /><p><em>"Persons in the education industry are tired of the same conventional,
ineffective methods of getting information across to students.
EduFocal presents a refreshing method of incorporating
learning with play."</em></p>
                    <p class="small">Hon. Rev. Ronald Thwaites<br />Minister of Education, Jamaica</p></li>
                    <li><img src="<?php echo site_url(); ?>/assets/img/testimonials/warren.jpg" alt="Warren Cassell Jr." /><p><em>"Unlike most learning platforms, EduFocal makes revising a non-monotonous and enjoyable task. Not only has preparing for exams with EduFocal's practice questions boosted my confidence but also my overall performance."</em></p>
                    <p class="small">Warren Cassell Jr.<br />Secondary student and teen entrepreneur</p></li>
                    <li><img src="<?php echo site_url(); ?>/assets/img/shantol.jpg" alt="Shantol Barton" /><p><em>"EduFocal has helped me to prepare myself for the CSEC exam in one of the most outstanding ways. I will be sitting four subjects this year in fourth form and I am way ahead in my studies. I've excelled in all four subject areas as a result of EduFocal."</em></p>
                    <p class="small">Shantol Barton<br />Secondary student</p></li>

