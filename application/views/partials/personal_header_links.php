<?php
$count = MessageUser::count(array('conditions' => array(
                                            'user_id = ? and `read` = ? and `deleted` = ?',
                                            $this->auth->user_id(),
                                            'no',
                                            'no')));
$counter = $count > 0 ? " ({$count})" : "";
$mylinks = array (
	'dashboard/profile' => 'My Profile',
	'rankings' => 'My Rankings',
	'messages' => "Messages{$counter}"
);
?>
<ul class="nav navbar-nav">
<?php foreach ( $mylinks as $k => $v ) : 
     $base = explode('/', $k); ?>
	<li<?php if ( current_url() == site_url($k)  || ( (isset($base) && count($base) == 1) && $base[0] == $this->uri->segment(1))) echo ' class=active'; ?>><a href="<?php echo site_url($k); ?>"><?php echo $v; ?></a></li>
<?php endforeach; ?>
</ul>
