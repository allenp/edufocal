<?php
$counter = count(MessageUser::find_all_by_user_id_and_read_and_deleted($this->auth->user_id(), 'no', 'no'));

if ( $counter > 0 )
{
	$counter = ' ('.$counter.')';
}
else
{
	$counter = '';
}

$_links = array (
	'messages' => 'Messages' . $counter
);
?>
<ul class="nav navbar-nav">
<?php foreach ( $_links as $k => $v ) : $base = explode('/', $k); ?>
	<li<?php if ( current_url() == site_url($k)  || ( (count($base) == 1) && $base[0] == $this->uri->segment(1))) echo ' class=active'; ?>><a href="<?php echo site_url($k); ?>"><?php echo $v; ?></a></li>
<?php endforeach; ?>
</ul>
