<ul class="nav navbar-nav">
    <li<?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) === false) print ' class="active"'; ?>><a href="<?php print site_url('dashboard'); ?>">Dashboard</a></li>
    <li<?php if($this->uri->segment(2) == 'subjects' || $this->uri->segment(2) == 'videos') print ' class="active"'; ?>><a href="<?php print site_url('dashboard/subjects'); ?>">Subjects</a></li>
<?php if($this->auth->role_id() == ADMIN_ROLE || $this->config->item('student_report.toggle') === TRUE): ?>
    <li<?php if($this->uri->segment(2) == 'reports') print ' class="active"'; ?>><a href="<?php print site_url('dashboard/reports'); ?>">My Report Card</a></li>
<?php endif; ?>
</ul>
