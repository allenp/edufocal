    <div class="divider1"></div>
    <div id="main">
		<div id="column_1">
        	<div class="section">
            	<h2>EduFocal is a 21st Century learning tool</h2>
                <p>Online Education Tools make it possible for students to learn in new &amp; exciting ways. EduFocal is an online tool created specially for CXC &amp; GSAT Students.</p>
                <ul class="benefits">
                	<li><strong>Comprehensive Questions &amp; Answers.</strong><br />
					It improves the way students learn by reinforcing what is taught in class.</li>
					<li><strong>Video Game Progression.</strong><br />
					Study never gets boring, as there's always an incentive to keep going</li>
					<li><strong>Students can earn Cash.</strong><br />
					EduFocal provides test questions and answers for topics that give students trouble.</li>
					<li><strong>Content Prepared by Top Teachers.</strong><br />
					Students earn "Experience Points" at each level and can qualify to become Tutors</li>
					<li><strong>Students Earn Study Rewards.</strong><br />
					Tutors can earn cash by helping other students</li>
                </ul>
            </div>
        	<div class="divider2"></div>
            <div class="section">
            	<h2>Students prefer to play than to study.</h2>
                <p>The reason is simple. Play is fun! EduFocal combines the challenge and fun of play with the structure and discipline of study. The result is a tool that students will enjoy!</p>
                <div id="comparison">
                	<div class="heading">
                    	<img src="assets/img/logo2.png" alt="EduFocal" class="logo" />
                        <img src="assets/img/vs.png" alt="Vs" class="vs" />
                        <h3>Traditional Approach</h3>
                    </div>
                	<div class="good">
                    	<ul class="benefits">
                    		<li><strong>Makes Learning Fun </strong><br />
							EduFocal integrate the challenge and rewards of play in a study environment.</li>
							<li><strong>Easily Track your Progress</strong><br />
							Students gain "Experience Points" at each stage, allowing them to track their progress.</li>
							<li><strong>Win Prizes &amp; Earn Rewards</strong><br />
							Students can earn rewards in a number of ways, and can even earn cash incentives from EduFocal.</li>
							<li><strong>Supportive Community</strong><br />
							Students study in a friendly, supportive online community.</li>
							<li><strong>Unlimited Access to the best Material</strong><br />
							Students get unlimited access to study material prepared by some of the best Teachers.</li>
                        </ul>
                    </div>
                    <div class="bad">
                    	<ul class="shortcoming">
                    		<li><strong>Offers little or no Interaction</strong><br />
							Traditional Study is often rigid, and is not seen as being "fun".</li>
							<li><strong>Difficult to Measure Progress</strong><br />
							Students often have no way of measuring their progress.</li>
							<li><strong>Offers No Immediate Reward</strong><br />
							The rewards of study are often long term. There is no instant gratification, which is important to a 21st century student.</li>
							<li><strong>Isolation for Long Periods </strong><br />
							Students who study in isolation don't enjoy the benefits from group interaction</li>
							<li><strong>Limited Access to Study Material</strong><br />
							Students often have little access to material not prepared by their own teachers. They don't benefit from new ways of teaching the same subject.</li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            
            <div id="sign_up" class="section">
            	<div class="left">
                	<img src="assets/img/sign_up.png" alt="Join EduFocal" />
                    <h1>EduFocal: Best in Class</h1>
                    <ul>
                    	<li>Video Game Progression</li>
                    	<li>Earn experience points and level up</li>
                    	<li>Tutor others and redeem cash</li>
                    	<li>Compete for the top of our leader boards</li>
                    </ul>
                </div>
                <div class="right">
                	<a href="<?php echo site_url('signup'); ?>"><img src="assets/img/btn/pricing_02.png" alt="Sign Up!" /></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="divider2"></div>
            <div id="subjects" class="section">
            	<h2>EduFocal is packed with amazing features and rich, interactive content from many subject areas.</h2>
                <p>Through prep tests, and timed tests, EduFocal helps students reinforce the topics they learn in class. Get unlimited access to content for a low annual fee and can earn CASH and FREE membership by becoming an EduFocal tutor.</p>
                <div class="left">
                	<h3>CXC Subjects:</h3>
                    <ul>
                    	<?php $outer_limit = 4;
						foreach($cxcs as $cxc) :
							if($outer_limit == 0) break; ?>
                        <li><img src="<?php echo site_url("assets/".$cxc->icon); ?>" alt="<?php echo $cxc->name; ?>" /><strong>
                        <!--<a href="<?php echo site_url('browse/cxc#'.$cxc->permalink); ?>">-->
						<?php echo $cxc->name; ?>
                        <!--</a>-->
                        </strong> (<?php echo count($cxc->topics); ?>)<br />
                        <span class="small">
                        	<?php $limit = 4;
                        	foreach($cxc->topics as $topic) : 
                        		if($limit == 0) break; ?>
                        	<!--<a href="<?php echo site_url('browse/cxc#'.$topic->permalink); ?>">-->
							<?php echo $topic->name; ?>
                            <!--</a>-->
                            , 
                        	<?php $limit--;
                        	endforeach; ?>
                        	<!--<a href="<?php echo site_url('browse/cxc#'.$cxc->permalink); ?>">-->
                            more...
                            <!--</a>-->
                        </span></li>
                        <?php $outer_limit--;
						endforeach; ?>
                    </ul>
                    <!--<p class="small"><a href="<?php echo site_url('browse/cxc'); ?>">Browse the CXC Subjects Library</a></p>-->
                </div>
                <div class="right">
                	<h3>GSAT Subjects:</h3>
                    <ul>
                        <?php $outer_limit = 4;
						foreach($gsats as $gsat) : 
							if($outer_limit == 0) break; ?>
                        <li><img src="<?php echo site_url("assets/".$gsat->icon); ?>" alt="<?php echo $gsat->name; ?>" /><strong>
                        <!--<a href="<?php echo site_url('browse/gsat#'.$gsat->permalink); ?>">-->
						<?php echo $gsat->name; ?>
                        <!--</a>-->
                        </strong> (<?php echo count($gsat->topics); ?>)<br />
                        <span class="small">
                        	<?php $limit = 4;
                        	foreach($gsat->topics as $topic) :
								if($limit == 0) break; ?>
                        	<!--<a href="<?php echo site_url('browse/gsat#'.$topic->permalink); ?>">-->
							<?php echo $topic->name; ?>
                            <!--</a>-->
                            , 
                        	<?php $limit--;
                        	endforeach; ?>
                        	<!--<a href="<?php echo site_url('browse/gsat#'.$gsat->permalink); ?>">-->
                            more...
                            <!--</a>-->
                        </span></li>
                        <?php $outer_limit--;
						endforeach; ?>
                    </ul>
                    <!--<p class="small"><a href="<?php echo site_url('browse/gsat'); ?>">Browse the GSAT Subjects Library</a></p>-->
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="divider2"></div>
            <div id="exams" class="section">
            	<h2>EduFocal is specially designed for CXC &amp; GSAT Students to get more from their learning experience.</h2>
                <div class="left">
                	<h1>CXC</h1>
                    <ul>
                    	<li>EduFocal offers modules in x Subject Areas</li>
						<li>Students get unlimited access to CXC Study Material</li>
						<li>Students compete for the top spot on the EduFocal LeaderBoard</li>
                    </ul>
                </div>
                <div class="right">
                	<h1>GSAT</h1>
                    <ul>
                    	<li>Students benefit from friendly group interaction with their peers from other schools.</li>
						<li>EduFocal helps students with subjects that give them trouble.</li>
						<li>Students earn incentives by being part of the community</li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="sign_up_2" class="section">
            	<img src="assets/img/sign_up.png" alt="Join EduFocal" class="cert" />
                <h1>Study your way!</h1>
                <div class="left">
                    <ul>
                    	<li>Over 40* Subject Areas</li>
                    	<li>Compete against your peers</li>
                    	<li>Earn Exciting Rewards</li>
                    	<li>Get Unlimited 24 hour Access</li>
                    </ul>
                </div>
                <div class="right">
                    <a href="<?php echo site_url('signup'); ?>" class="button"><img src="assets/img/btn/pricing_02.png" alt="Sign Up!" /></a>
                    <p>From the comfort of home or school for one Low Annual Fee.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        
        </div>
        <div id="column_2">
            <iframe width="306" height="172" src="//www.youtube.com/embed/4oZo3OtCPrA" frameborder="0" allowfullscreen></iframe>
            <div id="testimonial" class="section">
            	<h3>What Others Are Saying</h3>
                <ul>
                	<?php $this->load->view('partials/testimonials'); ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="divider3"></div>
            <div class="section">
            	<h3>Our Top Performers</h3>
                <p>Check out the people who are shaking things up at EduFocal!</p>
                <div class="tabs" id="top_student">
                	<ul class="tab">
                    	<li class="active"><a href="#top-gsat-students" class="left">Top GSAT Students</a></li>
                        <li><a href="#top-csec-students" class="right">Top CSEC Students</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div id="top-gsat-students" class="tab_body activated">
                    	<ol class="standings">
                        	<?php $i = 0;
							foreach($top_gsat_students as $student) :
								$i++; ?>
                            <li>
                                <div class="standing"><?php echo $i; ?></div>
                                <div class="user">
                                <?php $avatar = $student->user->avatar;
								if(empty($avatar)) : ?>
									<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $student->user->first_name.' '.$student->user->last_name; ?>" class="img" />
								<?php else : ?>
									<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $student->user->first_name.' '.$student->user->last_name; ?>" class="img" />
								<?php endif; ?>
                                <p><strong><?php echo $student->user->first_name.' '.$student->user->last_name; ?></strong><br /><?php echo round($student->score); ?> Points</p></div>
                                <div class="clearfix"></div>
                            </li>
                            <?php endforeach; ?>
                        </ol>
                        <div class="clearfix"></div>
                        <p class="padded">&nbsp;</p>
                    </div>

                    <div id="top-csec-students" class="tab_body">
                    	<ol class="standings">
                        	<?php $i = 0;
							foreach($top_cxc_students as $student) :
								$i++; ?>
                            <li>
                                <div class="standing"><?php echo $i; ?></div>
                                <div class="user">
                                <?php $avatar = $student->user->avatar;
								if(empty($avatar)) : ?>
									<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $student->user->first_name.' '.$student->user->last_name; ?>" class="img" />
								<?php else : ?>
									<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $student->user->first_name.' '.$student->user->last_name; ?>" class="img" />
								<?php endif; ?>
                                <p><strong><?php echo $student->user->first_name.' '.$student->user->last_name; ?></strong><br /><?php echo round($student->score); ?> Points</p></div>
                                <div class="clearfix"></div>
                            </li>
                            <?php endforeach; ?>
                        </ol>
                        <div class="clearfix"></div>
                        <p class="padded">&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    
