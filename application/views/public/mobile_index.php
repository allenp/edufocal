<?php if(! $this->auth->is_logged_in()) : ?>
<div id="login" style="display: block; ">
    <?php print form_open(site_url(array('dashboard')), array('id'=>'global_login')); ?>
        <div class="control-group">
            <label for="login_email">Email Address</label>
            <input size="30" type="text" name="email" id="login_email" />
        </div>
        <div class="control-group">
            <label for="login_password">Password</label>
            <input size="30" type="password" name="password" id="login_password" />
        </div>
        <div class="control-group">
            <button type="submit" name="submit">Sign In</button>
        </div>
        <div class="clearfix"></div>
        <p><a href="<?php site_url(array('forgot_password')) ?>">Forgot Password?</a></p> 
        <div class="clearfix"></div>
        <?php print form_close(); ?>
</div>
<?php else : ?>
<ul class="menu">
<li><a href="<?php print site_url(array('codes')) ?>">Sell Code</a></li>
</ul>
<?php endif; ?>
