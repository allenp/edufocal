<?php

class Migration_Referrals_Limit_Fix extends Migration
{
	public function up ()
	{	
		$prefix = $this->db->dbprefix;
		$fields = array(
			'type' => array('type' => 'TINYINT'),
			'status' => array('type' => 'TINYINT','default' => 0),
			'limit' => array('type' => 'INT'),
		);
		$this->dbforge->add_column($prefix.'contests',$fields);		
	}
	
	public function down ()
	{
		$prefix = $this->db->dbprefix;
		
		$this->dbforge->drop_column($prefix.'contests','type');
		$this->dbforge->drop_column($prefix.'contests','status');
		$this->dbforge->drop_column($prefix.'contests','limit');
	}
}

?>

