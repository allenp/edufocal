<?php

class Migration_Install_User_Referrals extends Migration
{
	public function up ()
	{	
		$prefix = $this->db->dbprefix;
		$prefix = '';
		
		//$this->db->query("DROP TABLE IF EXISTS `{$prefix}user_referrals`;");
		
        $this->db->query("CREATE TABLE IF NOT EXISTS `user_referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `referred_user_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_ip` varchar(50) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;");
		
	}
	
	public function down ()
	{
		$prefix = $this->db->dbprefix;
		$prefix = '';
		
		$this->db->query("DROP TABLE IF EXISTS {$prefix}user_referrals");
	}
}

?>
