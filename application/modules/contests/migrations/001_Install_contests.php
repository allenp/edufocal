<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_contests extends Migration {
	
	public function up() 
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->add_field('`id` int(11) NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('`name` VARCHAR(255) NOT NULL');
		$this->dbforge->add_field('`permalink` VARCHAR(255) NOT NULL');
		$this->dbforge->add_field('`sponsor` VARCHAR(255) NOT NULL');
		$this->dbforge->add_field('`description` TEXT NOT NULL');
		$this->dbforge->add_field('`start_date` DATE NOT NULL');
		$this->dbforge->add_field('`end_date` DATE NOT NULL');
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('contests');

		// permissions
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Content.View','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Content.Create','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Content.Edit','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Content.Delete','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Reports.View','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Reports.Create','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Reports.Edit','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Contests.Reports.Delete','','active');");

	}
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('contests');
		// permissions
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Content.View';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Content.View';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Content.Create';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Content.Create';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Content.Edit';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Content.Edit';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Content.Delete';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Content.Delete';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Reports.View';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Reports.View';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Reports.Create';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Reports.Create';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Reports.Edit';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Reports.Edit';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Contests.Reports.Delete';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Contests.Reports.Delete';");
	}
	
	//--------------------------------------------------------------------
	
}