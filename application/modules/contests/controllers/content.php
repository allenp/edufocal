<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller {
               
	function __construct()
	{
 		parent::__construct();

		$this->auth->restrict('Contests.Content.View');
		$this->load->model('contests_model');
		$this->lang->load('contests');
		
		Assets::add_js($this->load->view('content/js', null, true), 'inline');
		Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
		Assets::add_js('jquery-ui-1.8.8.min.js');
	}
	
	
	/** 
	 * function index
	 *
	 * list form data
	 */
	public function index()
	{
		$records = $this->contests_model->find_all();

		Template::set_view("content/index");
		Template::set('records', $records);
		Template::set("toolbar_title", "Manage Contests");
		Template::render();
	}
	
	//--------------------------------------------------------------------
	
	
	public function create() 
	{
		$this->auth->restrict('Contests.Content.Create');

		if ($this->input->post('submit'))
		{
			if ($this->save_contests())
			{
				Template::set_message(lang("contests_create_success"), 'success');
				Template::redirect('admin/content/contests');
			}
			else 
			{
				Template::set_message(lang("contests_create_failure") . $this->contests_model->error, 'error');
			}
		}
	
		Template::set_view('content/create');
		Template::render();
	}
	//--------------------------------------------------------------------
	
	
	public function edit() 
	{
		$this->auth->restrict('Contests.Content.Edit');

		$id = (int)$this->uri->segment(5);
		
		if (empty($id))
		{
			Template::set_message(lang("contests_invalid_id"), 'error');
			redirect('admin/content/contests');
		}
	
		if ($this->input->post('submit'))
		{
			if ($this->save_contests('update', $id))
			{
				Template::set_message(lang("contests_edit_success"), 'success');
			}
			else 
			{
				Template::set_message(lang("contests_edit_failure") . $this->contests_model->error, 'error');
			}
		}
		
		Template::set('contests', $this->contests_model->find($id));
	
		Template::set('toolbar_title', lang("contests_edit_heading"));
		Template::set_view('content/edit');
		Template::set("toolbar_title", "Edit Contests");
		Template::render();		
	}
	
			
	public function delete() 
	{	
		$this->auth->restrict('Contests.Content.Delete');

		$id = $this->uri->segment(5);
	
		if (!empty($id))
		{	
			if ($this->contests_model->delete($id))
			{
				Template::set_message(lang("contests_delete_success"), 'success');
			} else
			{
				Template::set_message(lang("contests_delete_failure") . $this->contests_model->error, 'error');
			}
		}
		
		redirect('/admin/content/contests');
	}
		
	public function save_contests($type='insert', $id=0) 
	{	
			
		$this->form_validation->set_rules('name','Name','required|trim|max_length[255]');			
		$this->form_validation->set_rules('permalink','Slug','required|max_length[255]');			
		$this->form_validation->set_rules('sponsor','Sponsor','required|max_length[255]');			
		$this->form_validation->set_rules('description','Description','required|trim|xss_clean');			
		$this->form_validation->set_rules('start_date','Start Date','required');			
		$this->form_validation->set_rules('end_date','End Date','required');
		$this->form_validation->set_rules('type','Type','required|integer');
		$this->form_validation->set_rules('status','Status','required|integer');
		$this->form_validation->set_rules('limit','Limit','required|integer');

		if ($this->form_validation->run() === false)
		{
			return false;
		}
		
		$config['upload_path'] = './uploads/contests/';
		$config['allowed_types'] = 'gif|jpg|png';
		
		$this->load->library('upload', $config);
		if( $this->upload->do_upload('contest_image'))
		{
			$upload = $this->upload->data();
			
			$resize['source_image']	= $upload['full_path'];
			$resize['maintain_ratio'] = TRUE;
			$resize['width']	 = 206;
			$resize['height']	= 206;
			
			$this->load->library('image_lib', $resize); 
			
			$this->image_lib->resize();
			
			$_POST['image'] = $upload['file_name'];
		}

        $fields = array();
        $fields['name'] = $this->input->post('name');
        $fields['permalink'] = $this->input->post('permalink');
        $fields['sponsor'] = $this->input->post('sponsor');
        $fields['description'] = $this->input->post('description');
        $fields['start_date'] = $this->input->post('start_date');
        $fields['end_date'] = $this->input->post('end_date');
        $fields['type'] = $this->input->post('type');
        $fields['status'] = $this->input->post('status');
        $fields['limit'] = $this->input->post('limit');
	 
        if(isset($_POST['image']))
            $fields['image'] = $this->input->post('image');
		
		if ($type == 'insert')
		{	
			$id = $this->contests_model->insert($fields);
			
			if (is_numeric($id))
			{
				$return = true;
			} else
			{
				$return = false;
			}
		}
		else if ($type == 'update')
		{
			$return = $this->contests_model->update($id, $fields);
		}
		
		return $return;
	}

}
