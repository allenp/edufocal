<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contests extends EF_Dashboard_Controller {
               
	function __construct()
	{
 		parent::__construct();

		$this->load->library('form_validation');
		$this->load->database();
		$this->load->model('contests_model');
		$this->lang->load('contests');
		$this->load->helper('typography');
		
		Template::set('body_id', 'referral');
		
		Assets::add_js($this->load->view('contests/js', null, true), 'inline');
		
		Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
		Assets::add_js('jquery-ui-1.8.8.min.js');
	}
	
	
	/** 
	 * function index
	 *
	 * list form data
	 */
	public function index()
	{
		if($this->uri->segment(1) != 'admin') redirect('admin/content/contests');
		
		$data = array();
		$data["records"] = $this->contests_model->find_all();

		Template::set_view("contests/index");
		Template::set("data", $data);
		Template::set("toolbar_title", "Manage Contests");
		Template::render();
	}
	
	/**
	 * Single page of a contest. 
	 *
	 * @param string $id ID of the contest.
	 * @param string $permalink Permalink of the contest.
	 * @author Jamie Chung
	 */
	public function single ( $id, $permalink )
	{
		$contest = Contest::find_by_id_and_permalink($id, $permalink);
		if ( !$contest )
		{
			redirect('contests');
		}
		
		$users = User::find_by_sql("
			SELECT u.*,
			(
				SELECT COUNT(*)
				FROM user_referrals ur 
				WHERE ur.user_id = u.id
				AND ur.status = 'completed'
				AND ur.contest_id = ?
			) AS referrals
			FROM users u
			LEFT JOIN 
				user_referrals ur 
				ON ( ur.user_id = u.id )
			WHERE ur.contest_id = ? 
			AND (
				SELECT COUNT(*)
				FROM user_referrals ur 
				WHERE ur.user_id = u.id
				AND ur.status = 'completed'
			) > 0
			AND u.role_id = 8
			GROUP BY ur.user_id
			ORDER BY referrals DESC
		", array($contest->id, $contest->id));
		
		$rank = 0;
		$prev = 0;
		$referrals = 0;
		foreach($users as $user) {
			if($user->referrals != $prev) {
				$rank++;
				$prev = $user->referrals;
			}
			if($user->id == $this->auth->user_id()) {
				$referrals = $user->referrals;
				break;
			}
		}
		if($referrals == 0) $rank = 0;
		
		Template::set('rank', $rank);
		Template::set('referrals', $referrals);
		Template::set('users', $users);
		Template::set('title', $contest->name .' - EduFocal');
		Template::set('contest', $contest);
		Template::render();
	}
	
	//--------------------------------------------------------------------
	
	
	public function create() 
	{
		if($this->uri->segment(1) != 'admin') redirect('admin/content/contests/create');
		
		$this->auth->restrict('Contests.Contests.Create');

		if ($this->input->post('submit'))
		{
			if ($this->save_contests())
			{
				Template::set_message(lang("contests_create_success"), 'success');
				Template::redirect('/admin/contests/contests');
			}
			else 
			{
				Template::set_message(lang("contests_create_failure") . $this->contests_model->error, 'error');
			}
		}
	
		Template::set('toolbar_title', lang("contests_create_new_button"));
		Template::set_view('contests/create');
		Template::set("toolbar_title", "Create Contests");
		Template::render();
	}
	//--------------------------------------------------------------------
	
	
	public function edit() 
	{
		if($this->uri->segment(1) != 'admin') redirect('admin/content/contests/edit');
		
		$this->auth->restrict('Contests.Contests.Edit');

		$id = (int)$this->uri->segment(5);
		
		if (empty($id))
		{
			Template::set_message(lang("contests_invalid_id"), 'error');
			redirect('/admin/contests/contests');
		}
	
		if ($this->input->post('submit'))
		{
			if ($this->save_contests('update', $id))
			{
				Template::set_message(lang("contests_edit_success"), 'success');
			}
			else 
			{
				Template::set_message(lang("contests_edit_failure") . $this->contests_model->error, 'error');
			}
		}
		
		Template::set('contests', $this->contests_model->find($id));
	
		Template::set('toolbar_title', lang("contests_edit_heading"));
		Template::set_view('contests/edit');
		Template::set("toolbar_title", "Edit Contests");
		Template::render();		
	}
	
			
	public function delete() 
	{	
		if($this->uri->segment(1) != 'admin') redirect('admin/content/contests/delete');
		
		$this->auth->restrict('Contests.Contests.Delete');

		$id = $this->uri->segment(5);
	
		if (!empty($id))
		{	
			if ($this->contests_model->delete($id))
			{
				Template::set_message(lang("contests_delete_success"), 'success');
			} else
			{
				Template::set_message(lang("contests_delete_failure") . $this->contests_model->error, 'error');
			}
		}
		
		redirect('/admin/contests/contests');
	}
		
	public function save_contests($type='insert', $id=0) 
	{	
			
		$this->form_validation->set_rules('name','Name','required|trim|max_length[255]');			
		$this->form_validation->set_rules('permalink','Slug','required|max_length[255]');			
		$this->form_validation->set_rules('sponsor','Sponsor','required|max_length[255]');			
		$this->form_validation->set_rules('description','Description','required|trim|xss_clean');			
		$this->form_validation->set_rules('start_date','Start Date','required');			
		$this->form_validation->set_rules('end_date','End Date','required');
		if ($this->form_validation->run() === false)
		{
			return false;
		}
		
		if ($type == 'insert')
		{
			$id = $this->contests_model->insert($_POST);
			
			if (is_numeric($id))
			{
				$return = true;
			} else
			{
				$return = false;
			}
		}
		else if ($type == 'update')
		{
			$return = $this->contests_model->update($id, $_POST);
		}
		
		return $return;
	}

}
