<div id="main">
	<h1><?php echo $contest->name; ?></h1>
    
    <div class="contest">
    	<img src="<?php echo $contest->image(); ?>" alt="<?php echo $contest->name; ?>" class="prize" width="206" height="206" />
        <div class="details">

            <?php echo auto_typography($contest->description); ?>
            <h4>Sponsored by: <span class="grey"><?php echo $contest->sponsor; ?></span></h4>
            <h4>Ends <?php echo $contest->end_date->format('M j, Y'); ?> <span class="grey">(<?php echo $contest->days_remaining(); ?> left)</span></h4>
        </div>
        <div class="code">
        	<h4 class="grey">Your Referral Code:</h4>
            <input type="text" onclick="this.select();" value="<?php echo site_url('r/'.$this->auth->user_id().'/'.$contest->permalink); ?>" size="40" />
            <h4>Your referrals for this contest: <span class="grey"><?php echo $referrals; ?></span></h4>
            <h4>Leaderboard position: <span class="grey"><?php if($rank == 0) echo 'N/A';
			else echo $rank; ?></span></h4>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <table width="100%" cellspacing="0" cellpadding="8" border="0" class="styled">
		<?php $rank = 0;
		$prev = 0;
		foreach ( $users as $k => $user ) :
			if($user->referrals != $prev) {
				$rank++;
				$prev = $user->referrals;
			} ?>
        <tr>
            <td width="10%"><h2><sup>#</sup><?php echo $rank; ?></h2></td>
            <td width="41">
				<?php $avatar = $user->avatar;
				if(empty($avatar)) : ?>
					<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" width="40" height="40" />
				<?php else : ?>
					<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" width="40" height="40" />
				<?php endif; ?>
			</td>
            <td>
                <a href="<?php echo site_url("dashboard/profile/view/".$user->id); ?>"><?php echo $user->public_name(); ?></a>
            </td>
            <td width="200"><!--<div class="bar_graph"><div class="bar_yellow" style="width: 131px;"></div></div>--></td>
            <td width="14%"><!--0000000 Points--></td>
            <td width="5%" class="tutor"><!--<img src="<?php echo site_url('assets/dbrd/img/star.png'); ?>" alt="Tutor" /><br />Tutor--></td>
            <td><h2><?php echo $user->referrals; ?> <small>Referral(s)</small></h2></td>
            <td width="5%"></td>
        </tr>
		<?php endforeach	; ?>
        <tr class="heading">
            <td colspan="3"><strong>Showing 1 - <?php echo count($users); ?> Student(s)</strong></td>
            <td colspan="5">
            	<!-- <ul>
                            	<li>1</li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next</a></li>
                            </ul> -->
            </td>
        </tr>
    </table>
</div>