
<div class="view split-view">
	
	<!-- Role List -->
	<div class="view">
	
	<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<div class="scrollable">
			<div class="list-view" id="role-list">
				<?php foreach ($records as $record) : ?>
					<?php $record = (array)$record;?>
					<div class="list-item" data-id="<?php echo $record['id']; ?>">
						<p>
							<b><?php echo $record['name']; ?></b>
						</p>
					</div>
				<?php endforeach; ?>
			</div>	<!-- /list-view -->
		</div>
	
	<?php else: ?>
	
	<div class="notification attention">
		<p><?php echo lang('contests_no_records'); ?> <?php echo anchor('admin/content/contests/create', lang('contests_create_new'), array("class" => "ajaxify")) ?></p>
	</div>
	
	<?php endif; ?>
	</div>
	<!-- Role Editor -->
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
				
			<div class="box create rounded">
				<a class="button good ajaxify" href="<?php echo site_url('/admin/content/contests/create')?>"><?php echo lang('contests_create_new_button');?></a>

				<h3><?php echo lang('contests_create_new');?></h3>

				<p><?php echo lang('contests_edit_text'); ?></p>
			</div>
			<br />
				<?php if (isset($records) && is_array($records) && count($records)) : ?>
				
					<h2>Contests</h2>
	<table>
		<thead>
		<th>Name</th>
		<th>Slug</th>
		<th>Sponsor</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Type</th>
		<th>Status</th>
		<th>Limit</th>
        <th><?php echo lang('contests_actions'); ?></th>
		</thead>
		<tbody>
<?php
foreach ($records as $record) : ?>
<?php $record = (array)$record;?>
			<tr>
<?php
	foreach($record as $field => $value)
	{
		if($field != "id" && $field != 'image' && $field != 'description' && $field != 'type' && $field != 'status') {
?>
				<td><?php echo $value;?></td>

<?php
		} else if($field=='type') {
			if($value==0) echo '<td>Other</td>';
			else if($value==1) echo '<td>Referral</td>';
		} else if($field=='status') {
			if($value==0) echo '<td>Inactive</td>';
			else if($value==1) echo '<td>Active</td>';
		}
	}
?>
				<td><?php echo anchor('admin/content/contests/edit/'. $record['id'], 'Edit', 'class="ajaxify"') ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
				<?php endif; ?>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
