
<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; ?>
<?php // Change the css classes to suit your needs    
if( isset($contests) ) {
	$contests = (array)$contests;
}
$id = isset($contests['id']) ? "/".$contests['id'] : '';
?>
<?php echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<div>
        <?php echo form_label('Name', 'name'); ?> <span class="required">*</span>
        <input id="name" type="text" name="name" maxlength="255" value="<?php echo set_value('name', isset($contests['name']) ? $contests['name'] : ''); ?>"  />
</div>

<div>
        <?php echo form_label('Slug', 'permalink'); ?> <span class="required">*</span>
        <input id="permalink" type="text" name="permalink" maxlength="255" value="<?php echo set_value('permalink', isset($contests['permalink']) ? $contests['permalink'] : ''); ?>"  />
</div>

<div>
        <?php echo form_label('Sponsor', 'sponsor'); ?> <span class="required">*</span>
        <input id="sponsor" type="text" name="sponsor" maxlength="255" value="<?php echo set_value('sponsor', isset($contests['sponsor']) ? $contests['sponsor'] : ''); ?>"  />
</div>

<div>
        <?php echo form_label('Description', 'description'); ?> <span class="required">*</span>
	<?php echo form_textarea( array( 'name' => 'description', 'id' => 'description', 'rows' => '5', 'cols' => '80', 'value' => set_value('description', isset($contests['description']) ? $contests['description'] : '') ) )?>
</div>
<div>
        <?php echo form_label('Start Date', 'start_date'); ?> <span class="required">*</span>
			<script>$(function() {$('#start_date').datepicker({ dateFormat: 'yy-mm-dd'});})</script>
        <input id="start_date" type="text" name="start_date"  value="<?php echo set_value('start_date', isset($contests['start_date']) ? $contests['start_date'] : ''); ?>"  />
</div>

<div>
        <?php echo form_label('End Date', 'end_date'); ?> <span class="required">*</span>
			<script>$(function() {$('#end_date').datepicker({ dateFormat: 'yy-mm-dd'});})</script>
        <input id="end_date" type="text" name="end_date"  value="<?php echo set_value('end_date', isset($contests['end_date']) ? $contests['end_date'] : ''); ?>"  />
</div>



	<div class="text-right">
		<br/>
		<input type="submit" name="submit" value="Edit Contests" /> or <?php echo anchor('admin/reports/contests', lang('contests_cancel')); ?>
	</div>
	<?php echo form_close(); ?>

	<div class="box delete rounded">
		<a class="button" id="delete-me" href="<?php echo site_url('admin/reports/contests/delete/'. $id); ?>" onclick="return confirm('<?php echo lang('contests_delete_confirm'); ?>')"><?php echo lang('contests_delete_record'); ?></a>
		
		<h3><?php echo lang('contests_delete_record'); ?></h3>
		
		<p><?php echo lang('contests_edit_text'); ?></p>
	</div>
