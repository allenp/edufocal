$(function() {
    $form = $('#seenForm');
    var options = {
        url: $form.attr('action') + '?_=' + new Date().getTime(),
        type: 'POST',
        cache: false,
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data.seen_id) {
                $('input[name=seen_id]').val(data.seen_id);
            }
        }
    };

    var playerOpts = {
        success: function(media, node, player) {
          var events = ['pause', 'ended'];
          for (var i=0, il=events.length; i<il; i++) {
              media.addEventListener(events[i], function(e) {
                  $form.ajaxSubmit(options);
              }, false);
          }
        }
    };

    player = new MediaElementPlayer('#player', playerOpts);
    if(null !== player) {
        player.play();
    }

    $form.ajaxSubmit(options);
});
