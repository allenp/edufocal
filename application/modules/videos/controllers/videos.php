<?php

class Videos extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();
		Assets::add_module_js('videos', 'video');
	}

	public function index($subject, $topic, $title)
	{
		$sub = Subject::find_by_permalink_and_level(
			$subject, $this->auth->profile()->testing_level());

		if (null == $sub) {
			redirect('dashboard');
		}

		$top = Topic::find_by_subject_id_and_permalink(
			$sub->id, $topic);

		if (null == $top) {
			redirect('dashboard');
		}

		$videos = Video::all(
			['conditions' => ['topic_id = ?', $top->id],
			 'order' => 'sort_order asc',
			]);

		if (count($videos) == 0) {
			redirect('dashboard');
		}

		$v = [];
		foreach ($videos as $video) {
			$v[$video->permalink] = $video;
		}

		Template::set('videos', $v);
		Template::set('subject', $sub);
		Template::set('topic', $top);
		Template::set('selected', $title);
		Template::set('title', 'Introduction to Fractions - EduFocal');
		Template::render();
	}

	public function seen($id)
	{
		$seen_id = $this->input->post('seen_id');

		if ($seen_id) {
			$seen = VideoView::find($seen_id);
			if (null !== $seen) {
				$seen->ended = new \ActiveRecord\DateTime();
				$seen->seen = 1;
				$seen->save();
				echo 1;
			} else {
				echo 0;
			}
			exit;
		} else {
			$seen = VideoView::create([
				'video_id' => $id,
				'user_id' => $this->auth->user_id(),
				'started' => new \ActiveRecord\DateTime(),
				]);

			echo json_encode(['seen_id' => $seen->id]);
			exit;
		}
	}
}
