<?php
  Assets::add_js(array(
      site_url('assets/js/jquery.form.js'),
  ));
?>
<div class="chooser row">
	<div class="heading">
		<div class="back_button hidden-xs">
		<a href="<?php print by_subject_url($subject, $topic) ?>">
				<img src="http://ef.paulallen.org/assets/dbrd/img/back_btn.png" alt="Back" width="53" height="24">
			</a>
		</div>
		<ul class="breadcrumb list-unstyled">
			<li><a href="<?php print by_subject_url($subject) ?>"><?php print $subject->name ?></a></li>
			<li><a href="<?php print by_subject_url($subject, $topic) ?>"><?php print $topic->name ?></a></li>
			<li><?php print $videos[$selected]->title ?></li>
		</ul>
	</div>
	<div class="container">
		<div class="row">
		<?php print form_open(site_url(['videos', 'seen', $videos[$selected]->id]), ['id' => 'seenForm']) ?>
		<?php print form_hidden('seen_id', '') ?>
		<?php print form_close() ?>
		<div class="video-wrap col-md-8 col-xs-12 pull-right">
			<video id="player" preload="none" width="640" height="360" style="width: 100
%; height: 100%">
			<?php if(count($videos[$selected]->sources()) > 0): ?>
			<?php foreach($videos[$selected]->sources() as $source) : ?>
<source src="<?php print $source->url() ?>" media="all"/>
			<?php endforeach ?>
			<?php endif ?>
				<object width="640px" height="360px" type="application/x-shockwave-flash" data="<?php print site_url() ?>/public/assets/mediaelement/flashmediaelement.swf">
					<param name="movie" value="flashmediaelement.swf" />
					<param name="flashvars" value="controls=true&file=<?php print $videos[$selected]->video_sources[0]->url() ?>" />
					<!-- Image as a last resort -->
					<!--<img src="myvideo.jpg" width="640px" height="360px" title="No video playback capabilities" />-->
				</object>
			</video>
			<h2><?php print $videos[$selected]->title ?></h2>
			<p>
			<?php print $videos[$selected]->description ?>
			</p>
		</div>
		<div class="col-xs-12 col-md-4 pull-left">
			<ul class="menu list-unstyled listing clearfix video-more row">
			<?php if(count($videos) > 0) : ?>
			<li><h4 class="video-more-title"><?php print $topic->name ?></h4></li>
				<?php foreach($videos as $key => $video): ?>
				<li class="clearfix <?php print $key == $selected ? 'active' : ''?>">
					<a class="" href="<?php print site_url('dashboard/videos') .'/'. $subject->permalink .'/'.$topic->permalink .'/'.$video->permalink?>">
					<span class="glyphicon glyphicon-film"></span>
					<?php print $video->title ?>
					</a>
				</li>
				<?php endforeach ?>
			<?php endif; ?>
			</ul>
		</div>
		</div>
	</div>
</div>
