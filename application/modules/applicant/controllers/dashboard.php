<?php
/**
 * Student Dashboard
 *
 * @package EduFocal
 */
class Dashboard extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();

		$this->load->helper('text');
		$this->load->helper('typography');
	}

	/*
	 * Basic overview of Students Progress and Messages
	 */
	public function index ()
	{
        $unread = MessageUser::count(
            array('conditions' => array(
                'user_id = ? and `read` = ? and `deleted` = ?',
                $this->auth->user_id(),'no', 'no')
            )
        );

		$message_ids = array();
		$status = array();
		$mUser = MessageUser::find_all_by_user_id_and_deleted($this->auth->user_id(), 'no');
		foreach ( $mUser as $m ) {
			$status[$m->message_id] = $m->read;
			$message_ids[] = $m->message_id;
		}

		$messages = array();
		if (count($message_ids ) > 0) {
            $messages = Message::find_all_by_id(
                $message_ids,
                array('limit' => 5, 'order' => 'last_stamp desc')
            );
        }

		Template::set('status', $status);
		Template::set('unread', $unread);
		Template::set('messages', $messages);
		Template::set('title', 'Dashboard - EduFocal');
        Template::set_view('applicant/index');
		Template::render();
	}
}
