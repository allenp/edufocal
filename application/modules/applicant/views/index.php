<?php if ( $this->settings_lib->item('site.dashboard.message_status') == 1 ) : ?>
<div class="alert alert-dismissable alert-msg alert-dashboard">
    <a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
    <h4><?php echo $this->settings_lib->item('site.dashboard.message'); ?></h4>
</div>
<?php endif; ?>
<?php print Template::message(); ?>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-7 col-md-6 greeting">
            <h4 class="flush-top">Hello, <?php echo $this->auth->profile()->first_name; ?></h4>
            <p>Please attempt the courses below at least once. We will contact you with further information if necessary.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-5 col-md-6 callout">
            <a id="report" href="<?php print site_url('dashboard/reports')?>" title="My Report Card">My Report Card</a>
        </div>
    </div>
</div>
<div class="col-xs-12">
<table class="table table-bordered">
<thead>
    <tr><th>Course</th><th colspan="2">Status</tr>
</thead>
<tbody>
    <tr><td>Mathematics</td><td>Your enrollment request is awaiting approval</td></tr>
</tbody>
</table>
</div>
