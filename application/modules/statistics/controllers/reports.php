<?php

class Reports extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->_active();
        $this->_tests();

        $questions = $this->db->query(
            "SELECT count(*) total, status from questions group by status"
        );
        Template::set('questions', $questions);

        $questions_per_subject = $this->db->query(
                "select subjects.name, subjects.level, count(*) total
                from questions, topics, subjects
                where questions.topic_id = topics.id and topics.subject_id = subjects.id
                group by subjects.name, subjects.level
                order by subjects.level, count(*)"
                );
        Template::set('questions_per_subject', $questions_per_subject);

        Template::set('cards', Card::all());
        Template::render();
    }


    /*
     * Total active users per month
     */
    private function _active()
    {
        $users = $this->db->query(
            "select count(distinct user_id) total, date_format(from_unixtime(started), '%Y-%M') active_month
            from tests inner join users on tests.user_id = users.id
            where users.role_id = 8
            group by date_format(from_unixtime(started), '%Y-%M')
            order by started asc"
        );

        $active = array();
        foreach ($users->result() as $row) {
            $active[$row->active_month] = $row->total;
        }

        $js = $this->load->view('reports/active-js', compact('active'), true);
        Assets::add_js($js, 'inline');

        $query = "select count(distinct user_id) total
            from tests inner join users on tests.user_id = users.id
            where users.role_id = 8
            and from_unixtime(started) >= DATE_ADD(CURDATE(), INTERVAL ? DAY)";


        $days90 = $this->db->query($query, array(-90));
        $days30 = $this->db->query($query, array(-30));
        $days7 = $this->db->query($query, array(-7));

        Template::set('days90', $days90->row()->total);
        Template::set('days30', $days30->row()->total);
        Template::set('days7', $days7->row()->total);
    }

    private function _tests()
    {

        $total = $this->db->query("SELECT count(*) total from tests");
        Template::set('total_tests', $total->row()->total);

        $avg_test = $this->db->query(
            "SELECT subject_id, subjects.name, subjects.level, AVG( percentage ) avg_percent
            FROM  `tests`
            INNER JOIN subjects ON tests.subject_id = subjects.id
            WHERE marked =1
            GROUP BY subject_id, subjects.name
            ORDER BY level, AVG( percentage ) DESC"
        );

        Template::set('avg_test_scores', $avg_test);
        $tests = $this->db->query(
            "SELECT count(*) total, date_format(from_unixtime(started), '%Y-%M') test_month
            FROM `tests` inner join users on users.id = tests.user_id
            where users.role_id = 8
            group by date_format(from_unixtime(started), '%Y-%M')
            order by started asc"
        );

        $started = array();
        foreach ($tests->result() as $row) {
            $started[$row->test_month] = $row->total;
        }

        $js = $this->load->view('reports/tests-js', compact('started'), true);
        Assets::add_js($js, 'inline');
    }
}
