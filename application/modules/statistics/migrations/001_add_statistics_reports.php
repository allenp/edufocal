<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_statistics_reports extends Migration {
	
	public function up() 
	{
        $this->db->query("INSERT INTO `permissions` (
            `permission_id` ,
            `name` ,
            `description` ,
            `status`
        )
        VALUES (
            NULL ,  'Statistics.Reports.View',  'Allow users to view the statistics reports',  'active'
        );");
	}
	
	public function down() 
	{
        $this->db->query("DELETE FROM permissions WHERE name = 'Statistics.Reports.View'");
	}
}
