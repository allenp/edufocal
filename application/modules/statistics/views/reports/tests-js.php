    $(function() {
        var ctx = document.getElementById("testChart").getContext("2d");
        var data = {
            labels : <?php print json_encode(array_keys($started)) ?>,
                datasets: [
                {
                    label: "Tests taken",
                        fillColor: "rgba(220, 220, 220, 0.2)",
                        strokeColor: "rgba(220, 220, 220, 1)",
                        pointColor: "rgba(220, 220, 220, 1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220, 220, 220, 1)",
                        data: <?php print json_encode(array_values($started)) ?>
                }
            ]
        };

        var options = {
            scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 20,
                datasetStroke : true,
                datasetStrokeWidth : 2,
                datasetFill : true,
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

        }; 

        var newTestChart = new Chart(ctx).Line(data, options);
    });
