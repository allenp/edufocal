<?php
  Assets::add_js(
      array('Chart.min.js')
  );
?>
<div class="row-fluid">
    <h2>Active users per month</h2>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>90 days</th><th>30 days</th><th>7 days</th>
            </tr>
        </thead>
        <tbody>
            <td><?php print number_format($days90) ?></td>
            <td><?php print number_format($days30) ?></td>
            <td><?php print number_format($days7) ?></td>
        </tbody>
    </table>
    <canvas id="activeChart" width="1020" height="400"></canvas>
</div>
<div class="row-fluid">
    <h2>Tests taken per month</h2>
    <table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Total tests taken</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php print number_format($total_tests) ?></td>
        </tr>
    </tbody>
    </table>
    <canvas id="testChart" width="1020" height="400"></canvas>
</div>
<div class="row-fluid">
    <h2>Average test scores per subject</h2>
    <table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Subject</th>
            <th>Level</th>
            <th>Average percentage score</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($avg_test_scores->result() as $row) : ?>
        <tr>
            <td><?php print $row->name ?></td>
            <td><?php print $row->level ?></td>
            <td><?php print round($row->avg_percent, 2) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
</div>
<div class="row-fluid">
    <h2>Questions</h2>
    <table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Status</th>
            <th>Total</th>
        <tr>
    </thead>
    <tbody>
    <?php foreach($questions->result() as $row) : ?>
        <tr>
            <td><?php print $row->status ?></td>
            <td><?php print number_format($row->total) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>

    <h2>Questions Per Subject</h2>
    <table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Subject</th>
            <th>Level</th>
            <th>Total</th>
        <tr>
    </thead>
    <tbody>
    <?php foreach($questions_per_subject->result() as $row) : ?>
        <tr>
            <td><?php print trim($row->name) ?></td>
            <td><?php print $row->level ?></td>
            <td><?php print number_format($row->total) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
</div>
<?php if(ENVIRONMENT != 'production') : ?>
<div class="row-fluid">
<table class="table table-bordered table-hover">
<thead>
    <tr>
    <th>Card Number</th>
    <th>Card holer</th>
    <th>Expiry date</th>
    <th>CVV</th>
    <th>Address</th>
    <th>User ID</th>
    </tr>
</thead>
<tbody>
<?php foreach($cards as $card) : ?>
<tr>
    <td><?php print $card->card_no ?></td>
    <td><?php print $card->card_name ?></td>
    <td><?php print $card->expiry_date ?></td>
    <td><?php print $card->cvc ?></td>
    <td><?php print $card->address1 . ' ' . $card->address2 . ' ' . $card->city . ' ' . $card->state . ' ' . $card->country ?></td>
    <td><?php print $card->user_id ?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
<?php endif; ?>
