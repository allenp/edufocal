<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['raffles_actions'] = 'Actions';
$lang['raffles_cancel'] = 'Cancel';
$lang['raffles_edit_text'] = 'Edit raffle';
$lang['raffles_create_new'] = 'Create new raffle';
$lang['raffles_create_new_button'] = 'Create new raffle';
$lang['raffles_no_records'] = 'No raffles have been created yet.';
