	<!-- Role Editor -->
	<div id="content" class="view">
		<div id="ajax-content">
			<div class="well well-shadow">
                <a class="btn btn-primary" href="<?php echo site_url(SITE_AREA . '/content/raffles/create') ?>">Create new raffle</a>
   			</div>
			<br />
            <?php if (isset($raffles) && is_array($raffles) && count($raffles)) : ?>
            <h2>Raffles</h2>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <th>Name</th><th>Prizes</th><th>Won</th> <th>Started</th> <th>Ends</th><th>Status</th> <th><?php echo lang('raffles_actions'); ?></th>
                </thead>
            <tbody>
            <?php foreach ($raffles as $record) : ?>
                <tr>
                    <td><?php print $record->name; ?></td>
                    <td><?php print $record->total_baskets; ?></td>
                    <td><?php print $record->baskets_won; ?></td>
                    <td><?php print unix_to_human($record->starts); ?></td>
                    <td><?php print unix_to_human($record->ends); ?></td>
                    <td><?php print $record->active == 1 && $record->ends > time() ?
        "On-going" : $record->active == 1 ? "Ended" : "In-active"; ?></td>
                    <td><?php echo anchor('admin/content/raffles/edit/'. $record->id, 'Edit', 'class="ajaxify"') ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
        <?php endif; ?>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
