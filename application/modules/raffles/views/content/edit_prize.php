<?php
        $a = range(1, 10);
        $quantity = array_combine($a, $a);
?>
            <?php echo form_open_multipart('', 'class="form-horizontal"'); ?>
            <?php if(isset($raffle)) : ?>
                <h3>Prizes for <em><?php print isset($raffle) ? $raffle->name : 'this raffle promotion'; ?></em></h3>
            <?php print form_hidden('raffle_id', $raffle->id); ?>
        <ul class="breadcrumb">
            <li><a href="<?php print site_url(SITE_AREA.'/content/raffles/edit/'.$raffle->id); ?>" class="ajaxify">Edit raffle details</a><span class="divider">/</span></li>
        <li class="active">Prizes</li>
        </ul>
            <?php endif; ?>
            <fieldset>
                <!--<legend>Prizes for raffle: <?php if(isset($raffle)) print $raffle->name; ?></legend>-->
                <div class="row">
<h3>Create new prize</h3>
                    <div class="span4">
                <?php echo form_input('name', set_value('name', isset($name) ? $name : ''), 'Name'); ?>
                <label class="control-label" for="name">Details</label>
                <div class="controls">
                <textarea name="description" id="description" rows="5" cols="80"><?php print isset($description) ? $description : ''?></textarea>
                </div>
                <label class="control-label" for="name">Claim</label>
                <div class="controls">
                <textarea name="claim" id="claim" rows="5" cols="80"><?php print isset($claim) ? $claim : ''?></textarea>
                </div>
                <input type="hidden" name="raffle" id="raffle_id" value="<?php print $raffle->id ?>"/>
                <!--<?php print form_file('img', set_value('img', isset($img) ? $img : ''), 'Prize Photo'); ?>-->
                <?php echo form_dropdown('qty', $quantity, isset($qty) ? $qty : array(), 'Quantity'); ?>
                    </div>
                    <div class="span4">
                    </div>
                </div>
                <div class="row">
                
                </div>
                <div class="row">
                    <div class="span8">
                    <div class="form-actions">
                        <input type="submit" name="submit" value="Save" class="btn-primary btn-large ajaxify-submit"/> or <?php echo anchor(SITE_AREA .'/content/raffles', "Cancel"); ?>
                    </div>
                    </div>
                </div>
            </fieldset>
            <?php echo form_close(); ?>
			</div>
