<?php
                $sex_list = array('All' => 'All', 'Male' => 'Male', 'Female' => 'Female');
                $subscription = array('All' => 'All', 'weekly' => 'Weekly', 'monthly' => 'Monthly', 'yearly' => 'Yearly');
                $a = range(9, 18);
                $ages = array_combine($a, $a);
                $a = range(0, 100, 10);
                $scores = array_combine($a, $a);
                $a = range(10, 100, 10);
                $chance = array_combine(range(0.1,1, 0.1), $a);
                $a = array_keys(ef_get_disperse_func_array());
                $disbursement = array_combine($a, $a);
?>
            <?php echo form_open('', 'class="form-horizontal"'); ?>
            <?php if(isset($id)): ?>
                <h3>Details for <em><?php print isset($name) ? $name : 'this raffle promotion'; ?></em></h3>
            <?php  print form_hidden('id', $id); ?>
        <ul class="breadcrumb">
        <li class="active">Raffle Details<span class="divider">/</span></li>
        <li><a href="<?php print site_url(SITE_AREA.'/content/raffles/prizes/'.$id);?>" class="ajaxify">Prizes</a></li>
        </ul>
        <?php endif; ?>

            <fieldset>
            <!--<legend>Edit Raffle</legend>-->
                <div class="row">
                    <div class="span6">
                <?php echo form_input('name', set_value('name', isset($name) ? $name : ''), 'Name'); ?>
                <?php echo form_date('starts', set_value('starts', isset($starts) ? $starts : ''), 'Starts'); ?>
                <?php echo form_date('ends', set_value('ends', isset($ends) ? $ends : ''), 'Ends'); ?>
                <?php echo form_date('joined', set_value('joined', isset($joined) ? $joined : ''), 'Joined'); ?>
                <?php echo form_dropdown('level', $level_list, isset($level) ? $level : array(), 'Level'); ?>
                <?php echo form_dropdown('schools[]', $school_list, isset($schools) ? $schools : array(), 'Schools', 'multiple="multiple"'); ?>
                <?php echo form_dropdown('subjects[]', $subject_list, isset($subjects) ? $subjects : array(), 'Subjects', 'multiple="multiple"'); ?>
                    </div>
                    <div class="span6">
                <?php echo form_dropdown('sex', $sex_list, isset($sex) ? $sex : array(), 'Sex'); ?>
                <?php echo form_dropdown('membership', $subscription, isset($membership) ? $membership : array(), 'Membership'); ?>
                <?php echo form_dropdown('min_age', $ages, isset($min_age) ? $min_age : 10, 'Minimum Age'); ?>
                <?php echo form_dropdown('max_age', $ages, isset($max_age) ? $max_age : 18, 'Maximum Age'); ?>
                <?php echo form_dropdown('min_score', $scores, isset($min_score) ? $min_score : 90, 'Minimum Score'); ?>
                <?php echo form_dropdown('max_score', $scores, isset($max_score) ? $max_score : 100, 'Maximum Score'); ?>
                <?php echo form_dropdown('winnable', $chance, isset($winnable) ? $winnable : 0.3, 'How Lucky? (%)'); ?>
                <?php echo form_dropdown('disperse', $disbursement, isset($disperse) ? $disperse : 'linear', 'Disbursement profile'); ?>
                <?php echo ef_form_checkbox('active', isset($active) ? $active == 1 : FALSE, 'Active'); ?>
                    </div>
                </div>
                <div class="row">
                
                </div>
                <div class="row">
                    <div class="span12">
                    <div class="form-actions">
                        <input type="submit" name="submit" value="Save" class="btn-primary btn-large"/> or <?php echo anchor(SITE_AREA .'/content/raffles', "Cancel"); ?>
                    </div>
                    </div>
                </div>
            </fieldset>
            <?php echo form_close(); ?>
			</div>
