<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author: Paul Allen
 * Description: Raffle class that manages winning raffles and
 * sending out the announcement to users. 
 *
 */

class Raffle_runner
{
    private static $ci;

    public function __construct ()
    {
        self::init();
        log_message('debug', 'Raffle library loaded.');
    }

    public static function init()
    {
        self::$ci =& get_instance();
    }

    //---------------------------------------------------------------------
    /*
     * Description: check for raffles that this student qualifies for.
     * ensure they meet the criteria then give them a spin of the wheel to
     * see if they win a basket. perform all the house keeping and return
     * the winner to the caller.
     *
     */
    public function run($student_id, $test_id)
    {
        $raffles = $this->find_all($student_id);

        if(count($raffles))
            $test = Test::find($test_id);

        foreach($raffles as $raffle)
        {
            $qualify = $this->is_qualified($raffle, $test);

            if($qualify == FALSE)
                continue;

            //seed the dice
            mt_srand();

            if(($raffle->winnable * 10) >= mt_rand(1,10))
            {
                $winner = $this->send_basket($raffle, $test_id, $student_id);
                $this->send_basket_message($winner);
                return $winner;
            }
            break;
        }

        return FALSE;
    }

    public function find_all($student_id)
    {
        self::$ci->load->helper('db');

        $conditions = array();
        $conditions[] = array('starts <= UNIX_TIMESTAMP()');
        $conditions[] = array('ends >= UNIX_TIMESTAMP()');
        $conditions[] = array('active = 1');
        $conditions[] = array('baskets_won < total_baskets');
        $conditions[] = array('raffle_entrants.student_id = ?', $student_id);
        $conditions[] = array('raffle_entrants.winnable > 0.0');

        $join = 'LEFT JOIN raffle_entrants ON(raffles.id = raffle_entrants.raffle_id)';

        $params = ef_where($conditions);

        $options = array();
        $options['select'] = 'raffles.*, raffle_entrants.winnable';
        $options['joins'] = $join;
        $options['conditions'] = $params;

        $raffles = Raffle::all($options);

        return $raffles;
    }

    //attempts to save.
    private function send_basket($raffle, $test_id, $student_id)
    {
        //find the gift basket to give away.
        $basket = RaffleBasket::first(array(
            'conditions' => array(
                'raffle_id = ? AND qty > won',
                $raffle->id
            )
        ));

        //record the winner of the gift basket.
        $winner = new RaffleWinner(
            array(
                'test_id' => $test_id,
                'student_id' => $student_id,
                'raffle_id' => $raffle->id,
                'raffle_basket_id' => $basket->id,
            )
        );

        $basket->won += 1;

        $raffle->baskets_won += 1;

        //lower this student's probability of winning again.
        $entrant = RaffleEntrant::first(array(
            'conditions' => array(
                'student_id = ? AND raffle_id = ?',
                $student_id,
                $raffle->id
            )
        ));

        $entrant->winnable -= 0.2;

        if(Raffle::transaction(
            function() use($winner, $raffle, $basket, $entrant) {
                $winner->save();
                $raffle->save();
                $basket->save();
                $entrant->save();
            }
        ))
            return $winner;
       else
           return FALSE;
    }

    private function is_qualified($raffle, $test)
    {
        //-- min test score
        if($test->percentage < $raffle->min_score)
            return FALSE;

        //-- max test score
        if($test->percentage > $raffle->max_score)
            return FALSE;

        //-- disbursement rate
        $dratio = ef_disbursement_rate(time(), $raffle->starts,
            $raffle->ends, $raffle->disperse);

        $lratio = ef_in_linear(
            $raffle->baskets_won, 1.0 * $raffle->total_baskets);

        if($dratio < $lratio)
            return FALSE;

        return TRUE;
    }

    /*
     * Send internal message and email to user about the prize
     * they just won.
     */
    public function send_basket_message($winner)
    {
        self::$ci->load->library('messages/ef_message');

        //TODO: CHANGE SANTA FROM 5
        //5 = Gordon Swaby
        $santa = 5;

        $result1 =  self::$ci->ef_message->send(
            $santa,
            $winner->student_id,
            $winner->raffle_basket->claim
        );

        $ci->load->library('emailer/emailer');

        $user = User::find($winner->student_id);

        $view_params = array(
            'name' => $user->first_name . ' '. $user->last_name,
            'basket' => $winner->raffle_basket,
        );

        $info = array(
            'to' => $this->auth->email(),
            'subject' => 'EduFocal.com: You just won a prize on EduFocal!',
            /*'message' => $ci->load->view('email', $view_params, TRUE),*/
            'message' => 'You just won a prize on EduFocal. Check your messages on EduFocal.com for details.',
        );

        $result2 = $ci->emailer->send($info);

        return $result1 && $result2['success'];
    }

    //-------------------------------------------------------------------
    
    function find_qualified($raffle)
    {
        self::$ci->load->helper('db');

        $options = array();
        $all_schools = _is_all(json_decode($raffle->schools));
        $all_levels = _is_all($raffle->level);
        $all_memberships = _is_all($raffle->membership);

        if(!$all_schools || !$all_levels) {
            $options['joins'][] = 
                'LEFT JOIN schools on users.school_id = schools.id';

            if(!$all_schools)
                $options['conditions'][] = array(
                    'schools.id in (?)',
                    json_decode($raffle->schools)
                );
        }

        if(!$all_levels) {
            $options['conditions'][] = array(
                'users.exam_level = ?', $raffle->level
            );
        }

        $options['conditions'][] = array(
            'UNIX_TIMESTAMP(users.created_on) > ?', $raffle->joined
        );

        if(!$all_memberships)
        {
            $options['joins'][] =
                'LEFT JOIN codes on users.code_id = codes.id';
            $options['conditions'][] = array(
                'codes.type = ?', $raffle->membership
            );
        }

        if(isset($options['joins']))
            $options['joins'] = implode(' ', $options['joins']);

        $options['conditions'] = ef_where($options['conditions']); 

        $users = User::all($options);

        return $users;
    }
}


//---------------------------------------------------------
// HELPER FUNCTIONS
// --------------------------------------------------------

if(!function_exists('_is_all'))
{
    function _is_all($option)
    {
        if(is_array($option) && count($option) >= 1)
            return strtolower($option[0]) == strtolower(EF_ALL);
        else
            return strtolower($option) == strtolower(EF_ALL);
    }
}

if(!function_exists('ef_get_disperse_func_array'))
{
    function ef_get_disperse_func_array()
    {
        return array(
            'quad' => 'ef_in_quad',
            'sine' => 'ef_in_sine',
            'linear' => 'ef_in_linear',
            'cubic' => 'ef_in_cubic'
        );
    }
}

//-------------------------------------------------------
//How fast should we release presents?
//------------------------------------------------------
if(!function_exists('ef_disbursement_rate'))
{
    /*
     * Determine the disbursement rate
     * given the current, start and end dates
     * Select from among available disbursement
     * rate functions.
     */
    function ef_disbursement_rate($current, $start, $end,
        $disperse = 'linear')
    {
        $funcs = ef_get_disperse_func_array();
        $function = isset($funcs[$disperse]) ?
            $funcs[$disperse] : $funcs['linear'];

        $rend = $end - $start;
        $rcurrent = $end - $current;

        return $function($rcurrent, $rend);
    }
}

if(!function_exists('ef_in_quad'))
{
    function ef_in_quad($t, $d)
    {
        return ($t /= $d) * $t;
    }
}

if(!function_exists('ef_in_linear'))
{
    function ef_in_linear($t,$d)
    {
        return $t / $d;
    }
}

if(!function_exists('ef_in_cubic'))
{
    function ef_in_cubic($t, $d)
    {
        return ($t/=$d)*$t*$t*$t;
    }
}

if(!function_exists('ef_in_sine'))
{
    function ef_in_sine($t, $d)
    {
        return -1 * cos($t/$d * (M_PI/2));
    }
}
