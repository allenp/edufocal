<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_raffles extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'INT(11)', 'auto_increment' => true),
            'name' => array('type' => 'varchar(256)'),
            'starts' => array('type' => 'INT(11)', 'default' => 0),
            'ends' => array('type' => 'INT(11)', 'default' => 0),
            'joined' => array('type' => 'INT(11)', 'default' => 0),
            'schools' => array('type' => 'VARCHAR(500)'),
            'subjects' => array('type' => 'VARCHAR(500)'),
            'level' => array('type' => 'VARCHAR(6)'),
            'sex' => array('type' => 'VARCHAR(6)'),
            'min_age' => array('type' => 'int'),
            'max_age' => array('type' => 'int'),
            'min_score' => array('type' => 'int'),
            'max_score' => array('type' => 'int'),
            'membership' => array('type' => 'VARCHAR(10)'),
            'disperse' => array('type' => 'VARCHAR(15)', 'default' => 'linear'),
            'total_baskets' => array('type' => 'INT(11)', 'default' => 0),
            'baskets_won' => array('type' => 'INT(11)', 'default' => 0),
            'count' => array('type' => 'INT(11)', 'default' => 0),
            'active' => array('type' => 'TINYINT', 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'raffles');

        $fields = array(
            'id' => array('type' => 'INT(11)', 'auto_increment' => true, 'unsigned' => true),
            'raffle_id' => array('type' => 'INT(11)', 'unsigned' => true),
            'student_id' => array('type' => 'INT(11)', 'unsigned' => true),
            'winnable' => array('type' => 'float', 'default' => 0.2),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'raffle_entrants');

        $fields = array();
        $fields['id'] = array('type' => 'INT(11)', 'auto_increment' => true, 'unsigned' => true);
        $fields['raffle_id'] = array('type' => 'INT(11)', 'unsigned' => true);
        $fields['student_id'] =  array('type' => 'INT(11)', 'unsigned' => true);
        $fields['test_id'] = array('type' => 'int(11)', 'unsigned' => true, 'null' => true);
        $fields['raffle_basket_id'] = array('type' => 'int(11)', 'unsigned' => true);
        $fields['notified'] = array('type' => 'INT(11)', 'unsigned' => true, 'default' => 0);
        $fields['claimed'] = array('type' => 'INT(11)', 'unsigned' => true, 'default' => 0);
        $fields['created_at'] = array('type' => 'DATETIME');
        
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'raffle_winners');

        $fields = array(
            'id' => array('type' => 'INT(11)',
                'auto_increment' => true, 'unsigned' => true),
            'raffle_id' => array('type' => 'INT(11)',
                                'unsigned' => true, 'null' => true),
            'name' => array('type' => 'varchar(256)'),
            'description' => array('type' => 'TEXT'),
            'img' => array('type' => 'varchar(255)', 'default' => null),
            'qty' => array('type' => 'INT', 'default' => 1),
            'won' => array('type' => 'INT', 'default' => 0),
            'claim' => array('type' => 'TEXT')
        );
        
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'raffle_baskets');

        $fields = array(
            'percentage' => array('type' => 'float', 'default' => 0.0),
        );

        $this->dbforge->add_column('tests', $fields);
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_table($prefix . 'raffle_entrants');
        $this->dbforge->drop_table($prefix . 'raffle_winners');
        $this->dbforge->drop_table($prefix . 'raffle_baskets');
        $this->dbforge->drop_table($prefix . 'raffles');
    }
}
