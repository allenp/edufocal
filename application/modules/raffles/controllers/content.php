<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

define('EF_ALL', 'All');

class Content extends Admin_Controller
{
	function __construct()
	{
 		parent::__construct();
        $this->lang->load('raffles');
        $this->load->helper('ui/ui');
        $this->load->helper('date');
        $this->load->library('raffles/raffle_runner');
	}

    public function index()
    {
        $raffles = Raffle::all();

        Template::set('raffles', $raffles);
        Template::set_view("content/index");
        Template::set("toolbar_title", "Manage Raffles");
        Template::render();
    }

    public function create()
    {
        if(isset($_POST['submit']))
        {
            if(($id = $this->save()) !== FALSE)
            {
                Template::set_message("Save successful. Now add some prizes to the raffle.","success");
                redirect(site_url(SITE_AREA.'/content/raffles/prizes/'.$id));
            }
            else
            {
                Template::set('schools', $this->input->post('schools'));
                Template::set('subjects', $this->input->post('subjects'));
            }
        }

        $raffles = Raffle::all();
        $sch = School::all();
        $sub = Subject::all();

        $school_list = array('All' => EF_ALL);
        $level_list = array('All' => EF_ALL);
        $subject_list = array('All' => EF_ALL);

        foreach($sch as $s)
        {
            $school_list[$s->id] = $s->name;
            if(!isset($level_list[$s->level]))
                $level_list[$s->level] = $s->level;
        }

        foreach($sub as $sj)
            $subject_list[$sj->id] = $sj->name;

        $this->load->helper('ui/ui');
        Template::set('raffles', $raffles);
        Template::set('school_list', $school_list);
        Template::set('level_list', $level_list);
        Template::set('subject_list', $subject_list);
        Template::set("toolbar_title", "Manage Raffles");
        Template::set_view('content/edit');
        Template::render();
    }

    public function edit($id=NULL)
    {
        if($id === NULL)
            redirect(site_url(SITE_AREA . '/content/raffles'));

        if(isset($_POST['submit']))
        {
            if($this->save($id) !== FALSE)
            {
                //redirect(site_url(SITE_AREA.'/content/raffles/edit/'.$id));
            }
            else
            {
                Template::set('schools', $this->input->post('schools'));
                Template::set('subjects', $this->input->post('subjects'));
            }
        }

        $raffle = Raffle::find($id);

        $sch = School::all();
        $sub = Subject::all();

        $school_list = array('All' => EF_ALL);
        $level_list = array('All' => EF_ALL);
        $subject_list = array('All' => EF_ALL);

        foreach($sch as $s)
        {
            $school_list[$s->id] = $s->name;
            if(!isset($level_list[$s->level]))
                $level_list[$s->level] = $s->level;
        }

        foreach($sub as $sj)
            $subject_list[$sj->id] = $sj->name;

        $this->load->helper('ui/ui');

        Template::set('school_list', $school_list);
        Template::set('level_list', $level_list);
        Template::set('subject_list', $subject_list);

        Template::set('id', $raffle->id);
        Template::set('name', $raffle->name);
        Template::set('membership', $raffle->membership);
        Template::set('starts', unix_to_human_date($raffle->starts));
        Template::set('ends', unix_to_human_date($raffle->ends));
        Template::set('joined', unix_to_human_date($raffle->joined));
        Template::set('schools', json_decode($raffle->schools));
        Template::set('subjects', json_decode($raffle->subjects));
        Template::set('level', $raffle->level);
        Template::set('min_age', $raffle->min_age);
        Template::set('sex', $raffle->sex);
        Template::set('max_age', $raffle->max_age);
        Template::set('min_score', $raffle->min_score);
        Template::set('max_score', $raffle->max_score);
        Template::set('disperse', $raffle->disperse);
        Template::set('active', $raffle->active);
        
        Template::render();
    }

    public function prizes($id=NULL)
    {
        if($id === NULL)
            redirect(site_url(SITE_AREA . '/content/raffles'));

        if(isset($_POST['submit']))
        {
            $this->save_prize($id);
        }

        $raffle = Raffle::find($id);
        $prizes = $raffle->raffle_baskets;

        Template::set('prizes', $prizes);
        Template::set('raffle', $raffle);
        Template::set("toobar_title", "Prizes for $raffle->name");
        Template::render();
    }

    public function save_prize($id=NULL,$prize_id=NULL)
    {
        $rules = array(
            'name' => 'required|trim|max_length[255]',
            'qty' => 'required|number',
            'claim' => 'xss_clean',
            'description' => 'required|xss_clean',
        );

        foreach($rules as $field => $validation)
        {
            $this->form_validation->set_rules($field,
                ucfirst($field), $validation);
        }

        if($this->form_validation->run() === TRUE)
        {

            $prize = new RaffleBasket();

            $config = array();
            $config['upload_path'] = './uploads/prizes/';
            $config['allowed_types'] = 'gif|jpg|png';
            $cnfig['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            if( $this->upload->do_upload('img'))
            {
                $upload = $this->upload->data();
                $resize = array(
                    'source_image' => $upload['full_path'],
                    'maintain_ratio' => TRUE,
                    'width' => 100,
                    'height' => 100,
                );

                $this->load->library('image_lib', $resize);
                $this->image_lib->resize();

                $_POST['image'] = $upload['file_name'];
            }

            if(isset($upload))
                $prize->img = $upload['file_name'];

          try
          {
            if($prize_id!=NULL)
                $prize = RaffleBasket::find($prize_id);
            $prize->raffle_id = $id;
            $prize->claim = $this->input->post('claim');
            $prize->description = $this->input->post('description');
            $prize->name = $this->input->post('name');
            $prize->qty = $this->input->post('qty');

            $raffle = Raffle::find($id);
            $raffle->total_baskets += $prize->qty;

            return Raffle::transaction(
                function() use($prize, $raffle) {
                    $prize->save();
                    $raffle->save();
                }
            );
          }
          catch(ActiveRecord\RecordNotFound $e)
          {
            redirect(site_url(SITE_AREA.'/content/raffles/'));
          }
        }
        return FALSE;
    }

    public function save($id=NULL)
    {
        $rules = array(
            'name' => 'required|trim|max_length[255]',
            'starts' => 'required',
            'ends' => 'required',
            'joined' => 'xss_clean',
            'level' => 'xss_clean',
            'schools' => 'required',
            'subjects' => 'required',
            'sex' => 'required',
            'membership' => 'required',
            'min_age' => 'integer',
            'max_age' => 'integer',
            'min_score' => 'integer',
            'max_score' => 'integer',
            'active' => 'xss_clean',
            'winnable' => 'required',
            'disperse' =>  'required|callback__is_known_method'
        );

        foreach($rules as $field => $validation)
        {
            $this->form_validation->set_rules($field,
                ucfirst($field), $validation);
        }

        if($this->form_validation->run() === TRUE)
        {
            if(is_numeric($id))
                $raffle = Raffle::find($id);
            else
                $raffle = new Raffle();

            $raffle->name = $this->input->post('name');
            $raffle->starts = strtotime($this->input->post('starts'));
            $raffle->ends = strtotime($this->input->post('ends'));
            $raffle->joined = strtotime($this->input->post('joined'));
            $raffle->level = $this->input->post('level');
            $raffle->schools = json_encode($this->input->post('schools'));
            $raffle->subjects = json_encode($this->input->post('subjects'));
            $raffle->sex = $this->input->post('sex');
            $raffle->membership = $this->input->post('membership');
            $raffle->min_age = $this->input->post('min_age');
            $raffle->max_age = $this->input->post('max_age');
            $raffle->min_score = $this->input->post('min_score');
            $raffle->max_score = $this->input->post('max_score');
            $raffle->disperse = $this->input->post('disperse');
            $raffle->active = $this->input->post('active') ? 1 : 0;

            $users = $this->raffle_runner->find_qualified($raffle);
            $winnable = $this->input->post('winnable');
            $winnable = $winnable <= 1.0? $winnable : 0.3;

            Raffle::transaction(function() use($users, $raffle, $winnable) {
                $raffle->count = count($users);
                $raffle->save();
                foreach($users as $user)
                {
                   $re = RaffleEntrant::find_or_create_by_student_id_and_raffle_id($user->id, $raffle->id);
                   $re->winnable = $winnable;
                   $re->save();
                }
            });

            return $raffle->id;
        } 
        else
        {
            return FALSE;
        }
    }

    public function edit_prize($id)
    {
        if(isset($_POST['submit']))
        {
            $this->save_prize($this->input->post('raffle_id'), $id);
            redirect(site_url(SITE_AREA.'/content/raffles/'));
        }

        try
        {
            $prize = RaffleBasket::find($id);
            $raffle = Raffle::find($prize->raffle_id);
        }
        catch(ActiveRecord\RecordNotFound $e)
        {
            redirect(site_url(SITE_AREA.'/content/raffles/'));
        }

        Template::set('name', $prize->name);
        Template::set('description', $prize->description);
        Template::set('qty', $prize->qty);
        Template::set('claim', $prize->claim);
        Template::set('raffle', $raffle);
        Template::render();
    }

    public function _is_known_method($str)
    {
        $methods = ef_get_disperse_func_array();
        if(isset($methods[$str]))
            return TRUE;
        else
            $this->form_validation->set_message('disperse',
                'The %s field contains an unkown method');
        return FALSE;
    }
}

    
