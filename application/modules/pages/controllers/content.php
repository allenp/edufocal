<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller
{
               
	public function __construct()
	{
 		parent::__construct();
	}
	
	
	public function index()
	{
		$pages = Page::find('all', array('order' => 'nav_order'));
		
		Template::set_view("content/index");
		Template::set("pages", $pages);
		Template::set("toolbar_title", "Manage Pages");
		Template::render();
	}
	
	//--------------------------------------------------------------------
	
	
	public function create() 
	{
		if ($this->input->post('draft') || $this->input->post('publish'))
		{
			if ($this->save())
			{
				Template::set_message('Page Added Successfully', 'success');
				Template::redirect(SITE_AREA .'/content/pages');
			}
			else 
			{
				Template::set_message('An Error Occurred', 'error');
			}
		}
		
		Template::set('toolbar_title', 'New Page');
		Template::set_view('content/create');
		Template::render();
	}
	//--------------------------------------------------------------------
	
	
	public function edit() 
	{
		$id = (int)$this->uri->segment(5);
		
		if (empty($id))
		{
			redirect(SITE_AREA .'/content/pages');
		}
	
		if ($this->input->post('draft') || $this->input->post('publish'))
		{
			if ($this->save('update', $id))
			{
				Template::set_message('Page Updated Successfully', 'success');
			}
			else 
			{
				Template::set_message('An Error Occurred', 'error');
			}
		}
		
		$page = Page::find($id);

        Assets::add_module_js('pages', 'js/edit.js');
		
		Template::set('page', $page);
		Template::set_view('content/edit');
		Template::set("toolbar_title", "Edit Page");
		Template::render();		
	}
	
	public function delete() 
	{	
		$id = $this->uri->segment(5);
	
		if (!empty($id))
		{	
			if ($this->drop($id))
			{
				Template::set_message('Page Deleted Successfully', 'success');
			} else
			{
				Template::set_message('An Error Occurred', 'error');
			}
		}
		
		redirect(SITE_AREA .'/content/pages');
	}
	
	private function drop($id) 
	{
		// Delete Page
		$page = Page::find($id);
		$page->delete();
		
		return true;
	}
		
	public function save($type='insert', $id=0) 
	{
		$this->form_validation->set_rules('page_title','Page Title','required');			
		$this->form_validation->set_rules('page_content','Page Content','required');
		
		if ($this->form_validation->run() === false)
		{
			return false;
		}
		
		$attributes = array(
			'title' => $this->input->post('page_title'),
			'nav_location' => $this->input->post('location'),
			'content' => $this->input->post('page_content'),
			'keywords' => $this->input->post('keywords'),
			'description' => $this->input->post('description')
		);
		
		if(isset($_POST['draft'])) $attributes['status'] = 'draft';
		else $attributes['status'] = 'publish';
		
		if ($type == 'insert')
		{
			$attributes['permalink'] = $this->permalink($this->input->post('page_title'));
			
			Page::create($attributes);
			$return = true;
		}
		else if ($type == 'update')
		{
			$page = Page::find($id);
			
			if($page->title != $this->input->post('page_title')) 
				$attributes['permalink'] = $this->permalink($this->input->post('page_title'));
			
			$page->update_attributes($attributes);
			
			$return = true;
		}
		
		return $return;
	}
	
	private function permalink($title) {
		
		if(!function_exists('url_title')) {
			$this->load->helper('url');
		}
		
		$permalink = url_title($title, 'underscore', TRUE);
		
		$per = Page::find_by_sql("SELECT `permalink` FROM `pages` WHERE `permalink` = ?", array($permalink));
		
		if(count($per) == 0)
			return $permalink;
		else {
			$page = Page::find_by_sql("SELECT `permalink` FROM `pages` WHERE `permalink` LIKE ? ORDER BY `permalink` DESC LIMIT 1", array($permalink.'_%'));
			$current = end(explode("-", $page->permalink));
			
			return $permalink . '-' . ($current + 1);
		}
	}
}
