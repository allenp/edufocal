<?php 

class Pages extends Front_Controller {
               
	public function __construct()
	{
 		parent::__construct();
		Template::set_theme('public');
	
		if ( $this->auth->is_logged_in() )
			redirect('dashboard');
	}
	
	public function view($permalink) 
	{
		$page = Page::find_by_permalink($permalink);
		
		Template::set('page', $page);
		Template::render();
	}
}