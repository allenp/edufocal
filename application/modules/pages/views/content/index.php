
<div class="view split-view">
	
	<!-- Role List -->
	<div class="view">
	
	<?php if (isset($pages) && count($pages)) : ?>
		<div class="scrollable">
			<div class="list-view" id="role-list">
				<?php foreach ($pages as $page) : ?>
					<div class="list-item" data-id="<?php echo $page->id; ?>">
						<p>
							<b><?php echo anchor(SITE_AREA .'/content/pages/edit/'. $page->id, $page->title, 'class="ajaxify"') ?></b>
						</p>
					</div>
				<?php endforeach; ?>
			</div>	<!-- /list-view -->
		</div>
	
	<?php else: ?>
	
	<div class="notification attention">
		<p>There are no pages currently.<br /><?php echo anchor(SITE_AREA .'/content/pages/create', 'Add a Page', array("class" => "ajaxify")) ?></p>
	</div>
	
	<?php endif; ?>
	</div>
	<!-- Role Editor -->
	<div id="content" class="view">
		<div id="ajax-content">
				
			<div class="box create rounded">
				<a class="button good ajaxify" href="<?php echo site_url(SITE_AREA .'/content/pages/create')?>">New Page</a>

				<h3>Create a new Page.</h3>

				<p>Manage pages.</p>
			</div>
			<br />
				<?php if (isset($pages) && count($pages)) : ?>
				
					<h2>Pages</h2>
	<table>
		<thead>
		<th>Name</th>
		<th>Actions</th>
		</thead>
		<tbody>
<?php
foreach ($pages as $page) : ?>
			<tr>
				<td><?php echo $page->title;?></td>
				<td><?php echo anchor(SITE_AREA .'/content/pages/edit/'. $page->id, 'Edit', 'class="ajaxify"') ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
				<?php endif; ?>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
