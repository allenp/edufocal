<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<h3>General</h3>
<div>
    <?php echo form_label('Page Title', 'page_title'); ?>
    <input type="text" name="page_title" maxlength="255" value="<?php echo set_value('page_title', isset($page->title) ? $page->title : ''); ?>" />
</div>
<div>
    <?php echo form_label('Menu Location', 'location'); ?>
    <select name="location">
    	<option value="0">Header</option>
    </select>
</div>
<div>
    <?php echo form_label('Page Content', 'page_content'); ?>
    <textarea name="page_content" class="editor"><?php echo set_value('page_content', isset($page->content) ? $page->content : ''); ?></textarea>
</div>
<h3>SEO</h3>
<div>
    <?php echo form_label('Keywords', 'keywords'); ?>
    <input type="text" name="keywords" maxlength="255" value="<?php echo set_value('keywords', isset($page->keywords) ? $page->keywords : ''); ?>" />
</div>
<div>
    <?php echo form_label('Description', 'description'); ?>
    <textarea name="description"><?php echo set_value('description', isset($page->description) ? $page->description : ''); ?></textarea>
</div>

<div class="text-right">
	<input type="submit" name="draft" value="Save as Draft" /> 
    <input type="submit" name="publish" value="Save &amp; Publish" /> 
    or <?php echo anchor(SITE_AREA .'/content/pages', 'Cancel'); ?>
</div>

<div class="box delete rounded">
	<a class="button" id="delete-me" href="<?php echo site_url(SITE_AREA .'/content/pages/delete/'. $page->id); ?>" onclick="return confirm('<?php echo "Are you sure you want to delete this page?"; ?>')">Delete this Page</a>
	<h3>Delete this Page</h3>
	<p>The page will be permanently removed from the system.</p>
</div>
<?php echo form_close(); ?>

<?php
    Assets::add_js( array(
        base_url() . "/assets/tiny_mce/jquery.tinymce.js"
        ));
?>
