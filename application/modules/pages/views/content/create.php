<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<h3>General</h3>
<div>
    <?php echo form_label('Page Title', 'page_title'); ?>
    <input type="text" name="page_title" maxlength="255" />
</div>
<div>
    <?php echo form_label('Menu Location', 'location'); ?>
    <select name="location">
    	<option value="0">Header</option>
    </select>
</div>
<h3>Content</h3>
<textarea name="page_content" class="editor"></textarea>

<h3>SEO</h3>
<div>
    <?php echo form_label('Keywords', 'keywords'); ?>
    <input type="text" name="keywords" maxlength="255" />
</div>
<div>
    <?php echo form_label('Description', 'description'); ?>
    <textarea name="description"></textarea>
</div>

<div class="text-right">
	<input type="submit" name="draft" value="Save as Draft" /> 
    <input type="submit" name="publish" value="Save &amp; Publish" /> 
    or <?php echo anchor(SITE_AREA .'/content/pages', 'Cancel'); ?>
</div>
<?php echo form_close(); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
$(function() {
	$('.editor').tinymce({
		script_url : 'https://www.edufocal.com/assets/tiny_mce/tiny_mce.js',

		theme : "advanced",
		skin : "default",
		skin_variant : "default",
		plugins : "advimage, advlink, advlist, autolink, autosave, contextmenu, fullscreen, inlinepopups, lists, media, pagebreak, paste, preview, save, searchreplace, style, table, wordcount",

		// Theme options
		theme_advanced_buttons1 : "save,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontsizeselect,forecolor",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,link,unlink,anchor,image,media,|,pagebreak,preview",
		theme_advanced_buttons3 : "tablecontrols,|,hr,visualaid,|,sub,sup,charmap,|,fullscreen,code,cleanup,help",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resize_horizontal : false,
		width : '100%',
		height: 250,
		theme_advanced_resizing : true
	});
});
</script>