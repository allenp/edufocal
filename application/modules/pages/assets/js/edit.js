$(function() {
    $('.editor').tinymce({
        script_url : 'https://www.edufocal.com/assets/tiny_mce/tiny_mce.js',

        theme : "advanced",
        skin : "default",
        skin_variant : "default",
        plugins : "advimage, advlink, advlist, autolink, autosave, contextmenu, fullscreen, inlinepopups, lists, media, pagebreak, paste, preview, save, searchreplace, style, table, wordcount",

        // Theme options
        theme_advanced_buttons1 : "save,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontsizeselect,forecolor",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,link,unlink,anchor,image,media,|,pagebreak,preview",
        theme_advanced_buttons3 : "tablecontrols,|,hr,visualaid,|,sub,sup,charmap,|,fullscreen,code,cleanup,help",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resize_horizontal : false,
        width : '100%',
        height: 250,
        theme_advanced_resizing : true
    });
});
