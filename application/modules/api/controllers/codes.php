<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

define('MAX_REQUEST_AMOUNT', 25000);

class Codes extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index_get()
    {
        $msg = array();
        $msg['errors'][] = 'Use HTTP METHOD POST instead';
        $this->response($msg, 405);
        return;
    }

    /*
      <params>
        <amount>5</amount>
        <type>yearly|monthly</type>
        <currency>JMD|USD</currency>
        <sponsor>OBSERVER</sponsor>
        <expiry>sliding|fixed</sponsor>
        <purchase>0|1</purchase>
      </params>
    */
    function index_post()
    {
        try
        {
            $errors = array();

            if(!isset($this->request->body['type']) || !in_array($this->request->body['type'], array('yearly', 'monthly', 'weekly', 'quarterly')))
                $errors['errors'][] = 'parameter \'type\' is missing or invalid. Valid values: yearly, monthly, weekly, qaurterly';
            if(!isset($this->request->body['amount']))
                $errors['errors'][] = 'parameter \'amount\' is missing.';
            if(!isset($this->request->body['currency']))
                $errors['errors'][] = 'parameter \'currency\' is missing. Valid values: USD, JMD';

            if(isset($this->request->body['expiry']) && !in_array($this->request->body['expiry'], array('sliding', 'fixed')))
                $errors['errors'][] = 'parameter \'expiry\' has invalid values, only fixed, sliding accepted.';

            if(isset($errors['errors']))
            {
                $this->response($errors, 400);
                return;
            }

            $type = isset($this->request->body['type']) ?
                strtolower($this->request->body['type']) : 'yearly';

            $amount = isset($this->request->body['amount']) ?
                abs($this->request->body['amount']) : 1;

            $currency = isset($this->request->body['currency']) ?
                strtoupper($this->request->body['currency']) : 'JMD';

            $fixed = isset($this->request->body['expiry']) ?
                strtolower($this->request->body['expiry']) == 'fixed' :
                false; 

            $active = isset($this->request->body['active']) &&
                is_numeric($this->request->body['active']) ?
                $this->request->body['active'] : 0;

            $purchase = isset($this->request->body['purchase']) &&
               is_numeric($this->request->body['purchase']) ?
               $this->request->body['purchase'] : 0;

            if ($amount > MAX_REQUEST_AMOUNT) {
                $errors = array();
                $errors['errors'][] = 'Requested amount of ' . $amount . ' is too large. Maximum is '.MAX_REQUEST_AMOUNT;
                $this->response($errors, 413);
                return;
            }

            if (!in_array($currency, array('JMD', 'USD'))) {
                $this->response(array('errors' => array('Invalid currency. Valid currencies: JMD, USD')), 400);
                return;
            }

            $configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );
            $charges = $this->config->item($configs[$currency]);

            $list = array();

            $request_total = $amount * $charges[strtolower($type)]['charge'];

            $this->db->select('IFNULL(SUM(amount - used),0) as account', FALSE)
                     ->where('api_key', $this->username)
                     ->where('currency', $currency)
                     ->where('amount > used');

            $query = $this->db->get('api_purchase_orders');
            $po = $query->num_rows() > 0 ? $query->row() : (object)array('account' => 0);

            if ($po->account < $request_total) {
                if ($purchase == 0) {
                    $this->response(array('errors' => array('Exceeding purchase order limit. Account balance: ' . strval($po->account) . '.')), 413);
                    return;
                } else {
                    //make purchase
                    $purchase_amount = $request_total - $po->account;
                    $this->db->insert('api_purchase_orders',
                        array(
                            'api_key' => $this->username,
                            'currency' => $currency,
                            'amount' => $purchase_amount,
                            'used' => 0,
                            'created_on' => time()
                        )
                    );
                }
            }
            

            for ($i = 1; $i <= $amount; $i++) {
                $options = array (
                    'source'  => 1,
                    'type' => $type,
                    'currency' => $currency,
                    'value' => $charges[strtolower($type)]['charge'],
                    'source_ref' => $this->log_id(),
                    'active' => $active,
                );

                if($fixed) {
                    $options['expiry_date'] = strtotime('+ ' . $charges[strtolower($type)]['valid_for'], time());
                }

                if (isset($this->request->body['sponsor'])) {
                    $this->db->select('id', FALSE)
                             ->where('api_key', strtolower($this->request->body['sponsor']));
                    $query = $this->db->get('sponsors');

                    if ($query->num_rows() > 0)
                        $options['sponsor_id'] = $query->row()->id; 
                }

                if ($code = Code::Generate($options)) {
                    if($code === FALSE) continue;
                    unset($options['source']);
                    unset($options['expiry_date']);
                    unset($options['source_ref']);
                    unset($options['sponsor_id']);
                    unset($options['active']);

                    $options['code'] = $code->code;
                    $list['educodes'][] = $options;
                }
            }
            
            $this->db->where('api_key', $this->username)
                     ->where('currency', $currency)
                     ->where('amount > used')
                     ->select('id, amount, used');
            $query = $this->db->get('api_purchase_orders');

            $filled = 0;
            foreach ($query->result() as $row) {
                $filled = min($request_total, $row->amount - $row->used);
                $this->db->where('id', $row->id)->update('api_purchase_orders', array('used'=> $row->used + $filled));
                $request_total -= $filled;
                if($request_total == 0)
                    break;
            }

            if (count($list) &&
                !isset($this->request->body['noresponse'])) {
                $this->response($list, 200);
            } else {
                $this->response(array('count' => count($list['educodes']), 'source' => '1', 'source_ref' => $this->log_id()));
            }
        }
        catch(Exception $e)
        {
            log_message('error', $e->getMessage());
            error_log($e->getMessage());
            $this->response(array('errors' => array('Internal server error. Contact the administrator')), 500);
        }
    }
}
