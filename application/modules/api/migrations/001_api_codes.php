<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_api_codes extends Migration
{
    public function up()
    {
        $dbprefix = $this->db->dbprefix;
        $fields = array(
                        'id' => array('type' => 'INT(11)', 'auto_increment' => true),
                        'uri' => array('type' => 'varchar(255)'),
                        'method' => array('type' => 'varchar(6)'),
                        'params' => array('type' => 'text'),
                        'api_key' => array('type' => 'varchar(40)'),
                        'ip_address' => array('type' => 'varchar(15)'),
                        'time' => array('type' => 'int(11)', 'unsigned' => true),
                        'authorized' => array('type' => 'tinyint(1)'),
                     );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'api_request_logs');
    }

    public function down()
    {
        $dbprefix = $this->db->dbprefix;
        $this->dbforge->drop_table($dbprefix . 'api_request_logs');
    }
}

