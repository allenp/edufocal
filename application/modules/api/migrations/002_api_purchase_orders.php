<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_api_purchase_orders extends Migration
{
    public function up()
    {
        $dbprefix = $this->db->dbprefix;
        $fields = array(
                        'id' => array('type' => 'INT(11)', 'auto_increment' => true),
                        'api_key' => array('type' => 'varchar(255)'),
                        'currency' => array('type' => 'varchar(3)'),
                        'amount' => array('type' => 'float', 'default' => 0),
                        'used' => array('type' => 'float', 'default' => 0),
                        'created_on' => array('type' => 'int(11)', 'unsigned' => true),
                 );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'api_purchase_orders');
    }

    public function down()
    {
        $dbprefix = $this->db->dbprefix;
        $this->dbforge->drop_table($dbprefix . 'api_purchase_orders');
    }
}

