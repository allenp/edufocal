<?php

class Front_Controller_Hooks
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function before_controller()
    {
         $this->ci->load->library('sessions/ef_auth', array(
             'auth' => $this->ci->auth
         ), 'auth');
    }
}
