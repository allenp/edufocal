<?php

class Base_Controller_Hooks
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function before_controller()
    {
        if (is_production()) {
            $this->setup_production();
        }

        $this->ci->load->library('users/auth');
        $this->ci->load->library('sessions/ef_auth', array(
             'auth' => $this->ci->auth
        ), 'auth');
    }

    public function setup_production()
    {
        if($this->ci->input->is_cli_request()) {
            return;
        }

        $needles = array(
            '/teachers/coursework/short_answer',
            '/teachers/coursework/multiple',
            '/reviewer/question_queue/edit'
        );

        $request_uri = $_SERVER['REQUEST_URI'];
        $found = FALSE;

        foreach($needles as $u) {
            if(stripos($request_uri, $u) !== FALSE) {
                $found = TRUE;
                break;
            }
        }

        //bug fixing stuff.. soon to be removed
        /*
        if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on" || substr($_SERVER['SERVER_NAME'], 0, 3) != 'www') {
            $url = "https://". (substr($_SERVER['SERVER_NAME'], 0, 3) == 'www' ? '' : 'www.') . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if(!$found) {
                redirect($url);
                exit;
            }
        } else { // Make shift fix until we get a better math editor plugin
            if($found) {
                redirect('http://www.edufocal.com'.$_SERVER['REQUEST_URI']);
                exit;
            }
        }
        */
    }
}
