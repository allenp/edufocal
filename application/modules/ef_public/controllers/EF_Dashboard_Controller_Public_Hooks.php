<?php

class EF_Dashboard_Controller_Public_Hooks
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function requireAccountVerification( &$e )
    {
        //this user has not completed registration.
        //will eventually be redirected to the payment page
        if ($this->ci->auth->profile()->code_id == 0) {
           redirect('/');
        }

        //if their code has expired and they don't have any pre-ordered codes
        if($this->ci->auth->is_expired() && ! $this->ci->auth->use_saved_code_if_any()) {
           redirect('/account/payment');
        }
    }
}
