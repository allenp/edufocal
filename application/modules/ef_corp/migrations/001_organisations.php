<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_organisations extends Migration
{
    public function up()
    {
        $fields = array(
            'code_id' => array('type' => 'int(11)', 'default' => 0)
        );

        $this->dbforge->add_column($pre.'schools', $fields);

        $fields = array(
            'school_id' => array('type' => 'int(11)', 'default' => 0)
        );

        $this->dbforge->add_column($pre.'codes', $fields);
        $this->dbforge->add_column($pre.'receipts', $fields);
    }

    public function down()
    {
        $pre = $this->db->dbprefix;
        $this->dbforge->drop_column('code_id', $pre.'schools');
        $this->dbforge->drop_column('school_id', $pre.'codes');
        $this->dbforge->drop_column('school_id', $pre.'receipts');
    }
}
