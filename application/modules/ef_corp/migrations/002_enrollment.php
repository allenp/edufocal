<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_enrollment extends Migration
{
    public function up ()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'user_id' => array('type' => 'int(11)'),
            'topic_id' => array('type' => 'int(11)'),
            'status' => array('type' => 'varchar(15)', 'default' => 'pending'),
            'created_at' => array('type' => 'datetime'),
            'modified_at' => array('type' => 'datetime'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix.'enrollments');
    }

    public function down ()
    {
        $this->dbforge->drop_table($this->db->dbprefix.'enrollments');
    }
}
