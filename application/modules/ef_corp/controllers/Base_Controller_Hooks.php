<?php

class Base_Controller_Hooks
{
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function before_controller()
    {
        //the admin area doesn't need a new theme does it?
        if(strtolower($this->ci->uri->segment(1)) == strtolower(SITE_AREA)) {
            $this->ci->config->set_item('template.theme_paths', array('themes'));
        }

        /*
        if (is_production()) {
            $this->setup_production();
        }
         */

        $this->ci->load->library('users/auth');
        $this->ci->load->library(
            'sessions/ef_auth',
            array('auth' => $this->ci->auth),
            'auth'
        );
    }

    public function after_controller_constructor()
    {
        $this->ci->load->driver(
            'cache',
            array('adapter' => 'file', 'backup' => 'file')
        );
    }
}
