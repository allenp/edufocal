
    <div id="main">
		<div id="column_1">
            <div id="subjects" class="section">
            	<h2>Browse Subjects</h2>
                <p>Through prep tests, and timed tests, EduFocal helps students reinforce the topics they learn in class. Get unlimited access to content for a low annual fee and can earn CASH and FREE membership by becoming an EduFocal tutor.</p>
                <div class="left">
                	<h3>CXC Subjects:</h3>
                    <ul>
                    	<?php foreach($cxcs as $cxc) : ?>
                        <li><img src="<?php echo site_url("assets/".$cxc->icon); ?>" alt="<?php echo $cxc->name; ?>" /><strong><a href="<?php echo site_url('browse/cxc#'.$cxc->permalink); ?>"><?php echo $cxc->name; ?></a></strong> (<?php echo count($cxc->topics); ?>)<br />
                        <span class="small">
                        	<?php $limit = 4;
                        	foreach($cxc->topics as $topic) : 
                        		if($limit == 0) break; ?>
                        	<a href="<?php echo site_url('browse/cxc#'.$topic->permalink); ?>"><?php echo $topic->name; ?></a>, 
                        	<?php $limit--;
                        	endforeach; ?>
                        	<a href="<?php echo site_url('browse/cxc#'.$cxc->permalink); ?>">Learn More...</a>
                        </span></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="right">
                	<h3>GSAT Subjects:</h3>
                    <ul>
                        <?php foreach($gsats as $gsat) : ?>
                        <li><img src="<?php echo site_url("assets/".$gsat->icon); ?>" alt="<?php echo $gsat->name; ?>" /><strong><a href="<?php echo site_url('browse/gsat#'.$gsat->permalink); ?>"><?php echo $gsat->name; ?></a></strong> (<?php echo count($gsat->topics); ?>)<br />
                        <span class="small">
                        	<?php $limit = 4;
                        	foreach($gsat->topics as $topic) :
								if($limit == 0) break; ?>
                        	<a href="<?php echo site_url('browse/gsat#'.$topic->permalink); ?>"><?php echo $topic->name; ?></a>, 
                        	<?php $limit--;
                        	endforeach; ?>
                        	<a href="<?php echo site_url('browse/gsat#'.$gsat->permalink); ?>">Learn More...</a>
                        </span></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="column_2">
        	<div class="section">
            	<h3>CXC Subjects:</h3>
                <ul>
                	<?php foreach($cxcs as $cxc) : ?>
                    <li><strong><a href="<?php echo site_url('browse/cxc#'.$cxc->permalink); ?>"><?php echo $cxc->name; ?></a></strong></li>
                    <?php endforeach; ?>
                </ul>
        	</div>
        	<div class="divider3"></div>
        	<div class="section">
                <h3>GSAT Subjects:</h3>
                <ul>
	                <?php foreach($gsats as $gsat) : ?>
	                <li><strong><a href="<?php echo site_url('browse/gsat#'.$gsat->permalink); ?>"><?php echo $gsat->name; ?></a></strong></li>
	                <?php endforeach; ?>
	            </ul>
        	</div>
        </div>
        <div class="clearfix"></div>
    </div>

    