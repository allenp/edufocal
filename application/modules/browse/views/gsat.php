
    <div id="main">
		<div id="column_1">
        	<div class="section">
            	<h2>Browse Subjects</h2>
                <?php foreach($gsats as $gsat) : ?>
                <h3><a name="<?php echo $gsat->permalink; ?>"></a><?php echo $gsat->name; ?></h3>
                	<?php foreach($gsat->topics as $topic) : ?>
                    	<h4><a name="<?php echo $topic->permalink; ?>"></a><?php echo $topic->name; ?></h4>
                    	<?php echo $topic->description; ?>
                	<?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="column_2">
        	<div class="section">
            	<h3>GSAT Subjects:</h3>
                <ul>
                	<?php foreach($gsats as $gsat) : ?>
                    <li><strong><a href="#<?php echo $gsat->permalink; ?>"><?php echo $gsat->name; ?></a></strong>
	                    <ul>
	                    	<?php foreach($gsat->topics as $topic) : ?>
	                    	<li><a href="#<?php echo $topic->permalink; ?>"><?php echo $topic->name; ?></a></li> 
	                    	<?php endforeach; ?>
	                    </ul>
                    </li>
                    <?php endforeach; ?>
                </ul>
        	</div>
        	<div class="divider3"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    