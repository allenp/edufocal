<?php

class Browse extends Front_Controller
{
	public function __construct ()
	{
		parent::__construct();
		Template::set_theme('public');
	
		if ( $this->auth->is_logged_in() )
			redirect('dashboard');

	}
	
	public function index ()
	{
		$cxcs = Subject::find_all_by_level("CXC", array("order" => "RAND()"));
		$gsats = Subject::find_all_by_level("GSAT", array("order" => "RAND()"));
		Template::set('cxcs', $cxcs);
		Template::set('gsats', $gsats);
		Template::set_view('browse/index');
		Template::render();
	}
	
	public function gsat ()
	{
		$gsats = Subject::find_all_by_level("GSAT", array("order" => "RAND()"));
		Template::set('gsats', $gsats);
		Template::set_view('browse/gsat');
		Template::render();
	}
	
	public function cxc ()
	{
		$cxcs = Subject::find_all_by_level("CXC", array("order" => "RAND()"));
		Template::set('cxcs', $cxcs);
		Template::set_view('browse/cxc');
		Template::render();
	}
}