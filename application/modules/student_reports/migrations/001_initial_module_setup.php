<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_initial_module_setup extends Migration
{
    public function up()
    {
        $prefix = $this->db->prefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'user_id' => array('type' => 'int(11)'),
            'from_dt' => array('type' => 'datetime'),
            'to_dt' => array('type' => 'datetime'),
            'status' => array('type' => 'char(10)', 'default' => 'new'),
            'report_id' => array('type' => 'int(11)', 'null' => true),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'report_requests');

        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'report_request_id' => array('type' => 'int(11)'),
            'user_id' => array('type' => 'int(11)'),
            'from_dt' => array('type' => 'datetime'),
            'to_dt' => array('type' => 'datetime'),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'reports');

        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'report_id' => array('type' => 'int(11)', 'null' => true),
            'user_id' => array('type' => 'int(11)'),
            'subject_id' => array('type' => 'int(11)'),
            'avg_percent' => array('type' => 'int'),
            'max_percent' => array('type' => 'int'),
            'min_percent' => array('type' => 'int'),
            'avg_mins' => array('type' => 'int'),
            'max_mins' => array('type' => 'int'),
            'min_mins' => array('type' => 'int'),
            'num_tests' => array('type' => 'int'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'report_subjects');

        $fields['topic_id'] = $fields['subject_id'];
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'report_topics');
    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix . 'report_requests');
        $this->dbforge->drop_table($prefix . 'report_subjects');
        $this->dbforge->drop_table($prefix . 'report_topics');
        $this->dbforge->drop_table($prefix . 'reports');
    }
}
