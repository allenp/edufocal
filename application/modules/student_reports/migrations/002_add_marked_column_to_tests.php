<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_marked_column_to_tests extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'marked' => array('type' => 'tinyint', 'default' => 0)
        );

        $this->dbforge->add_column($prefix.'tests', $fields);
        $this->db->query("UPDATE {$prefix}tests SET marked = 1 WHERE ifnull(percentage,0) > 0");
    }

    public function down()
    {
        $this->dbforge->drop_column($this->db->dbprefix.'tests', 'marked');
    }
}
