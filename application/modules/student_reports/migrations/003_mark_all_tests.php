<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_mark_all_tests extends Migration
{
    public function up()
    {
         while($unmarked = Test::all(array(
                 'conditions' => array(
                     'marked = 0 and finished > 0',
                 ),
                 'limit' => 50 
             )
         ))
         {
             foreach($unmarked as $test)
                 $test->mark();
         }
    }

    public function down()
    {
    }
}
