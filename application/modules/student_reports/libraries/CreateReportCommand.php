<?php

class CreateReportCommand
{
    public $report_request_id;
    public static $ci = null;

    public function __construct($id)
    {
        $this->report_request_id = $id;
        if (self::$ci == null) {
            self::$ci =& get_instance();
        }
    }

    public function execute()
    {
        $request = ReportRequest::find_by_id($this->report_request_id);

        if ($request->report_id != null && $request->status = 'completed') {
            $report = Report::find_by_id($request->report_id);
        }

        if (isset($report) && $report != null) {
            return $report;
        }

        $request->status = 'processing';
        $request->save();

        //find all unmarked tests and mark them.
        //only necessary to support legacy tests
        //whose marked results were not saved
        $cond = array(
            'conditions' => array(
                'user_id = ? and marked = 0 and finished > 0 and started BETWEEN ? AND ?',
                $request->user_id, strtotime($request->from_dt), strtotime($request->to_dt)
            ),
            'limit' => 20);

        while ($unmarked = Test::all($cond)) {
            foreach ($unmarked as $test) {
                $test->mark();
            }
        }

        $report = Report::create(array(
            'user_id' => $request->user_id,
            'from_dt' => $request->from_dt,
            'to_dt' => $request->to_dt,
            'report_request_id' => $request->id
        ));

        //broken down by topic
        $q = "INSERT INTO report_topics(report_id, user_id, subject_id,
            topic_id, avg_percent, max_percent, min_percent,
            num_tests, avg_mins, max_mins, min_mins)
            SELECT {$report->id}, {$request->user_id}, subject_id, topic_id,
            FLOOR(AVG(percentage)) avg_percent,
            FLOOR(MAX(percentage)) max_percent,
            FLOOR(MIN(percentage)) min_percent,
            COUNT(*) num_tests,
            CEIL((finished-started)/60) avg_mins,
            CEIL(MAX(finished-started)/60) max_mins,
            CEIL(MIN(finished-started)/60) min_mins
            FROM `tests`
            WHERE user_id = ?
            AND started BETWEEN ? AND ?
            AND finished > 0
            GROUP BY subject_id, topic_id";

        self::$ci->db->query(
            $q,
            array(
                $request->user_id,
                strtotime($request->from_dt),
                strtotime($request->to_dt)
            )
        );

        //broken down by subject
        $q = "INSERT INTO report_subjects(report_id, user_id, subject_id,
            avg_percent, max_percent, min_percent, num_tests, avg_mins,
            max_mins, min_mins)
            SELECT {$report->id}, {$request->user_id}, subject_id,
            FLOOR(AVG(percentage)) avg_percent,
            FLOOR(MAX(percentage)) max_percent,
            FLOOR(MIN(percentage)) min_percent,
            COUNT(*) num_tests,
            CEIL((finished-started)/60) avg_mins,
            CEIL(MAX(finished-started)/60) max_mins,
            CEIL(MIN(finished-started)/60) min_mins
            FROM `tests`
            WHERE user_id = ?
            AND started BETWEEN ? AND ?
            AND finished > 0
            GROUP BY subject_id";

        self::$ci->db->query(
            $q,
            array(
                $request->user_id,
                strtotime($request->from_dt),
                strtotime($request->to_dt)
            )
        );

        $request->report_id = $report->id;
        $request->status = 'completed';
        $request->save();

        return $report;
    }
}
