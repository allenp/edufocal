<?php

class ReportRequestedCommand
{
    var $user_id;
    var $from_dt;
    var $to_dt;
    var $status = 'new'; // new | processing | completed

    public static $rules = array(
        'from_dt' => array('label' => 'From', 'rules' => 'required'),
        'to_dt' => array('label' => 'To Date', 'rules' => 'required'),
    );

    public function __construct($user_id=null, $from_dt=null, $to_dt=null)
    {
        $this->user_id = $user_id;
        $this->from_dt = new ActiveRecord\DateTime();
        $this->to_dt = new ActiveRecord\DateTime();

        $this->from_dt->setTimestamp(strtotime($from_dt));
        $this->from_dt->setTime(0, 0, 0);
        $this->to_dt->setTimestamp(strtotime($to_dt));
        $this->to_dt->setTime(23, 59, 59);
    }

    public function Execute()
    {
        $rrc = ReportRequest::find(
            array('conditions' => array(
                'user_id = ? AND from_dt = ? AND to_dt = ?',
                $this->user_id,
                $this->from_dt->format('db'),
                $this->to_dt->format('db')
            ))
        );

        if($rrc == null)
        {
            $rrc = ReportRequest::create(
                array(
                    'user_id' => $this->user_id,
                    'from_dt' => $this->from_dt,
                    'to_dt' => $this->to_dt,
                    'status' => $this->status
                )
            );
        }
        else
        {
            //if the end date for this report is greater than the create_at date
            //then this report was generated at a time when the results of the report
            //could change in the future (because the user took more tests, for instance).
            //so regenerate the report.
            if(is_numeric($rrc->report_id) && $this->to_dt->getTimestamp() >= $rrc->created_at->getTimestamp())
            {
                $report = Report::find_by_id($rrc->report_id);
                if($report != null) {
                    $report->delete();
                }

                $rrc->created_at = new DateTime();
                $rrc->status = $this->status;
                $rrc->save();
            }
        }
        return $rrc;
    }
}
