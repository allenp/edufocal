$('#reportrange').daterangepicker(
    {
      ranges: {
<?php if(isset($range) && $range != null): ?>
         'Full Report' : [moment('<?php print date('Ymd', $range->earliest); ?>', 'YYYYMMDD'), moment('<?php print date('Ymd', $range->latest); ?>', 'YYYYMMDD')],
<?php endif; ?>
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
      },
<?php if($this->input->post('from_dt') !== FALSE): ?>
      startDate: moment('<?php print date('Ymd', strtotime($this->input->post('from_dt'))); ?>', 'YYYYMMDD'),
<?php else: ?>
      startDate: moment().subtract('days', 30),
<?php endif; ?>
<?php if($this->input->post('to_dt') !== FALSE): ?>
      endDate: moment('<?php print date('Ymd', strtotime($this->input->post('to_dt'))); ?>', 'YYYYMMDD'),
<?php else: ?>
      endDate: moment(),
<?php endif; ?>
    },
    function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('input[name="from_dt"]').val(start);
        $('input[name="to_dt"]').val(end);
        $("#reportForm").submit();
    }
);

$(document).on('click', '.toggle', function(e) {
    e.preventDefault();
    subject = $(this).data('subject');
    $topic = $('.topics').filter(function(){
        return $(this).data("subject") == subject;
    });

    if($topic.is(':hidden')) {
        $(this).text('Hide Details');
    } else {
        $(this).text('Show Details');
    }
    $topic.toggle();
});
