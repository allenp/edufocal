<?php Assets::add_js(array('moment.js', 'daterangepicker.js')); ?>
<?php print Template::message(); ?>
<div class="report-header clearfix">
    <h1><?php print $this->auth->profile()->name(); ?></h1>
<div id="reportrange" class="col-sm-6 col-sm-offset-3">
    <i class="icon-calendar icon-large"></i>
<?php

$from = $this->input->post('from_dt');
$to = $this->input->post('to_dt'); 

if($from == FALSE)
    $from = date("F j, Y", strtotime('-30 day'));
else
    $from = date("F j, Y", strtotime($from));

if($to == FALSE)
    $to = date("F j, Y", strtotime('-1 day'));
else
    $to = date("F j, Y", strtotime($to));

?>
    <span><?php echo $from; ?> - <?php echo $to; ?></span> <b class="caret"></b>
    <?php print form_open(site_url('dashboard/reports'), array('id' => 'reportForm', 'method' => 'post')); ?>
    <?php print form_hidden('from_dt', ''); ?>
    <?php print form_hidden('to_dt', ''); ?>
    <?php print form_hidden('report', '1'); ?>
    <?php print form_close(); ?>
</div>
</div>
<div class="report-body">
<div class="shiv">
<div class="container">
<h1>Get Access to Your Full Report Card</h1>
<p>Subscribe to any of our paid plans to get full access to your report card. <a href="http://blog.edufocal.com/introducing-edufocal-reports/" target="_blank" title="Introducing EduFocal Reports" style="color: #fff; text-decoration: underline;">Read more about the report card here.</a></p>
<p>Email us at help@edufocal.com or call us at 1876-545-6316 between the hours of 9:00 AM to 5:00 PM, Mondays to Fridays for help!</p>
<p><a href="<?php print site_url('account/payment'); ?>" class="btn btn-info btn-lg">Get Full Report Card</a></p>
</div>
</div>
<?php if(isset($report)) : ?>
<?php if (count($report->report_subjects)): ?>
<?php $subject = $report->report_subjects[0]; ?>
<?php endif; ?>
<?php if(isset($subject)) : ?>
<h3 class="section-title"><?php print $subjects[$subject->subject_id]; ?></h3>
<div class="row">
<div class="col-xs-12 col-sm-4">
    <table class="table table-bordered">
    <thead>
    <tr class="title"><th colspan="3">Score Achieved</th></tr>
    <tr><th>Average</th><th>Highest</th><th>Lowest</th></tr>
    </thead>
    <tbody>
    <tr>
        <td class="key-point"><?php print $subject->avg_percent; ?>%</td>
        <td><?php print $subject->max_percent; ?>%</td>
        <td><?php print $subject->min_percent; ?>%</td>
    </tr>
    </table>
</div>
<div class="col-xs-12 col-sm-4">
    <table class="table table-bordered">
    <thead>
    <tr class="title"><th>Tests Taken</th></tr>
    </thead>
    <tbody>
    <tr>
        <td class="key-point"><?php print $subject->num_tests; ?> <span>tests</span></td>
    </tr>
    </table>
</div>
<div class="col-xs-12 col-sm-4">
    <table class="table table-bordered">
    <thead>
    <tr class="title"><th colspan="3">Time Per Test</th></tr>
    <tr><th>Average</th><th>Fastest</th><th>Slowest</th></tr>
    </thead>
    <tbody>
    <tr>
        <td class="key-point"><?php print $subject->avg_mins;?> <span>mins</span></td>
        <td><?php print $subject->min_mins;?> mins</td>
        <td><?php print $subject->max_mins;?> mins</td>
    </tr>
    </table>
</div>
<div class="col-xs-12 clearfix">
<button class="subject-details btn btn-default toggle pull-right" data-subject="<?php print $subject->subject_id ?>">Hide Details</button>
</div>
<div class="col-xs-12 topics" data-subject="<?php print $subject->subject_id; ?>">
    <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
            <tr class="title">
                <th rowspan="2">Topics</th>
                <th colspan="3">Scores</th>
                <th colspan="3">Time Taken</th>
                <th rowspan="2">Tests</th>
            </tr>
            <tr>
                <th>Average</th><th>Highest</th><th>Lowest</th>
                <th>Average</th><th>Fastest</th><th>Slowest</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($report->report_topics as $topic) : ?>
        <?php if($topic->subject_id == $subject->subject_id) : ?>
            <tr>
                <th><?php print $topics[$topic->subject_id][$topic->topic_id]; ?></th>
                <td><?php print $topic->avg_percent; ?>%</td>
                <td><?php print $topic->max_percent; ?>%</td>
                <td><?php print $topic->min_percent; ?>%</td>
                <td><?php print $topic->avg_mins;?> mins</td>
                <td><?php print $topic->min_mins; ?> mins</td>
                <td><?php print $topic->max_mins; ?> mins</td>
                <td><?php print $topic->num_tests; ?></td>
            </tr>
        <?php endif; ?>
        <?php endforeach; ?>
        </tbody>
        </table>
    </div>
</div>
</div> <!-- row -->
<?php endif; ?>
<?php endif; ?>
</div>
