<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TestReport extends EF_Dashboard_Controller
{
    public function __construct()
    {
        parent::__construct();
		if(in_array($this->auth->role_id(), array(ADMIN_ROLE, MANAGER_ROLE)) &&
			$this->config->item('student_report.toggle') !== true) {
            redirect('dashboard');
		}

		if (in_array($this->auth->role_id(), array(ADMIN_ROLE, MANAGER_ROLE))) {
			Template::set_block('main_navigation', 'managers/partials/main_navigation');
			Template::set_block('personal_header_links', 'managers/partials/manager_header_links');
		}

		Template::set_theme('dashboard');
    }

    public function index($id=null)
    {
		$id = null == $id ? $this->auth->user_id() : $id;
		$own = $id == $this->auth->user_id();

        $this->load->library('ReportRequestedCommand');

        if (isset($_POST['report'])) {
            foreach (ReportRequestedCommand::$rules as $rule => $field) {
                $this->form_validation->set_rules(
                    $rule, $field['label'], $field['rules']
                );
			}

            if ($this->form_validation->run() === true) {
                $request_cmd = new ReportRequestedCommand(
                    $id,
                    $this->input->post('from_dt'),
                    $this->input->post('to_dt')
                );
            } else {
                //TODO: Remove this and show validation errors
                //in the view
                Template::set_message(
                    'Validation rules failed.',
                    'warning'
                );
            }
        } else { //initial load
            $request_cmd = new ReportRequestedCommand(
                $id,
                date('F j Y', strtotime('- 30 day')),
                date('F j Y', strtotime('-1 day'))
            );
        }

        if (isset($request_cmd)) {
            $request = $request_cmd->Execute();
            Template::set('report_request', $request);

            if ($this->config->item('student_report.queue') === false) {
                $this->load->library('CreateReportCommand');
                $report_cmd = new CreateReportCommand($request->id);
                $report = $report_cmd->execute();
                Template::set('report', $report);
                $this->setup();
            }
        }

        $range = Test::first(array(
            'select' => 'MIN(started) earliest, MAX(started) latest',
            'conditions' => array('user_id = ? and finished > 0', $id)
        ));

		$user = User::find_by_id($id);

		if (null !== $user) {
			$user = new EduFocal\User\UserSecurityProfile($user);
		}

		$paid = $own && $user->isPaidMember();
		$king = in_array($this->auth->role_id(), array(ADMIN_ROLE, MANAGER_ROLE));


		//students see a different url
		//when viewing their reports
		if ($own) {
			$url = array('dashboard', 'reports');
		} else {
			$url = array('student', 'reports', $user->id);
		}

		Template::set('url', $url);
		Template::set('own', $own);

		if ($king || ($own && $paid)) {
            //implicitly uses view: index.php
			Template::set('title', 'My Report - EduFocal');
		} else {
            Template::set('report', $report);
            Template::set_view('unpaid');
            Template::set('title', 'Get Student Report - EduFocal');
            Template::set('title', 'My Report - EduFocal');
        }

		$js = $this->load->view(
			'student_reports/testreport/script',
			array('range' => $range, 'user' => $user), true);

		Assets::add_js($js, 'inline');
        Template::render();
    }

    //helper function to setup
    //variables needed by the view to show the report
    private function setup()
    {
        $t = Topic::all(array('select' => 'id, subject_id, name'));

        $s = Subject::all(array('select' => 'id, name, level'));

        $subjects = array();
        foreach ($s as $v) {
            $subjects[$v->id] = trim($v->name) . ' : ' . ($v->level == 'CXC' ? 'CSEC' : $v->level);
        }

        $topics = array();
        foreach ($t as $v) {
            $topics[$v->subject_id][$v->id] = trim($v->name);
        }

        Template::set('topics', $topics);
        Template::set('subjects', $subjects);
    }
}
