<?php

/**
 * Handles login and logout calls to the system.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class Sessions extends Front_Controller
{

    /**
     * Constructs the session module to handle login and logout calls.
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * AJAX call to login a user regardless of role or type.
     *
     * @author Jamie Chung
     */
    public function login ()
    {
        $messages = array();
        $code = 0;

        if (count($_POST) > 0) {
            if ($this->input->post('remember_me') == '1' || !isset($_POST['remember_me'])) {
                $remember = true;
            } else {
                $remember = false;
            }

            //validate form input
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            // Try to login
            if($this->form_validation->run() && $this->auth->login($email, $password, $remember) === TRUE) {
                $code = 1;
                $messages = array('Login succeeded.');
            } else {
                $messages = array('Incorrect username or password.');
                $code = 2;
            }
        }

        $messages = (validation_errors()) ? array(validation_errors()): $messages;

        if ($this->input->is_ajax_request()) {
            header("Content-type: application/json");
            $data = array('code' => $code, 'messages' => $messages);
            echo json_encode($data);
            exit;
        }

        if ($this->auth->is_logged_in()) {
            switch ($this->auth->role_id())
            {
            case STUDENT_ROLE:
            default:
                redirect('dashboard');
                break;

            case TEACHER_ROLE:
                redirect('teachers');
                break;

            case ADMIN_ROLE:
                redirect('admin');
                break;
            }
        }
    }
}
