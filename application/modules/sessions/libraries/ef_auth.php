<?php

use EduFocal\User\UserSecurityProfile;
use EduFocal\Code\AcceptNextUserCodeIfAny;
use EduFocal\Support\Listener;

/**
 * Authorization library to manage users.
 *
 * @package EduFocal
 * @author Jamie Chung
 */
class EF_Auth extends Auth
{
    /**
     * Cached copy of the current user.
     *
     * @var object User object from ActiveRecord namespace.
     */
    private $user = NULL;


    /**
     * Fields currently editable by the user.
     *
     * @var string
     */
    public $fields = array(
        'first_name', 'last_name', 'school_id', 'birthday'
    );

    /**
     * Rules of the various field of a user.
     *
     * @var array
     */
    public $rules = array(
        'first_name' => array (
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'required|alpha|trim|min_length[2]|xss_clean|htmlspecialchars'
        ),
        'last_name' => array (
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'required|alpha|trim|xss_clean|xss_clean|htmlspecialchars'
        ),
        'phone' => array (
            'field' => 'phone',
            'label' => 'Phone Number',
            'rules' => 'trim|xss_clean|htmlspecialchars'
        ),
        'sex' => array (
            'field' => 'sex',
            'label' => 'Gender',
            'rules' => 'trim|max_length[1]|alpha|xss_clean|htmlspecialchars'
        ),
        'birth_month' => array (
            'field' => 'birth_month',
            'label' => 'Birthday Month',
            'rules' => 'trim|max_length[2]|is_natural_no_zero|xss_clean|htmlspecialchars'
        ),
        'birth_day' => array (
            'field' => 'birth_day',
            'label' => 'Birthday Day',
            'rules' => 'trim|max_length[2]|is_natural_no_zero|xss_clean|htmlspecialchars'
        ),
        'birth_year' => array (
            'field' => 'birth_year',
            'label' => 'Birth Year',
            'rules' => 'trim|exact_length[4]|is_natural_no_zero|xss_clean|htmlspecialchars'
        ),
    );

    /**
     * Instance of CI object.
     *
     * @var string
     * @access private
     */
    private $ci;

    /**
     * Constructor to setup the CI instance.
     */
    public function __construct ()
    {
        parent::__construct();
        $this->ci =& get_instance();
    }

    /**
     * Gets the profile of the current logged in user.
     *
     */
    public function profile ()
    {
        if ( $this->is_logged_in() && count($this->user) == 0 ) {
            try {
                $this->user = User::find($this->user_id());
            } catch ( Exception $e ) {
                $this->logout();
                redirect();
            }
        }

        if (null != $this->user && ! $this->user instanceof UserSecurityProfile) {
            $this->user = new UserSecurityProfile(clone $this->user);
        }
        return $this->user;
    }

    /**
     * Get the Role ID of the current user.
     *
     * @return int Role ID
     * @author Jamie Chung
     */
    public function role_id ()
    {
        return $this->profile() ? $this->profile()->role_id : 0;
    }

    /**
     * Generates a proper name for a user.
     *
     * @return string
     * @author Jamie Chung
     */
    public function name ()
    {
        if ( $this->is_logged_in() ) {
            return $this->profile()->name();
        }

        return 'Guest';
    }

    /**
     * Gets a field from the current user object.
     *
     * @param string $field Field name to obtain.
     * @return string Value of the user's profile field. False if nonexisting.
     * @author Jamie Chung
     */
    public function field ( $field )
    {
        return $this->profile()->profile($field);
    }

    public function email()
    {
        return $this->profile()->email;
    }

    public function track_id()
    {
        $track = StudentTrack::find('first', array('conditions' => array('user_id = ?', $this->profile()->id)));
        if($track != NULL) {
            return $track->track_id;
        } else {
            return -1;
        }
    }

    public function is_expired()
    {
        return $this->profile()->isExpired();
    }

    public function is_paid_member()
    {
        return $this->profile()->isPaidMember();
    }

    /*
     * The user may have unused codes assigned to their name.
     * This can occur if they pre-order a code, or one was gifted to them, and it is added
     * to the system before their current code expires.
     * this method allows us to pick-up this code and accept it
     */
    public function use_saved_code_if_any()
    {
		$e = Listener::get();
        return (new AcceptNextUserCodeIfAny($this->profile(), $e))->apply();
    }
}

