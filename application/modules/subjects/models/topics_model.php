<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Topics_model extends BF_Model {

	protected $table_name	= "topics";
	protected $key			= "id";
	protected $soft_deletes	= false;

}
