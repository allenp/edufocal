<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subjects_model extends BF_Model {

	protected $table_name	= "subjects";
	protected $key			= "id";
	protected $soft_deletes	= false;

}
