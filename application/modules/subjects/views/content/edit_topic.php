<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 


if(isset($topic) ) {
	$topic = (array)$topic;
}
$id = isset($topic['id']) ? "/" . $topic['id'] : '';

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<input type="hidden" name="subject_id" value="<?php print $subject->id ?>" />
<div>
    <?php echo form_label('Name', 'name'); ?> <span class="required">*</span>
    <input id="name" type="text" name="name" maxlength="255" value="<?php echo set_value('name', isset($topic['name']) ? $topic['name'] : ''); ?>"  />
</div>
<div>
    <?php echo form_label('Perma Link', 'perma'); ?> <span class="required">*</span>
    <input id="permalink" type="text" name="permalink" maxlength="255" value="<?php echo set_value('permalink', isset($topic['permalink']) ? $topic['permalink'] : ''); ?>"  />
</div>

<div class="text-right">
	<input type="submit" name="submit" value="Update Topic" /> or <?php echo anchor(SITE_AREA .'/content/subjects', "Cancel"); ?>
</div>
<?php echo form_close(); ?>

<div class="box delete rounded">
	<a class="button" id="delete-me" href="<?php print site_url(SITE_AREA .'/content/subjects/delete_topic/'. $id); ?>" onclick="return confirm('Delete this topic?')">Delete Topic</a>
	<h3>Delete Topic</h3>
	<p>Edit Topic</p>
</div>
