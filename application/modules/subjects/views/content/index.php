<div class="">
	<!-- Role Editor -->
	<div id="content" class="view">
		<div id="ajax-content">
				
    <div class="well well-shadow">
        <a class="btn btn-primary ajaxify" href="<?php echo site_url(SITE_AREA .'/content/subjects/create')?>"><?php echo lang('subjects_create_new_button');?></a>
    </div>
<?php if (isset($subjects) && is_array($subjects) && count($subjects)) : ?>
				
    <h2>Subjects</h2>
    <div class="pull-right">
        <select name="level" id="level">
            <option value="CXC">CXC</option>
            <option value="GSAT" <?php if($level == "GSAT")echo "selected"?> > GSAT</option>
        </select>
    </div>
	<table class="table table-striped table-bordered">
		<thead>
		<th>Subject ID</th>
		<th>Name</th>
		<th>Permalink</th>
		<th>Type</th><th><?php echo lang('subjects_actions'); ?></th>
		</thead>
		<tbody>
<?php
foreach ($subjects as $subject) : ?>
			<tr>
                <td><?php echo $subject->id ?></td><td><?php echo $subject->name ?></td><td><?php echo $subject->permalink ?></td>
                <td><?php echo $subject->level ?></td>
				<td><?php echo anchor(SITE_AREA .'/content/subjects/edit/'. $subject->id, 'Edit', array('class' => 'btn btn-small btn-info')) ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
