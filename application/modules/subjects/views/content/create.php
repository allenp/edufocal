<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 

if( isset($subject) ) {
	$subject = (array)$subject;
}
$id = isset($subject['id']) ? "/".$subject['id'] : '';

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<div>
    <?php echo form_label('Name', 'name'); ?> <span class="required">*</span>
    <input id="name" type="text" name="name" maxlength="255" value="<?php echo set_value('name', isset($subject['name']) ? $subject['name'] : ''); ?>"  />
</div>
	<?php 
	$levels = array(
		'CXC' => 'CXC',
		'GSAT' =>  'GSAT',
        'SAT' => 'SAT',
        'SSAT' => 'SSAT',
        'GRE' => 'GRE',
        'GMAT' => 'GMAT',
        'LSAT' => 'LSAT'
	); ?>
<div>
<div>
    <?php echo form_label('Abbrev', 'abbrev'); ?> <span class="required">*</span>
    <input id="abbrev" type="text" name="abbrev" maxlength="255" value="<?php echo set_value('abbrev', isset($subject['abbrev']) ? $subject['abbrev'] : ''); ?>"  />
</div>
<div>
    <?php echo form_label('Perma Link', 'permalink'); ?> <span class="required">*</span>
    <input id="permalink" type="text" name="permalink" maxlength="255" value="<?php echo set_value('permalink', isset($subject['permalink']) ? $subject['permalink'] : ''); ?>"  />
</div>
    <?php echo form_label('Level', 'level'); ?> <span class="required">*</span>
    <?php echo form_dropdown('level', $levels, set_value('level', isset($schools['level']) ? $subject['level'] : ''))?>
</div>

<div class="text-right">
	<input type="submit" name="submit" value="Create Subject" /> or <?php echo anchor(SITE_AREA .'/content/subjects', lang('subjects_cancel')); ?>
</div>
<?php echo form_close(); ?>
