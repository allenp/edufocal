<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 


if(isset($topic) ) {
	$topic = (array)$topic;
}

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<input type="hidden" name="subject_id" value="<?php print $subject->id ?>" />
<div>
    <?php echo form_label('Name', 'name'); ?> <span class="required">*</span>
    <input id="name" type="text" name="name" maxlength="255" value="<?php echo set_value('name', isset($topic['name']) ? $topic['name'] : ''); ?>"  />
</div>
<div>
    <?php echo form_label('Perma Link', 'perma'); ?> <span class="required">*</span>
    <input id="permalink" type="text" name="permalink" maxlength="255" value="<?php echo set_value('permalink', isset($topic['permalink']) ? $topic['permalink'] : ''); ?>"  />
</div>

<div class="text-right">
	<input type="submit" name="submit" value="Create Topic" /> or <?php echo anchor(SITE_AREA .'/content/subjects', "Cancel"); ?>
</div>
<?php echo form_close(); ?>
