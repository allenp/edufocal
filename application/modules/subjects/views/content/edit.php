<div id="content" class="view">
<div id="ajax-content">
<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 

if( isset($subject) ) {
	$subject = (array)$subject;
}
$id = isset($subject['id']) ? "/".$subject['id'] : '';
?>
<div class="well well-shadow ">
<a class="btn btn-danger delete" id="delete-me" href="<?php echo site_url(SITE_AREA .'/content/subjects/delete/'. $id); ?>" onclick="return confirm('<?php echo lang('subjects_delete_confirm'); ?>')"><?php echo lang('subjects_delete_record'); ?></a>
<a class="btn btn-info ajaxify" href="<?php echo site_url(SITE_AREA .'/content/subjects/create_topic/' . $subject['id'])?>">New Topic</a>
</div>

<?php
echo form_open($this->uri->uri_string(), 'class="form-horizontal ajax-form"'); ?>
    <div class="control-group">
        <?php echo form_label('Name *', 'name'); ?>
        <div class="controls">
            <input id="name" type="text" name="name" maxlength="255" value="<?php echo set_value('name', isset($subject['name']) ? $subject['name'] : ''); ?>"  />
        </div>
    </div>
    <div class="control-group">
        <?php echo form_label('Abbrev *', 'abbrev'); ?>
        <div class="controls">
            <input id="abbrev" type="text" name="abbrev" maxlength="255" value="<?php echo set_value('abbrev', isset($subject['abbrev']) ? $subject['abbrev'] : ''); ?>"  />
        </div>
    </div> 
    <div class="control-group">
        <?php echo form_label('Perma Link *', 'perma'); ?>
        <div class="controls">
            <input id="perma" type="text" name="perma" maxlength="255" value="<?php echo set_value('permalink', isset($subject['permalink']) ? $subject['permalink'] : ''); ?>"  />
        </div>
    </div>

    <div class="control-group">
        <?php 
        $options = array(
            'cxc' => 'CXC',
            'gsat' =>  'GSAT',
        ); ?>
        <?php echo form_dropdown('level', $options, set_value('level', isset($subject['level']) ? $subject['level'] : ''))?>
    </div>
<div class="form-actions">
    <input class="btn btn-primary" type="submit" name="submit" value="Update subject" /> or <?php echo anchor(SITE_AREA .'/content/subjects', 'Cancel'); ?>
</div>

<?php echo form_close(); ?>


<h4>Topics</h4>
<?php if(isset($topics) && count($topics)) : ?>
<table class="table table-hover table-stripe table-bordered">
    <thead>
    <th>Name</th>
    <th>Actions</th>
    </thead>
    <tbody>
    <?php
    foreach($topics as $topic) : ?>
    <tr>
    <td><?php print $topic->name ?></td>
    <td><?php print anchor(SITE_AREA . '/content/subjects/topic/' . $topic->id, 'Edit', 'class="ajaxify"') ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif; ?>
</div>
</div>
