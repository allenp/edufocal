
			<div class="box create rounded">
				<a class="button good" href="<?php echo site_url('/admin/content/schools/create')?>"><?php echo lang('schools_create_new_button');?></a>

				<h3><?php echo lang('schools_create_new');?></h3>

				<p><?php echo lang('schools_edit_text'); ?></p>
			</div>
			<br />
				<?php if (isset($records) && is_array($records) && count($records)) : ?>
				
					<h2>Schools</h2>
	<table>
		<thead>
		<th>Name</th>
		<th>Type</th><th><?php echo lang('schools_actions'); ?></th>
		</thead>
		<tbody>
<?php
foreach ($records as $record) : ?>
<?php $record = (array)$record;?>
			<tr>
<?php
	foreach($record as $field => $value)
	{
		if($field != "id") {
?>
				<td><?php echo $value;?></td>

<?php
		}
	}
?>
				<td><?php echo anchor('admin/content/schools/edit/'. $record['id'], 'Edit', '') ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
				<?php endif; ?>
