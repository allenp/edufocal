<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller
{
	function __construct()
	{
 		parent::__construct();

        $this->auth->restrict('Subjects.Content.View');
        $this->load->model('subjects_model');
        $this->load->model('topics_model');
        $this->lang->load('subjects');

        Assets::add_js($this->load->view('content/js', null, true), 'inline');
	}
	
	public function index ()
	{
        $level = $this->input->get('level');
        if(!isset($level) || trim($level) === '')
            $level = $this->session->userdata("level");

        if (!isset($level) || trim($level) === '')
            $level = 'cxc';

        $this->session->set_userdata("level", $level);

		$subjects = Subject::find_all_by_level($level);

		Template::set_view("content/index");
		Template::set('subjects', $subjects);
		Template::set('level', $level);
		Template::set("toolbar_title", "Edit Subjects");
		Template::render();
	}

    //-------------------------------------------------------------------------

    public function create()
    {

        //$this->auth->restrict('Subjects.Content.Create');

        if($this->input->post('submit'))
        {
            if($this->save_subject())
            {
                //on success
                Template::redirect(SITE_AREA . '/content/subjects');
            }
            else
            {
                //on error
            }
        }

        Template::set_view('content/create');
        Template::set("toolbar_title", "New Subject");
        Template::render();
       //do other stuff here
    }

    //------------------------------------------------------------------------

    public function edit()
    {
        $this->auth->restrict('Subjects.Content.Edit');

        $id = (int)$this->uri->segment(5);

        if(empty($id))
        {
            Template::set_message(lang("subjects_invalid_id"), 'error');
            redirect(SITE_AREA . 'content/subjects');
        }

        if($this->input->post('submit'))
        {

            if ($this->save_subject('update', $id))
            {
                Template::set_message(lang("subjects_edit_success"), 'success');
            }
            else
            {
                Template::set_message(lang("subjects_edit_failure") . $this->subjects_model->error, 'error');
            }
        }

        $data = array();
        $level = $this->session->userdata("level");
        if(!isset($level) || trim($level) === '')
            $level = 'CXC';


		$data["subjects"] = Subject::find_all_by_level($level);
        $data["level"] = $level;

		Template::set("data", $data);
        Template::set('subject', $this->subjects_model->find($id));
        Template::set('topics', Topic::all(array('conditions' => array('subject_id' => $id))));
        Template::set_view('content/edit');
        Template::set("toolbar_title", "Subjects");
        Template::render();
    }

    public function topic()
    {
        $this->auth->restrict('Subjects.Content.Edit');

        $id = (int)$this->uri->segment(5);

        if(empty($id))
        {
            Template::set_message("Invalid topic id", "error");
            Template::redirect(SITE_AREA . '/content/subjects');
            return;
        }

        if($this->input->post('submit'))
        {
            if($this->save_topic('update', $id))
                Template::set_message("Topic updated!", "success");
            else
                Template::set_message("Failed to save topic: " . $this->topics_model->error, "error");
        }

        $topic = $this->topics_model->find($id); 
        $subject = Subject::find($topic->subject_id);

        Template::set('subject', $subject);
        Template::set('topic', $topic);
        Template::set_view("content/edit_topic");
        Template::set("toolbar_title", "Edit Topic");
        Template::render();
    }

  //-------------------------------------------------------------------------

    public function create_topic()
    {

        //$this->auth->restrict('Subjects.Content.Create');

        $id = (int)$this->uri->segment(5);

        if($this->input->post('submit'))
        {
            if($this->save_topic())
            {
                //on success
                Template::set_message("Topic successfully added!", 'success');
                Template::redirect(SITE_AREA . '/content/subjects/edit/' . $id);
            }
            else
            {
                //on error
            }
        }

        Template::set_view('content/create_topic');
        Template::set('subject', Subject::find($id));
        Template::set("toolbar_title", "New Topic");
        Template::render();
       //do other stuff here
    }

  
    private function save_subject( $op = 'insert', $id = 0)
    {
        $this->form_validation->set_rules('name','Name','required|max_length[255]');
        $this->form_validation->set_rules('abbrev','Abbrev','required');
        $this->load->helper('url');
        if ($this->form_validation->run() === false)
        {
            return false;
        }

        $new = array(
            'level' => $this->input->post('level'),
            'abbrev' => $this->input->post('abbrev'),
            'name' => $this->input->post('name'),
            'permalink' => $this->input->post('permalink')
        );

        if ($op == 'insert')
        {
            $subject = Subject::create($new);
            $id = $subject->id;

            if (is_numeric($id))
            {
                $return = true;
            } else
            {
                $return = false;
            }
        }
        else if ($op == 'update')
        {
            $subject = Subject::find($id);
            $subject->update_attributes($new);
            $return = true;
        }
        return $return;
    }

    private function save_topic($op = 'insert', $id = 0)
    {
        $rules = array();
        $rules['name'] = array('Name', 'required|max_length[255]');
        $rules['subject_id'] = array('Subject', 'required');
        $rules['permalink'] = array('Permalink', 'required');

        foreach($rules as $name => $rule)
            $this->form_validation->set_rules($name,$rule[0], $rule[1]);

        if ($this->form_validation->run() === false)
            return false;

        $new = array(
            'name' => $this->input->post('name'),
            'subject_id' => $this->input->post('subject_id'),
            'permalink' => $this->input->post('permalink')
        );

        if ($op == 'insert')
        {
            $topic = Topic::create($new);
            $id = $topic->id;
            return is_numeric($id);
        }
        else if ($op == 'update')
        {
            $topic = Topic::find($id);
            $topic->update_attributes($new);
            return true;
        }
    }
}
