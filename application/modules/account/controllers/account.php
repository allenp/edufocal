<?php

use EduFocal\Code\CodePurchase;
use EduFocal\Code\CodePurchaseValidation;
use EduFocal\Code\CodeRedemption;
use EduFocal\Code\CodeRedemptionValidation;
use EduFocal\Subscription\Subscriber;
use EduFocal\Support\Listener;

class Account extends EF_Authenticated_Controller
{
	public function __construct ()
	{
		parent::__construct();

		switch ( $this->auth->role_id() )
		{
		case EDITOR_ROLE:
		case TEACHER_ROLE:
			Template::set_theme('teachers');
			break;
		case REVIEWER_ROLE:
			Template::set_theme('reviewer');
			break;
		case STUDENT_ROLE:
		default:
			Template::set_theme('dashboard');
			break;
		}

		if (! class_exists('User_model')) {
			$this->load->model('users/User_model', 'user_model');
		}

		Assets::add_js(array(
			site_url('/assets/js/typeaheadjs/typeahead.bundle.min.js')
		));
	}

	public function index ()
	{
		if (isset($_POST['submit']) || isset($_POST['exit'])) {
			$this->save();
		}

		$schools = School::find('all', array('conditions' => array('level = ?', $this->auth->profile()->testing_level()), 'order' =>  'type asc, name asc'));
		$schools_options = array();
		$autos = [];
		foreach ($schools as $school) {
			$schools_options[$school->type][$school->id] = $school->name;
			$autos[] = $school->name;
		}

		Assets::add_js(
			$this->load->view('account/_schools', array('schools' => $autos), true), 'inline');

		Template::set('schools', $schools_options);
		Template::set('title', 'Account Settings - EduFocal');
		Template::set('user', $this->auth->profile());
		Template::render();
	}

	/**
	 * Save Account Settings
	 */
	public function save ()
	{
		foreach ($this->auth->rules as $info) {
			$this->form_validation->set_rules(
				$info['field'],
				$info['label'],
				$info['rules']
			);
		}

		if ($this->form_validation->run() === TRUE) {

			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png';

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('avatar')) {
				$upload = $this->upload->data();
				$resize['source_image']	= $upload['full_path'];
				$resize['maintain_ratio'] = TRUE;
				$resize['width']	 = 128;
				$resize['height']	= 128;

				$this->load->library('image_lib', $resize);
				$this->image_lib->resize();
				$avatar = $upload['file_name'];
			} else {
				$avatar = $this->auth->profile()->avatar;
			}

			$user = User::find($this->auth->user_id());

			if ($user->notification != $this->input->post('notification')) {
				$notification = $this->input->post('notification');

				if($notification == 'yes') {
					$this->add_notification($user);
				} else {
					$this->remove_notification($user);
				}

				$user->notification = $notification;
			}

			$user->first_name = $this->input->post('first_name', TRUE);
			$user->last_name = $this->input->post('last_name', TRUE);
			$user->sex = $this->input->post('sex', TRUE);
			$user->phone = $this->input->post('phone', TRUE);
			if($this->input->post('school')) {
				$school = School::find_by_name($this->input->post('school'));
				if ($school != null) {
					$user->school_id = $school->id;
				}
			}

			if ($this->input->post('email') != $user->email) {
				$exists = $this->db->where('email =', $this->input->post('email'))
					->where('id !=', $user->id)
					->from('users')
					->count_all_results();

				if($exists == 0) {
					$user->email = $this->input->post('email');
				}
			}

			$user->birthday = new DateTime();
			$user->birthday->setDate(
				$this->input->post('birth_year', TRUE),
				$this->input->post('birth_month', TRUE),
				$this->input->post('birth_day', TRUE)
			);

			$user->avatar = $avatar;
			$user->save();

			$teacher = Teacher::find_by_user_id($this->auth->user_id());

			if(count($teacher) > 0) {
				$teacher->user_picture = $avatar;
				$teacher->save();
			}

			Template::set_message('Changes saved successfully', 'success');
			redirect('account');
		}
	}

	public function password()
	{
		$this->load->helper('security');

		$rules = array(
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
			),
			array(
				'field' => 'new_password',
				'label' => 'New Password',
				'rules' => 'required'
			),
			array(
				'field' => 'confirm_password',
				'label' => 'Confirm Password',
				'rules' => 'required|matches[new_password]'
			),
		);

		foreach ( $rules as $info )
			$this->form_validation->set_rules($info['field'], $info['label'], $info['rules']);

		if ($this->form_validation->run() === TRUE) {
			if (! empty($_POST['password']) && $this->auth->profile()->password_hash == do_hash($this->auth->profile()->salt . $this->input->post('password'))) {
				$update['password'] = $this->input->post('new_password');
				$update['pass_confirm'] = $this->input->post('confirm_password');
				$this->user_model->update($this->auth->user_id(), $update);
				Template::set_message('Password changed successfully', 'success');
			} else {
				Template::set_message('Invalid current password', 'error');
			}
		}

		Template::set('title', 'Account Settings - EduFocal');
		Template::render();

	}

	public function receipts($id = '')
	{
		if($id != '')
		{
			//display one receipts
			$receipt = Receipt::find_by_id_and_user_id($id,$this->auth->user_id());
			Template::set('receipt', $receipt);
			Template::set('title', 'Payment Receipt - EduFocal');
			Template::set_view('account/receipt');
			Template::render();
			return;
		}
		else
		{
			//display all receipts
			$codes = Code::find('all', array('conditions' => array('user_id' => $this->auth->user_id())));
			Template::set('codes', $codes);
			Template::set('title', 'Receipts - EduFocal');
			Template::render();
		}
	}

	public function receipt_pdf($id = '')
	{
		if($id != '')
		{
			//download as pdf.
			$receipt = Receipt::find_by_id_and_user_id($id, $this->auth->user_id());
			if($receipt != null)
				$html = $this->load->view('receipt_pdf', array('receipt' => $receipt), TRUE);
			if(isset($html) && strlen($html) > 0)
			{
				$this->load->helper('dompdf');
				pdf_create($html, $this->clean_filename($receipt->card_holder .'_EduFocal_Receipt'. $receipt->id), TRUE);
			}
			return;
		}
	}

	private function clean_filename($filename)
	{
		$filename = str_replace(' ', '_', $filename);
		$filename = preg_replace('![^0-9A-Za-z_.-]!', '', $filename);
		$filename = strtolower($filename);
		return $filename;
	}

	private function add_notification($user_data) {
		$config = array(
			'apikey' => '6cb450b898a52643f1604b9cf8fea02d-us2',      // Insert your api key
			'secure' => FALSE   // Optional (defaults to FALSE)
		);
		$this->load->library('MCAPI', $config, 'mail_chimp');

		$merge_vars = array(
			'FNAME' => ucwords($user_data->first_name),
			'LNAME' => ucwords($user_data->last_name),
			'PHONE' => $user_data->profile('phone'),
			'SCHOOL' => $user_data->school->name,
			'GENDER' => $user_data->profile('sex')
		);

		$this->mail_chimp->listSubscribe('698fab3a3d', $this->auth->email(), $merge_vars);
	}

	private function remove_notification($user_data) {
		$config = array(
			'apikey' => '6cb450b898a52643f1604b9cf8fea02d-us2',      // Insert your api key
			'secure' => FALSE   // Optional (defaults to FALSE)
		);
		$this->load->library('MCAPI', $config, 'mail_chimp');

		$this->mail_chimp->listUnsubscribe('698fab3a3d', $this->auth->email(), true, false, false);
	}

	public function subscribe()
	{
		$config = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );
		$plan = $this->input->post('subscriber-plan');
		$card = $this->input->post('subscriber-card');
		$currency = $this->input->post('subscriber-currency');
		$charge = $this->config->item($config[$currency]);
		$cost = $charge[$plan];

		$sub = new Subscriber($this->auth->profile());
		$sub->subscribe($plan, $card, $currency, $cost['charge']);


		Template::set_message(ucfirst($plan).' subscription updated sucessfully', 'success');
		redirect('account/payment');
	}

	public function unsubscribe()
	{
		$sub = new Subscriber($this->auth->profile());
		$sub->unsubscribe();

		Template::set_message('Your subscription has been cancelled.', 'success');
		redirect('account/payment');
	}

	public function payment ()
	{
		$rule_group = '';

		if (count($_POST) > 0) {
			if (isset($_POST['subscribe_option'])) {
				$this->subscribe();
			}

			$option = 'code';
			$rule_group = 'edufocal_code';
		}

		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );


		$validator = new CodeRedemptionValidation();

		if ($validator->validate($this)) {
			$processor = new CodeRedemption(
				$validator->getUserInput(), $this->auth->profile(), Listener::get());
			$result = $processor->process();

			if ($result->success) {
				$code = $result->code;
				$this->session->set_userdata('paid.member', FALSE); //invalidate cached permission

				$msg = 'Thank you! You can view your current and past receipts on the <a href="' .
					site_url('account/receipts') . '">payments history page</a>';
				$status_type = 'success';
				Template::set_message($msg, $status_type);

				redirect('account/payment');
			}
		}

		$current_code = $this->auth->profile()->code;

		if ($this->auth->is_expired()) {
			if ($current_code != null)
				Template::set_message('Your subscription expired on ' . date('F j, Y', $current_code->expiry_date) , 'error');
			else
				Template::set_message('You have to subscribe to access EduFocal. Use an option below.', 'error');
		} else {
			if ($current_code != null) {
				$unused = Code::all(array('conditions' => array('user_id' => $this->auth->user_id(), 'accepted_on' => 0)));
				$expiry_date = $current_code->expiry_date;
				if (count($unused) > 0) {
					$code_charges = $this->config->item('educodes');
					foreach ($unused as $c) {
						$code_charge = isset($code_charges[$c->type]) ? $code_charges[$c->type] : null;
						if ($code_charge != null) {
							if($c->expiry_date <= 0) {
								$valid_for = $code_charge['valid_for'];
								$expiry_date = strtotime('+ ' . $valid_for, $expiry_date);
							} elseif ($c->expiry_date > $expiry_date) {
								$expiry_date = $c->expiry_date;
							}
						}
					}
				}
			} else {
				$expiry_date = time();
			}

			$msg = '<h2>Your current subscription will expire on ' . date('F j, Y', $expiry_date) . '</h2>';
			$status_type = 'success';

			Template::set_message($msg, $status_type);
		}

		$currencies = array();
		foreach ($configs as $curr => $key) {
			$currencies[$curr] = $this->config->item($key);
		}

		$subscriber = Subscription::find_by_user_id($this->auth->user_id());
		$cards = $this->preferred_cards();
		Assets::add_js($this->load->view('account/payment_script', array('currency' => $currencies),  true), 'inline');

		Template::set('currencies', $currencies);
		Template::set('cards', $cards);
		Template::set('subscriber', $subscriber);
		Template::set('is_subscribe', NULL !== $subscriber);
		Template::set('title', 'Payment Options - EduFocal');
		Template::render();
	}

	private function payment_get()
	{
		$this->load->helper('country_dropdown');
		$currencies = array();
		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

		foreach ($configs as $curr => $key) {
			$currencies[$curr] = $this->config->item($key);
		}


		if (isset($option)) {
			Template::set('option', $option);
		}

		Assets::add_js($this->load->view('account/payment_script', array('currency' => $currencies),  true), 'inline');

		Assets::add_module_js('account', 'confirm.js');

		$subscriber = Subscription::find_by_user_id($this->auth->user_id());

		Template::set('is_subscribe', NULL !== $subscriber);
		Template::set('currencies', $currencies);
		Template::set('title', 'Payment Options - EduFocal');
		Template::render();
	}


	/**
	 * Payment page of the signup process.
	 */
	public function card ()
	{
		if($_SERVER['REQUEST_METHOD'] === 'GET') {
			return $this->payment_get();
		}

		if (!$this->input->post('confirm')) {
			return $this->confirm();
		}

		$validator = new CodePurchaseValidation(
			$this->auth->profile(),
			'account'
		);

		if ($validator->validate($this)) {
			$processor = new CodePurchase(
				$validator->getUserInput(), $this->auth->profile(), Listener::get());

			$result = $processor->process();
			if (null != $result && $result->success == true) {
				$code = $result->code;
			}
		}

		//failed. show error messages on payment page.
		if ( !isset($code) || null == $code) {
			return $this->payment_get();
		}

		$this->session->set_userdata('paid.member', FALSE);

		//TODO: Move all this code into subscription
		//Handler on PAYMENT_SUCCESS
		if ($this->input->post('subscription') === 'on') {
			$sub = new Subscriber($code->user);
			$sub->subscribe(
				$code->type, $result->card->redacted,
				$code->currency, $code->value);

		}

		$msg = 'Thank you! You can view your current and past receipts on the <a href="' .
			site_url('account/receipts') . '">payments history page</a>';
		$status_type = 'success';
		Template::set_message($msg, $status_type);
		redirect('account/payment');
	}

	private function confirm()
	{
		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );
		$plan = $this->input->post('plan');
		$currency = $this->input->post('currency');
		$config_key = $configs[$currency];
		$charges = $this->config->item($config_key);

		$amount = $charges[strtolower($plan)]['charge'];

		$hidden = array(
			'confirm' => 'true',
			'plan' => $plan,
			'currency' => $currency,
			'charge' => $amount,
			'card_number' => $this->input->post('card_number'),
			'card_name' => $this->input->post('card_name'),
			'card_cvc' => $this->input->post('card_cvc'),
			'card_expiry_month' => $this->input->post('card_expiry_month'),
			'card_expiry_year' => $this->input->post('card_expiry_year'),
			'option' => $this->input->post('option'),
			'address1' => $this->input->post('address1'),
			'address2' => $this->input->post('address2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'country' => $this->input->post('country'),
			'subscription' => $this->input->post('subscription')
		);

		Assets::add_module_js('account', 'confirm.js');
		Template::set_view('confirm');
		Template::set('hidden', $hidden);
		Template::set('title', 'Confirm your purchase - EduFocal');
		Template::render();
	}

	private function setup_payment_view()
	{
		$this->load->helper('country_dropdown');

		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

		$currencies = array();
		foreach($configs as $curr => $key)
			$currencies[$curr] = $this->config->item($key);

		Assets::add_js($this->load->view('account/payment_script', array('currency' => $currencies),  true), 'inline');
		Template::set('currencies', $currencies);
	}

	public function _valid_card($number)
	{
		return valid_card($number, $this);
	}

	public function _valid_efcode(&$input)
	{
		return valid_efcode($input, $this);
	}

	private function preferred_cards()
	{
		$stored = Card::all(array('user_id' => $this->auth->user_id()));
		$cards = array();

		foreach ($stored as $card) {
			$cards[] = $card->redacted;
		}

		return $cards;
	}

	private function has_paid()
	{
		if ($this->session->userdata('has.paid') === FALSE) {
			$payment = Receipt::find(
				'first',
				array('condtions' => array('user_id = ?', $this->auth->user_id()))
			);
			$this->session->set_userdata('has.paid', 1);
		}

		return $this->session->userdata('has.paid') == 1;
	}
}
