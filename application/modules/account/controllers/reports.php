<?php

class Reports extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->db->select('subscriptions.*,users.first_name, users.last_name')
            ->join('users', 'subscriptions.user_id = users.id');
        $subscribers = $this->db->get('subscriptions')->result();
        Template::set('subscribers', $subscribers);
        Template::set_view('reports/subscription');
        Template::render();
    }
}
