<?php

/**
 *
 *  cron task to find users that have no code_id
 *  in the first two weeks of signup and offer 
 *  a free one week code. 
 *
 * */
class TrialCron extends EF_Cron_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    //index.php account/usertrial/offer
    public function offer()
    {
        print date(\DateTime::RSS) . ' - starting trial cron job' . PHP_EOL;

        //get amount of offer as per config section offering load
        //
        $max_load = 10;

        $users = User::all(
            array(
                'conditions' => array(
                    'code_id <= 0 and created_on > DATE_SUB(NOW(), INTERVAL 14 DAY)
                    and created_on < DATE_SUB(NOW(), INTERVAL 1 DAY)
                    and id not in (select user_id from user_trials)'
                ),
                'limit' => $max_load
            )
        );

        if (count($users) == 0) {
            return;
        }

        //notify users of their trial
        foreach ($users as $user) {

            $code = Code::Generate( array(
                'type' => 'weekly',
                'value' => 1,
                'currency' => 'JMD',
                'active' => 1,
            ));

            UserTrial::Create(
                array(
                    'user_id' => $user->id,
                    'code_id' => $code->id,
                )
            );

            $this->load->library('emailer/emailer');
            $info = array(
                'to' => $user->email,
                'subject' => 'EduFocal: We noticed you didn\'t finish signing up!',
                'message' => $this->load->view('free_trial',
                array(
                    'name' => $user->first_name . ' ' . $user->last_name, 
                    'code' => $code->code
                ), 
                true)
            );

            $this->emailer->queue_emails = true;
            $this->emailer->send($info);
        }
    }
}
