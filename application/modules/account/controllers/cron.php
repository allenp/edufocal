<?php

use EduFocal\Code\CodePurchase;
use EduFocal\Support\Listener;
use EduFocal\Payment\PaymentOptions;

use EduFocal\User;

/**
 *
 *  Subscription auto renew payment
 *  TODO: Rewrite this under the Subscription namespace
 *
 * */
class Cron extends EF_Cron_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    //index.php account/cron/renew
    public function renew()
    {

        print PHP_EOL . date(\DateTime::RSS) . ' - starting subscription renewal job.'. PHP_EOL;

        //get amount of renewal as per config section renewal load
        //
        $max_fail_attempts = 3;
        $max_load = 10;

        $configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

        $subscriptions = \Subscription::all(
            array(
                'conditions' => array(
                    'subscription_end_dt <= date_add(CURDATE(), interval 1 day) and failed_attempts < ?',
                    $max_fail_attempts
                ),
                'limit' => $max_load
            )
        );

        if (count($subscriptions) == 0) {
			print 'no subscriptions found to run' . PHP_EOL;
            return;
		} else {
			print count($subscriptions) . ' found to run' . PHP_EOL;
		}

        //renew each account accoundingly
        foreach ($subscriptions as $subscription) {

			$user = new User\UserSecurityProfile(\User::find($subscription->user_id));
            $timestamp = $user->calculateExpiryDate();

            //will only renew if within one hour of expiration
            if ($timestamp->getTimestamp() > $subscription->subscription_end_dt->getTimestamp()) {
				print '\r\nSubscription time was incorrect. Setting new time to ' . $timestamp;
                $subscription->subscription_end_dt = $timestamp;
                $subscription->save();
                continue;
            }

            $card = \Card::find(
                array(
                    'conditions' => array(
                        'user_id = ? and redacted = ?',
                        $subscription->user_id,
                        $subscription->preferred_card
                    ),
                )
            );

            if ($card === null) { //no card exists, what?
                $subscription->failed_attempts = $max_fail_attempts + 1;
                $subscription->save();

                \SubscriptionRequestLog::create(
                    array(
                        'subscription_id' => $subscription->id,
                        'result' => 'No cards found with preferred card.'
                    )
                );

                continue;
            }

            $code_charges = $this->config->item($configs[$subscription->currency]);
            $plan = strtolower($subscription->type);
            $code_charge = $code_charges[$plan];

            if ($code_charge) {

                $options = new PaymentOptions();
                $options->cardAmount($code_charge['charge'])
                    ->userId($subscription->user_id)
                    ->cardNumber($card->card_no)
                    ->cardName($card->card_name)
                    ->cardCVV($card->cvc)
                    ->cardExp($card->expiry_date)
                    ->currency($subscription->currency)
                    ->email($user->email)
                    ->address1($card->address1)
                    ->address2($card->address2)
                    ->city($card->city)
                    ->state($card->state)
                    ->country($card->country)
                    ->fireSourceEvent(true)
                    ->source('subscription')
                    ->plan($plan);

				$processor = new CodePurchase($options, $user, Listener::get());
				$result = $processor->process();

                if ($result->success !== true) {
                    print '\r\nsubscription payment failed';

                    $subscription->failed_attempts += 1;
                    $subscription->save();

                    \SubscriptionRequestLog::create(
                        array(
                            'subscription_id' => $subscription->id,
                            'result' => $result->response->MErrMsg
                        )
                    );

                    if ($subscription->failed_attempts >= $max_fail_attempts) {
                        $this->load->library('emailer/emailer');
                        $info = array(
                            'to' => $user->email,
                            'subject' => 'EduFocal.com: Payment',
                            'message' => $this->load->view('subscription_fail', array('name' => $card->card_name, 'receipt' => $result->receipt), true)
                        );

						$this->emailer->queue_emails = true;
                        $this->emailer->send($info);
                    }
                } else {
					print '\r\nsubscription payment successful for ' . $subscription->id;
                    $subscription->subscription_end_dt = $user->calculateExpiryDate();
                    $subscription->failed_attempts = 0;
                    $subscription->save();
                }
            }
        }
        //send email as per transaction result
    }
}
