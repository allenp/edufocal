<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_user_trials_table extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'user_id' => array('type' => 'int(11)', 'unsigned' => true),
            'code_id' => array('type' => 'int(11)', 'unsigned' => true),
            'created_at' => array('type' => 'datetime'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'user_trials');
    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix . 'user_trials');
    }
}
