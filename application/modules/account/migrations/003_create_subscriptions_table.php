<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_subscriptions_table extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'user_id' => array('type' => 'int(11)'),
            'type' => array('type' => 'varchar(15)'),
            'preferred_card' => array('type' => 'varchar(26)'),
            'currency' => array('type' => 'varchar(3)'),
            'failed_attempts' => array('type' => 'int(11)', 'default' => 0),
            'created_at' => array('type' => 'datetime', 'null' => true),
            'modified_at' => array('type' => 'datetime', 'null' => true),
            'subscription_end_dt' => array('type' => 'datetime', 'null' => true),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'subscriptions');
  }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix . 'subscriptions');
    }
}
