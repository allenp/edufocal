<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_subscription_reports extends Migration {
	
	public function up() 
	{
        $this->db->query("INSERT INTO `permissions` (
            `permission_id` ,
            `name` ,
            `description` ,
            `status`
        )
        VALUES (
            NULL ,  'Account.Reports.View',  'Allow users to view the subscription reports',  'active'
        );");
	}
	
	public function down() 
	{
        $this->db->query("DELETE FROM permissions WHERE name = 'Subscription.Reports.View'");
	}
}
