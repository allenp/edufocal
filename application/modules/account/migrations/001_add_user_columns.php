<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_user_columns extends Migration {
	
	public function up() 
	{
        $prefix = $this->db->dbprefix;
		$fields = array(
			'birthday' => array('type' => 'datetime', 'null' => true),
            'phone' => array('type' => 'varchar(30)', 'null' => true),
            'notification' => array('type' => 'varchar(5)', 'null' => true),
            'sex' => array('type' => 'varchar(1)', 'null' => true),
		);
		
		$this->dbforge->add_column($prefix.'users', $fields);

        $users = User::all();
        foreach($users as $user) {
            $user->field('sex', $user->profile('gender'));
            $user->field('phone', $user->profile('phone'));
            $user->field('notification', $user->profile('notification'));
            $birth_month = $user->profile('birth_month');
            $birth_day = $user->profile('birth_day');
            $birth_year = $user->profile('birth_year');
            if($birth_month && $birth_day && $birth_year)
            {
                $birth = new DateTime();
                $birth->setDate($birth_year, $birth_month, $birth_day);
                $user->birthday = $birth;
            }
            $user->save();
        }
	}
	
	public function down() 
	{
        $prefix = $this->db->dbprefix;
		$this->dbforge->drop_column($prefix.'users', 'sex');
		$this->dbforge->drop_column($prefix.'users', 'notification');
		$this->dbforge->drop_column($prefix.'users', 'phone');
		$this->dbforge->drop_column($prefix.'users', 'birthday');
	}
}
