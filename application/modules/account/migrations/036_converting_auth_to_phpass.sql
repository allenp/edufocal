ALTER TABLE  `users` CHANGE  `password_hash`  `password_hash` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

 Alter TABLE `users`
 ADD COLUMN `force_password_reset` TINYINT(1) NOT NULL DEFAULT 0 AFTER `password_hash` ,
 ADD COLUMN `password_iterations` TINYINT(4) NOT NULL DEFAULT 8 AFTER `force_password_reset` ;

UPDATE `schema_version`
SET version = 36
WHERE type = 'core';

INSERT INTO `permissions` (`permission_id`, `name`, `description`, `status`) VALUES (NULL, 'Raffles.Content.View', 'View raffles', 'active');
INSERT INTO `permissions` (`permission_id`, `name`, `description`, `status`) VALUES (NULL, 'Teachers.Content.View', 'View teachers', 'active');
INSERT INTO `permissions` (`permission_id`, `name`, `description`, `status`) VALUES (NULL, 'Pages.Content.View', 'View pages', 'active');
INSERT INTO `permissions` (`permission_id`, `name`, `description`, `status`) VALUES (NULL, 'Dashboard.Settings.View', 'View student dashboard settings', 'active');

/* Don't forget to add admin to the new permissions in the backend */
