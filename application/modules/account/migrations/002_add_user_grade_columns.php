<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_user_grade_columns extends Migration {
	
	public function up() 
	{
        $prefix = $this->db->dbprefix;
		$fields = array(
            'grade' => array('type' => 'int', 'null' => true),
		);
		
		$this->dbforge->add_column($prefix.'users', $fields);

        $users = User::all();
        foreach($users as $user)
        {
            $user->grade = $user->profile('grade');
            $user->save();
        }
	}
	
	public function down() 
	{
        $prefix = $this->db->dbprefix;
		$this->dbforge->drop_column($prefix.'users', 'grade');
	}
}
