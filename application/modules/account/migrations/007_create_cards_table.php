<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_cards_table extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'user_id' => array('type' => 'int(11)', 'unsigned' => true),
            'redacted' => array('type' => 'varchar(26)'),
            'card_no' => array('type' => 'varchar(256)'),
            'card_name' => array('type' => 'varchar(500)'),
            'address1' => array('type' => 'varchar(500)'),
            'address2' => array('type' => 'varchar(500)'),
            'city' => array('type' => 'varchar(500)'),
            'state' => array('type' => 'varchar(500)'),
            'country' => array('type' => 'varchar(256)'),
            'expiry_date' => array('type' => 'varchar(256)'),
            'cvc' => array('type' => 'varchar(256)'),
            'updated_at' => array('type' => 'datetime'),
            'created_at' => array('type' => 'datetime'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'cards');
    }

    private function migrate_cards()
    {
    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix . 'cards');
    }
}
