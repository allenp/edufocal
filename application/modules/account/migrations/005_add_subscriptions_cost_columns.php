<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_subscriptions_cost_columns extends Migration {

    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'cost' => array('type' => 'int', 'null' => true),
        );

        $this->dbforge->add_column($prefix.'subscriptions', $fields);


        $configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

        foreach ($configs as $currency => $index) {
            $charges = $this->config->item($index);
            foreach ($charges as $type => $details) {
                $query = "update subscriptions set cost = ? WHERE type = ? and currency = ?";
                $parameters = array($details['charge'], $type, $currency);
                $this->db->query($query, $parameters);
            }
        }
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix.'subscriptions', 'cost');
    }
}
