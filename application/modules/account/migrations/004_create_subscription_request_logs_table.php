<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_subscription_request_logs_table extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'subscription_id' => array('type' => 'int(11)'),
            'result' => array('type' => 'nvarchar(255)', 'null' => true),
            'created_at' => array('type' => 'datetime', 'null' => true)
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'subscription_request_logs');
    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix . 'subscription_request_logs');
    }
}
