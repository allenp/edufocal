$(function(){
var schools = <?php print json_encode($schools) ?>;
var shound = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // `states` is an array of state names defined in "The Basics"
  local: $.map(schools, function(school) { return { value: school }; })
});

// kicks off the loading/processing of `local` and `prefetch`
shound.initialize();

$('.typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'school',
  displayKey: 'value',
  // `ttAdapter` wraps the suggestion engine in an adapter that
  // is compatible with the typeahead jQuery plugin
  source: shound.ttAdapter()
});
});
