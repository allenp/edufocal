<?php
    Assets::add_module_js('account', 'settings.js');
?>
<div class="settings">
<!--    <div class="heading hidden-xs">
        <p>Here you can manage your personal information, subscription and see payment history.</p>
    </div>
-->
    <?php print Template::message(); ?>
    <?php echo form_open_multipart("account", array("id" => "save-settings")); ?>
    <?php if ( validation_errors()) : ?>
        <div class="alert alert-warning">
            <h4>Please check the following before continuing:</h4>
            <?php echo validation_errors(); ?>
        </div>
    <?php endif; ?>
    <h3>Personal Information</h3>

    <div class="col-xs-12 col-sm-5 pull-right">
        <div class="form-group">
        <?php $avatar = $user->avatar;
        if(empty($avatar)) : ?>
            <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" class="img" />
        <?php else : ?>
            <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" class="img" />
        <?php endif; ?>
        </div>
        <div class="form-group">
            <label class="control-label">Upload a new Profile Image</label>
            <small>jpg, png, gif</small>
        <?php echo form_upload('avatar'); ?>
        </div>
        <div class="form-group">
            <?php print form_submit('submit', 'Save changes', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>

    <div class="opaque col-xs-12 col-sm-7 pull-left">
        <div class="nothing">
        <div class="row">
        <div class="col-xs-6">
        <?php print form_input('first_name', $user->first_name, 'First Name', array('placeholder' => 'First name')) ?>
        </div>
        <div class="col-xs-6">
        <?php print form_input('last_name', $user->last_name, 'Last Name', array('placeholder' => 'Last name')) ?>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-6 col-sm-4">
        <?php echo form_dropdown('sex', array('m' => 'Male', 'f' => 'Female'), $user->sex, 'Sex', array('class' => 'form-control')); ?>
        </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            <label class="control-label">Date of Birth:</label></td>
            </div>
            <div class="col-xs-3 dob-fields">
            <?php $b = $this->auth->profile()->birthday; ?>
            <?php $b = $b == null ? new \DateTime() : $b; ?>
            <?php for($i = 1; $i <= 12; $i++) $date[$i] = $i;
            echo form_dropdown('birth_month', $date, $b->format('m'), '', array('class' => 'form-control')); ?>
            </div>
            <div class="col-xs-3 dob-fields">
            <?php for($i = 13; $i <= 31; $i++) $date[$i] = $i;
            echo form_dropdown('birth_day', $date, $b->format('j'), '', array('class' => 'form-control')); ?>
            </div>
            <div class="col-xs-3 dob-fields">
            <?php for($i = date("Y"); $i >= date("Y")-100; $i--) $year[$i] = $i;
            echo form_dropdown('birth_year', $year, $b->format('Y'), '', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="row">
        <div class="col-xs-12">
            <?php print form_email('email', $user->email, 'Email'); ?>
            <?php print form_input('phone', $user->phone, 'Phone'); ?>
            <?php //echo form_dropdown('school', $schools, $this->auth->profile()->school_id, 'School', array('class' => 'form-control')); ?>
<?php print form_input('school', '', 'School (currently <strong>'.  $user->school->name .'</strong>)', array('class' => 'form-control typeahead', 'placeholder' => 'Enter new school name to change')) ?>
        </div>
        </div>
        <div class="form-group clearfix">
            <label class="control-label">Would you like to receive emails from EduFocal? These contain tips/tricks and special offers.</label>
            <div class="form-static col-xs-3 row">
            <label>Yes <?php echo form_radio('notification', 'yes', $user->notification == 'yes' ? TRUE : FALSE); ?></label>
            </div>
            <div class="form-static col-xs-3">
            <label>No <?php echo form_radio('notification', 'no', $user->notification == 'no' ? TRUE : FALSE); ?></label>
            </div>
        </div>
        <div class="form-group">
        <?php print form_submit('submit', 'Save Changes', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
    </div>
    <?php print form_close(); ?>
</div>
