<div id="main" class="clearfix">
<div class="heading padded">
    <div class="section">
        <p>Your account has expired, please renew to continue to enjoy edufocal.</p>
    </div>
</div>
<div class="settings columns clearfix">
    <div class="main-content column">
        <div class="section">
            <div id="expired">
            	<?php echo Template::message(); ?>

			<?php if(validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>

            <h3>Your bills are due!</h3>
            <p>Hello, <?php print $user->first_name ?></p>
            <p>It appears your access to EduFocal has expired. It expired on <?php print date('j M Y', $code->expiry_date); ?>.</p>
            <p>To continue receiving access, <a href="<?php print site_url('account/payment') ?>">please renew your subscription.</a></p>
            	
                <?php $this->load->view('account/pay', array('currencies' => $currencies)); ?>
            </div>
        </div>
    </div>
    <div class="navigation column">
        <div class="section">
        <?php $this->load->view('partials/nav'); ?>
        </div>
    </div>
    </div>
</div>
</div>
