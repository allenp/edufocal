<?php print form_open(site_url('account/payment'), array('id' => 'payment-form', 'class' => 'form-horizontal')); ?>
<?php if($this->auth->is_expired()) : ?>
<p>Hello <?php print $this->auth->profile()->first_name ?>, we're sorry to say, but your EduFocal membership has expired. Don't worry though. You can find your new EduFocal access code in the <strong>Sunday Observer</strong>. You may also use the credit card option below to renew your membership. If you need help please contact us via the support tab to the left or email us at help@edufocal.com</p>
<?php endif; ?>

<div class="opaque pull-right col-xs-12 col-md-5">
    <div class="trench">
        <div class="row">
            <h4><label for="code_option">Use EduFocal Access Code</label></h4>
        </div>
        <?php print form_input('educode', set_value('educode'), '', array('class' => 'form-control educode-field', 'placeholder' => 'Enter EduFocal code here')); ?>
        <div class="form-group">
        <?php print form_submit('code_option', 'Accept Code', array('class' => 'btn btn-success btn-lg')); ?>
        </div>
    </div>
</div>

<div class="pull-right col-xs-12 col-md-1" align="center">
    <p>OR</p>
</div>

<div class="opaque pull-left col-xs-12 col-md-6">
    <div class="trench">
        <div class="row">
            <h4><label for="code_option">Pay with Credit Card</label></h4>
        </div>
        <div class="form-group">
            <div class="row col-lg-12">
            <label for="plan" class="control-label">Pay to access for:</label>
            </div>
<?php 
$curr = array_keys($currencies);
$curr = $curr[0];
$def = array_values($currencies); 
$def = $def[0];
?>
            <div class="col-xs-6 col-sm-4 row">
            <select id="plan" name="plan" class="form-control">
            <?php foreach($def as $term => $item) : ?>
            <?php if(!isset($item['show_publicly']) || $item['show_publicly'] == TRUE) : ?>
                <option value="<?php print $term ?>"><?php print $item['valid_for']; ?> - <?php print $curr; ?> $<?php print $item['charge'] ?></option>
            <?php endif; ?>
            <?php endforeach; ?>
            </select>
            </div>
            <div class="col-xs-6 col-sm-4">
            <select id="currency" name="currency" class="form-control">
                <?php foreach(array_keys($currencies) as $cu) : ?>
                <option value="<?php print $cu; ?>"><?php print $cu; ?></option>
                <?php endforeach; ?>
            </select>
            </div>
            
        </div>
        <div class="form-group">
            <p>We Accept</p>
            <img src="<?php echo site_url(); ?>/assets/img/credit2.png" alt="Visa, Mastercard, Keycard" width="141" height="25" />
        </div>
        <div class="form-group">
            <div class="col-lg-12 row">
                <label for="card_number" class="control-label">Credit Card Number</label>
            </div>
            <div class="col-xs-9 row">
                <input id="card_number" name="card_number" class="form-control" autocomplete="off" value="" />
            </div>
            <div class="col-xs-3">
                <input id="card_cvc" name="card_cvc" autocomplete="off" class="form-control card-cvc" size="4" placeholder='CVC' />
            </div>
        </div>
            <?php print form_input('card_name', set_value('card_name'), 'Card Holder\'s Name (First &amp; Last)', array('autocomplete' => 'off' , 'class' => 'form-control card-name', 'size' => 40)); ?>
            <div class="form-group">
                <div class="col-lg-12 row">
                <label for="card_expiry_month" class="control-label">Expiry date</label>
                </div>
                <div class="col-xs-6 col-sm-3 row">
                <select name="card_expiry_month" class="card-expiry-month form-control">
                    <option></option>
                    <?php foreach ( range(1, 12) as $r ) : ?>
                    <option><?php echo str_pad($r,2,'0',STR_PAD_LEFT); ?></option>
                    <?php endforeach; ?>
                </select>
                </div>
                <div class="col-xs-6 col-sm-3">
                <select name="card_expiry_year" class="card-expiry-year form-control">
                    <option></option>
                    <?php foreach ( range(11, 20) as $r ) : ?>
                    <option value="<?php print "20" . $r ?>"><?php echo $r; ?></option>
                    <?php endforeach; ?>
                </select>
                </div>
                <div class="col-xs-6">
                <?php if ($is_subscribe === false): ?>
                <div class="row">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="subscription" id="subscription" checked="checked"/>
                        Auto-renew my subscription
                    </label>
                </div>
                </div>
                <?php endif; ?>
                </div>
            </div>
 
        <div class="row">
        <h4>Billing Address</h4>
        </div>
        <?php print form_input('address1', set_value('address1'), '', array('autocomplete' => 'on', 'size' => 40, 'placeholder' => 'Address line 1')); ?>
        <?php print form_input('address2', set_value('address2'), '', array('autocomplete' => 'on', 'size' => 40, 'placeholder' => 'Address line 2')); ?>
        <?php print form_input('city', set_value('city'), '', array('autocomplete' => 'on', 'size' => 40, 'placeholder' => 'City')); ?>
        <?php print form_input('state', set_value('state'), 'State (if applicable)', array('autocomplete' => 'on', 'size' => 40)); ?>
        <?php $countries = array('JM', 'US', 'CA', 'GB'); ?>
        <div class="form-group">
        <label class="control-label" for="country">Country</label>
        <?php print country_dropdown('country', $countries); ?>
        </div>
        <div class="form-group">
        <?php print form_submit('card_option', 'Process Payment', array('class' => 'btn btn-success btn-lg')); ?>
        </div>
    </div>
</div>
<div class="pull-right col-xs-12 col-md-5">
<p></p>
</div>


<div class="opaque pull-right col-xs-12 col-md-5" <?php if(count($cards) == 0) print"style='display:none;'";?>>
    <div class="trench">
        <div class="row">
            <h4><label for="code_option">Subscription</label></h4>
        </div>
<?php if(isset($is_subscribe) && $is_subscribe === true): ?>
<div class="row">
<p>You are currently subscribed to EduFocal on a <?php print $subscriber->type ?> plan. Your subscription is set to renew on <?php print date('Y j d', $subscriber->subscription_end_dt) ?></p>
</div>
<?php endif; ?>
<div class="row"></div>
        <div class="row col-lg-12">
            <div class="form-group">
                <div class="row col-lg-12">
                    <label for="subscriber-plan" class="control-label">Subscribe to access for:</label>
<?php 
  $curr = array_keys($currencies);
  $curr = $curr[0];
  $def = array_values($currencies); 
  $def = $def[0];
?>
                </div>
                <div class="col-xs-6 col-sm-5 row">
                    <select id="subscriber-plan" name="subscriber-plan" class="form-control">
                    <?php foreach($def as $term => $item) : ?>
                    <?php if(!isset($item['show_publicly']) || $item['show_publicly'] == TRUE) : ?>
                        <option value="<?php print $term ?>"><?php print $item['valid_for']; ?> - <?php print $curr; ?> $<?php print $item['charge'] ?></option>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    </select>
                    </div>
                    <div class="col-xs-6 col-sm-5">
                    <select id="subscriber-currency" name="subscriber-currency" class="form-control">
                        <?php foreach(array_keys($currencies) as $cu) : ?>
                        <option value="<?php print $cu; ?>"><?php print $cu; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row col-lg-12">
             <div class="form-group">
                <div class="row col-lg-12">
                    <label for="subscriber-card" class="control-label">Preferred Card:</label>
                </div>
                <div class="col-xs-10 col-sm-8 row">
                        <select id="subscriber-card" name="subscriber-card" class="form-control">
                        <?php foreach($cards as $card) : ?>
                            <option value="<?php print $card ?>"><?php print $card; ?></option>
                        <?php endforeach; ?>
                        </select>
                </div>
             </div>
             <div class="form-group">
<?php print form_submit('subscribe_option', 'Subscribe', array('class' => 'btn btn-success btn-lg')); ?> 
<?php
if ($is_subscribe === true) {
    print ' or ';
    print anchor('account/unsubscribe', 'Unsubscribe');
}
?> 
             </div>
        </div>
   </div>
</div>
<?php print form_close(); ?>
