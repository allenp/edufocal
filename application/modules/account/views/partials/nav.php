<?php
   $scount = $this->uri->total_segments();
   $first = $this->uri->segment(1);
   $second = $this->uri->segment(2);
?>
<ul id="account-menu" class="nav list-unstyled">
    <li <?php print $scount == 1 && $first == 'account' ? 'class="active"' : '' ?>><a href="<?php print site_url("account") ?>">Personal Information</a></li>
    <li <?php print $scount > 1 && ($second == 'payment' || $second == 'expired') ? 'class="active"' : '' ?>><a href="<?php print site_url("account/payment") ?>">Renew Subscription</a></li>
    <li <?php print $scount > 1 && $second == 'receipts' ? 'class="active"' : '' ?>><a href="<?php print site_url("account/receipts") ?>">Payment History</a></li>
    <li <?php print $scount > 1 && $second == 'password' ? 'class="active"' : '' ?>><a href="<?php print site_url("account/password") ?>">Change Password</a></li>
</ul>

