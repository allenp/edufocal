<div class="settings wrapper">
    <?php if(isset($receipt) && $receipt) : ?>
    <table border="0" width="100%" cellpadding="8" class="styled">
    <tr>
    <td colspan="2" align="right">
        <p><a href="<?php print site_url(array('account', 'receipt_pdf', $receipt->id)); ?>" alt="Print receipt">Printable copy</a></p>
    </td>
    </tr>
    <tr>
        <td style="vertical-align: top">
           <p>EduFocal Limited<br />
           Suite #8,<br />
           Technology Innovation Centre,<br />
           University of Technology,<br />
           237 Old Hope Road,<br />
           Kingston 6,<br />
           Jamaica W.I.<br />
           <a href="<?php print site_url() ?>" title="EduFocal"><?php print site_url() ?></a>
        </p>
        </td>
        <td style="vertical-align: top"><h1>RECEIPT #: <?php print $receipt->id ?></h1>
            <p>Order Date: <?php print date('F j, Y', $receipt->created_on) ?><br />
            Order #: <?php print $receipt->payment_ref ?><br />
            Payed by: <?php print $receipt->card_holder ?><br />
            Payment Method: <?print $receipt->card_type . ' | ' . $receipt->card ?><br />
            Authorization Code: <?print $receipt->auth_code ?><br />
            Transaction Type: Purchase</p>
          <p>Billing Address: <br />
            <?php print $receipt->address1 ?><br />
            <?php print $receipt->address2 ?><br />
            <?php print $receipt->city ?><br />
            <?php print $receipt->state ?><br />
            <?php print $receipt->country ?>
            </p>
             
        </td>
        </tr>
        <tr>
        <td colspan="2">
      <?php if(count($receipt->codes)) : ?>
      <table id="items" border="0" width="100%" cellpadding="8">
      <thead>
      <tr><th style="text-align: left">Item</th><th style="text-align: left">Cost (<?php print strtoupper($receipt->currency) ?>)</th></tr>
      </thead>
      <tbody>
      <?php foreach($receipt->codes as $code) : ?>
      <tr><td>1 <?php print $code->type ?> subscription - <?php print $code->code ?></td><td><?php print money_format('%(#6n',$code->value) ?></td>
      <?php endforeach; ?>
      <tr><td>Total:</td><td><?php print money_format('%(#6n',$receipt->amount) ?></td></tr>
      </tbody>
      </table>
      <?php endif; ?>
      </td>
      </tr>
      </tbody>
      </table>
      <div id="footer" style="text-align: center"><p>Thank you for your purchase!</p></div>
      <?php else : ?>
      <h2>No receipt found</h2>
      <?php endif; ?>
    </div>
