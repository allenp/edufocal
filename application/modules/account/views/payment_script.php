<?php 
foreach($currency as $curr => &$item)
{
    foreach($item as $p => &$period) 
    {
        if(isset($period['show_publicly']) && $period['show_publicly'] == FALSE) {
            unset($item[$p]);
        }
    }
}
?>
function updatePlan (plan, currency) {
    var currencies = $.parseJSON(<?php print json_encode(json_encode($currency)) ?>);
    var options = '';
    for(curr in currencies) {
        if(curr && currencies.hasOwnProperty(curr) && curr == currency) {
            for(period in currencies[curr]) {
                options += '<option value="' + period + '">' + currencies[curr][period]['valid_for'] + ' - ' + curr + ' $' + currencies[curr][period]['charge'] + '</option>';
            }
        }
    }
    $(plan).html(options);
}
$("#subscriber-currency").change( function(e) {
    updatePlan("#subscriber-plan", $("#subscriber-currency").val());
});

$("#currency").change( function(e) {
    updatePlan("#plan", $("#currency").val());
});
