<div class="settings">
    <!--<div class="heading">
        <p>You can download and save a copy of your receipts for your personal records.</p>
    </div>
-->
    <h3>Payment History</h3>
    <?php if(count($codes)) : ?>
    <table class="table table-bordered"> 
    <thead>
        <tr> <th>Date</th> <th>Description</th> <th>Amount</th> <th>Status</th> </tr>
    </thead>
    <tbody>
    <?php foreach($codes as $code): ?>
        <tr>
        <?php if($code->accepted_on > 0) : ?>
            <td><?php print date('F j, Y', $code->accepted_on) . ' - ' . date('F j, Y', $code->expiry_date)?></td>
        <?php elseif($code->expiry_date > 0) : ?>
            <td>Expires <?php print date('F j, Y', $code->expiry_date) ?></td>
        <?php else : ?>
            <td>Pending</td>
        <?php endif; ?>
        <?php if($code->source == 0 && $code->source_ref > 0): ?>
            <td><a href="<?php print site_url(array('account', 'receipts', $code->source_ref)) ?>"><?php print $code->type ?> subscription</a> - <a href="<?php print site_url(array('account', 'receipt_pdf', $code->source_ref)) ?>">[PDF]</a></td>
        <?php else : ?>
            <td>1 <?php print $code->type ?> subscription: <?php print $code->code ?></td>
        <?php endif; ?>
            <td><?php print strtoupper($code->currency) ?> $<?php print $code->value ?></td>
            <td><?php print $code->expiry_date >= time() ? 'In use' : 
                        ($code->expiry_date == 0 ? 'Pending' : 'Used'); ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php endif; ?>
</div>
