<div class="settings">
    <h3>Change Password</h3>
    <?php print Template::message(); ?>
    <?php if ( validation_errors()) : ?>
    <div class="alert alert-warning">
        <h4>Please check the following before continuing:</h4>
        <br clear="all" />
        <?php echo validation_errors(); ?>
    </div>
    <?php endif; ?>

    <div class="row col-xs-12 col-sm-6 col-md-4 col-lg-4">
    <?php echo form_open("account/password", array("id" => "save-settings")); ?>
        <?php print form_password('password', '', 'Password'); ?>
        <?php print form_password('new_password', '', 'New Password'); ?>
        <?php print form_password('confirm_password', '', 'Confirm Password'); ?>
        <?php print form_submit('submit', 'Change Password', array('class' => 'btn btn-primary')); ?>
    <?php print form_close(); ?>
    </div>
</div>
