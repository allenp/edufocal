<p>Hello <?php print $name ?>,</p>
<p>Sorry to inform you but there was an issue during the auto renewal process. Please see contact your credit card company. </p>

<p>If you have any questions or concerns, please respond to this email.</p>
<p>Thanks! <br />
The EduFocal Team</p>
<p>EduFocal Limited<br />
Suite #8<br />
Technology Innovation Centre<br />
University of Technology<br />
237 Old Hope Road<br />
Kingston 6<br />
Jamaica W.I.<br />
<a href="<?php print site_url() ?>"><?php print site_url() ?></a></p>
