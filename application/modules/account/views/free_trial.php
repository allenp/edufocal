<p>We noticed that you didn't finish your signup at www.edufocal.com. We're here to help.</p>
<p>If you can't pay via credit card we can also make arrangements for payment to be completed through the bank. Please respond to this email and we'll be happy to help you with the process.</p>
<p>Here are some of the benefits of EduFocal:</p>
<ul>
    <li>Questions and answers for 4 GSAT subjects and 10 CSEC subjects and worked solutions for our math questions!</li>
    <li>A $30,000 JMD cash bursary for our top student annually.</li>
    <li>Doing tests on EduFocal is cool! Answer enough questions correctly and win movie tickets, phone credit and so much more!</li>
    <li>Topic Segmentation: spend time on the topics that you're weak on in each subject area</li>
    <li>Track your progress with EduFocal Reports</li>
</ul>
<p>And so much more!</p>
<h3>Ready to finish signing up?</h3>
<p>If you can't remember your EduFocal password, go to:<br />
https://www.edufocal.com/forgot_password and follow the instructions</p>
<ol>
    <li>Log into your EduFocal account at www.EduFocal.com</li>
    <li>You will be prompted to complete your membership by entering an EduFocal access code or by paying via credit card (Visa/Visa Debit, Mastercard, and NCB's Keycard)
</ol>
<h3>Free Trial</h3>
<p>Still not sure? Well, we're so sure that you'll love EduFocal that we're giving you a week <strong>FREE</strong>! Please log into EduFocal and enter this code when prompted: <br /><div style="padding: 10px; color: rgb(183, 0, 158); background-color: #ececec; font-weight: bold; font-size: 16px; text-align: center;"><?php print $code ?></div></p>
<p>You're Welcome! <br />
The EduFocal Team</p>
<p>EduFocal Limited<br />
Suite #8<br />
Technology Innovation Centre<br />
University of Technology<br />
237 Old Hope Road<br />
Kingston 6<br />
Jamaica W.I.<br />
<a href="https://www.edufocal.com/">www.edufocal.com</a></p>
