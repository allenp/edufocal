<div class="settings">
<?php echo Template::message(); ?>
<?php if ( validation_errors()) : ?>
    <div class="alert alert-warning">
        <h4>Please check the following before continuing:</h4>
<?php echo validation_errors(); ?>
    </div>
<?php endif; ?>
    <div class="col-xs-12 col-sm-6">
<?php if($this->auth->is_expired()) : ?>
        <p>Hello <?php print $this->auth->profile()->first_name ?>, we're sorry to say, but your EduFocal membership has expired. Don't worry though. You can find your new EduFocal access code in the <em>Sunday Observer</em>. You may also use the credit card option below to renew your membership. If you need help please contact us via the support tab to the left or email us at help@edufocal.com</p>
<?php endif; ?>
        <h4>To renew your EduFocal membership:</h4>
        <p><em>Option 1</em>: Use your credit card to pay for membership.</p>
        <a class="btn btn-success btn-lg btn-block" href="<?php print site_url('account/card')?>" alt="Pay with card">Pay with your credit card</a>
        <br /><br />
<?php print form_open(site_url('account/payment'), array('class' => 'form-vertical')); ?>
        <?php print form_input('educode', set_value('educode'), '<em>Option 2</em>: Enter your EduFocal Access Code.', array('class' => 'form-control educode-field', 'placeholder' => 'Enter EduFocal code here')); ?>
        <div class="form-group">
        <?php print form_submit('code_option', 'Accept Code', array('class' => 'btn btn-default btn-lg')); ?>
        </div>
<?php print form_close(); ?>

    </div>

    <div class="pull-right col-xs-12 col-sm-6" <?php if(count($cards) == 0) print"style='display:none;'";?>>
<?php if(isset($is_subscribe) && $is_subscribe === true): ?>
    <h4>Subscription Enabled</h4>
    <p>You are currently subscribed to EduFocal on a <strong><?php print $subscriber->type ?></strong> plan. Your subscription is set to renew on <strong><?php print $subscriber->subscription_end_dt->format('Y-m-d') ?></strong>.</p>
<?php print anchor('account/unsubscribe', 'Unsubscribe'); ?> 
    <?php else: ?>
    <h4>Setup Auto-renew Subscription</h4>
    <p>Do you often forget to renew your membership? Now you can set your account to automatically renew on the day your service ends.</p>
    <?php print form_open(site_url('account/payment'), array('class' => 'form-horizontal')); ?>
        <div class="col-lg-12">
            <div class="form-group">
                <div class="row col-lg-12">
                    <label for="subscriber-plan" class="control-label">Set your subscription plan</label>
<?php 
  $curr = array_keys($currencies);
  $curr = $curr[0];
  $def = array_values($currencies); 
  $def = $def[0];
?>
                </div>
                <div class="col-xs-6 col-sm-6 row">
                    <select id="subscriber-plan" name="subscriber-plan" class="form-control">
<?php foreach($def as $term => $item) : ?>
<?php if(!isset($item['show_publicly']) || $item['show_publicly'] == TRUE) : ?>
                        <option value="<?php print $term ?>"><?php print $item['valid_for']; ?> - <?php print $curr; ?> $<?php print $item['charge'] ?></option>
<?php endif; ?>
<?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-5">
                    <select id="subscriber-currency" name="subscriber-currency" class="form-control">
<?php foreach(array_keys($currencies) as $cu) : ?>
                        <option value="<?php print $cu; ?>"><?php print $cu; ?></option>
<?php endforeach; ?>
                    </select>
                </div>
            </div><!-- form-group -->
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <div class="row col-lg-12">
                    <label for="subscriber-card" class="control-label">Preferred Card:</label>
                </div>
                <div class="col-xs-10 col-sm-8 row">
                    <select id="subscriber-card" name="subscriber-card" class="form-control">
<?php foreach($cards as $card) : ?>
                        <option value="<?php print $card ?>"><?php print $card; ?></option>
<?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
<?php print form_submit('subscribe_option', 'Subscribe', array('class' => 'btn btn-default btn-lg')); ?> 
             </div>
        </div>
<?php print form_close(); ?>
<?php endif; ?>
</div>
</div>
