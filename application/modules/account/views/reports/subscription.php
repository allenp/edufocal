<?php if (isset($subscribers) && count($subscribers)) : ?>
<h2>Subscribers</h2>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Subscription</th>
            <th>Subscriber since</th>
            <th>Subscriber renewal date</th>
            <th>Subscription cost</th>
            <th>Currency</th>
        <tr>
    </thead>
    <tbody>
        <?php foreach ($subscribers as $subscriber) : ?>
            <tr>
                <td><?php echo $subscriber->user_id ?></td>
                <td><?php echo $subscriber->first_name.' '.$subscriber->last_name; ?></td>
                <td><?php echo $subscriber->type; ?></td>
                <td><?php echo $subscriber->created_at; ?></td>
                <td><?php echo $subscriber->subscription_end_dt; ?></td>
                <td><?php echo $subscriber->cost; ?></td>
                <td><?php echo $subscriber->currency; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
