<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_testing_level extends Migration {
	
	public function up() 
	{
		$prefix = $this->db->dbprefix;

        $fields = array( 
                        'level' => array('type' => 'VARCHAR(6)', 'null' => true)
                       );
        $this->dbforge->add_column($prefix."schools", $fields);
        $this->db->query("UPDATE {$prefix}schools SET level='CXC' WHERE type='Secondary'");
        $this->db->query("UPDATE {$prefix}schools SET level='GSAT' WHERE type IN ('Primary', 'Preparatory')");
	}
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix."schools", "level");
	}
	
	//--------------------------------------------------------------------
	
}
