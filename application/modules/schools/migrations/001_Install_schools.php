<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_schools extends Migration {
	
	public function up() 
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->add_field('`id` int(11) NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('`name` VARCHAR(255) NOT NULL');
		$this->dbforge->add_field("`type` VARCHAR('Primary', 'Secondary') NOT NULL");
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('schools');

		// permissions
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Schools.Content.View','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Schools.Content.Create','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Schools.Content.Edit','','active');");
					$this->db->query("INSERT INTO {$prefix}permissions VALUES (0,'Schools.Content.Delete','','active');");

	}
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('schools');
		// permissions
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Schools.Content.View';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Schools.Content.View';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Schools.Content.Create';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Schools.Content.Create';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Schools.Content.Edit';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Schools.Content.Edit';");
					$query = $this->db->query("SELECT permission_id FROM {$prefix}permissions WHERE name='Schools.Content.Delete';");
					foreach ($query->result_array() as $row)
					{
						$permission_id = $row['permission_id'];
						$this->db->query("DELETE FROM {$prefix}role_permissions WHERE permission_id='$permission_id';");
					}
					$this->db->query("DELETE FROM {$prefix}permissions WHERE name='Schools.Content.Delete';");
	}
	
	//--------------------------------------------------------------------
	
}