<?php if (isset($schools) && count($schools)) : ?>
<h2>Schools</h2>
<?php print form_open(site_url(SITE_AREA.'/content/schools/search'), array( 'id' => 'search-form', 'class' => 'form-horizontal')); ?>
    <div class="container">
        <?php print form_input('search', $search !== 'all_schools' ? $search : '', '', array('class' => 'form-control pull-right', 'placeholder' => 'Search school name')); ?>
    </div>
<?php print form_close(); ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Type</th>
            <th><?php echo lang('schools_actions'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    $count = $offset;
    foreach ($schools as $school) : ?>
        <tr>
        <td><?php print ($count + 1) ?></td>
        <td><?php print $school->name ?></td>
        <td><?php print $school->type  ?></td>
        <td><?php print $school->level  ?></td>
            <td><?php echo anchor(SITE_AREA .'/content/schools/edit/'. $school->id, 'Edit') ?></td>
        </tr>
    <?php $count++; ?>
    <?php endforeach; ?>

    </tbody>
</table>
<?php endif;
echo $this->pagination->create_links();
?>
