<?php if (validation_errors()) : ?>
<div class="alert notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form form-horizontal"'); ?>
<div class="control-group">
    <?php echo form_label('School Name *', 'name', array('class' => 'control-label')); ?>
    <div class="controls">
        <input type="text" name="name" maxlength="255" />
    </div>
</div>

 <div class="control-group">
     <?php echo form_label('Parish', 'parish_id', array('class' => 'control-label')); ?>
     <div class="controls">
         <select name="parish_id">
             <?php foreach($parishes as $key => $value) : ?>
                 <option value="<?php echo $key ?>" ><?php echo $value ?></option>
             <?php endforeach; ?>
         </select>
     </div>
 </div>
 <div class="control-group">
     <?php echo form_label('Type', 'type', array('class' => 'control-label')); ?>
     <?php
        $options = array(
            'Primary' => 'Primary',
            'Secondary' =>  'Secondary',
            'Preparatory' => 'Preparatory'
        ); ?>
     <div class="controls">
     <select name="type">
         <?php foreach($options as $key => $value) : ?>
             <option value="<?php echo $key ?>" ><?php echo $value ?></option>
         <?php endforeach; ?>
     </select>
 </div>
 </div>

<div class="form-actions">
	<input class="btn btn-primary" type="submit" name="submit" value="Create School" /> or <?php echo anchor(SITE_AREA .'/content/schools', lang('schools_cancel')); ?>
</div>
<?php echo form_close(); ?>
