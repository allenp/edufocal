<?php if (validation_errors()) : ?>
<div class="alert notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; ?>

<?php echo form_open($this->uri->uri_string(), 'class="constrained ajax-form form-horizontal"'); ?>
<div class="control-group">
    <?php echo form_label('School Name *', 'name', array('class' => 'control-label')); ?>
    <div class="controls">
        <input type="text" name="name" maxlength="255" value="<?php echo set_value('name', $school->name); ?>" />
    </div>
</div>

 <div class="control-group">
     <?php echo form_label('Parish', 'parish_id', array('class' => 'control-label')); ?>
     <div class="controls">
         <select name="parish_id">
             <?php foreach($parishes as $key => $value) : ?>
                 <option value="<?php echo $key ?>" <?php if($key == $school->parish_id) echo ' selected="selected"'?>><?php echo $value ?></option>
             <?php endforeach; ?>
         </select>
     </div>
 </div>
 <div class="control-group">
     <?php echo form_label('Type', 'type', array('class' => 'control-label')); ?>
     <?php
        $options = array(
            'Primary' => 'Primary',
            'Secondary' =>  'Secondary',
            'Preparatory' => 'Preparatory'
        ); ?>
     <div class="controls">
     <select name="type">
         <?php foreach($options as $key => $value) : ?>
             <option value="<?php echo $key ?>" <?php if($key == $school->type) echo ' selected="selected"'?>><?php echo $value ?></option>
         <?php endforeach; ?>
     </select>
 </div>
 </div>
 <div class="form-actions">
     <input class="btn btn-primary" type="submit" name="submit" value="Update School" /> or <?php echo anchor(SITE_AREA .'/content/schools', 'Cancel'); ?>
 </div>
 <div class="box delete rounded">
     <h3>Delete this School</h3>
     <p>The school will be permanently removed from the system.</p>
     <a class="btn btn-danger" id="delete-me" href="<?php echo site_url(SITE_AREA .'/content/schools/delete/'. $school->id); ?>" onclick="return confirm('<?php echo "Are you sure you want to delete this school?"; ?>')">Delete this School</a>
 </div>
<?php echo form_close(); ?>

