<ul class="nav nav-pills">
    <li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
        <?php echo anchor(SITE_AREA.'/content/schools', 'All schools') ?>
    </li>
    <li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
        <?php echo anchor(SITE_AREA.'/content/schools/create', 'Create a school'); ?>
    </li>
</ul>
