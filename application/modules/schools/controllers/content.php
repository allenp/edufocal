<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller {

    function __construct()
    {
        parent::__construct();

        $this->auth->restrict('Schools.Content.View');
        $this->lang->load('schools');

        Assets::add_js($this->load->view('content/js', null, true), 'inline');
        Template::set_block('sub_nav', 'content/_sub_nav');

    }


    /** 
     * function index
     *
     * list form data
     */
    public function index($search='all_schools', $offset=0)
    {
        if ($search !== 'all_schools') {
            $condition = array('`name` LIKE ?', '%'.$search.'%',);
            $records = School::find('all',array(
                'conditions' => $condition,
                'order'      => 'name',
                'limit'      => 20,
                'offset'     => $offset,
            ));
            $total_schools = School::count(array('conditions' => $condition));
        } else {
            $total_schools = School::count();
            $records = School::find('all',array(
                'order' => 'name',
                'limit'      => 20,
                'offset'     => $offset,
            ));
        }

        $this->load->helper('ui/ui');

        $this->load->library('pagination');
        $this->pager['base_url'] = site_url(SITE_AREA ."/content/schools/index/$search/");
        $this->pager['total_rows'] = $total_schools;
        $this->pager['per_page'] = 20;
        $this->pager['uri_segment'] = 6;

        $this->pagination->initialize($this->pager);


        Template::set('search', $search);
        Template::set('offset', $offset);
        Template::set_view("content/index");
        Template::set('schools', $records);
        Template::set("toolbar_title", "Manage Schools");
        Template::render();
    }

    //--------------------------------------------------------------------
    
    public function search()
    {
        $search = $this->input->post('search', true);
        redirect(SITE_AREA . "/content/schools/index/$search");
    }


    public function create() 
    {
        $this->auth->restrict('Schools.Content.Create');

        if ($this->input->post('submit'))
        {
            if ($this->save_schools())
            {
                Template::set_message(lang("schools_create_success"), 'success');
                Template::redirect(SITE_AREA .'/content/schools');
            }
            else 
            {
                Template::set_message(lang("schools_create_failure") , 'error');
            }
        }

        $parishes = Parish::find('all', array('order' => 'name'));
        foreach($parishes as $parish) {
            $par[$parish->id] = $parish->name;
        }

        Template::set('parishes', $par);
        Template::set_view('content/create');
        Template::set("toolbar_title", "Create Schools");
        Template::render();
    }
    //--------------------------------------------------------------------


    public function edit() 
    {
        $this->auth->restrict('Schools.Content.Edit');

        $id = (int)$this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang("schools_invalid_id"), 'error');
            redirect(SITE_AREA .'/content/schools');
        }

        if ($this->input->post('submit'))
        {
            if ($this->save_schools('update', $id))
            {
                Template::set_message(lang("schools_edit_success"), 'success');
            }
            else 
            {
                Template::set_message(lang("schools_edit_failure") . $this->schools_model->error, 'error');
            }
        }

        $parishes = Parish::find('all', array('order' => 'name'));
        foreach($parishes as $parish) {
            $par[$parish->id] = $parish->name;
        }

        $school = School::find($id); 

        Template::set('parishes', $par);
        Template::set('school', $school);
        Template::set_view('content/edit');
        Template::set("toolbar_title", "Edit Schools");
        Template::render();		
    }


    public function delete() 
    {	
        $this->auth->restrict('Schools.Content.Delete');

        $id = $this->uri->segment(5);

        if (!empty($id))
        {	
            if ($this->schools_model->delete($id))
            {
                Template::set_message(lang("schools_delete_success"), 'success');
            } else
            {
                Template::set_message(lang("schools_delete_failure") . $this->schools_model->error, 'error');
            }
        }

        Template::redirect(SITE_AREA .'/content/schools');
    }

    public function save_schools($type='insert', $id=0) 
    {	

        $this->form_validation->set_rules('name','Name','required|max_length[255]');			
        $this->form_validation->set_rules('type','Type','required');
        if ($this->form_validation->run() === false)
        {
            return false;
        }

        $new = array(
            'parish_id' => $this->input->post('parish_id'),
            'name' => $this->input->post('name'),
            'type' => $this->input->post('type'),
            'level' => in_array($this->input->post('type'), array('Primary', 'Preparatory')) ? 'GSAT' : 'CXC',
        );

        if ($type == 'insert')
        {
            $school = School::create($new);
            $id = $school->id;

            if (is_numeric($id))
            {
                $return = true;
            } else
            {
                $return = false;
            }
        }
        else if ($type == 'update')
        {
            $school = School::find($id);
            $school->update_attributes($new);
            $return = true;
        }

        return $return;
    }

}
