<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set("precision", 16);

require dirname(__FILE__) . '/../../../../PHP_Excel/Classes/PHPExcel.php';


class Teacher_Payout extends EF_Cron_Controller
{
    public function __contruct()
    {
       parent::__construct();
       $this->output->enable_profiler(FALSE);
    }

    public function calculate($period = 0)
    {
        $this->load->helper('teacher_payout');
        ef_teacher_payout($period);
        Template::set('title', 'Teacher payout');
        Template::render();
       /* 
        $votes = Rating::find_by_sql("select AVG(1.0 * rating) avg_rating, teacher_id, topic_id, student_id 
                                        from ratings 
                                        where timestamp between ? and ?
                                        group by teacher_id, student_id, topic_id order by teacher_id", array($payout_period->startdate, $payout_period->enddate)); 

        $teacher_raw_votes = array();

        //segment votes per teacher, per topic
        foreach($votes as $vote) {
           $teacher_raw_votes[$vote->teacher_id][$vote->topic_id][] = $vote->avg_rating; 
        }
                        
        $teacher_scores = array(); 

        foreach($teacher_raw_votes as $teacher_id => $teacher_votes)
        {
            foreach($teacher_votes as $topic_id => $topic_votes)
            {
                $total = count($topic_votes);
                $avg = array_sum($topic_votes) / (1.0 * $total); 

                $teacher_scores[$teacher_id][$topic_id] = Teacher_Payout::score($avg, $total, 0.7); //the .7 is the teacher's trust rating 

                Teacher_Topic_Payout::create(array(
                                                'teacher_id' => $teacher_id, 
                                                'period_id' => $payout_period->id,
                                                'topic_id' => $topic_id, 
                                                'rating' => $teacher_scores[$teacher_id][$topic_id],
                                                'process_id' => $process->id
                                                ));             
                
            }
        }
        $this->db->where("timestamp between $payout_period->startdate and $payout_period->enddate"); 
        $this->db->update('ratings', array('process_id', $process->id));
        */
        /* 2. Calculate question usage: 
           Note to self: Teacher may not have any student ratings, but have
           relevant question usage. Therefore check to see if payout record exists, then update
           with usage score, or else, insert new payout record setting the rating value to 1. 
        */
    }

    private function calculate_effort()
    {
    }

    private static function ciLowerBound($pos, $n, $power)
    {
        if($n == 0.0) 
            return 0;
        $z = PHPExcel_Calculation_Statistical::NORMSDIST(1 - $power/ 2);
        $phat = 1.0 * $pos / $n;
        return ( $phat + $z * $z / (2 * $n) - $z * sqrt(($phat*(1 - $phat) + $z * $z / (4*$n)) / $n )) / (1 + $z * $z / $n);
    }

    private static function score($f, $t, $k)
    {
         return Teacher_Payout::ciLowerBound($f * $k, $t * $k , 0.8);
    }
}
