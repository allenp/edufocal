<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Send_Emails extends EF_Cron_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index()
    {
        print date(\DateTime::RSS) . ' - sending emails from queue...' . PHP_EOL;

        $this->load->library('emailer/emailer');
        ob_start();
        $this->emailer->process_queue();
        ob_end_clean();
    }
}
