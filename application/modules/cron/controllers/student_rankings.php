<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Student_Rankings extends EF_Cron_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function calculate()
    {
        $this->load->helper('student_rank');
        ef_update_subject_rank();
        Template::set('title', 'Cron rank');
        Template::render();
    }
}
