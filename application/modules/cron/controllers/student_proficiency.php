<?php


// how many students must've answered a question for it to be considered for sampling
define('MIN_SAMPLE_SIZE', 25);

// how many questions must be grouped together in a sampling
define('MIN_QUESTION_SAMPLE', 25);

class Student_Proficiency extends EF_Cron_Controller
{
    public function __construct ()
    {
        parent::__construct();
    }
    
    public function calculate()
    {
        $answers = Answer::find_by_sql("select question_id, answers.topic_id, questions.level, count(*) occurs 
                                            from answers
                                            inner join questions on questions.id = answers.question_id
                                            where questions.type = 1 and answers.test_type = 'timed'
                                            group by question_id, questions.topic_id
                                            having count(*) >= " .  MIN_SAMPLE_SIZE .
                                            " order by topic_id, count(*) desc");
        
        //so these are all questions that may have enough users,
        //group them by topic and by level
        $per_topic = array();
        $this->load->database();
        foreach($answers as $answer)
        {
            $result = $this->db->query("select distinct user_id from answers where user_id " .
                                       "is not null and question_id = ". $answer->question_id);
            $u = array();
            foreach($result->result_array() as $row)
                $u[] = $row['user_id']; 

            $per_topic[$answer->topic_id][$answer->level][$answer->question_id] = $u; 
        }
        
        $processes = array();
        foreach($per_topic as $topic_id => $topic)
        {
            foreach($topic as $level_id => $level)
            {
                if(count($level) >= MIN_QUESTION_SAMPLE)
                {
                    
                    $questions = array_keys($level);
                    $subset = array_shift($level); //$subset represents an array of users indexed/keyed by a question_id
                    
                    $current_question = key($level);
                    $second = array_shift($level);  

                    while($second)
                    {
                        $test = array_intersect($subset, $second);
                        if(count($test) >= MIN_SAMPLE_SIZE)
                        {
                            $subset = $test;
                        }
                        else
                        {
                            //in this case we're discarding this question
                            //from the sample as it makes the sample invalid.
                            unset($questions[$current_question]);
                        }
                        $current_question = key($level);
                        $second = array_shift($level);
                    }
                    
                    if(count($subset) >= MIN_SAMPLE_SIZE && count($questions) >= MIN_QUESTION_SAMPLE)
                    {
                        $process = CronRank::create( array('level' => $level_id,
                                                              'description' => 'Automated ranking',
                                                              'topic_id' => $topic_id,
                                                              ) );
                        $processes[] = $process->id;

                        foreach($subset as $key => $user)
                            foreach($questions as $question)
                                $item = CronRankItem::create( array('process_id' => $process->id,
                                                               'question_id' => $question,
                                                               'student_id' => (int)$user,
                                                               'score' => 0)
                                                        );
                    }
                }
            }
        }
        if(count($processes))
            $this->computeTentativeDifficulty( $processes );
    }
    
    private function computeTentativeDifficulty( $processes )
    {
        foreach($processes as $process)
        {
            $procObj = Process::find($process);
            /* sets score to 1 for all correct answers. scores are set to 0 by default so the remaining are incorrect */
            $query = $this->db->query("update rank_process_items r 
                                         join questions q on q.id = r.question_id
                                         join answers a on a.question_id = r.question_id and a.user_id = r.user_id
                                         and a.answer = q.accepted_answers
                                         set score = 1
                                         where r.process_id = " . $process);
             
            /* set tentative question difficulty based on how many people got the question correct */
            $query = $this->db->query("update questions join ( select question_id, 1 - sum(score) / count(*) difficulty " .
                                        " from rank_process_items where process_id = " . $process .  
                                        " group by question_id ) calc  on questions.id = calc.question_id  " .
                                        " set questions.difficulty = calc.difficulty");
                                        
            /* set student proficiency across the sampled topic+level based on performance on grouped questions */
            $query = $this->db->query("delete from student_levels where topic_id = " . $procObj->topic_id . " and level = " . $procObj->level );

            $query = $this->db->query("insert into student_levels(student_id, topic_id, level, proficiency)
                                            SELECT student_id, p.topic_id, p.level, sum(score)/count(score) proficiency FROM rank_process_items i inner join rank_processes p
                                            on i.process_id = p.id
                                            where process_id = " . $process .
                                            " group by student_id");
                                            
            /* calculates the probability that a student with proficiency in a given topic at a given level
             * will answer a question with a given difficulty correctly */
             $query = $this->db->query("select question_id, i.student_id, difficulty, proficiency, 1 / ( 1 + EXP( difficulty - proficiency)) as probabilityij
                                            from rank_process_items i
                                            inner join questions q on i.question_id = q.id
                                            inner join rank_processes p on p.id = i.process_id
                                            inner join student_levels l on p.level = l.level
                                            and p.topic_id = l.topic_id and i.student_id = l.student_id");

        }
    }
   
}


