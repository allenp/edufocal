<?php


// how many students must've answered a question for it to be considered for sampling
define('MIN_SAMPLE_SIZE', 25);

// how many questions must be grouped together in a sampling
define('MIN_QUESTION_SAMPLE', 25);

class Cron extends EF_Controller
{
    public function __construct ()
    {
        parent::__construct();
    }

    public function index()
    {

        $start = 1330913953;
        $finish = 1360913953;
        $ans_count = Answer::find('all', array('select' => 'question_id, answers.teacher_id, answers.topic_id, answers.subject_id, answers.user_id, count(*) used',
                                               'group' => 'question_id, answers.user_id',
                                               'joins' => array('test'),
                                               'conditions' => array('tests.started >= ? and tests.finished <= ?', $start, $finish)));
        
        print '<pre>';
        print_r($ans_count);
        print '</pre>';
    }
    
    private function generateAnswers()
    {
    
        $users = User::find_all_by_role_id('8');
        //$teachers = Teacher::find_all(array('limit);
        $topics = Topic::all(array('limit' => '5'));
        $choices = array(1 =>'yes', 2=>'no', 3 => 'maybe');
        
        foreach($users as $user)
        {
            $attr = array(
                "user_id" => $user->id, 
                "teacher_id" => 1,
                "subject_id" => 6,
                "topic_id" => 4,
            );
            
            $test = Test::create($attr);
            
            $questions = Question::all(array("conditions" => array("topic_id = ? AND type = ? AND level = ?", $attr['topic_id'], 1, 1), "limit" => 25, "order" => "RAND()"));

            foreach($questions as $question) {
                Answer::create(
                    array(
                        "test_id" => $test->id,
                        "answer" => array_rand($choices, 1),
                        "question_id" => $question->id,
                        "subject_id" => $attr['subject_id'],
                        "topic_id" => $attr['topic_id'],
                        "test_type" => 'timed',
                        "question_type" => 'multiple',
                        "completed" => "yes",
                        "resolved" => "no",
                        "user_id" => $attr['user_id'],
                        "teacher_id" => $attr['teacher_id']
                    )
                );
            }
        }
    }
    
    private function generateQuestions()
    {
        $topics = Topic::find('all', array('limit' => 10));
        $users = User::find('all', array('limit' => 20));
        
        $contents = file(BASEPATH . '../../tests/questions.txt');
        
        $choices = array(1 =>'yes', 2=>'no', 3 => 'maybe');
        
        foreach($contents as $question)
        {
            $q = new Question();
            $q->user_id = $users[array_rand($users,1)]->id;
            $q->topic_id = $topics[array_rand($topics,1)]->id;
            $q->question = $question;
            $q->type = 1; //multiple choice
            $q->difficulty = rand(1,5);
            $q->accepted_answers = rand(1,3);
            $q->choices = json_encode($choices);
            $q->save();
        }
    }
    
    private function generateUsers()
    {
        
        $this->load->model('users/User_model', 'user_model');
        
        $schools = School::find('all', array('limit' => 40));
        $sex = array('m','f');
        
        $filecontents = file(BASEPATH . '../../tests/names.txt');
        $names = array();
        $name_keys = array_rand($filecontents, 100);
        
        foreach($name_keys as $key)
        {
            $names[] = $filecontents[$key];
        }
                
        for( $i = 0; $i < 10; $i++ )
        {
            $additional = array(
                'gender' => $sex[rand(0,1)],
                'phone' => '3133333'
            );
            
            $fname = trim(ucwords($names[array_rand($names,1)]));
            $lname = trim(ucwords($names[array_rand($names,1)]));
            
            $data = array(
                'email'		=> $fname.$lname.'@gmail.com',
                'username'	=> $fname . $lname,
                'password'	=> 'user1',
                'first_name' => $fname,
                'last_name' => $lname . '-Beta',
                'school_id' => $schools[array_rand($schools,1)]->id,
                'profile' => json_encode($additional),
                'referral_code' => $this->get_unique_referral_code()
            );
            //print_r($data);
            $user_id = $this->user_model->insert($data);
        }
        
    }
    
    private function get_unique_referral_code ()
    {
        $code = random_string('alnum', 10);
        
        if ( User::exists(array('referral_code', $code)) )
        {
            return $this->get_unique_referral_code();
        }
        
        return $code;
    }
    
}


