<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Teacher_Ratings extends Migration
{
    public function up()
    {
       $dbprefix = $this->db->dbprefix;


       $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'processed' => array('type' => 'timestamp default CURRENT_TIMESTAMP'),
            'period_start' => array('type' => 'int(11)'),
            'period_end' => array('type' => 'int(11)')
       );

       $this->dbforge->add_field($fields);

       $this->dbforge->add_key('id', TRUE);

       $this->dbforge->create_table($dbprefix . 'cron_ratings');

       $fields = array(
            'process_id' => array('type' => 'int(11)', 'null' => true, 'unsigned' => true)
        );

        $this->dbforge->add_column('ratings', $fields);
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_table($prefix . 'cron_ratings');
        $this->dbforge->drop_column($prefix . 'ratings', 'process_id');
    }
}

