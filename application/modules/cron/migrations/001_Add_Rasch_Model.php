<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_Rasch_Model extends Migration
{
	public function up ()
	{
		$prefix = $this->db->dbprefix;
		
		$this->db->query("CREATE TABLE  `". $prefix . "student_question_diffs` (
				`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`question_id` INT NOT NULL,
				`student_id` INT NOT NULL,
				`difficulty` float NOT NULL,
                `seen` bool not null
			) ENGINE = MYISAM ;");

        $this->db->query("CREATE TABLE `rank_processes` (
                    `id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    `description` VARCHAR( 255 ) NULL,
                    `level` SMALLINT,
                    `topic_id` INT NOT NULL
             ) ENGINE = MYISAM ;");

        $this->db->query("CREATE TABLE `rank_process_items` (
                    `id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    `process_id` INT( 11 ) NOT NULL,
                    `question_id` BIGINT NOT NULL,
                    `student_id` BIGINT NOT NULL,
                    `score` TINYINT
            ) ENGINE = MYISAM ;");
	}
	
	public function down ()
	{
		$this->dbforge->drop_table('student_question_diffs');
        $this->dbforge->drop_table('rank_processes');
        $this->dbforge->drop_table('rank_process_items');
	}
}
