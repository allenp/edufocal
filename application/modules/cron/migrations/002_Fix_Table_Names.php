<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Fix_Table_Names extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->rename_table($prefix .'rank_processes', $prefix .'cron_ranks');
        $this->dbforge->rename_table($prefix .'rank_process_items', $prefix .'cron_rank_items');
    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->rename_table($prefix .'cron_ranks', $prefix .'rank_processes');
        $this->dbforge->rename_table($prefix .'cron_ratings', $prefix .'ratings_processes');
        $this->dbforge->rename_table($prefix .'cron_rank_items', $prefix .'process_rank_items');
    }
}
