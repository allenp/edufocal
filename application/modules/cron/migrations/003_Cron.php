<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Cron extends Migration
{
    public function up()
    {
       $dbprefix = $this->db->dbprefix;
       

       $fields = array(
            'task' => array('type' => 'varchar(256)'),
            'access_key' => array('type' => 'varchar(256)'),
            'last_run' => array('type' => 'int(11)')
       );
       
       $this->dbforge->add_key('task', TRUE);
       $this->dbforge->add_field($fields);
       $this->dbforge->create_table($dbprefix . 'cron');
    }

    public function down()
    {
        $this->dbforge->drop_table('cron');
    }
}
