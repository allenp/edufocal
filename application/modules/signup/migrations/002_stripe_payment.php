<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_stripe_payment extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'payment_ref' => array('type' => 'varchar(256)', 'null' => true),
            'currency'    => array('type' => 'varchar(3)', 'null' => true, 'default' => 'JMD'),
        );
        $this->dbforge->add_column($prefix .'codes', $fields);
        $this->db->query("update " . $prefix . "codes set currency = 'JMD'");
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->remove_column($prefix . 'codes', 'payment_ref');
        $this->dbforge->remove_column($prefix . 'codes', 'currency');
    }
}
