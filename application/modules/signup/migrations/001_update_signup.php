<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_update_signup extends Migration {
	
	public function up() 
	{
		$fields = array(
			'status' => array('type' => 'varchar(255)')
		);
		$this->dbforge->add_column('user_referrals', $fields);

	}
	
	public function down() 
	{
		$this->dbforge->drop_column('user_referrals', 'status');
	}
	
}