<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_receipt extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'unsigned' => 'true', 'auto_increment' => true),
            'payment_ref' => array('type' => 'varchar(256)', 'null' => true),
            'user_id' => array('type' => 'int(11)', 'unsigned' => 'true'),
            'amount' => array('type' => 'decimal'),
            'currency' => array('type' => 'varchar(3)'),
            'card_holder' => array('type' => 'varchar(256)'),
            'email' => array('type' => 'varchar(256)', 'null' => true),
            'paymethod' => array('type' => 'varchar(32)', 'null' => true),
            'card' => array('type' => 'varchar(26)', 'null' => true),
            'card_type' => array('type' => 'varchar(16)', 'null' => true),
            'auth_code' => array('type' => 'varchar(16)', 'null' => true),
            'email_sent' => array('type' => 'int(3)', 'default' => 0),
            'created_on' => array('type' => 'int(11)', 'unsigned' => true),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'receipts');
        $this->dbforge->drop_column($prefix . 'codes', 'payment_ref');
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_table($prefix . 'receipts');
        $fields = array(
                'payment_ref' => array('type' => 'varchar(256)', 'null' => true),
                );
        $this->dbforge->add_column($prefix . 'codes', $fields);
    }
}

