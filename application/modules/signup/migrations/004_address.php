<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_address extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'address1' => array('type' => 'varchar(256)', 'null' => true),
            'address2' => array('type' => 'varchar(256)', 'null' => true),
            'city' => array('type' => 'varchar(256)', 'null' => true),
            'state' => array('type' => 'varchar(256)', 'null' => true),
            'country' => array('type' => 'varchar(5)', 'null' => true),
        );

        $this->dbforge->add_column($prefix . 'receipts', $fields);
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'receipts', 'address1');
        $this->dbforge->drop_column($prefix . 'receipts', 'address2');
        $this->dbforge->drop_column($prefix . 'receipts', 'city');
        $this->dbforge->drop_column($prefix . 'receipts', 'country');
    }
}

