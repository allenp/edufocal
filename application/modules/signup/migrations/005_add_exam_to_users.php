<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');

class Migration_add_exam_to_users extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'exam_level' => array('type' => 'varchar(15)', 'null' => 'true')
        );

        $this->dbforge->add_column($prefix . 'users', $fields);
        $this->db->query("update users, schools set users.exam_level = schools.level where users.school_id = schools.id");
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'users', 'exam_level');
    }
}
