<?php

//user creation
use EduFocal\User\DefaultUserService;
use EduFocal\User\DefaultUserValidation;

//handling code purchases and redemption
use EduFocal\Code\CodeRedemption;
use EduFocal\Code\CodeRedemptionValidation;
use EduFocal\Code\CodePurchase;
use EduFocal\Code\CodePurchaseValidation;

use EduFocal\Subscription\Subscriber;

//Shared event listener
use EduFocal\Support\Listener;

/**
 * Signup module for user registration and payments.
 *
 * @package default
 */
class Signup extends Front_Controller
{
	private static $ef_code_rules = array(
		array(
			'field' => 'educode',
			'label' => 'EduFocal Code',
			'rules' => 'trim|required|callback__valid_efcode'
		)
	);

	protected $userValidator = null;
	protected $accountService = null;

	/**
	 * Sets up the user model and authorization libraries.
	 */
	public function __construct ()
	{
		parent::__construct();
		Template::set_theme('public');

		$this->userValidator = new DefaultUserValidation();
		$this->accountService = new DefaultUserService(
			Listener::get()
		);
	}

	/*
	 * Handle GET request to the index
	 */
	private function index_get()
	{
		$exams = array('GSAT' => 'GSAT', 'CXC' => 'CSEC');

		Template::set('exams', $exams);
		Template::set('title', 'Pricing and Signup');
		Template::set_view('signup/step1');
		Template::render();
	}

	/**
	 * Step one of registration. Fill out basic account information.
	 * Redirects GET to index_get.
	 */
	public function index ()
	{
		if ( $this->auth->is_logged_in() ) {
			redirect('signup/payment');
		}

		if($_SERVER['REQUEST_METHOD'] === 'GET') {
			return $this->index_get();
		}

		if ($this->userValidator->validate($this) == false) {
			Template::set_message(
				'There was a problem creating your account:'.
				$this->user_model->error ,
				'error');
			return $this->index_get();
		}

		$data = $this->userValidator->getUserInput();
		$user = $this->accountService->createUser($data);

		$login = $this->accountService->login(
			$data['email'],
			$data['password']
		);

		if ($login === false) {
			Template::set_message(
				'You were unable to login... contact support.');
			return $this->index_get();
		}

		redirect('signup/payment');
	}

	private function payment_get()
	{
		$this->load->helper('country_dropdown');
		$currencies = array();
		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

		foreach ($configs as $curr => $key) {
			$currencies[$curr] = $this->config->item($key);
		}

		Assets::add_js($this->load->view('signup/payment_script', array('currency' => $currencies),  true), 'inline');
		Assets::add_module_js('signup', 'confirm.js');

		if (isset($option)) {
			Template::set('option', $option);
		}

		Template::set('currencies', $currencies);
		Template::set('title', 'Payment Options - EduFocal');
		Template::render();
	}

	/**
	 * Payment page of the signup process.
	 */
	public function payment ()
	{
		if (!$this->auth->is_logged_in()) {
			redirect('signup');
		}

		if($_SERVER['REQUEST_METHOD'] === 'GET') {
			return $this->payment_get();
		}

		$redeem = ($this->input->post('option') == 'code');

		//Purchases via credit card have to be confirmed.
		//So send to confirmation page.
		if (!$redeem && !$this->input->post('confirm')) {
			return $this->confirm();
		}

		if ($redeem) {
			$validator = new CodeRedemptionValidation();
		} else {
			$validator = new CodePurchaseValidation(
				$this->auth->profile(),
				'signup'
			);
		}

		if ($validator->validate($this)) {
			if ($redeem == true) {
				$processor = new CodeRedemption(
					$validator->getUserInput(), $this->auth->profile(), Listener::get());
			} else {
				$processor = new CodePurchase(
					$validator->getUserInput(), $this->auth->profile(), Listener::get());
			}

			$result = $processor->process();

			if ($result->success !== true) {
				if (! $redeem) {
					$this->form_validation->set_errors([
						'card_number' =>
						'Transaction unsuccessful. Please contact your financial institution.'
						]);
				}
			} else {
				$code = $result->code;
			}
		} else {
			//TODO: change validator classes to use an ErrorBag or something
			//And then manually peel the results into something the view understands here
			//Currently the validators are accessing the underlying CI object
			//which is not SOLID.
		}

		//failed. show error messages on payment page.
		if ( !isset($code) || null == $code) {
			return $this->payment_get();
		}

		//TODO: Move all this code into subscription
		//Handler on PAYMENT_SUCCESS
		if ( !$redeem && $this->input->post('subscription') === 'on') {
			$sub = new Subscriber($code->user);
			$sub->subscribe($code->type, $result->card->redacted, $code->currency, $code->value);
		}

		//This is miscellaneous mailchimp code
		//that moves the user from one mailing list to the next
		//This should be elsewhere tbh. Fix that sometime
		$this->load->library('EFMCAPI', null, 'mailchimp');
		$user = $this->auth->profile();
		$merge_vars = array(
			'FNAME' => ucwords($user->first_name),
			'LNAME' => ucwords($user->last_name),
			'PHONE' => $user->phone,
			'SCHOOL' => $user->school->name,
			'GENDER' => $user->sex
		);

		$this->mailchimp->listUnsubscribe(
			$this->mailchimp->get_list_id(EFMCAPI::REGISTRATION_START),
			$user->email
		);

		$this->mailchimp->listSubscribe(
			$this->mailchimp->get_list_id(EFMCAPI::REGISTRATION_COMPLETE),
			$user->email,
			$merge_vars
		);

		//setup default track pool for leaderboard
		$this->set_track();

		if (isset($result) && isset($result->receipt) && null !== $result->receipt) {
			redirect(site_url(array('start', $result->receipt->payment_ref)));
		} else {
			redirect('start');
		}
	}

	private function set_track()
	{
		$track = StudentTrack::find(
			'first',
			array(
				'conditions' => array(
					'user_id = ?',
					$this->auth->user_id()
				),
				'order' => 'created_at desc',
			)
		);

		if($track == null)
		{
			$default = Track::find(
				'first',
				array(
					'conditions' => array(
						'exam_type = ? and active = 1',
						$this->auth->profile()->testing_level()
					),
					'order' => 'created_at desc',
				)
			);

			if($default == null)
				$default = Track::find(
					'first',
					array(
						'conditions' => array('exam_type = ? and active = 1', 'ALL')
					)
				);

			if($default != null)
				StudentTrack::create(array('user_id' => $this->auth->user_id(),
					'track_id' => $default->id));
		}
	}

	private function confirm()
	{
		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

		$plan = $this->input->post('plan');
		$currency = $this->input->post('currency');

		$config_key = $configs[$currency];
		$charges = $this->config->item($config_key);

		$amount = $charges[strtolower($plan)]['charge'];

		$hidden = array(
			'confirm' => 'true',
			'plan' => $plan,
			'currency' => $currency,
			'charge' => $amount,
			'card_number' => $this->input->post('card_number'),
			'card_name' => $this->input->post('card_name'),
			'card_cvc' => $this->input->post('card_cvc'),
			'card_expiry_month' => $this->input->post('card_expiry_month'),
			'card_expiry_year' => $this->input->post('card_expiry_year'),
			'option' => $this->input->post('option'),
			'address1' => $this->input->post('address1'),
			'address2' => $this->input->post('address2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'country' => $this->input->post('country'),
			'subscription' => $this->input->post('subscription')
		);

		Assets::add_module_js('signup', 'confirm.js');
		Template::set_view('confirm');
		Template::set('hidden', $hidden);
		Template::render();
	}

	/**
	 * Start page of the signup process. After a user has signed up or entered code.
	 */
	public function start ( $id = '')
	{
		if (!$this->auth->is_logged_in()) {
			redirect('signup');
		}

		if ($id != '' &&
			$receipt = Receipt::find_by_user_id_and_payment_ref(
				$this->auth->user_id(), $id)) {
					Template::set('receipt', $receipt);
				}

		$contest = Contest::find('last');

		Template::set('contest', $contest);
		Template::set('title', 'Get Started! - EduFocal');
		Template::render();
	}


	//TODO: find a way to put these inside
	//the validation classes
	public function _valid_efcode ( &$input)
	{
		return valid_efcode($input, $this);
	}

	public function unique_email($email)
	{
		$user = User::find_by_email($email);
		if(count($user) > 0) {
			$this->form_validation->set_message(
				'unique_email',
				'The Email you have selected is already in use.'
			);
			return FALSE;
		}
		return TRUE;
	}

	public function _valid_card($number)
	{
		return valid_card($number, $this);
	}
}
