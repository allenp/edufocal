<?php

class Reports extends Admin_Controller
{
	public function __construct ()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->_users();
		$this->_signups_graph();
		$this->_trials();

		$total_life = $this->db->query(
            "SELECT SUM(datediff(last_login, created_on)) / count(*) as days
            from users
            where role_id = 8 and code_id > 0"
        );

        Template::set('lifespan', $total_life->row()->days);

		$users = $this->db->query(
			"Select users.id, first_name, last_name, users.created_on, exam_level, code_id, code, codes.type as code_type
			from users left join codes on users.code_id = codes.id
			order by users.created_on desc
			limit 0, 100")->result();
		Template::set('users', $users);
		Template::render();
	}

	/*
	 * Total signups (both active and never-used accounts.
	 */
	private function _signups_graph()
	{
		$signups = $this->db->query(
			"select count(*) total, date_format(created_on, '%Y-%M') signup_day,
			case when code_id > 0 then 1 else 0 end has_code from users
			where role_id = 8 and date_format(created_on, '%Y-%M') IS NOT NULL
			and created_on > DATE_ADD(CURDATE(), INTERVAL - 365 * 2 DAY)
			group by date_format(created_on, '%Y-%M'),
				case when code_id > 0 then 1 else 0 end
				order by has_code, created_on"
			);

		$completed = array();
		$stalled = array();

		foreach ($signups->result() as $row) {
			if ($row->has_code == 1) {
				$completed[$row->signup_day] = $row->total;

				//fill in the space if it doesn't exist
				if (! isset($stalled[$row->signup_day])) {
					$stalled[$row->signup_day] = 0;
				}
			} else {
				$stalled[$row->signup_day] = $row->total;

				//fill in the space for if it doesn't exist
				if (! isset($completed[$row->signup_day])) {
					$completed[$row->signup_day] = 0;
				}
			}
		}

		$labels = array_keys($completed);

		$js = $this->load->view('reports/signups-js', compact('stalled', 'completed', 'labels'), true);
		Assets::add_js($js, 'inline');
	}

	private function _users()
	{
		$total_users = $this->db->query("SELECT count(*) total from users where role_id = 8;");
		Template::set('total_users', $total_users->row()->total);

		$total_users_w_code = $this->db->query("SELECT count(*) total from users where role_id = 8 and code_id > 0");
		Template::set('total_users_w_code', $total_users_w_code->row()->total);

		$total_dormant_thirty = $this->db->query(
			"select count(id) total from users
			where role_id = 8 and code_id > 0 and last_login < DATE_ADD(CURDATE(), INTERVAL -30 DAY)");
		Template::set('total_dormant_thirty', $total_dormant_thirty->row()->total);

		$paying_members = $this->db->query("select count(*) total
			from users where role_id = 8 and code_id
			IN(SELECT id FROM codes where type <> 'weekly' and active = 1)"
		);
		Template::set('paying_members', $paying_members->row()->total);

		$observer = $this->db->query(
			"SELECT count(*) total from codes
			where user_id > 0 and accepted_on > 0
			and source=1 and type='weekly' and sponsor_id = 2 and active=1");
		Template::set('observer', $observer->row()->total);
	}


	private function _trials()
	{
		$accepted_trials = $this->db->query(
			"select count(*) total from codes, user_trials where codes.id = user_trials.code_id and accepted_on > 0"
		);
		$current_trials = $this->db->query(
			"select count(*) total from users
			inner join user_trials on users.code_id = user_trials.code_id
			inner join codes on codes.id = user_trials.code_id and codes.expiry_date > unix_timestamp()"
		);

		Template::set('accepted_trials', $accepted_trials->row()->total);
		Template::set('current_trials', $current_trials->row()->total);
	}
}
