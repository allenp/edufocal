<?php

/**
 * Enrollment logic for corporate students to be associated with an organization.
 *
 * @package EduFocal
 * @author Jamie Chung
 */

class Enrollment extends Front_Controller
{
    /**
     * Sets up the user model and authorization libraries.
     */
    public function __construct ()
    {
        parent::__construct();

        Template::set_theme('public');

        if (!class_exists('User_model')) {
            $this->load->model('users/User_model', 'user_model');
        }
    }

    public function index ($topic_id)
    {
        settype($topic_id, 'integer');
        try {
            $topic = Topic::find($topic_id);
        } catch ( Exception $e ) {
            Template::set_message('Unable to find associated topic', 'error');
            redirect(site_url());
        }

        $topic_url = site_url('enrollment/'.$topic->id.'-'.$topic->permalink);

        // Check that the URL is correct based on the given ID
        if ( current_url() !=  $topic_url )
        {
            redirect($topic_url);
        }

        $this->load->library('form_validation');
        if ($this->form_validation->run('enrollment') == TRUE)
        {
            // Success
            // TODO: double check that _unique_email callback is working

            $fields = array('first_name', 'last_name', 'email', 'phone');
            $data = array();
            foreach ( $fields as $field )
            {
                $data[$field] = $this->input->post($field);
            }

            // Attempt to persist the data
            try {

                // TODO: Should be using $this->user_model->insert($data)
                // Will fail because we do not have default password
                $user = User::create($data);
                if ( $user->is_invalid() )
                {
                    throw new Exception('Unable to properly create user: '.$user->errors);
                }

                $enrollment = UserEnrollment::create(array(
                    'user_id' => $user->id,
                    'topic_id' => $topic_id,
                    'status' => 'pending'
                ));

                if ( $enrollment->is_invalid() )
                {
                    throw new Exception('Unable to properly create enrollment association: '.$enrollment->errors);
                }

                Template::set_message('Account successfully created!', 'success');
                redirect('enrollment/success');

            } catch ( Exception $e ) {
                Template::set_message($e->getMessage(), 'error');
            }
        }

        Template::set('topic', $topic);
        Template::set_view('enrollment/index');
        Template::render();
    }

    public function success ()
    {
        Template::render();
    }

    public function _unique_email($email)
    {
        var_dump($email);
        exit;

        return unique_email($this);
    }
}