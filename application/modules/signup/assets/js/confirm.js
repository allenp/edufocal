$(function() {
  $("form").bind("submit", function(e) {
    if(typeof $.data(this, "disabledOnSubmit") == 'undefined')
    {
        $.data(this, "disabledOnSubmit", { submitted : true });
        $('input[type=submit], input[type=button]', this).each(function(){
            $(this).attr("disabled", "disabled").val("Processing...");
        });
        return true;
    }
    else
    {
        e.preventDefault();
        return false;
    }
  });
});
