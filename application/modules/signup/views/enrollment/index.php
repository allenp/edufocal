<div class="container">
    <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'role' => 'form')); ?>
        <div class="form-group">
            <legend>Begin enrollment: <?php echo $topic->name; ?></legend>
        </div>

        <?php if ( validation_errors() ) : ?>
        <div class="alert alert-danger">
          <?php echo validation_errors(); ?>
        </div>
        <?php endif; ?>

        <div class="form-group">
          <label class="control-label col-sm-2" for="first-name">First Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="first_name" id="first-name" placeholder="First Name" value="<?php echo set_value('first_name'); ?>" />
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="last-name">Last Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="last_name" id="last-name" placeholder="Last Name" value="<?php echo set_value('last_name'); ?>" />
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="email">Email</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" name="email" id="email" placeholder="email@domain.com" value="<?php echo set_value('email'); ?>" />
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="phone">Phone Number</label>
          <div class="col-sm-10">
            <input type="phone" class="form-control" name="phone" id="phone" placeholder="xxx-xxx-xxxx" value="<?php echo set_value('phone'); ?>" />
          </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>