   
    <div id="main">
		<div id="steps" class="step3"></div>
        
        <div class="centered">
        	<h3>Your payment <?php print isset($receipt) ? 'of ' . strtoupper($receipt->currency) . ' $' . $receipt->amount : '' ; ?> has been processed successfully.</h3>
            <h2>Hi <strong><?php echo $this->auth->profile()->first_name; ?></strong>, welcome to EduFocal!</h2>
            <p>You're finally a member of the EduFocal family, congratulations! Below are a few things that
should help you to get acquainted with the system.</p>
            <ul>
                <?php if(isset($receipt)) : ?>
                <li><p><a href="<?php print site_url(array('account', 'receipts')) ?>">View and print payment receipt</a></p>
                    <?php if(count($receipt->codes)) : ?>
                        <?php foreach($receipt->codes as $code) : ?>
                    <p>1 <?php print strtoupper($code->type) ?> EduFocal subscription</p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <p>Total: <?php print strtoupper($receipt->currency) . ' $' . $receipt->amount ?></p>
                    <p>Please keep a copy of your receipt for reference</p>
                </li>
                <?php endif; ?>
            	<li><p><a href="<?php echo site_url('dashboard/profile'); ?>">Setup your EduFocal profile</a></p>
                <p>Choose your avatar, add your date of birth and your country of origin.</p></li>
                <li><p><a href="<?php echo site_url('dashboard/subjects'); ?>">Browse the EduFocal subjects</a></p>
                <p>The good stuff! Try a few questions, earn some experience points, tackle your weakest
topic. One day at a time.</p></li>
                <li><p><a href="<?php echo site_url('rankings'); ?>">The Leaderboard!</a></p>
                <p>Check it out. Your friends might be on it. Work hard and you'll make it there too.</p></li>
                <li><p><a href="<?php echo $contest->permalink(); ?>">The Referral Board</a></p>
                <p>Hmm, which company is holding a contest over at our referral board? You should find out and
get in on the action!</p></li>
            </ul>
        </div>
    </div>
