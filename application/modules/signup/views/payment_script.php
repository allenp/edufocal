var toggle_visible = function(e) {
    var checked = $("#code_option").is(':checked');
    if(checked){
      $(".card-field").fadeOut();
      $(".educode-field").fadeIn();
    }
    else {
      $(".card-field").fadeIn();
      $(".educode-field").fadeOut();
    }
}

$("#code_option").change(toggle_visible);
$("#card_option").change(toggle_visible);

<?php 
foreach($currency as $curr => &$item)
{
    foreach($item as $p => &$period) 
    {
        if(isset($period['show_publicly']) && $period['show_publicly'] == FALSE) {
            unset($item[$p]);
        }
    }
}
?>
$("#currency").change( function(e) {
    var currencies = $.parseJSON(<?php print json_encode(json_encode($currency)) ?>);
    var options = '';
    for(curr in currencies) {
        if(curr && currencies.hasOwnProperty(curr) && curr == $(this).val()) {
            for(period in currencies[curr]) {
                options += '<option value="' + period + '">' + currencies[curr][period]['valid_for'] + ' - ' + curr + ' $' + currencies[curr][period]['charge'] + '</option>';
            }
        }
    }
    $('#plan').html(options);
});
