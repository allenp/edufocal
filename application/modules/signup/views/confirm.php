    <div id="main">
		<div id="steps" class="step2"></div>

        <div class="pricing">
        	<div class="left">
            	<h2>Change the way You Learn</h2>
            	<p>EduFocal will change the way you learn. Amazing features and rich, interactive content from
many subject areas. Get Started!</p>
                <ul>
                    <li><strong>Unlimited Access to the best Material</strong><br />
                    Students get unlimited access to study material prepared by some of the best Teachers.</li>
                    <li><strong>Makes Learning Fun</strong><br />
                    EduFocal integrate the challenge and rewards of play in a study environment.</li>
                    <li><strong>Reward your efforts</strong><br />
                    Work hard and you’ll be rewarded for your efforts. Guaranteed!</li>
                </ul>
            </div>
            <div class="right">
            	<div class="price">
                	<span class="only">Only</span>
                    <span class="value">$1500 JMD</span>
                    <span class="period">Monthly</span>
                </div>
                <p align="center"><small>Or $4200 JMD for 3 months</small></p>
            </div>
        </div>

        <div id="register">
            <div class="left">

            	<?php echo Template::message(); ?>

			<?php if ( validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
            <?php echo form_open('signup/payment', '', $hidden); ?>
                	<h3>Review and confirm your purchase</h3>
                    <p>1 <?php print ucfirst($hidden['plan']) ?> EduFocal Subscription for <?php print $hidden['currency'] . ' $' . money_format('%(#6n', $hidden['charge']); ?></p>
                    <p>Total: <?php print $hidden['currency'] . ' $' . money_format('%(#6n', $hidden['charge']); ?></p>
                    <p>To be payed by: <?php print $hidden['card_name'] ?></p>
                    <p>Using card: <?php print $hidden['card_number'] ?></p>
                    <p>Address:<br />
                    <?php print $hidden['address1'] ?><br /><?php print $hidden['address2'] ?><br /><?php if(strlen($hidden['city']) > 0) print $hidden['city'] ?></br>
                    <?php if(strlen($hidden['state']) > 0) print $hidden['state'] . '<br />' ?>
                    <?php print $hidden['country'] ?>
                    <p>&nbsp;</p>
                    <button name="submit" type="submit" class="submit-button"><span>Process Payment</span></button>&nbsp;<a href="<?php print site_url('payment'); ?>" alt="Payment">Or cancel</a>
            </form>
            </div>
            <div class="right">
            	<div id="testimonial" class="section">
                    <h3>What Others Are Saying</h3>
                    <ul>
                        <?php $this->load->view('partials/testimonials'); ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

