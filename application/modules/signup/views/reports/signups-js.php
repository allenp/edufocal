    $(function() {
        var ctx = document.getElementById("signupChart").getContext("2d");
        var data = {
            labels : <?php print json_encode($labels) ?>,
                datasets: [
                {
                    label: "Completed signups",
                        fillColor: "rgba(151, 187, 205, 0.2)",
                        strokeColor: "rgba(151, 187, 205, 1)",
                        pointColor: "rgba(151, 187, 205, 1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151, 187, 205, 1)",
                        data: <?php print json_encode(array_values($completed)) ?>
                },
                {
                    label: "Stalled signups",
                        fillColor: "rgba(245, 146, 146, 0.2)",
                        strokeColor: "rgba(245, 146, 146, 1)",
                        pointColor: "rgba(245, 146, 146, 1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(245, 146, 146, 1)",
                        data: <?php print json_encode(array_values($stalled)) ?>
                }
            ]
        };

        var options = {

            scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 20,
                datasetStroke : true,
                datasetStrokeWidth : 2,
                datasetFill : true,
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

        }; 

        var newSignupChart = new Chart(ctx).Line(data, options);
    });
