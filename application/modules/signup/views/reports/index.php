<?php
  Assets::add_js(
      array('Chart.min.js')
  );
?>

<h1>Signups</h1>
<div class="row-fluid">
    <div class="span8">
    <h4>Conversions</h4>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Total users</th>
                <th>... that completed sign-up</th>
                <th>Paying Members*</th>
                <th>Trial offers accepted</th>
                <th>Current trials</th>
                <th>Observer codes used</th>
            </tr>
        <tbody>
            <tr>
                <td><?php print number_format($total_users); ?></td>
                <td><?php print number_format($total_users_w_code); ?> (<?php print round($total_users_w_code / max($total_users,1) * 100, 2) ?>%)</td>
                <td><?php print number_format($paying_members); ?></td>
                <td><?php print number_format($accepted_trials); ?></td>
                <td><?php print number_format($current_trials); ?></td>
                <td><?php print number_format($observer); ?></td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="span4">
    <p>Paying members are those whose current EF code is not a free weekly code (Jamaica Observer, trial).</p>
    </div>
</div>
<div class="row-fluid">
    <div class="span8">
    <h4>Lifespan</h4>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Dormant for last 30 days*</th><th>Avg. life-span (active)</th>
            </tr>
        <tbody>
            <tr>
                <td><?php print number_format($total_dormant_thirty); ?></td>
                <td><?php print number_format($lifespan, 2); ?> days</td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="span4">
    <p>
    Of the activated accounts, those who haven't logged in in past 30 days.
    </p>
    </div>
</div>
<div class="row-fluid">
    <h2>Sign-ups per month</h2>
    <div class="row">
        <div class="span2">
            <span style="background-color: rgba(151, 187, 205, 1); padding: 3px 10px; margin-right: 5px"></span>
            Activated sign-ups
        </div>
        <div class="span2">
            <span style="background-color: rgba(245, 146, 146, 1); padding: 3px 10px; margin-right: 5px"></span>
            Stalled sign-ups
        </div>
    </div>
    <canvas id="signupChart" width="1020" height="400"></canvas>
</div>

<?php if (isset($users) && count($users)) : ?>
<h2>Recent Signups</h2>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Level</th>
            <th>Signup</th>
            <th>Code</th>
            <th>Type</th>
        <tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?php echo anchor(site_url(['dashboard', 'profile', 'view', $user->id]), $user->first_name.' '.$user->last_name); ?></td>
                <td><?php echo $user->exam_level; ?></td>
                <td><?php echo (new \DateTime($user->created_on))->setTimezone(new DateTimeZone('America/Jamaica'))->format('Y-M-j H:i:s P'); ?></td>
                <td><?php echo $user->code_id ? $user->code : ''; ?></td>
                <td><?php echo $user->code_id ? $user->code_type : ''; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
