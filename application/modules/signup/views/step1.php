    <div id="main">
		<div id="steps" class="step1"></div>

        <div class="pricing">
        	<div class="left">
            	<h2>Change the way You Learn</h2>
            	<p>EduFocal will change the way you learn. Amazing features and rich, interactive content from
many subject areas. Get Started!</p>
                <ul>
                    <li><strong>Unlimited Access to the best Material</strong><br />
                    Students get unlimited access to study material prepared by some of the best Teachers.</li>
                    <li><strong>Makes Learning Fun</strong><br />
                    EduFocal integrate the challenge and rewards of play in a study environment.</li>
                    <li><strong>Reward your efforts</strong><br />
                    Work hard and you’ll be rewarded for your efforts. Guaranteed!</li>
                </ul>
            </div>
            <div class="right">
            	<div class="price">
                	<span class="only">Only</span>
                    <span class="value">$1500 JMD</span>
                    <span class="period">Monthly</span>
                </div>
                <p align="center"><small>Or $4200 JMD for 3 months</small></p>
            </div>
        </div>

        <div id="register">
            <div class="left">
            <div class="alert" style="background-color: #992222; text-shadow: none; color: white">
           If you previously registered on EduFocal <a href="/login" style="color: yellow; text-decoration: underline;">please click here to log in</a> after which you will be asked to enter your EduFocal Access Code.
           </div>
            <?php if ( isset($refer)) : ?>
			<div class="alert">
				<h4>Hi, You're here because <em><?php echo $refer->public_name(); ?></em> referred you:</h4>
				<br clear="all" />
			</div>
			<?php endif; ?>

			<?php if ( validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>

                <?php print form_open(site_url('signup')); ?>
                	<h3>Register <small>(All fields required)</small></h3>
                    <table width="90%" cellpadding="5">
                    	<tr>
                        	<td width="40%">Student's First name</td>
                            <td><input type="text" name="first_name" size="41" value="<?php echo set_value('first_name'); ?>" /> </td>
                        </tr>
                        <tr>
                        	<td>Student's Last name</td>
                            <td><input type="text" name="last_name" size="41" value="<?php echo set_value('last_name'); ?>" /> </td>
                        </tr>
                        <tr>
                        	<td>Sex</td>
                            <td>
                            	<label><input type="radio" name="sex" value="m" <?php echo set_radio('sex', 'm'); ?> /> Male</label>
                                <label><input type="radio" name="sex" value="f" <?php echo set_radio('sex', 'f'); ?> /> Female</label>
                                <div class="clearfix"></div>
                            </td>
                        </tr>
                        <tr>
                        	<td>Phone</td>
                            <td><input type="text" name="phone" size="10" value="<?php echo set_value('phone'); ?>" /></td>
                        </tr>
                        <tr>
                        	<td>Which exams will you be sitting?</td>
                            <td><?php echo form_dropdown('exam_level', $exams, $this->input->post('exam_level')); ?></td>
                        </tr>
                        <tr>
                        	<td>Email</td>
                            <td><input type="text" name="email" size="41" value="<?php echo set_value('email'); ?>" /></td>
                        </tr>
                        <tr>
                        	<td>Password (8 characters minimum)</td>
                            <td><input type="password" name="password" size="41" /></td>
                        </tr>
                        <tr>
                        	<td>Confirm Password</td>
                            <td><input type="password" name="password_confirmation" size="41" /></td>
                        </tr>
                        <tr>
                        	<td></td>
                            <td align="left"><button type="submit"><span>Select a Payment Option</span></button></td>
                        </tr>
                    </table>
                <?php print form_close(); ?>
            </div>
            <div class="right">
            	<div id="testimonial" class="section">
                    <h3>What Others Are Saying</h3>
                    <ul>
                    	<?php $this->load->view('partials/testimonials'); ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
