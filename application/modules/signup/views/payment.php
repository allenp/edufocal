    <div id="main">
		<div id="steps" class="step2"></div>
        
        <div class="pricing">
        	<div class="left">
            	<h2>Change the way You Learn</h2>
            	<p>EduFocal will change the way you learn. Amazing features and rich, interactive content from
many subject areas. Get Started!</p>
                <ul>
                    <li><strong>Unlimited Access to the best Material</strong><br />
                    Students get unlimited access to study material prepared by some of the best Teachers.</li>
                    <li><strong>Makes Learning Fun</strong><br />
                    EduFocal integrate the challenge and rewards of play in a study environment.</li>
                    <li><strong>Reward your efforts</strong><br />
                    Work hard and you’ll be rewarded for your efforts. Guaranteed!</li>
                </ul>
            </div>
            <div class="right">
            	<div class="price">
                    <span class="only">Only</span>
                    <span class="value">$1500 JMD</span>
                    <span class="period">Monthly</span>
                </div>
                <p align="center"><small>Or $4200 JMD for 3 months</small></p>
            </div>
        </div>
        
        <div id="register">
            <div class="left">
            	
            	<?php echo Template::message(); ?>

			<?php if ( validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
            	
            	
                <?php print form_open(site_url('signup/payment'), array('id' => 'payment-form')); ?>
                	<h3>Select a payment option</h3>
                    <?php $option = isset($option) && in_array($option, array('code', 'card')) ? $option : 'code'; ?>
                    <table width="80%" cellpadding="5">
                    	<tr>
                        	<td width="10%"><input type="radio" id="code_option" name="option" value="code" <?php if($option == 'code') : ?>checked="checked" <?php endif; ?> <?php //echo set_radio('option', 'code'); ?> /></td>
                            <td colspan="2"><h4><label for="code_option">EduFocal Access Code</label></h4></td>
                        </tr>
                        <tr class="educode-field" <?php if($option != 'code') :?> style="display:none;" <?php endif; ?>>
                        	<td></td>
                            <td colspan="2"><input type="text" class="educode" name="educode" size="41" value="<?php echo set_value('educode'); ?>" />
                           <p style="font-size:small">EduFocal access codes can be found in the Sunday Observer. These codes are valid for 1 week.</p>
                            </td>
                        </tr>
                        <tr>
                        	<td></td>
                            <td colspan="2"><!--<p>Some sort of explanation regarding what this is, how one gets an EduFocal code, etc.</p>--></td>
                        </tr>
                        <!-- 
                        <tr>
                        	<td><input type="radio" name="option" value="paypal" /></td>
                            <td colspan="2"><img src="<?php echo site_url(); ?>/assets/img/paypal.png" alt="Paypal" width="91" height="55" /></td>
                        </tr>
                        <tr>
                        	<td></td>
                            <td colspan="2"><p>Pay with your bank account, credit card or balance without sharing your financial information.</p></td>
                        </tr>-->
                        <tr>
                        	<td><input type="radio" id="card_option" name="option" value="card" <?php if($option == 'card') : ?>checked="checked" <?php endif; ?>/></td>
                            <td colspan="2"><h4><label for="card_option">Credit Card</label></h4></td>
                        </tr>
                        <tr class="card-field" <?php if($option != 'card') :?> style="display:none;" <?php endif; ?>><td colspan="2"><span class="payment-errors"></span></tr>
                        <tr class="card-field" <?php if($option != 'card') :?> style="display:none;" <?php endif; ?>>
                        	<td></td>
                            <td>
                                <p>Pay to access for:</p>
                                <?php 
                                      $curr = array_keys($currencies);
                                      $curr = $curr[0];
                                      $def = array_values($currencies); 
                                      $def = $def[0];
                                ?>
                                <select id="plan" name="plan">
                                <?php foreach($def as $term => $item) : ?>
                                <?php if(!isset($item['show_publicly']) || $item['show_publicly'] == TRUE) : ?>
                                    <option value="<?php print $term ?>"><?php print $item['valid_for']; ?> - <?php print $curr; ?> $<?php print $item['charge'] ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                </select>
                                <select id="currency" name="currency">
                                <?php foreach(array_keys($currencies) as $cu) : ?>
                                    <option value="<?php print $cu; ?>"><?php print $cu; ?></option>
                                <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <p>We Accept</p>
                                <img src="<?php echo site_url(); ?>/assets/img/credit2.png" alt="Visa, Mastercard, Keycard" width="141" height="25" /></td>
                        </tr>
                        <tr class="card-field" <?php if($option != 'card') :?> style="display:none;" <?php endif; ?>>
                        	<td></td>
                            <td><p>Credit Card Number</p>
                            <input name="card_number" type="text" class="card-number" autocomplete="off" size="20" /></td>
                            <td><p>Verification Number</p>
                            <input name="card_cvc" type="text" autocomplete="off" class="card-cvc" size="4" /></td>
                        </tr>
                        <tr class="card-field" <?php if($option != 'card') :?> style="display:none;" <?php endif; ?>>
                            <td></td>
                            <td colspan="2"><p>Card Holder's Name (First &amp; Last)</p>
                            <input name="card_name" type="text" class="card-name" value="<?php print set_value('card_name') ?>" autocomplete="off" size="40" /></td>
                        <tr class="card-field" <?php if($option != 'card') :?> style="display:none;" <?php endif; ?>>
                        	<td></td>
                            <td colspan="2"><p>Expiration</p>
                            <select name="card_expiry_month" class="card-expiry-month">
                            	<option></option>
								<?php foreach ( range(1, 12) as $r ) : ?>
                            	<option><?php echo str_pad($r,2,'0',STR_PAD_LEFT); ?></option>
								<?php endforeach; ?>
                            </select>
                            <select name="card_expiry_year" class="card-expiry-year">
                            	<option></option>
								<?php foreach ( range(11, 20) as $r ) : ?>
                            	<option value="<?php print "20" . $r ?>"><?php echo $r; ?></option>
								<?php endforeach; ?>
                            </select>
                <div class="checkbox">
                    <label style="width: 100%">
                        <input type="checkbox" name="subscription" id="subscription" checked="checked"/>
                        Automatically renew my subscription to EduFocal
                    </label>
                </div>
</td>
                        </tr> 
                        <tr class="card-field" <?php if($option != 'card') :?> style="display:none;" <?php endif; ?>>
                        <td></td>
                        <td colspan="2"><h4>Billing Address</h4>
                        <p>Address 1</p>
                        <input name="address1" type="text" autocomplete="on" size="40" value="<?php print set_value('address1') ?>" class="address" /><br />
                        <p>Address 2</p>
                        <input name="address2" type="text" autocomplete="on" size="40" value="<?php print set_value('address2') ?>" class="address" /><br />
                        <p>City</p>
                        <input name="city" type="text" autocomplete="on" size="40" value="<?php print set_value('city') ?>" class="address" /><br />
                        <p>State (if applicable)</p>
                        <input name="state" type="text" autocomplete="on" size="40" value="<?php print set_value('state') ?>" class="address" />
                        <p>Country</p>
                        <?php $countries = array('JM', 'US', 'CA', 'GB'); ?>
                        <?php print country_dropdown('country', $countries); ?>
                        <tr>
                        	<td></td>
                            <td align="left" colspan="2"><p><button name="submit" type="submit" class="submit-button"><span>Process Payment</span></button></p></td>
                        </tr>
                    </table>
                <?php print form_close(); ?>
            </div>
            <div class="right">
            	<div id="testimonial" class="section">
                    <h3>What Others Are Saying</h3>
                    <ul>
                        <?php $this->load->view('partials/testimonials'); ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

