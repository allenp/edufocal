<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_student_exp_points extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array( 'student_id' => array( 'name' => 'user_id', 'type'=> 'int(11)', 'unsigned' => true ) );
        $this->dbforge->modify_column($prefix . 'student_exp_test_points', $fields);
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $fields = array( 'user_id' => array( 'name' => 'student_id', type=> 'int(11)', 'unsigned' => true ) );
        $this->dbforge->modify_column($prefix . 'student_exp_test_points', $fields);
    }
}
