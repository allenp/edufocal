<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_videos extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;

        //videos
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'title' => array('type' => 'nvarchar(256)'),
            'description' => array('type' => 'nvarchar(256)', 'nullable' => true),
            'permalink' => array('type' => 'nvarchar(256)'),
            'topic_id' => array('type' => 'int(11)', 'unsigned' => true),
            'duration' => array('type' => 'int(11)', 'unsigned' => true),
            'sort_order' => array('type' => 'int(11)'),
            'free' => array('type' => 'int(11)', 'default' => '0'),
			'created_at' => array('type' => 'datetime'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'videos');

        //video_formats
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'video_id' => array('type' => 'int(11)', 'unsigned' => true),
            'format' => array('type' => 'varchar(20)'),
            'w' => array('type' => 'int(11)', 'unsigned' => true),
            'h' => array('type' => 'int(11)', 'unsigned' => true),
            'path' => array('type' => 'nvarchar(256)', 'default' => null),
			'created_at' => array('type' => 'datetime'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'video_sources');

        //video_seen
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'video_id' => array('type' => 'int(11)', 'unsigned' => true),
            'user_id' => array('type' => 'int(11)', 'unsigned' => true),
            'started' => array('type' => 'datetime', 'nullable' => true),
            'ended' => array('type' => 'datetime', 'nullable' => true),
            'seen' => array('type' => 'tinyint', 'default' => '0'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'video_views');

	}

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_table($prefix . 'videos');
        $this->dbforge->drop_table($prefix . 'video_sources');
        $this->dbforge->drop_table($prefix . 'video_views');
    }
}
