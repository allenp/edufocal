<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_subject_score_timestamp extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        
        //cron_rank_subjects
        $fields = array(
            'timestamp' => array('type' => 'int(11)', 'unsigned' => true),
        );

        $this->dbforge->add_column($prefix . 'student_subject_scores', $fields);
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'student_subject_scores', 'timestamp');
    }
}
