<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_update_student_rankings extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array( 'student_id' => array( 'name' => 'user_id', 'type'=> 'int(11)', 'unsigned' => true ) );
        $this->dbforge->modify_column($prefix . 'cron_rank_subjects', $fields);
        $this->dbforge->modify_column($prefix . 'cron_rank_subject_logs', $fields);
        $this->dbforge->modify_column($prefix . 'cron_rank_overalls', $fields);
        $this->dbforge->modify_column($prefix . 'cron_rank_overall_logs', $fields);
        $this->dbforge->modify_column($prefix . 'student_subject_scores', $fields);
        $this->dbforge->modify_column($prefix . 'cron_proficiency_items', $fields);
        $this->dbforge->modify_column($prefix . 'student_exp_points', $fields);
        $this->dbforge->drop_table($prefix . 'students');
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $fields = array( 'user_id' => array( 'name' => 'student_id', type=> 'int(11)', 'unsigned' => true ) );
        $this->dbforge->modify_column($prefix . 'cron_rank_subjects', $fields);
        $this->dbforge->modify_column($prefix . 'cron_rank_subject_logs', $fields);
        $this->dbforge->modify_column($prefix . 'cron_rank_overall_logs', $fields);
        $this->dbforge->modify_column($prefix . 'cron_rank_overalls', $fields);
        $this->dbforge->modify_column($prefix . 'cron_proficiency_items', $fields);
        $this->dbforge->modify_column($prefix . 'student_subject_scores', $fields);
        $this->dbforge->modify_column($prefix . 'student_exp_points', $fields);
    }
}
