<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_dashboard_message extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;

        $this->db->insert('settings', array(
            'name' => 'site.dashboard.message_status',
            'module' => 'core',
            'value' => 0
        ));

        $this->db->insert('settings', array(
            'name' => 'site.dashboard.message',
            'module' => 'core',
            'value' => 'Default Message'
        ));

        
    }

    function down()
    {
        $prefix = $this->db->dbprefix;

        $this->db->where_in('name', array('site.dashboard.message_status', 'site.dashboard.message'));
        $this->db->delete('settings');
    }
}
