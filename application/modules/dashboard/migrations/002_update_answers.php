<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_update_answers extends Migration {
	
	public function up() 
	{
		$fields = array(
			'subject_id' => array('type' => 'int(11)'),
			'topic_id' => array('type' => 'int(11)'),
			'test_type' => array('type' => 'varchar(255)'),
			'question_type' => array('type' => 'varchar(255)'),
			'completed' => array('type' => "enum('yes','no')"),
			'resolved' => array('type' => "enum('yes','no')")
		);
		$this->dbforge->add_column('answers', $fields);

	}
	
	public function down() 
	{
		$this->dbforge->drop_column('answers', 'subject_id');
		$this->dbforge->drop_column('answers', 'topic_id');
		$this->dbforge->drop_column('answers', 'test_type');
		$this->dbforge->drop_column('answers', 'question_type');
		$this->dbforge->drop_column('answers', 'completed');
		$this->dbforge->drop_column('answers', 'resolved');
	}
	
}