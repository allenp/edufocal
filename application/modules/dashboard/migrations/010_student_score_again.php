<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_student_score_again extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
            'curr_rank' => array('type' => 'int(11)', 'unsigned' => true, 'default' => 0),
            'prev_rank' => array('type' => 'int(11)', 'unsigned' => true, 'default' => 0),
            );

        $this->dbforge->add_column($prefix . 'cron_rank_overall_logs', $fields);
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'cron_rank_overall_logs', 'curr_rank');
        $this->dbforge->drop_column($prefix . 'cron_rank_overall_logs', 'prev_rank');
    }
}
