<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_avatar_support extends Migration {
	
	public function up() 
	{
		$fields = array(
			'avatar' => array('type' => 'varchar(255)')
		);
		$this->dbforge->add_column('users', $fields);

	}
	
	public function down() 
	{
		$this->dbforge->drop_column('users', 'avatar');
	}
	
}