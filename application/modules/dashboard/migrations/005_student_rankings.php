<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_student_rankings extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;
        
        //cron_rank_subjects
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'cron_rank_id' => array('type' => 'int(11)', 'unsigned' => true),
            'student_id' => array('type' => 'int(11)', 'unsigned' => true),
            'subject_id' => array('type' => 'int(11)', 'unsigned' => true),
            'level' => array('type' => 'int(4)', 'unsigned' => true),
            'score' => array('type' => 'float', 'default' => 0.00),
            'curr_rank' => array('type' => 'int(11)'),
            'prev_rank' => array('type' => 'int(11)', 'null' => true)
        );
        
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'cron_rank_subjects');

        //cron_rank_subject_logs
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'cron_rank_id' => array('type' => 'int(11)', 'unsigned' => true),
            'student_id' => array('type' => 'int(11)', 'unsigned' => true),
            'subject_id' => array('type' => 'int(11)', 'unsigned' => true),
            'level' => array('type' => 'int(4)', 'unsigned' => true),
            'score' => array('type' => 'float', 'default' => 0.00),
            'curr_rank' => array('type' => 'int(11)'),
            'prev_rank' => array('type' => 'int(11)', 'null' => true)
        );
       
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'cron_rank_subject_logs');

        //cron_rank_overalls
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'cron_rank_id' => array('type' => 'int(11)', 'unsigned' => true),
            'student_id' => array('type' => 'int(11)', 'unsigned' => true),
            'level' => array('type' => 'int(4)', 'unsigned' => true),
            'score' => array('type' => 'float', 'default' => 0.00),
            'curr_rank' => array('type' => 'int(11)'),
            'prev_rank' => array('type' => 'int(11)', 'null' => true)
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'cron_rank_overalls');

        //cron_rank_overall_logs
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'cron_rank_id' => array('type' => 'int(11)', 'unsigned' => true),
            'student_id' => array('type' => 'int(11)', 'unsigned' => true),
            'level' => array('type' => 'int(4)', 'unsigned' => true),
            'score' => array('type' => 'float', 'default' => 0.00),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'cron_rank_overall_logs');

        //rename the old badly named cron_ranks tables
        $this->dbforge->rename_table($prefix . 'cron_ranks', $prefix . 'cron_proficiencies');
        $this->dbforge->rename_table($prefix . 'cron_rank_items', $prefix . 'cron_proficiency_items');
        
        $fields = array( 'process_id' => array( 'name' => 'cron_proficiency_id', 'type'=> 'int(11)', 'unsigned' => true ) );
        $this->dbforge->modify_column($prefix . 'cron_proficiency_items', $fields);

        //new cron_ranks
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'timestamp' => array('type' => 'int(11)', 'unsigned' => true),
            );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'cron_ranks');

        //student_subject_scores
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'student_id' => array('type' => 'int(11)', 'unsigned' => true),
            'subject_id' => array('type' => 'int(11)', 'unsigned' => true),
            'level' => array('type' => 'int(4)', 'unsigned' => true),
            'score' => array('type' => 'float', 'default' => 0.00),
        );
        
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'student_subject_scores');

        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'subject_id' => array('type' => 'int(11)', 'unsigned' => true),
            'topic_id' => array('type' => 'int(11)', 'unsigned' => true),
            'weight' => array('type' => 'float', 'default' => 0.00),
            );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'topic_weights', TRUE);
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_table($prefix . 'student_subject_scores');
        $this->dbforge->drop_table($prefix . 'cron_rank_subjects');
        $this->dbforge->drop_table($prefix . 'cron_rank_subject_logs');
        $this->dbforge->drop_table($prefix . 'cron_rank_overalls');
        $this->dbforge->drop_table($prefix . 'cron_rank_overall_logs');
        $this->dbforge->drop_table($prefix . 'cron_ranks');
        $this->dbforge->drop_table($prefix . 'topic_weights');

        $fields = array( 'cron_proficiency_id' => array( 'name' => 'process_id', type=> 'int(11)', 'unsigned' => true ) );
        $this->dbforge->modify_column($prefix . 'cron_proficiency_items', $fields);

        $this->dbforge->rename_table($prefix . 'cron_proficiency_items', $prefix . 'cron_rank_items');
        $this->dbforge->rename_table($prefix . 'cron_proficiencies', $prefix . 'cron_ranks');
    }
}
