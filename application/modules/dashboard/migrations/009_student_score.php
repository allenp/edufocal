<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_student_score extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true, 'unsigned' => true),
            'user_id' => array('type' => 'int(11)', 'unsigned' => true),
            'score' => array('type' => 'float', 'default' => 0.00),
            'timestamp' => array('type' => 'int(11)', 'unsigned' => true),
            );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'student_scores');

        $this->dbforge->drop_column($prefix . 'cron_rank_overalls', 'level');
        $this->dbforge->drop_column($prefix . 'cron_rank_overall_logs', 'level');
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_table($prefix . 'student_scores');

        $fields = array(
            'level' => array('type' => 'int(4)', unsigned => true),
            );
        $this->dbforge->add_column($prefix . 'cron_rank_overalls', $fields);
        $this->dbforge->add_column($prefix . 'cron_rank_overall_logs', $fields);
    }
}
