<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_question_answer_topic_tables extends Migration
{
    public function up()
    {
        $prefix = $this->db->prefix;
        $fields = array(
            'id' => array('type' => 'int(11)','auto_increment'=>true),
            'user_id' => array('type' => 'int(11)'),
            'topic_id' => array('type' => 'int(11)'),
            'subject_id' => array('type' => 'int(11)'),
            'question_id' => array('type' => 'int(11)'),
            'num_questions' => array('type' => 'int(11)'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id',TRUE);
        $this->dbforge->create_table($prefix . 'cron_student_answer_questions');

        $fields = array(
            'id' => array('type' => 'int(11)','auto_increment'=>true),
            'user_id' => array('type' => 'int(11)'),
            'topic_id' => array('type' => 'int(11)'),
            'subject_id' => array('type' => 'int(11)'),
            'num_questions' => array('type' => 'int(11)'),
            'modified_at' => array('type' => 'datetime'),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id',TRUE);
        $this->dbforge->create_table($prefix . 'cron_student_question_topics');

        $fields = array(
            'id' => array('type' => 'int(11)','auto_increment'=>true),
            'user_id' => array('type' => 'int(11)'),
            'subject_id' => array('type' => 'int(11)'),
            'num_questions' => array('type' => 'int'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id',TRUE);
        $this->dbforge->create_table($prefix . 'cron_topic_questions');
    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix . 'cron_student_answer_questions');
        $this->dbforge->drop_table($prefix . 'cron_student_question_topics');
        $this->dbforge->drop_table($prefix . 'cron_topic_questions');
    }
}
