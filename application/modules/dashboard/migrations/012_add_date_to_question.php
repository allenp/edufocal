<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_date_to_question extends Migration
{
    function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
            'created_at' => array('type' => 'datetime', 'null' => true)
        );
        $this->dbforge->add_column($prefix.'questions', $fields);
    }

    function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix.'questions', 'created_at');
    }
}
