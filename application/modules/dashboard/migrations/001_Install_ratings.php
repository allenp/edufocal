<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_ratings extends Migration {
	
	public function up() 
	{
		$this->dbforge->add_field('`id` int(11) NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('`rating` INT NOT NULL');
		$this->dbforge->add_field("`timestamp` INT( 20 ) NOT NULL");
		$this->dbforge->add_field("`teacher_id` INT NOT NULL");
		$this->dbforge->add_field("`topic_id` INT NOT NULL");
		$this->dbforge->add_field("`student_id` INT NOT NULL");
		$this->dbforge->add_field("`test_id` INT NOT NULL");
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('ratings');

	}
	
	public function down() 
	{
		$this->dbforge->drop_table('ratings');
	}
	
}