<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author: Jay Hall
 * Description: Aggregate user Topic,Answers and Topic Answers
 *
 */


class Aggregate_user_answer
{

    static $ci = NULL;

    public function __construct()
    {
        if(self::$ci == NULL)
            self::$ci =& get_instance();
    }

    public function execute()
    {
        self::$ci->db->query("TRUNCATE cron_topic_questions");
        $topicQuery = "INSERT INTO cron_topic_questions (user_id,subject_id,num_questions)
                    SELECT q.topic_id,t.subject_id, COUNT(q.id)
                    FROM `questions` q
                        INNER JOIN `topics` t on q.topic_id = t.id
                    GROUP BY q.topic_id,t.subject_id";

        self::$ci->db->query($topicQuery);

        self::$ci->db->query("TRUNCATE cron_student_answer_questions");

        $answerQuery = "INSERT INTO cron_student_answer_questions(user_id,topic_id,subject_id,question_id,num_questions)
                    SELECT user_id, topic_id,subject_id,question_id,COUNT(id)
                    FROM   `answers`
                    GROUP BY user_id, topic_id,subject_id,question_id";

        self::$ci->db->query($answerQuery);

        self::$ci->db->query("TRUNCATE cron_student_question_topics");

        $questionQuery = "INSERT INTO cron_student_question_topics(user_id, topic_id, subject_id, num_questions, modified_at, created_at)
                    SELECT user_id, topic_id, subject_id, SUM(num_questions), NOW(), NOW()
                    FROM    `cron_student_answer_questions`
                    GROUP BY user_id,topic_id,subject_id";

        self::$ci->db->query($questionQuery);
    }
}
