<?php

class Settings extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        Template::set('toolbar_title', 'Dashboard Settings');

        $this->load->helper('config_file');
        $this->lang->load('settings');
    }


    public function index ()
    {
        if ($this->input->post('submit'))
        {
            if ($this->save_settings())
            {
                Template::set_message(lang('settings_saved_success'), 'success');
                redirect(SITE_AREA .'/settings/dashboard');
            }
            else
            {
                Template::set_message(lang('settings_error_success'), 'error');
            }
        }

        // Read our current settings
        $settings = $this->settings_lib->find_all();
        Template::set('settings', $settings);

        Template::render();
    }


    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Performs the form validation and saves the settings to the database
     *
     * @access private
     *
     * @return bool
     */
    private function save_settings()
    {
        $this->form_validation->set_rules('dashboard_message_status', 'Message Status', 'required|trim');
        $this->form_validation->set_rules('dashboard_message', 'Message', 'trim');

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }

        $data = array(

            array('name'=> 'site.dashboard.message_status', 'value' => $this->input->post('dashboard_message_status')),
            array('name'=> 'site.dashboard.message', 'value' => $this->input->post('dashboard_message')),

        );

        //destroy the saved update message in case they changed update preferences.
        if ($this->cache->get('update_message'))
        {
            $this->cache->delete('update_message');
        }

        //load activity model
        $this->load->model('activities/activity_model');

        // Log the activity
        $this->activity_model->log_activity($this->current_user->id, lang('bf_act_settings_saved').': ' . $this->input->ip_address(), 'core');

        // save the settings to the DB
        $updated = $this->settings_model->update_batch($data, 'name');

        return $updated;

    }//end save_settings()

}
