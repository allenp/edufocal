<?php

class Info extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();
	}
	
	public function index ()
	{
		define('NL', "\n");
		echo '<pre>';
		echo 'Hello, '.$this->auth->name().NL;
	    echo 'User ID: '.$this->auth->user_id().NL;	
		echo 'School ID: '.$this->auth->profile()->school_id.NL;
		echo 'Testing Type: '.$this->auth->profile()->testing_level().NL;
		exit;
	}
}
