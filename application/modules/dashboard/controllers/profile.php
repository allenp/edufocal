<?php

class Profile extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();
	}

	/**
	 * Overall Profile Details
	 */
	public function index ()
	{

        try
        {
            $track = Track::find($this->auth->track_id());
            if($track == null || $track->active !== 1)
            {
                $count = Track::count(
                    array(
                        'conditions' => array(
                            '(`exam_type` = ? or `exam_type` = ?) and `active` = 1',
                            $this->auth->profile()->testing_level(), 'ALL'
                        )
                    ));

                if( $count > 0)
                {
                    $link = site_url('rankings/change');
                    Template::set_message('Your current leaderboard has been retired. <a href="'.$link.'" class="alert-link">Click here to choose from our newer leaderboards for a chance to win prizes.</a>', "error");
                }
            }
        } catch(Exception $ex) { }

		return $this->view($this->auth->user_id());
	}

	/**
	 * Profile Edit
	 */
	public function edit ()
	{
		foreach ( $this->auth->rules as $info )
            $this->form_validation->set_rules(
                $info['field'],
                $info['label'],
                $info['rules']
            );

		if ( $this->form_validation->run() === TRUE )
		{
			$data = array();
			foreach ( $this->auth->rules as $info )
				$data[$info['field']] = $this->input->post($info['field']);

			$user = User::find($this->auth->user_id());
			$user->first_name =  $this->input->post('first_name');
			$user->last_name = $this->input->post('last_name');
			$user->sex = $this->input->post('sex');
			$user->phone = $this->input->post('phone');
			$user->field('grade',intval($this->input->post('grade')));
			$user->save();

            Template::set_message('Profile updated successfully.', 'success');
			redirect('dashboard/profile');
		}

        $fields = $this->auth->rules;

        //remove these fields becase they cant be set automated (see view)
        unset($fields['birth_day']);
        unset($fields['birth_month']);
        unset($fields['birth_year']);
        unset($fields['sex']);

		Template::set('fields', $fields);
		Template::set('title', 'Edit Profile - EduFocal');
		Template::render();
	}

	/**
	 * Public User Profile
	 */
	public function view($user)
	{
        $u = User::find($user);

        $subjects = Subject::find_by_sql("
            select sub.id, sub.name, sub.exp, sub.level,
            sub.permalink, points_required,
            (points_required - sub.exp) next_level_points
            from exp_points_schedules eps
            inner join (
                select s.id, s.name, s.permalink,
                coalesce(max(sep.level),1) level,
                floor(sum(coalesce(sep.score,0))) exp
                from subjects s
                left outer join student_subject_scores sep
                on sep.user_id = ? and sep.subject_id = s.id
                where s.level = ? and s.active = 1
                group by s.id, s.name, s.permalink
                order by exp desc
            ) sub
            on sub.level = eps.level",
            [$user, $u->testing_level()]
        );

		$colors = [];
		foreach($subjects as $subject) {
			$colors[$subject->permalink] = sprintf("#%06X", mt_rand( 0, 0xFFFFFF));
		}

		$pie = $this->load->view('profile/subjects-pie-js', compact('subjects', 'colors'), true);
		Assets::add_js($pie, 'inline');

		$join = "INNER JOIN ( select user_id, SUM(floor(score)) score
			from student_scores group by user_id) scores_total
			on scores_total.user_id = student_scores.user_id";

		$overall = StudentScore::find([
			'select' => 'id, student_scores.user_id, student_scores.score, scores_total.score total',
			'joins' => $join,
			'conditions' => [
				'student_scores.user_id = ?',
				$user
			],
			'order' => 'id desc'
		]);

		if($this->auth->role_id() == ADMIN_ROLE) {
			$this->_active($user);
		}

		Template::set('overall', $overall);
		Template::set('live_rank', $this->_current_rank($u));
		Template::set('user', $u);
		Template::set('school', $u->school);
		Template::set('subjects', $subjects);
		Template::set('colors', $colors);
		Template::set('title', $u->name()."'s Profile - EduFocal");
		Template::set_view('profile/flyprofile');
		Template::render();
	}

	private function _current_rank($user)
	{
		$track = StudentTrack::find('first', array('conditions' => array('user_id = ?', $user->id)));
		$track_id = $track ? $track->track_id : 0;

		$my_rank = $this->db->query(
			"select position, user_id from (
			SELECT  @rownum := @rownum + 1 as position, user_id FROM student_scores
			JOIN (select @rownum := 0) r
			INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1
			WHERE users.exam_level = ?
			and student_scores.track_id = ?
			order by score desc
		) g where user_id = ?", array(
			$user->testing_level(),
			$track_id,
			$user->id)
		);

        $live_rank = null;
        if($my_rank->num_rows() > 0) {
            $live_rank = $my_rank->row()->position;
		}

		$score = StudentScore::find_by_user_id_and_track_id($user->id, $track_id);
		if($score) {
			Template::set('current_score', $score);
		}

		return $live_rank;
	}

	private function _active($user)
    {
        $users = $this->db->query(
            "select count(id) total, date_format(from_unixtime(started), '%Y-%M-%D') active_month_day
            from tests where tests.user_id = ?
            group by date_format(from_unixtime(started), '%Y-%M-%D')
            order by started asc limit 0,30", $user
        );

        $active = array();
        foreach ($users->result() as $row) {
            $active[$row->active_month_day] = $row->total;
        }

        $js = $this->load->view('profile/active-js', compact('active'), true);
        Assets::add_js($js, 'inline');
    }
}
