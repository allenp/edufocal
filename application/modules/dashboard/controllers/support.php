<?php

class Support extends EF_Dashboard_Controller
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensure_role(REVIEWER_ROLE);
    }

    public function index ()
    {
        if (isset($_POST['checkcode'])) {
            $this->form_validation->set_rules('code', 'code', 'required');

            if ($this->form_validation->run() === TRUE) {
                $code = $this->input->post('code');
                $code = preg_replace('/\s+/', '', $code);
                try {
                    $info = Code::find_by_code($code);
                    if ($info !== NULL) {
                        $data['code'] = $info;
                        if($info->user_id > 0)
                            $data['user'] = User::find($info->user_id);
                        Template::set('info', $data);
                    }
                } catch(ActiveRecord\RecordNotFound $e) {
                    Template::set_message('Code not found. Invalid code entered.', 'error');
                }
            }

            if(isset($code) && count($code) > 0) {
                Template::set('code', $this->input->post('code'));
            }
        }

        if (isset($_POST['get_user'])) {
            $this->form_validation->set_rules('email', 'email', 'required');
            if ($this->form_validation->run() === TRUE) {
                $user = User::find_by_email_and_role_id($this->input->post('email'), 8);
                if ($user != NULL) {
                    Template::set('user', $user);
                    $levels = array('CXC' => 'CXC','GSAT' => 'GSAT');

                    Template::set('levels', $levels);
                    Template::set('level', $user->exam_level);
                } else {
                    Template::set_message('No student found with the information you provided.');
                }
            }
        }

        if (isset($_POST['change_level'])) {

            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('level', 'level', 'required');

            if ($this->form_validation->run() === TRUE) {

                $user = User::find_by_email_and_role_id($this->input->post('email'), 8);
                $level = $this->input->post('level');

                if ($user != NULL && $level != NULL) {
                    if ($user->exam_level !== $level) {
                        $default = Track::find(
                            'first',
                            array(
                                'conditions' => array(
                                    'exam_type = ? and active = 1',
                                    $user->testing_level()
                                ),
                                'order' => 'created_at desc',
                            )
                        );

                        if ($default == null) {
                            $default = Track::find(
                                'first',
                                array(
                                    'conditions' => array('exam_type = ? and active = 1', 'ALL')
                                )
                            );
                        }

                        if ($default != null) {
                            StudentTrack::create(array('user_id' => $user->id,
                                'track_id' => $default->id));
                        }
                    }

                    $user->exam_level = $level;
                    $user->save();

                    Template::set_message("{$user->name()}'s school level has been updated to {$level} successfully.", 'success');
                } else {
                    Template::set_message('Either student not found or level invalid. Contact administrator.', 'error');
                }
            }
        }

        Template::set('title', "Support - EduFocal");
        Template::render();
    }
}
