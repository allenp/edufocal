<?php
/**
 * Student Dashboard
 *
 * @package EduFocal
 */
class Dashboard extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();

		$this->load->helper('text');
		$this->load->helper('typography');
	}

	/*
	 * Basic overview of Students Progress and Messages
	 */
	public function index ()
	{
		$contest = Contest::find('last', array('status' => 1));

        if ($contest) {
            $users = User::find_by_sql("
                SELECT u.*,
                (
                    SELECT COUNT(*)
                    FROM user_referrals ur
                    WHERE ur.user_id = u.id
                    AND ur.status = 'completed'
                    AND ur.contest_id = ?
                ) AS referrals
                FROM users u
                LEFT JOIN
                    user_referrals ur
                    ON ( ur.user_id = u.id )
                WHERE ur.contest_id = ?
                AND (
                    SELECT COUNT(*)
                    FROM user_referrals ur
                    WHERE ur.user_id = u.id
                    AND ur.status = 'completed'
                ) > 0
                AND u.role_id = 8
                GROUP BY ur.user_id
                ORDER BY referrals DESC
            ", array($contest->id, $contest->id));

            $rank = 0;
            $prev = 0;
            $referrals = 0;
            foreach ($users as $user) {
                if ($user->referrals != $prev) {
                    $rank++;
                    $prev = $user->referrals;
                }
                if ($user->id == $this->auth->user_id()) {
                    $referrals = $user->referrals;
                    break;
                }
            }

            if ($referrals == 0) {
                $rank = 0;
            }

            Template::set('referrals', $referrals);
            Template::set('rank', $rank);
		}

		$subjects = Subject::find_by_sql("
			select sub.id, sub.name, sub.exp, sub.level
			from exp_points_schedules eps
            inner join (
                select s.id, s.name, coalesce(max(sep.level),1) level,
                floor(sum(coalesce(sep.score,0))) exp
                from subjects s
                left outer join student_subject_scores sep
                on sep.user_id = ? and sep.subject_id = s.id and sep.track_id = ?
                where s.level = ? and s.active = 1
                group by s.id, s.name
                order by exp desc
            ) sub
			on sub.level = eps.level",
            array($this->auth->user_id(),
            $this->auth->track_id(),
            $this->auth->profile()->testing_level())
        );

        $unread = MessageUser::count(
            array('conditions' => array(
                'user_id = ? and `read` = ? and `deleted` = ?',
                $this->auth->user_id(),'no', 'no')
            )
        );

		$mUser = MessageUser::find_all_by_user_id_and_deleted($this->auth->user_id(), 'no');
		$message_ids = array();
		$status = array();

		foreach ($mUser as $m) {
			$status[$m->message_id] = $m->read;
			$message_ids[] = $m->message_id;
		}

		$messages = array();

		if (count($message_ids ) > 0) {
            $messages = Message::find_all_by_id(
                $message_ids,
                array('limit' => 5, 'order' => 'last_stamp desc')
            );
        }

        $join = "INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1";
        $condition = array("users.exam_level = ? and student_scores.track_id = ?",
            $this->auth->profile()->testing_level(), $this->auth->track_id());

        $top_students = StudentScore::find('all',
            array(
                'limit' => 5,
                'order' => 'score desc',
                'joins' => $join,
                'conditions' => $condition
            )
        );

        $my_rank = $this->db->query("select position, user_id from (
                        SELECT  @rownum := @rownum + 1 as position, user_id FROM student_scores
                        JOIN (select @rownum := 0) r
                        INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1
                        WHERE users.exam_level = ?
                        and student_scores.track_id = ?
                        order by score desc
                    ) g where user_id = ?", array(
                        $this->auth->profile()->testing_level(),
                        $this->auth->track_id(),
                        $this->auth->user_id()));

        $live_rank = null;
        if($my_rank->num_rows() > 0)
            $live_rank = $my_rank->row()->position;

		Template::set('status', $status);
        Template::set('live_rank', $live_rank);
		Template::set('contest', $contest);
		Template::set('subjects', $subjects);
		Template::set('unread', $unread);
		Template::set('messages', $messages);
		Template::set('top_students', $top_students);

		Template::set('title', 'Dashboard - EduFocal');
		Template::render();
	}
}
