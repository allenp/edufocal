<?php

class Subjects extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();
		Template::set_block('dashboard_subnav', 'subjects/partials/dashboard_subnav');
	}

	public function index ()
	{
		redirect('dashboard/subjects/by_subject');
	}

	/**
	 * Teacher Listing Filtered by subject
	 */
	public function by_subject ( $subject = '', $topic = '')
	{
		$subjects = Subject::find_all_by_level_and_active(
			$this->auth->profile()->testing_level(), 1, array('order' => "rank asc"));

		$data['subjects'] = [];
		$data['topics'] = [];

		foreach ($subjects as $s) {
			$data['subjects'][$s->permalink] = $s;
		}

		$data['active_subject'] = $subject;
		$data['active_topic'] = $topic;
		$data['subject_title'] = '';

		if (strlen($subject) > 0) {
			$current_subject = $data['subjects'][$subject];
			$data['subject_title'] = $current_subject->name;

			$topics = Topic::find_all_by_subject_id_and_active(
				$current_subject->id, 1, array("order" => "id"));

			foreach ($topics as $t) {
				$data['topics'][$t->permalink] = $t;
			}
		}

		Template::set('video_enabled', $this->config->item('videos.toggle'));

		if (strlen($topic) > 0) {
			$this->track_check();
			$videos = Video::all(['conditions' => ['topic_id = ?', $data['topics'][$topic]->id]]);
			Template::set('videos', $videos);
		}

		Template::set('title', 'Subjects - EduFocal');
		Template::set($data);
		Template::render();
	}

	public function track_check()
	{
		try
		{
			$track = Track::find($this->auth->track_id());
			if($track !== null && $track->active !== 1)
			{
				$count = Track::count(
					array(
						'conditions' => array(
							'(`exam_type` = ? or `exam_type` = ?) and `active` = 1',
							$this->auth->profile()->testing_level(), 'ALL'
						)
					));

				if( $count > 0)
				{
					$link = site_url('rankings/change');
					Template::set_message(
						"Your current leaderboard has been retired. Your points won't count towards winning any awards. <a href=\"{$link}\" class=\"alert-link\">Click here to opt in to the new leaderboard.</a>", "error");
				}
			}
		}
		catch(Exception $ex)
		{
		}
	}

	/**
	 * Teacher Listing based on First/Last Name
	 */
	public function by_teacher ( $function = '', $value = '', $offset = 0)
	{
		$this->load->helper('form');
		$this->load->library("pagination");

		$config = array(
			'per_page' => 10,
			'first_link' => false,
			'last_link' => false,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
			'full_tag_open' => '<ul class="list-unstyled pager">',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li>',
			'cur_tag_close' => '</li>'
		);

		switch($function) {
		case 'search':
			if(isset($_POST['SearchTerm'])) $search = $this->input->post('SearchTerm');
			else $search = $value;

			$data['teachers'] = Teacher::all(array('conditions' => "level = '".$this->auth->profile()->testing_level()."' AND (first_name REGEXP '".$search."' OR last_name REGEXP '".$search."') AND (SELECT COUNT(*) FROM `teachers_topics` AS `tt` WHERE `tt`.`teacher_id` = `teachers`.`user_id` AND `active` = 1) >= 1", 'limit' => "10", "offset" => $offset));
			$data['total_teachers'] = count(Teacher::all(array('conditions' => "level = '".$this->auth->profile()->testing_level()."' AND (first_name REGEXP '".$search."' OR last_name REGEXP '".$search."') AND (SELECT COUNT(*) FROM `teachers_topics` AS `tt` WHERE `tt`.`teacher_id` = `teachers`.`user_id` AND `active` = 1) >= 1")));

			$config['base_url'] = site_url('dashboard/subjects/by_teacher/'.$function.'/'.$search.'/');
			$config['uri_segment'] = 6;

			break;
		case 'begins_with':
			$data['teachers'] = Teacher::all(array('conditions' => "level = '".$this->auth->profile()->testing_level()."' AND last_name LIKE '".$value."%' AND (SELECT COUNT(*) FROM `teachers_topics` AS `tt` WHERE `tt`.`teacher_id` = `teachers`.`user_id` AND `active` = 1) >= 1", 'limit' => "10", "offset" => $offset));
			$data['total_teachers'] = count(Teacher::all(array('conditions' => "level = '".$this->auth->profile()->testing_level()."' AND last_name LIKE '".$value."%' AND (SELECT COUNT(*) FROM `teachers_topics` AS `tt` WHERE `tt`.`teacher_id` = `teachers`.`user_id` AND `active` = 1) >= 1")));

			$config['base_url'] = site_url('dashboard/subjects/by_teacher/'.$function.'/'.$value.'/');
			$config['uri_segment'] = 6;

			break;
		default:
			if(is_numeric($function)) $offset = $function;
			$data['teachers'] = Teacher::all(array('conditions' => "level = '".$this->auth->profile()->testing_level()."' AND (SELECT COUNT(*) FROM `teachers_topics` AS `tt` WHERE `tt`.`teacher_id` = `teachers`.`user_id` AND `active` = 1) >= 1", 'limit' => "10", "offset" => $offset));
			$data['total_teachers'] = count(Teacher::all(array('conditions' => "level = '".$this->auth->profile()->testing_level()."' AND (SELECT COUNT(*) FROM `teachers_topics` AS `tt` WHERE `tt`.`teacher_id` = `teachers`.`user_id` AND `active` = 1) >= 1")));

			$config['base_url'] = site_url('dashboard/subjects/by_teacher/');
			$config['uri_segment'] = 4;

			break;
		}

		$config['total_rows'] = $data['total_teachers'];

		$this->pagination->initialize($config);

		Template::set('offset', $offset);
		Template::set('title', 'By Teacher/Tutor - Subjects - EduFocal');
		Template::set($data);
		Template::render();
	}

	/**
	 * Topic Overview Based on Teacher
	 */
	public function teacher ( $teacher = '', $subject = '', $topic = '')
	{
		$this->track_check();
		$data['teacher'] = Teacher::find_by_permalink($teacher);
		if(!empty($topic)) {
			$data['subject'] = Subject::find_by_permalink_and_level($subject, $this->auth->profile()->testing_level());
			$data['topic'] = Topic::find_by_permalink_and_subject_id($topic, $data['subject']->id);

			$data['short'] = count(Question::find_all_by_topic_id_and_user_id_and_type_and_status(
				$data['topic']->id,
				$data['teacher']->user_id,
				2,
				'Accepted'
			));

			$data['multi'] = count(Question::find_all_by_topic_id_and_user_id_and_type_and_status(
				$data['topic']->id,
				$data['teacher']->user_id,
				1,
				'Accepted'
			));

			Template::set('title', $data['teacher']->first_name.' '.$data['teacher']->last_name.' - Subjects - EduFocal');
			Template::set_view('dashboard/subjects/teacher_topic');
		} else {
			$this->load->helper('text');
			$data['listings'] = TeachersTopic::find_by_sql("SELECT * FROM `teachers_topics` AS `tt` WHERE `teacher_id` = ? AND `active` = ?", array($data['teacher']->user_id, 1));

			Template::set('title', $data['teacher']->first_name.' '.$data['teacher']->last_name.' - Subjects - EduFocal');
			Template::set_view('dashboard/subjects/teacher_profile');
		}

		Template::set($data);
		Template::render();
	}
}
