<?php

/**
 * Question Queue
 *
 * @package EduFocal
 * @author Shane Shipston
 */
class Question_Queue extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();
		
        $subjects = Subject::find_all_by_level_and_active(
            $this->auth->profile()->testing_level(), 1,
            array('order' => "rank asc"));
		Template::set('subjects', $subjects);
        Template::set_block('dashboard_subnav', 'question_queue/partials/dashboard_subnav');
	}
	
	/**
	 * General Subject Overview
	 */
	public function index ()
	{
        $this->load->helper('db');

        $conditions = array();
        $conditions[] = array('test_type = ?', 'timed');
        $conditions[] = array('question_type = ?', 'short');
        $conditions[] = array('completed = ?', 'yes');
        $conditions[] = array('resolved = ?', 'no');
        $conditions[] = array('answer <> ?', '');
        $conditions[] = array('user_id <> ?', $this->auth->profile()->id);

        $conditions = ef_where($conditions);

        $answers = Answer::all(array(
            'conditions' => $conditions,
            'select' => 'COUNT(*) as count, subject_id',
            'group' => 'subject_id'
        ));
		
		$counts = array();

        foreach($answers as $answer)
        {
            if(isset($counts[$answer->subject_id]))
                continue;

			$subject = Subject::find_by_sql("
                select sub.id, sub.name, sub.exp, sub.level,
                eps.points_required - sub.exp as needed
                from exp_points_schedules eps
                inner join (
                        select s.id, s.name,
                        coalesce(max(sep.level),1) level,
                        floor(sum(coalesce(sep.score,0))) exp
                        from subjects s
                        left outer join student_subject_scores sep
                        on sep.user_id = ? and sep.subject_id = s.id
                        and sep.track_id = ?
                        where s.level = ? and s.active = 1 and s.id = ?
                        group by s.id, s.name
                        order by exp desc
                    ) sub
                on sub.level = eps.level",
                array(
                    $this->auth->user_id(),
                    $this->auth->track_id(),
                    $this->auth->profile()->testing_level(),
                    $answer->subject_id
                )
            );
			
			$data = array();
			foreach($subject as $s) {
				$data = array('level' => $s->level, 'exp' => $s->exp, 'needed' => $s->needed);
			}
			
			$counts[$answer->subject_id] = array('total' => $answer->count, 'data' => $data);
		}
		
		Template::set('counts', $counts);
		
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}
	
	/**
	 * Pending answers based on subject
	 */
	public function subject ($subject = '', $offset = 0)
	{
		$this->load->library("pagination");
		$this->load->helper('text');
        $this->load->helper('db');
		
        $details = Subject::find_by_permalink_and_level(
            $subject,
            $this->auth->profile()->testing_level()
        );
        
        $answers = array();

        $conditions = array();
        $conditions[] = array('answers.question_type = ?', 'short');
        $conditions[] = array('answers.subject_id = ?', $details->id);
        $conditions[] = array('answers.resolved = ?', 'no');
        $conditions[] = array('answers.test_type = ?', 'timed');
        $conditions[] = array('answers.completed = ?', 'yes');
        $conditions[] = array('answers.user_id <> ?', $this->auth->profile()->id);
        $conditions[] = array('answers.answer <> \'\'');

        $cond = ef_where($conditions);

        $count = Answer::count(array('conditions' => $cond));

        if($count > $offset)
        {

            $joins = array();
            $joins[] = 'LEFT JOIN tests t on t.id = answers.test_id';
            $joins[] = 'LEFT JOIN teachers tch on tch.id = answers.teacher_id';
            $joins[] = 'LEFT JOIN users u on u.id = answers.user_id';
            $joins[] = 'LEFT JOIN questions q on q.id = answers.question_id';
            $joins[] = 'LEFT JOIN topics top on top.id = answers.topic_id';

            $answers = Answer::all(array(
                'select' => 'answers.id, answers.user_id, answers.question_id,
                t.started test_started, tch.first_name teacher_first_name, tch.last_name
                teacher_last_name, tch.permalink teacher_permalink,
                u.id student_id, u.first_name student_first_name, u.last_name
                student_last_name, top.permalink topic_permalink, top.name topic_name,
                q.question', 
                'conditions' => $cond,
                'joins' => implode(' ', $joins),
                'limit' => 10,
                'order' => 'id desc',
                'offset' => $offset)
            );
        }

		$config = array(
			'base_url' => site_url('dashboard/question_queue/subject/'.$subject.'/'),
			'total_rows' => $count,
			'per_page' => 10,
			'uri_segment' => 5,
			'first_link' => false,
			'last_link' => false,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
			'full_tag_open' => '<ul class="list-unstyled pager">',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li>',
			'cur_tag_close' => '</li>'
		);
		
		$this->pagination->initialize($config);
		
		Template::set('subject', $subject);
		Template::set('offset', $offset);
		Template::set('count', $count);
		Template::set('answers', $answers);
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}

	/**
	 * Answer Review
	 */
    public function review($id)
    {

        $this->load->helper('db');

        $joins = array();
        $joins[] = 'LEFT JOIN subjects s on s.id = answers.subject_id';
        $joins[] = 'LEFT JOIN teachers t on t.id = answers.teacher_id';
        $joins[] = 'LEFT JOIN users u on u.id = answers.user_id';
        $joins[] = 'LEFT JOIN topics top on top.id = answers.topic_id';
        $joins[] = 'LEFT JOIN tests tst on tst.id = answers.test_id';

        $answer = Answer::first(array(
            'select' => 'answers.id, t.first_name teacher_first_name,
            t.last_name teacher_last_name, answers.subject_id,
            answers.question_id, answers.answer,
            t.permalink teacher_permalink, s.name subject_name,
            top.name topic_name, u.first_name student_first_name,
            u.last_name student_last_name, u.id user_id,
            tst.finished test_finished',
            'joins' => implode(' ', $joins),
            'conditions' => array('answers.id = ?', $id),
        ));

        $conditions = array();
        $conditions[] = array('answers.question_type = ?', 'short');
        $conditions[] = array('answers.resolved = ?', 'no');
        $conditions[] = array('answers.test_type = ?', 'timed');
        $conditions[] = array('answers.completed = ?', 'yes');
        $conditions[] = array('answers.user_id <> ?', $this->auth->profile()->id);
        $conditions[] = array('answers.answer <> \'\'');

        $next = Answer::first(array(
            'select' => 'answers.id',
            'conditions' => ef_where(array_merge($conditions, array(array('answers.id > ?', $id)))), 
            'order' => 'id asc'
        ));

        $previous = Answer::first(array(
            'select' => 'answers.id',
            'conditions' =>  ef_where(array_merge($conditions, array(array('answers.id < ?', $id)))),
            'order' => 'id desc'
        ));

		$responded = Response::find_by_answer_id_and_user_id_and_response_id($id, $this->auth->profile()->id, 0);
		$responses = Response::find_all_by_answer_id_and_response_id($id, 0, array('order' => 'responded DESC'));
		
        if(isset($next) && count($next) > 0)
            Template::set('next', $next->id);
        if(isset($previous) && count($previous) > 0)
            Template::set('previous', $previous->id);

		Template::set('responded', $responded);
		Template::set('responses', $responses);
		Template::set('answer', $answer);
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}
	
	/**
	 * User Voted Yes
	 */
    public function vote_yes($id, $response = 0)
    {
		$check = Response::find_by_answer_id_and_user_id($id, $this->auth->profile()->id);
		
		if(count($check) == 0) {
			Response::create(
				array(
					'user_id' => $this->auth->profile()->id,
					'answer_id' => $id,
					'vote' => 'yes',
					'response_id' => $response
				)
			);
		}
		redirect('dashboard/question_queue/review/'.$id);
	}
	
	/**
	 * User Voted No
	 */
	public function vote_no($id, $response = 0) {
		$check = Response::find_by_answer_id_and_user_id($id, $this->auth->profile()->id);
		
		if(count($check) == 0) {
			Response::create(
				array(
					'user_id' => $this->auth->profile()->id,
					'answer_id' => $id,
					'vote' => 'no',
					'comment' => $this->input->post('comments'),
					'answer' => $this->input->post('your_answer'),
					'response_id' => $response
				)
			);
		}
		redirect('dashboard/question_queue/review/'.$id);
	}

	/**
	 * Undo Response
	 */
	public function undo($id) {
		$check = Response::find($id);
		if($check->user_id == $this->auth->profile()->id)
			$check->delete();
		
		redirect('dashboard/question_queue/review/'.$check->answer_id);
	}
}
