<?php

class Cron_Aggregate extends EF_Cron_Controller
{
    public function __construct ()
    {
        parent::__construct();
        $this->load->library('aggregate_user_answer');
    }

    public function index ()
    {
        print "executing aggregate answers";
        $agg = new Aggregate_user_answer();
        $agg->execute();
    }
}
