<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['set_allow_name_change_note']	= 'Allow users to change their display name after registering?';
$lang['settings_saved_success']     = 'Your settings were successfully saved.';
$lang['settings_error_success']     = 'There was an error saving your settings.';