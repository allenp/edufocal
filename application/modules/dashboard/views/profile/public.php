<?php 
    if(isset($subjects))
    {
        Assets::add_js(array('cssbar'));
        Assets::add_js($this->load->view('profile/view-script', array('subjects', $subjects), TRUE), 'inline');
    }
?>
<div class="row public-profile-header">
<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
<?php $avatar = $user->avatar;
if(empty($avatar)) : ?>
    <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $user->name(); ?>" class="user" />
<?php else : ?>
    <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $user->name(); ?>" class="user" />
<?php endif; ?>
</div>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
    <h2><?php echo $user->name(); ?></h2>
    <a href="<?php echo site_url('messages/compose/'.$user->id); ?>" class="btn btn-info">Send Message</a>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<table class="table table-bordered table-bordered-only">
    <tr>
        <td><h4>Grade</h4></td>
        <td width="40%"><?php $grade = $user->profile('grade');
        if(isset($grade)) echo $grade;
        else echo 'N/A'; ?></td>
        <td colspan="2"><h4><?php echo $referrals; ?> total referral<?php echo $referrals != 1 ? 's' : ''; ?></h4></td>
    </tr>
    <tr>
        <td><h4>School</h4></td>
        <td><?php $school = $user->school->name;
        if(isset($school)) echo $school;
        else echo 'N/A'; ?></td>
        <td colspan="2"><h4><?php echo floor(empty($overall) ? 0 : $overall->score); ?> points 
<?php if(isset($overall) && abs($overall->score - $overall->total) > 1):?>
(<strong><?php print floor($overall->total); ?></strong> of all time)<?php endif; ?>

</h4></td>
    </tr>
    <tr>
        <td><h4>Country</h4></td>
        <td>Jamaica</td>
        <td><!--<h4>Level #</h4>--></td>
        <td><!--<div class="bar_graph"><div class="bar_yellow" style="width: 100px;"></div></div>--></td>
    </tr>
    <?php if($this->auth->role_id() == ADMIN_ROLE): ?>
    <tr>
        <td><h4>Email:</h4></td>
        <td><?php print $user->email ?></td>
    </tr>
    <?php endif; ?>
</table>
</div> <!-- row -->
</div>
<div class="row">
<div class="col-lg-12">
<h3><?php echo $user->first_name; ?>'s Experience Levels by Subject</h3>
<table class="table table-bordered">
    <thead>
    <tr>
        <th width="40%"><a href="#">Subject</a></th>
        <th width="15%"><a href="#">Experience Points</a></th>
        <th colspan="2"><a href="#">Level</a></th>
    </tr>
    </thead>
    <?php foreach($subjects as $subject) : ?>
    <tr>
        <td><?php echo $subject->name; ?></td>
        <td><?php echo $subject->exp; ?></td>
        <td align="right"><?php echo $subject->level; ?></td>
        <td width="40%">
            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valueno="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% complete</span>
                </div>
            </div>
<div class="bar_graph"><div id="<?php print $subject->permalink ?>" class="bar_yellow"></div></div></td>
    </tr>
    <?php endforeach; ?>
</table>
</div>
</div> <!-- row -->
