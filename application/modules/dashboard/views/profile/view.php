<?php print Template::message(); ?>
<h3 class="flush-top">Personal Information</h3>
<div class="clearfix bordered">
    <div class="col-sm-4 pull-right">
        <?php $avatar = $this->auth->profile()->avatar;
        if(empty($avatar)) : ?>
            <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" />
        <?php else : ?>
            <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" />
        <?php endif; ?>
    </div>
    <div class="col-sm-8">
    <table class="table">
        <tr>
            <th>Name</th>
            <td><?php echo $this->auth->profile()->first_name; ?></td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td><?php echo $this->auth->profile()->last_name; ?></td>
        </tr>
        <tr>
            <th>Phone Number</th>
            <td><?php echo $this->auth->profile()->phone; ?></td>
        </tr>
        <tr>
            <th>Gender</th>
            <td><?php $gender = $this->auth->profile()->sex;
            if($gender == 'm') echo 'Male';
            else echo 'Female'; ?></td>
        </tr>
        <tr>
            <th>Grade</th>
            <td><?php echo $this->auth->profile()->grade; ?></td>
        </tr>
        <tr>
            <td colspan="2"> <a href="<?php echo site_url("dashboard/profile/edit"); ?>" class="btn btn-primary btn-lg">Edit</a></td>
        </tr>
    </table>
    </div>
</div>
<h3>Your Overall Experience Level</h3>
<table class="table table-bordered">
    <tr>
        <td width="38%"><?php echo isset($overall) ? round($overall->score) : 0; ?> Points
        <?php if(isset($overall) && abs($overall->score - $overall->total) > 1):?>
        (<strong><?php print floor($overall->total); ?></strong> of all time)<?php endif; ?>
        </td>
    </tr>
</table>
<h3><?php echo $this->auth->profile()->first_name; ?>'s Experience Levels by Subject</h3>
<table class="table table-bordered">
    <thead>
        <th>Subject</th>
        <th>Experience</th>
        <th colspan="2">Current Level</th>
        <th></th>
        <th align="right">Next Level</th>
        <th align="right">Question Queue</th>
    </thead>
    <tbody>
    <?php foreach($subjects as $subject) : ?>
    <tr>
        <td><?php echo $subject->name; ?></td>
        <td><?php echo $subject->exp; ?></td>
        <td align="right"><?php echo $subject->level; ?></td>
        <td><div class="bar_graph"><div id="<?php print $subject->permalink ?>" class="bar_yellow"></div></div></td>
        <td class="tutor"><?php if($subject->level >= 65) : ?>
        <img src="<?php echo site_url('assets/dbrd/img/star.png'); ?>" alt="Tutor" /><br />Tutor
        <?php endif; ?></td>
        <td align="right"><?php print ceil($subject->next_level_points); ?></td>
        <td align="right"><a href="<?php echo site_url('dashboard/question_queue/subject/'.$subject->permalink); ?>"><?php if(array_key_exists($subject->id, $counts)) echo $counts[$subject->id]; else echo '0'; ?></a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
