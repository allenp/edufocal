    <?php print Template::message(); ?>
    <?php if ( validation_errors() ) : ?>
    <div class="alert">
        <?php echo validation_errors(); ?>
    </div>
    <?php endif; ?>
    <h2>My Profile</h2>
    <h3>Personal Information</h3>
    <div class="bordered">
        <?php echo form_open_multipart('dashboard/profile/edit', 'class="form-vertical"'); ?>
            <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pull-right">
                <?php $avatar = $this->auth->profile()->avatar;
                if(empty($avatar)) : ?>
                    <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" />
                <?php else : ?>
                    <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" />
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?php foreach ( $fields as $info ) : ?>
            <?php echo form_input($info['field'], $this->auth->profile()->{$info['field']}, $info['label'], 'class="form-control"'); ?>
            <?php endforeach; ?>
            <?php echo form_dropdown('sex', array('m' => 'Male', 'f' => 'Female'), $this->auth->profile()->sex, 'Gender', 'class="form-control"'); ?>
            <?php echo form_input('grade', $this->auth->field('grade'), 'Grade', 'class="form-control"'); ?></td>
            </div>
            </div>
            <div class="form-group clearfix">
                <button type="submit" class="btn btn-primary btn-lg">Save</button> or <a href="<?php echo site_url('dashboard/profile'); ?>">cancel</a></p>
            </div>
        </form>
    </div>
