<?php
	Assets::add_js(['Chart.min.js'])
?>
<div class="profile report-header clearfix">
<div class="row">
	<div class="col-sm-8">
	<?php $avatar = $user->avatar;
	if(empty($avatar)) : ?>
		<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $user->name(); ?>" class="user" />
	<?php else : ?>
		<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $user->name(); ?>" class="user" />
	<?php endif; ?>
		<h1>
		<?php if($this->auth->role_id() == ADMIN_ROLE) : ?>
		<a href="<?php print site_url(['student', 'reports', $user->id]) ?>">
		<?php endif ?>
<?php print $user->name() ?>
		<?php if($this->auth->role_id() == ADMIN_ROLE) : ?>
		</a>
		<?php endif ?>
		</h1>
		<span class="school"><?php print trim($user->school->name) ?></span><?php if($user->grade > 0) : ?>, <span class="grade">Grade: <?php print $user->grade ?></span>
		<?php endif; ?>
		<?php if($this->auth->role_id() == ADMIN_ROLE): ?>
		<span class="email"><?php print $user->email ?></span>
		<?php endif; ?>
		<br />
		<a style="margin-top: 10px" href="<?php echo site_url(['messages', 'compose', $user->id]); ?>" class="btn btn-info btn-sm">Send Message</a>
	</div>
	<div class="col-sm-4">
	<?php if(null !== $live_rank) : ?>
	<h2 style="text-align: right"><?php if($live_rank <= 10) : ?><span class="glyphicon glyphicon-star"></span><?php endif ?> #<?php print $live_rank ?> <span style="color: #ccc"><?php print $current_score != null ? floor($current_score->score) : 0?> Points</span></h2>
	<p style="color: #ccc; text-align: right">for current school year</p>
	<?php endif ?>
	</div>
</div>
</div>
<div class="report-body clearfix" style="background-color: #fff; padding-bottom: 15px; margin-top: 10px;">
	<?php if(isset($subjects) && isset($overall)) : ?>
	<div class="col-xs-12">
		<h3 style="text-align:center; margin: 1em 0.8em;">Overall Points</h3>
	</div>
	<div class="col-sm-6 report-panels">
		<canvas id="subjectspie" width=300 height=300></canvas>
        <?php if(isset($overall)) : ?>
		<span id="overallpie"><?php print $overall->total ?></span>
        <?php endif ?>
	</div>
	<div class="col-sm-6 report-panels">
	<ul id="subjectskey" class="list-unstyled">
	<?php foreach($subjects as $sub) : ?>
		<?php if($sub->exp != 0) : ?>
		<li><span class="color" style="background-color: <?php print $colors[$sub->permalink] ?>"><?php print $sub->exp ?></span> <?php print $sub->name ?> [Level <?php print $sub->level ?>]</li>
		<?php endif ?>
	<?php endforeach ?>
	</ul>
	</div>
	<?php else: ?>
	<canvas id="subjectspie" width=0 height=0></canvas>
	<p style="margin: 15px; text-align: center; font-style: italic; color: #999;">Hey there! When you've done your first test your scores will appear here!</p>
	<?php endif ?>

	<?php if($this->auth->role_id() == ADMIN_ROLE) : ?>
	<div class="col-xs-12">
        <h3 style="margin-top: 35px; text-align:center">Tests Taken</h3>
	</div>
	<div class="col-xs-11 col-sm-10 col-sm-offset-1">
		<canvas id="activity" height=200 width=320></canvas>
	</div>
	<?php endif ?>
</div>
