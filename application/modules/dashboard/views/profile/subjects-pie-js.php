var dataset = [];
<?php $total = 0; foreach ($subjects as $sub) : $total += $sub->exp; ?>
dataset.push({ value: <?php print $sub->exp > 0 ? $sub->exp : 0 ?>, label: "<?php print $sub->name ?>", color: "<?php print $colors[$sub->permalink] ?>" });
<?php endforeach ?>
var ctx = document.getElementById("subjectspie").getContext("2d");
var subjectsChart = new Chart(ctx).Doughnut(dataset, {});
