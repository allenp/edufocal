<h2>Question Queue</h2>
<p>You can never have too much experience points. Can you? Check out the subjects below.</p>
<table class="table table-bordered">
    <thead>
    <tr>
        <th width="45%">Subjects:</th>
        <th colspan="3">My current stats:</th>
    </tr>
    <tr>
        <th></th>
        <th>Experience Points</th>
        <th>Level</th>
        <th>Needed for next level</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($subjects as $subject) : 
        if(array_key_exists($subject->id, $counts)) : ?>
    <tr>
        <td><h3><a href="<?php echo site_url('dashboard/question_queue/subject/'.$subject->permalink); ?>"><?php echo $subject->name; ?></a> - <?php echo $counts[$subject->id]['total']; ?> Question<?php if($counts[$subject->id]['total'] != 1) echo 's'; ?></h3></td>
        <td><?php echo (isset($counts[$subject->id]['data']['exp'])) ? $counts[$subject->id]['data']['exp'] : '0'; ?></td>
        <td><?php echo (isset($counts[$subject->id]['data']['level'])) ? $counts[$subject->id]['data']['level'] : '1'; ?></td>
        <td><?php echo (isset($counts[$subject->id]['data']['needed'])) ? ceil($counts[$subject->id]['data']['needed']) : '0'; ?></td>
    </tr>
    <?php endif;
    endforeach; ?>
    </tbody>
</table>
