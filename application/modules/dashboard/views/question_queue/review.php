<?php
Assets::add_module_js('dashboard', 'question_queue_review'); 
?>
<div class="heading padded">
    <div class="left">
        <h3><?php echo $answer->subject_name; ?>: <?php echo $answer->topic_name; ?> <small>by <a href="<?php echo site_url('dashboard/subjects/teacher/'.$answer->teacher_permalink); ?>"><?php echo $answer->teacher_first_name.' '.$answer->teacher_last_name; ?></a></small></h3>
    </div>
    <div class="right">
        <?php if(isset($previous)) { ?>
        <a href="<?php echo site_url('dashboard/question_queue/review/'.$previous); ?>"><img src="<?php echo site_url('assets/dbrd/img/previous_question.png'); ?>" alt="Previous Question" /></a>	
        <?php }
        if(isset($next)) { ?>
        <a href="<?php echo site_url('dashboard/question_queue/review/'.$next); ?>"><img src="<?php echo site_url('assets/dbrd/img/next_question.png'); ?>" alt="Next Question" /></a>
        <?php } ?>
    </div>
</div>
<div class="wrapper">
    <p>Question answered by <a href="<?php echo site_url("dashboard/profile/view/".$answer->user_id); ?>"><?php echo $answer->student_first_name . ' '. $answer->student_last_name; ?></a> on <?php echo date("F j, Y", $answer->test_finished); ?></p>
    <div class="selection">
        <h3>Q:</h3>
        <div class="question">
            <?php echo $answer->question->question; ?>
                <?php if(!empty($answer->question->choices)) {
                $choices = json_decode($answer->question->choices);
                foreach($choices as $num => $choice) { ?>
                <div class="clearfix"></div>
                <h3><?php echo $num; ?>.</h3>
                <div class="question">
                    <?php echo $choice; ?>
                </div>
                <?php } 
            } ?>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="answer">
        <div class="submitted">
            <h3>A:</h3>
            <div class="question">
                <?php if(!empty($answer->question->choices)) {
                    $answers = json_decode($answer->answer);
                    foreach($answers as $num => $choice) { ?>
                    <div class="clearfix"></div>
                    <h3><?php echo $num; ?>.</h3>
                    <p><?php echo nl2br($choice); ?></p>
                <?php } 
                } else { ?>
                <p><?php echo nl2br($answer->answer); ?></p>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="vote">
            <?php if(count($responded) > 0) {
                if($responded->vote == 'no') { ?>
                    <p class="no">You Voted NO that <?php echo $answer->student_first_name; ?>'s answer is incorrect.</p>
                    <p class="undo"><a href="<?php echo site_url('dashboard/question_queue/undo/'.$responded->id); ?>">Undo</a></p>
                    <div class="clearfix"></div>
                <?php } elseif($responded->vote == 'yes') { ?>
                    <p class="yes">You Voted YES that <?php echo $answer->student_first_name; ?>'s answer is correct.</p>
                    <p class="undo"><a href="<?php echo site_url('dashboard/question_queue/undo/'.$responded->id); ?>">Undo</a></p>
                    <div class="clearfix"></div>
                <?php }
            } else { ?>
                <div id="vote_selection">
                    <p>Is <?php echo $answer->student_first_name; ?>'s answer correct?</p>
                    <a href="<?php echo site_url('dashboard/question_queue/vote_yes/'.$answer->id); ?>"><img src="<?php echo site_url('assets/dbrd/img/vote_yes.png'); ?>" alt="Yes" /></a>
                    <a href="#" id="vote_no"><img src="<?php echo site_url('assets/dbrd/img/vote_no.png'); ?>" alt="No" /></a>
                </div>
                <?php print form_open(site_url('dashboard/question_queue/vote_no/'.$answer->id), array('id'=>'voting_no', 'class'=>'hidden')); ?>
                    <p>You are voting NO to <?php echo $answer->student_first_name; ?>'s answer. Please provide the answer you feel is correct.</p>
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td><label>Comments</label></td>
                            <td><label>Your answer</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><textarea name="comments" cols="50" rows="6"></textarea></td>
                            <td valign="top"><textarea name="your_answer" rows="6" cols="50"></textarea></td>
                            <td valign="top"><p><button type="submit" class="blue">Save</button> or <a href="#" id="cancel_vote">cancel</a></p></td>
                        </tr>
                    </table>
                <?php print form_close(); ?>
            <?php } ?>
        </div>
    </div>
    <h3>Responses to <?php echo $answer->student_first_name; ?>'s answer</h3>
    <?php if(count($responses) > 0) {
        foreach($responses as $response) { ?>
            <div class="response <?php echo $response->vote; ?>">
                <?php $avatar = $response->user->avatar;
                if(empty($avatar)) : ?>
                    <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" width="40" height="40" class="img" />
                <?php else : ?>
                    <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" width="40" height="40" class="img" />
                <?php endif; ?>
                <div class="user">
                    <h4><a href="<?php echo site_url("dashboard/profile/view/".$response->user->id); ?>"><?php echo $response->user->name(); ?></a></h4>
                    <p><?php echo $answer->subject->abbrev; ?> Level </p>
                    <div class="clearfix"></div>
                </div>
                <div class="vote">
                    <h3>Vote: <span class="voted_<?php echo $response->vote; ?>"><?php echo strtoupper($response->vote); ?></span></h3>
                </div>
                <div class="clearfix"></div>
                <?php if($response->vote == 'no') { ?>
                    <p class="comments"><strong><?php echo $response->user->first_name; ?>'s Comments:</strong><br />
                    <?php echo nl2br($response->comment); ?></p>
                    <div class="submitted">
                        <h3>A:</h3>
                        <div class="question">
                            <p><strong><?php echo nl2br($response->answer); ?></strong></p>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php if(count($responded) == 0) { ?>
                    <div class="correct">
                        <p>Is <?php echo $response->user->first_name; ?>'s answer correct?</p>
                        <a href="<?php echo site_url('dashboard/question_queue/vote_yes/'.$answer->id.'/'.$response->id); ?>"><img src="<?php echo site_url('assets/dbrd/img/vote_yes.png'); ?>" alt="Yes" /></a>
                        <a href="#" class="vote_no" title="<?php echo $response->id; ?>"><img src="<?php echo site_url('assets/dbrd/img/vote_no.png'); ?>" alt="No" /></a>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    
                    <h3>Responses to <?php echo $response->user->first_name; ?>'s answer</h3>
                    <?php
                    if(count($response->responses) > 0) {
                        foreach($response->responses as $r) { ?>
                            <div class="response <?php echo $r->vote; ?>">
                                <?php $avatar = $r->user->avatar;
                                if(empty($avatar)) : ?>
                                    <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" width="40" height="40" class="img" />
                                <?php else : ?>
                                    <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" width="40" height="40" class="img" />
                                <?php endif; ?>
                                <div class="user">
                                    <h4><a href="<?php echo site_url("dashboard/profile/view/".$r->user->id); ?>"><?php echo $r->user->name(); ?></a></h4>
                                    <?php $temp_user = $r->user->subject_score($answer->subject_id); ?>
                                    <p><?php echo $answer->subject->abbrev; ?> Level <?php echo ($temp_user) ? $temp_user->level : 1; ?> | <?php echo ($temp_user) ? floor($temp_user->score) : 0; ?> Points</p>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="vote">
                                    <h3>Vote: <span class="voted_<?php echo $r->vote; ?>"><?php echo strtoupper($r->vote); ?></span></h3>
                                </div>
                                <div class="clearfix"></div>
                                <?php if($r->vote == 'no') { ?>
                                <p class="comments"><strong><?php echo $r->user->first_name; ?>'s Comments:</strong><br />
                                <?php echo nl2br($r->comment); ?></p>
                                <div class="submitted">
                                    <h3>A:</h3>
                                    <div class="question">
                                        <p><strong><?php echo nl2br($r->answer); ?></strong></p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                        }
                    } else { ?>
                    <p>There have been no responses to this question yet. Be the first and vote now!</p>
                    <?php } ?>
                <?php } ?>
            </div>
    <?php }
    } else { ?>
    <p>There have been no responses to this question yet. Be the first and vote now!</p>
    <?php } ?>
</div>
