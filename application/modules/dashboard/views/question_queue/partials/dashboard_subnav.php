<nav class="category">
    <ul class="nav navbar-nav">
        <?php foreach($subjects as $subject) : ?>
        <li<?php if($this->uri->segment(4) == $subject->permalink) echo ' class="active"'; ?>>
            <a href="<?php echo site_url('dashboard/question_queue/subject/'.$subject->permalink); ?>">
            <?php echo $subject->abbrev; ?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</nav>
