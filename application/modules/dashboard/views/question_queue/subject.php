    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Topic</th>
            <th width="16%">Teacher/Tutor</th>
            <th width="16%">Answered By</th>
            <th width="11%">Answered On</th>
            <th width="22%">Question Excerpt</th>
            <th width="12%"></td>
        </tr>
        </thead>
        <tbody>
        <?php if(count($answers) == 0) echo '<tr><td colspan="7">No Questions in the Queue</td></tr>';
        foreach($answers as $answer) : ?>
        <tr>
            <td><!--<a href="<?php echo site_url("dashboard/subjects/by_subject/".$subject.'/'.$answer->topic_permalink); ?>">--><?php echo $answer->topic_name; ?><!--</a>--></td>
            <td><!--<a href="<?php echo site_url("dashboard/subjects/teacher/".$answer->teacher_permalink); ?>">--><?php echo $answer->teacher_first_name.' '.$answer->teacher_last_name; ?><!--</a>--></td>
            <td><a href="<?php echo site_url("dashboard/profile/view/".$answer->student_id); ?>"><?php echo $answer->student_first_name.' '.$answer->student_last_name; ?></a></td>
            <td><?php echo date("m/d/Y", $answer->test_started); ?></td>
            <td><?php echo word_limiter(strip_tags($answer->question), 15); ?></td>
            <td><a href="<?php echo site_url('dashboard/question_queue/review/'.$answer->id); ?>">Review Answer</a></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="3">Showing <?php echo $offset + 1 <= $count ? $offset + 1 : $count; ?> - <?php echo $offset + 10 <= $count ? $offset + 10 : $count; ?> of <?php echo $count; ?> match<?php echo $count != 1 ? "es": ""; ?></td>
            <td colspan="3" align="right">
                <?php echo $this->pagination->create_links(); ?>
            </td>
        </tr>
        </tbody>
    </table>
