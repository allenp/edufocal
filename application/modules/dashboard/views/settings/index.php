<?php if (validation_errors()) : ?>
<div class="alert alert-block alert-error fade in">
  <a class="close" data-dismiss="alert">&times;</a>
	<?php echo validation_errors(); ?>
</div>
<?php endif; ?>


<div class="admin-box">
	<h3><?php echo $toolbar_title ?></h3>

	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>

	<div class="tabbable">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#main-settings" data-toggle="tab">Main Settings</a>
			</li>
		</ul>

		<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">

		<!-- Start of Main Settings Tab Pane -->
		<div class="tab-pane active" id="main-settings">

			<fieldset>
				<legend>Dashboard Messages</legend>

				<div class="control-group">
					<label class="control-label" for="dashboard_message_status">Message Status</label>
					<div class="controls">
						<select name="dashboard_message_status" id="dashboard_message_status">
							<option value="1" <?php echo isset($settings['site.dashboard.message_status']) && $settings['site.dashboard.message_status'] == 1 ? 'selected="selected"' : set_select('site.dashboard.message_status', '1') ?>>Enabled</option>
							<option value="0" <?php echo isset($settings['site.dashboard.message_status']) && $settings['site.dashboard.message_status'] == 0 ? 'selected="selected"' : set_select('site.dashboard.message_status', '1') ?>>Disabled</option>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="dashboard_message">Message</label>
					<div class="controls">
						<textarea rows="10" id="dashboard_message" name="dashboard_message"><?php echo set_value('site.dashboard.message', isset($settings['site.dashboard.message']) ? $settings['site.dashboard.message'] : '') ?></textarea>
						<p class="help-inline">Allows HTML. Displayed to all users.</p>
					</div>
				</div>
			</fieldset>

		</div>

	</div>
</div>

	<div class="form-actions">
		<input type="submit" name="submit" class="btn btn-primary" value="<?php echo lang('bf_action_save') .' '. lang('bf_context_settings') ?>" />
	</div>

	<?php echo form_close(); ?>
</div> <!-- /admin-box -->