<?php if ( $this->settings_lib->item('site.dashboard.message_status') == 1 ) : ?>
<div class="alert alert-dismissable alert-msg alert-dashboard">
    <a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>
    <h4><?php echo $this->settings_lib->item('site.dashboard.message'); ?></h4>
</div>
<?php endif; ?>
<?php print Template::message(); ?>
<div class="col-xs-12 col-sm-8">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-7 greeting">
            <h4 class="flush-top">Hello, <?php echo $this->auth->profile()->first_name; ?></h4>
            <p>Nice to see you. While you're here why don't you earn some experience points by answering
            a couple questions in our <a href="/dashboard/subjects">Subjects area</a>.<!--Better yet, there are questions in the Question Queue waiting for you to vote on!--></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-5 callout">
<?php if($this->auth->role_id() == ADMIN_ROLE || $this->config->item('student_report.toggle') === TRUE): ?>
            <a id="report" href="<?php print site_url('dashboard/reports')?>" title="My Report Card">My Report Card</a>
<?php else: ?>
            <a id="question_queue" href="<?php print site_url('dashboard/question_queue')?>" title="My Question Queue">Question Queue</a>
<?php endif; ?>
            <a id="browse" href="<?php print site_url('dashboard/subjects'); ?>" title="Browse Subjects">Browse Subjects</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 experience-points-block">
            <h4>My Experience Points</h4>
            <table class="table table-bordered table-bordered-only">
                <thead>
                    <tr><th>Subject</th><th>Exp. Points</th><th align="right">Level</th></tr>
                </thead>
                <tbody>
                    <?php foreach($subjects as $subject) : ?>
                    <tr>
                        <td><?php echo $subject->name; ?></td>
                        <td align="left"><?php echo $subject->exp ?></td>
                        <td align="right"><?php echo $subject->level ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <p align="right"><a href="<?php echo site_url('dashboard/profile'); ?>" class="btn btn-default">View Profile</a></p>
        </div>
        <div class="tabs col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <ul class="tab nav nav-tabs" id="top_tab">
                <li class="active"><a href="#top-cxc-students" class="left" data-toggle="tab">Top Students</a></li>
                <!--<li><a href="#top-gsat-students" class="right" data-toggle="tab">Top Teachers</a></li>-->
            </ul>
            <div class="tab-content">
            <div id="top-cxc-students" class="tab-pane active">
            <?php if(isset($live_rank) && !empty($live_rank)) : ?>
                <p class="rank">You are currently ranked #<?php print $live_rank ?></p>
            <?php else : ?>
                <p>You are currently unranked</p>
            <?php endif; ?>
                <ol class="standings list-unstyled">
                <?php $i = 0;
                foreach($top_students as $student) :
                    $i++; ?>
                    <li class="row">
                        <div class="standing col-xs-1 col-sm-2 col-md-1 col-lg-1 "><?php echo $i; ?></div>
                        <div class="user col-xs-11 col-sm-10 col-md-11 col-lg-11 row">
                            <?php $avatar = $student->user->avatar;
                            if(empty($avatar)) : ?>
                                <div class="img col-xs-3 col-sm-4 col-md-3 col-lg-3">
                                <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $student->user->first_name.' '.$student->user->last_name; ?>" width="40" />
                                </div>
                            <?php else : ?>
                                <div class="img col-xs-3 col-sm-4 col-md-3 col-lg-3">
                                <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $student->user->first_name.' '.$student->user->last_name; ?>" width="40" />
                                </div>
                            <?php endif; ?>
                                <p class="col-xs-9 col-sm-8 col-md-9 col-lg-9"><strong><a href="<?php echo site_url("dashboard/profile/view/".$student->user->id); ?>"><?php echo $student->user->first_name.' '.$student->user->last_name; ?></a></strong><br /><?php echo floor($student->score); ?> Points</p>
                        </div>
                        </li>
                        <?php endforeach; ?>
                    </ol>
                        <p align="right"><a href="<?php echo site_url("rankings"); ?>" class="btn btn-default">View Complete Leaderboard</a></p>
                    </div>
                    <!--
                    <div id="top-gsat-students" class="tab-pane">
                    	<p class="rank">You are not qualified</p>
                        <ol class="standings">
                        	<li><p align="center">Coming Soon</p></li>
                        </ol>
                        <p align="right"><a href="#">View Complete Leaderboard</a></p>
                    </div>
                    -->
                </div>
            </div>
    </div>
</div>
<div class="col-xs-12 col-sm-4">
    <h4 class="flush-top"><?php echo $unread; ?> New Message<?php if($unread != 1) echo 's'; ?></h4>
        <ul class="list-unstyled messages">
            <?php foreach($messages as $message) :
                $lastReply = $message->replies[0]; ?>
            <li<?php /* class="starred" */ ?><?php if ( $status[$message->id] == 'no' ) echo ' class="unread"'; ?>><a href="<?php echo $message->permalink(); ?>"><?php echo $lastReply->user->name(); ?>: <?php echo word_limiter($lastReply->body, 5); ?></a></li>
            <?php endforeach; ?>
        </ul>
        <p align="right"><a href="<?php echo site_url('messages'); ?>" class="btn btn-default">View all messages</a></p>
        <?php if(isset($payout) && $payout > 100) : ?>
        <div class="payout">
            <div class="amount">
                <h1>$125<sup>00</sup></h1>
                <small>Cash Earnings</small>
            </div>
            <div class="msg">
                <p>Congratulations! You have earned enough points for a cash payment</p>
                <a href="#">View your points status &raquo;</a>
            </div>
        </div>
        <?php endif; ?>
        <?php if ( $contest ) : ?>
            <h3>Current Referral Contest</h3>
            <div class="referrals">
                <img src="<?php echo $contest->image(); ?>" width="100" alt="<?php echo $contest->name; ?>" />
                <h3><?php echo $contest->name; ?></h3>
                <?php echo auto_typography($contest->description); ?>
                <p><a href="<?php echo $contest->permalink(); ?>">View contest details &raquo;</a></p>
                <p align="center">You have <strong><?php echo $referrals; ?></strong> referrals for this contest.<br />
                <?php if($rank == 0) echo 'You are currently not ranked in the leaderboards</p>';
                else echo 'You are ranked <strong>'.$rank.'</strong> in the leaderboards</p>'; ?>
            </div>
        <?php endif; ?>
		<?php if($this->config->item('videos.toggle') && $this->auth->profile()->testing_level() == 'GSAT'): ?>
		<a href="<?php print site_url(['dashboard', 'subjects', 'by_subject', 'mathematics']) ?>">
			<img src="<?php print site_url(['assets', 'dashboard','img', 'live_videos.jpg']) ?>" alt="Videos are live!" />
		</a>
		<?php endif ?>
</div>
