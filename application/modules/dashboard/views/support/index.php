<?php
if(!function_exists('dtfmt'))
{
    function dtfmt($value)
    {
        return $value == 0 ? 'Not set' : date('l jS \of F Y h:i:s A', $value);
    }
}
?>
<?php print Template::message(); ?>
<?php if ( validation_errors() ) : ?>
    <div class="alert alert-warning">
        <?php echo validation_errors(); ?>
    </div>
<?php endif; ?>
<div class="row">
<div class="col-xs-12 col-sm-6">
    <h2>Check EduFocal Code</h2>
    <?php echo form_open(); ?>
    <?php echo form_input('code', isset($code) ? $code : '', '', 'class="form-control" placeholder="Enter code"'); ?>
    <div class="form-group">
        <button type="submit" name="checkcode" class="btn btn-primary">Check Code</button>
    </div>
    <?php if(isset($info) && isset($info['code'])) : ?>
    <table class="table table-bordered">
        <tr><th>Code</th><td><?php print $info['code']->code; ?></td></tr>
        <tr><th>Type</th><td><?php print $info['code']->type; ?></td></tr>
        <tr><th>Assigned to</th><td><?php print isset($info['user']) ? $info['user']->name() . ' (' .$info['user']->email.')': 'Not assigned'; ?></td></tr>
        <tr><th>Can be activated?</th><td><?php print $info['code']->active == 1? 'Yes' : 'No'; ?></td></tr>
    <?php if($info['code']->active == 1) : ?>
        <tr><th>Activated on</th><td><?php print dtfmt($info['code']->accepted_on); ?></td></tr>
        <tr><th>Expires</th><td><?php print dtfmt($info['code']->expiry_date); ?></td></tr>
    <?php endif; ?>
    </table>
    <?php elseif(isset($code)): ?>
        <h4>Code not found.</h4>
    <?php endif; ?>
    <?php print form_close(); ?>
</div>
<div class="col-xs-12 col-sm-6">
    <h2>Change Student School</h2>
<?php print form_open(); ?>
<?php print form_input('email', $this->input->post('email'), '', array('class' => 'form-control', 'placeholder' => 'Enter email address')); ?>
    <div class="form-group">
        <button type="submit" name="get_user" class="btn btn-primary btn-lg">Find Student</button>
    </div>
<?php if(isset($user)) : ?>
<h3><?php print $user->name() ?></h3>
<?php echo form_dropdown('level', $levels, $level, 'Pick new level', array('class' => 'form-control')); ?>
<button type="submit" name="change_level" class="btn btn-primary btn-lg">Change level</button>
<?php endif; ?>
<?php print form_close(); ?>
</div>
