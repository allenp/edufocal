<div class="chooser">
    <div class="heading hidden-xs">
        <div class="back_button"><a href="<?php echo site_url('dashboard/subjects/by_teacher'); ?>"><img src="<?php echo site_url(); ?>assets/dbrd/img/back_btn.png" alt="Back" width="53" height="24" /></a></div>
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('dashboard/subjects/by_teacher'); ?>">Teachers and Tutors</a></li>
            <li class="last"><?php echo $teacher->first_name.' '.$teacher->last_name; ?></li>
        </ul>
    </div>

    <div class="col-xs-12 col-sm-4">        
    <div class="row test-start-teacher-info">
        <div class="col-xs-12 col-sm-6">
        <?php if(!empty($teacher->user_picture)) : ?>
            <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php echo $teacher->first_name.' '.$teacher->last_name; ?>" class="img" />
        <?php endif; ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4><?php echo $teacher->first_name.' '.$teacher->last_name; ?></h4>
            <p><em><?php echo $teacher->title; ?></em></p>
        <div class="row">
            <div class="col-xs-12">
            <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
            <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
            <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
            <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
            <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
            </div>
        </div>
        </div>
        
    </div>
        <div class="row">
            <div class="bio container col-xs-12">
            <?php echo $teacher->biography; ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-8">
        <div class="subject_select">
            <h2>Content by <?php echo $teacher->first_name.' '.$teacher->last_name; ?></h2>
        </div>
        <ul class="list-unstyled listing subjects">
            <?php foreach($listings as $listing) : ?>
            <li>
                <a href="<?php echo site_url('dashboard/subjects/teacher/'.trim($teacher->permalink).'/'.trim($listing->subject->permalink).'/'.trim($listing->topic->permalink)); ?>"><?php echo $listing->subject->name; ?>: <?php echo $listing->topic->name; ?></a>
                <?php if(strlen($listing->topic->description > 0)) : ?>
                <p><?php echo word_limiter(strip_tags($listing->topic->description), 20); ?></p>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
        </div>
    </div>
</div>
