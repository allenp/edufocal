<?php Assets::add_js($this->load->view('subjects/by_subject-script',array(), TRUE), 'inline'); ?>
<div class="chooser row">
<?php $sublist = array('' => 'Please choose your subject');
      $toplist = array('' => 'Please choose your topic'); ?>

<?php foreach($subjects as $sub)
        $sublist[trim($sub->permalink)] = $sub->name; ?>

<?php foreach($topics as $topic)
         $toplist[trim($topic->permalink)] = $topic->name; ?>

<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
<?php print form_dropdown('subject', $sublist, trim($active_subject), '', array('class' => 'form-control')); ?>
</div>
<?php if(strlen($active_subject) > 0): ?>
<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
<?php print form_dropdown('topic', $toplist, trim($active_topic), '', array('class' => 'form-control')); ?>
</div>
<?php endif; ?>

    <div class="subject col-sm-3 col-md-3 hidden-xs">
        <h3 class="heading">Select a Subject</h3>
        <ul class="list-unstyled">
            <?php foreach($subjects as $key => $subject) : ?>
            <li class="<?php print $key == $active_subject ? 'active' : '' ?>">
                <a href="<?php echo by_subject_url($subject) ?>"><?php echo $subject->name; ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="topic col-sm-3 col-md-3 hidden-xs">
        <h3 class="heading">Select a Topic</h3>
        <ul class="list-unstyled">
            <?php foreach($topics as $topic) : ?>
<?php if($active_topic == trim($topic->permalink)) $active_topic_id = $topic->id; ?>

            <li<?php if($active_topic == trim($topic->permalink)) echo ' class="active"'; ?>>
                <a href="<?php echo by_subject_url($subjects[$active_subject], $topic) ?>"><?php echo ($active_topic == trim($topic->permalink) && strlen($topic->name) > 22) ? substr($topic->name, 0, 21).'...' : $topic->name; ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="teacher col-xs-12 col-sm-6 col-md-6">
        <div class="heading">
            <?php print form_open(site_url('dashboard/subjects/by_teacher/search')); ?>
                <button type="submit"></button><input type="text" name="SearchTerm" placeholder="Search" />
            <?php print form_close(); ?>
            <h3 class="heading hidden-xs">Learn and take tests</h3>
        </div>

		<ul class="list-unstyled listing clearfix">
		<?php if ($video_enabled && isset($videos) && count($videos) > 0) : ?>
			<li id="launch" class="clearfix">
				<strong>We've launched videos!</strong>
			</li>
			<?php foreach($videos as $video): ?>
			<li><a class="" href="<?php print video_url($video, $subjects[$active_subject], $topics[$active_topic])?>">
					<span class="glyphicon glyphicon-film"></span>
				<?php print $video->title ?>
					<?php if($video->free) : ?>
						<span class="label label-success pull-right">free!</span>
					<?php endif ?>
				</a>
			</li>
			<?php endforeach ?>
		<?php endif; ?>
		<?php if (isset($active_topic_id)) : ?>
		<li class="clearfix">
			<?php print form_open(site_url('testing/start'), array('id' => 'start')); ?>
				<?php print form_hidden('teacher', 'all'); ?>
				<?php print form_hidden('topic', $active_topic_id); ?>
				<?php print form_hidden('format', 'multiple'); ?>
				<div class="form-group text-center">
					<?php print form_submit('timed', 'Start Test Now', array('class' => 'btn btn-lg btn-danger')); ?>
						<p class="text-center" style="font-style: italic; font-size: 12px; margin-top: 15px">You will have twenty minutes to complete this test.</p>
				</div>
			<?php print form_close(); ?>
		</li>
		<?php endif ?>
		</ul>
    </div>
</div>
