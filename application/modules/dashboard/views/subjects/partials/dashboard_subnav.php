<nav class="category">
    <ul class="nav navbar-nav">
        <li<?php if(($this->uri->segment(2) == 'subjects' && !$this->uri->segment(3)) || $this->uri->segment(3) == 'by_subject') echo ' class="active"'; ?>><a href="<?php echo site_url('dashboard/subjects'); ?>">By Subject</a></li>
        <li<?php if($this->uri->segment(3) == 'by_teacher') echo ' class="active"'; ?>><a href="<?php echo site_url('dashboard/subjects/by_teacher'); ?>">By Teacher or Tutor</a></li>
    </ul>
</nav>
