<div class="chooser by_teacher row">
    <div class="name col-xs-12 col-sm-4 col-md-4">
        <h3 class="heading">Find a Teacher or Tutor</h3>
        <div class="section">
            <?php echo form_open('dashboard/subjects/by_teacher/search', 'class="form-inline"'); ?>
                <h4>Search by name</h4>
                <div class="form-group">
                    <input class="search form-control" name="SearchTerm" value="<?php set_value('SearchTerm'); ?>" />
                </div>
                <?php echo form_button(array('type' => 'submit', 'content' => 'Go')); ?>
                <div class="clearfix"></div>
            <?php echo form_close(); ?>
            <div class="divider"></div>
            <h4>View by Last Name</h4>
            <ul class="list-unstyled">
                <?php foreach(range('A', 'Z') as $alpha) : ?>
                <li><a href="<?php echo site_url('dashboard/subjects/by_teacher/begins_with/'.$alpha); ?>"><?php echo $alpha; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="teacher col-xs-12 col-sm-8 col-md-8">
        <div class="heading">
            <h3>Tutors and Teachers</h3>
            <small><?php echo $total_teachers; ?> Result<?php echo ($total_teachers != 1) ? 's' : ''; ?></small>
        </div>
        <ul class="list-unstyled listing">
            <?php foreach($teachers as $teacher) : ?>
            <li>
                <a href="<?php echo site_url('dashboard/subjects/teacher/'.$teacher->permalink); ?>">
                    <div class="img">
                        <?php if(!empty($teacher->user_picture)) : ?>
                            <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php echo $teacher->first_name.' '.$teacher->last_name; ?>" width="60"/>
                        <?php endif; ?>
                    </div>
                    <div class="rating">
                        <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                        <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                        <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                        <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                        <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                    </div>
                    <h2><?php echo $teacher->first_name.' '.$teacher->last_name; ?> <span class="title"><?php echo $teacher->title; ?></span></h2>
                    <p><?php echo $teacher->short_bio; ?></p>
                    <div class="clearfix"></div>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php if($total_teachers > 10) : ?>
        <div class="heading">
            <?php echo $this->pagination->create_links(); ?>
            <small><strong>Showing <?php echo $offset + 1; ?> - <?php echo $offset + 10; ?> of <?php echo $total_teachers; ?> matches</strong></small>
        </div>
        <?php endif; ?>
    </div>
</div>
