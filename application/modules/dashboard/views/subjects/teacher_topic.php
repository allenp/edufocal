<div class="chooser">
    <div class="heading hidden-xs">
        <div class="back_button hidden-xs"><a href="<?php print site_url('dashboard/subjects/by_subject/'.trim($subject->permalink).'/'.trim($topic->permalink)) ?>"><img src="<?php echo site_url(); ?>assets/dbrd/img/back_btn.png" alt="Back" width="53" height="24" /></a></div>
        <ul class="breadcrumb list-unstyled hidden-xs">
            <li><a href="<?php echo site_url('dashboard/subjects'); ?>">Subjects</a></li>
            <li><a href="<?php echo site_url('dashboard/subjects/by_subject/'.trim($subject->permalink)); ?>"><?php echo trim($subject->name); ?></a></li>
            <li><a href="<?php echo site_url('dashboard/subjects/by_subject/'.trim($subject->permalink).'/'.trim($topic->permalink)); ?>"><?php echo $topic->name; ?></a></li>
            <li class="last"><?php echo $teacher->first_name.' '.$teacher->last_name; ?></li>
        </ul>
    </div>
    
    <div class="container">
    <?php print Template::message(); ?>
        <?php if ( validation_errors()) : ?>
        <div>
            <div class="alert alert-warning">
                <h4>Please check the following before continuing:</h4>
                <?php echo validation_errors(); ?>
            </div>
        </div>
        <?php endif; ?>


        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
            <div class="row test-start-teacher-info">
                <div class="col-xs-3 col-sm-2 col-lg-1">
                <?php if(!empty($teacher->user_picture)) : ?>
                    <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php echo $teacher->first_name.' '.$teacher->last_name; ?>" width="60"  class="img" />
                <?php endif; ?>
                </div>
                <div class="col-xs-9 col-sm-7">
                    <h2><?php echo $teacher->first_name.' '.$teacher->last_name; ?></h2>
                    <p><em><?php echo $teacher->title; ?></em></p>
                </div>
                <div class="col-xs-12 col-xs-offset-4 col-sm-3">
                    <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                    <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                    <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                    <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                    <img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="" width="16" height="16" />
                </div>
            </div>
            <div class="row">
                <div class="bio container col-xs-12">
                <?php echo $teacher->biography; ?>
                </div>
            </div>

            <div class="row">
            <div class="opaque container bordered col-xs-12">
                <h2><?php echo $subject->name; ?>: <?php echo $topic->name; ?></h2>
                <div class="divider"></div>
                <?php echo $topic->description; ?>
           
                <?php print form_open(site_url('testing/start'), array('id' => 'start')); ?>
                    <?php print form_hidden('teacher', trim($teacher->permalink)); ?>
                    <?php print form_hidden('topic', trim($topic->id)); ?>
                    <?php if($multi >= 10) : ?>
                    <?php print form_hidden('format', 'multiple'); ?>
                    <?php endif; ?>
                    <?php if($short >= 10) : ?>
                    <?php print form_hidden('format', 'short'); ?>
                    <?php endif; ?>
                    <div class="form-group col-sm-offset-8">
                    <?php print form_submit('timed', 'Start Test Now', array('class' => 'btn btn-lg btn-danger')); ?>
                    <div class="advice">
                    <p>You will have 30 minutes to complete the test.</p>
                    </div>
                    </div>
                    
                <?php print form_close(); ?>
            </div>
            </div>
    </div>
</div>
