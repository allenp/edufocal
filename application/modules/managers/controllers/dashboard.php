<?php

class Dashboard extends EF_Manager_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $query = "SELECT users.id, users.first_name, users.last_name, users.avatar
            FROM users, classroom_students
            WHERE users.id = classroom_students.user_id
            AND classroom_students.active =1
			AND classroom_students.manager_id = ?
			ORDER by last_name ASC, first_name ASC";
        $result = $this->db->query($query, array($this->auth->user_id()));

        Template::set('users', $result->result());
        Template::set('title', 'Dashboard - EduFocal');
        Template::render();
    }
}
