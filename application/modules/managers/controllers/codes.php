<?php

class Codes extends EF_Manager_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	/*
	 * Get codes for a classroom
	 */
	public function for_class($id)
	{
		$codes = $this->db->from('codes')
			->join('classroom_codes', 'codes.id = classroom_codes.code_id and classroom_codes.classroom_id = '.$id)
			->select('codes.id, codes.code, classroom_codes.tagged tagged, classroom_codes.tagged_for')
			->where('codes.user_id = 0')
			->get()
			->result();
		$this->load->view('codes/index', array('codes' => $codes));
	}

	/*
	 * Tag codes as being issued to someone
	 * tags are markers to help track codes
	 * so managers don't issue same code twice.
	 */
	public function tag()
	{
		$id = $this->input->post('id');
		$tag_for = $this->input->post('name');
		$tag = $this->input->post('tag');

		$cs = ClassroomCode::find_by_code_id($id);

		if (null != $cs) {
			$cs->tagged = $tag;
			$cs->tagged_for = $tag_for;
			$cs->save();
			echo 1;
		} else {
			echo 0;
		}
	}
}
