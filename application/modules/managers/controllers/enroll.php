<?php

class Enroll extends EF_Manager_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        Template::set('classrooms', $this->get_classrooms());
        Template::set('title', 'Manage Enrollment - EduFocal');
        Template::render();
    }

    public function create_classroom()
    {
        if ($this->input->post('name') != '') {
            $classroom = Classroom::create(
                array(
                    'name' => $this->input->post('name', true),
                    'manager_id' => $this->auth->user_id(),
                ));

            $cview = $this->load->view('enroll/classrooms', array('classrooms' => $this->get_classrooms()), true);

            header("Content-type: application/json");

            echo json_encode(
                array(
                    'success' => true,
                    'broadcast' => 'classrooms.create',
                    'html' => $cview,
                ));

            exit;

        } else {
            $this->load->view('enroll/create_classroom');
        }
    }

    public function get_classrooms()
    {
        $classrooms = $this->db->from('classrooms')
            ->join('classroom_codes',
                'classrooms.id = classroom_codes.classroom_id', 'left outer')
                 ->join('codes', 'classroom_codes.code_id = codes.id', 'left outer')
                 ->group_by(array('classrooms.id', 'classrooms.name', 'classrooms.manager_id'))
                 ->select('classrooms.id, classrooms.name, classrooms.manager_id, IFNULL(SUM(`codes`.`user_id` > 0),0) used, COUNT(codes.id) total, COUNT(0) registered, COUNT(0) pending', false)
				 ->where('classrooms.manager_id ='.$this->auth->user_id())
                 ->get()->result();

        $students = $this->db->from('classrooms')
            ->join(
                'classroom_students',
                'classroom_students.classroom_id = classrooms.id and classrooms.manager_id ='.$this->auth->user_id(), 'left')
                 ->group_by(array('classrooms.id', 'classrooms.name', 'classrooms.manager_id'))
                 ->select(
                     'classrooms.id, classrooms.name,
                     classrooms.manager_id,
                     IFNULL(SUM(classroom_students.active = 1),0) registered,
                     IFNULL(SUM(classroom_students.active = 0),0) pending', false)
                 ->get()->result();

        if (count($students) > 0) {
            foreach ($students as $student) {
                foreach ($classrooms as $classroom) {
                    if ($classroom->id == $student->id) {
                        $classroom->registered = $student->registered;
                        $classroom->pending = $student->pending;
                    }
                }
            }
        }

        return $classrooms;
    }

    public function invite($id)
    {
        $classroom = Classroom::find_by_id_and_manager_id($id, $this->auth->user_id());
        if (null !== $classroom) {
            $this->load->view('enroll/invite', array('classroom_id' => $classroom->id));
        } else {
            echo 0;
        }
    }

    public function invite_check_emails()
    {
        $emails = $this->input->post('emails');
        $classroom_id = $this->input->post('classroom_id');

        //TODO: verify that the classroom belongs to the user.

        $emails = preg_split('#\s+#', $emails, null, PREG_SPLIT_NO_EMPTY);

        if (count($emails) == 0) {
            return $this->load->view(
                'enroll/error',
                array('messages' => array('No emails entered. Please try again.'))
            );
        }

        $users = User::find('all', array('conditions' => array('email in (?)', $emails)));

        //create keys as email address
        $emails = array_combine($emails, $emails);

        //replace the ones we found
        foreach ($users as $user) {
            $emails[$user->email] = $user;
        }

        $this->load->view(
            'enroll/review_invites',
            array('classroom_id' => $classroom_id, 'users' => $emails)
        );
    }

    public function invite_confirm()
    {
        $user_ids = $this->input->post('user_ids');
        $classroom_id = $this->input->post('classroom_id');
        $manager_id = $this->auth->user_id();

        $successes = array();
        $failures = array();
        foreach ($user_ids as $user_id) {
            try {
                if (is_numeric($user_id)) {
                    $this->add_to_classroom($user_id, $classroom_id, $manager_id);
                    $successes[] = $user_id;
                } else {
                    //send an email
                    //TODO: Send an email to non-registered user.
                    //What code should be sent? How is that determined.
                }
            } catch(\Exception $ex) {
                //TODO: check if the user simply existed already and set it to success
                $results[$user_id] = array('success' => false, 'message' => $ex->__toString());
                continue;
            }
        }

        $users = User::find('all', array('conditions' => array('id in (?)', $successes)));
        $this->load->view(
            'enroll/confirmed',
            array('classroom_id' => $classroom_id, 'users' => $users)
        );
    }

    private function add_to_classroom($user_id, $classroom_id, $manager_id)
    {
        ClassroomStudent::transaction(function()
            use($user_id, $classroom_id, $manager_id) {

                ClassroomStudent::create(
                    array(
                        'manager_id' => $manager_id,
                        'user_id' => $user_id,
                        'classroom_id' => $classroom_id,
                        'active' => 0,
                    ));

                ClassroomEvent::create(
                    array(
                        'user_id' => $user_id,
                        'classroom_id' => $classroom_id,
                        'event' => 'INVITED',
                    ));
            });
    }
}
