<?php

class Account extends EF_Authenticated_Controller
{
	public function __construct ()
	{
		parent::__construct();
		
		if (!class_exists('User_model')) {
			$this->load->model('users/User_model', 'user_model');
        }
	}
	
	public function index ()
	{
		if(isset($_POST['submit']) || isset($_POST['exit']))
			$this->save();

		Template::set('title', 'Account Settings - EduFocal');
        Template::set('user', $this->auth->profile());
		Template::render();
	}
	
    public function save ()
	{
		foreach ( $this->auth->rules as $info )
			$this->form_validation->set_rules($info['field'], $info['label'], $info['rules']);
		
		if ( $this->form_validation->run() === TRUE )
		{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			
			$this->load->library('upload', $config);
			if( $this->upload->do_upload('avatar'))
			{
				$upload = $this->upload->data();
				$resize['source_image']	= $upload['full_path'];
				$resize['maintain_ratio'] = TRUE;
				$resize['width']	 = 128;
				$resize['height']	= 128;
				
				$this->load->library('image_lib', $resize); 
				$this->image_lib->resize();
				$avatar = $upload['file_name'];
			}
			else
			{
				$avatar = $this->auth->profile()->avatar;
			}
			
			$user = User::find($this->auth->user_id());
			
            if($user->notification != $this->input->post('notification'))
            {
				$notification = $this->input->post('notification');
				
				if($notification == 'yes')
					$this->add_notification($user);
				else
					$this->remove_notification($user);
				
				$user->notification = $notification;
			}
			
			$user->first_name = $this->input->post('first_name', TRUE);
			$user->last_name = $this->input->post('last_name', TRUE);
			$user->sex = $this->input->post('sex', TRUE);
			$user->phone = $this->input->post('phone', TRUE);

            if($this->input->post('email') != $user->email)
            {
                $exists = $this->db->where('email =', $this->input->post('email'))
                    ->where('id !=', $user->id)
                    ->from('users')
                    ->count_all_results();

                if($exists == 0)
                {
                    $user->email = $this->input->post('email');
                }
            }

            //$user->email = $this->input->post('email', TRUE);
            $user->birthday = new DateTime();
            $user->birthday->setDate(
                $this->input->post('birth_year', TRUE),
                $this->input->post('birth_month', TRUE),
                $this->input->post('birth_day', TRUE)
            );
				
			$user->avatar = $avatar;
			$user->save();
			$teacher = Teacher::find_by_user_id($this->auth->user_id());
			
			if(count($teacher) > 0) 
			{
				$teacher->user_picture = $avatar;
				$teacher->save();
			}

            Template::set_message('Changes saved successfully', 'success');
            redirect('account');
		}
	}

    public function password()
    {
		$this->load->helper('security');

        $rules = array(
            array(
               'field' => 'password',
               'label' => 'Password',
               'rules' => 'required'
            ),
            array(
               'field' => 'new_password',
               'label' => 'New Password',
               'rules' => 'required'
            ),
            array(
               'field' => 'confirm_password',
               'label' => 'Confirm Password',
               'rules' => 'required|matches[new_password]'
            ),
        );

		foreach ( $rules as $info )
            $this->form_validation->set_rules($info['field'], $info['label'], $info['rules']);

        if ( $this->form_validation->run() === TRUE )
		{
            if(!empty($_POST['password']) && $this->auth->profile()->password_hash == do_hash($this->auth->profile()->salt . $this->input->post('password')))
            {
				$update['password'] = $this->input->post('new_password');
				$update['pass_confirm'] = $this->input->post('confirm_password');
				$this->user_model->update($this->auth->user_id(), $update);
                Template::set_message('Password changed successfully', 'success');
            }
            else
            {
                Template::set_message('Invalid current password', 'error');
            }
            
        }
	
		Template::set('title', 'Account Settings - EduFocal');
		Template::render();

    }

    public function receipts($id = '')
    {
        if($id != '')
        {
            //display one receipts
            $receipt = Receipt::find_by_id_and_user_id($id,$this->auth->user_id());
            Template::set('receipt', $receipt);
            Template::set('title', 'Payment Receipt - EduFocal');
            Template::set_view('account/receipt');
            Template::render();
            return;
        }
        else
        {
            //display all receipts
            $codes = Code::find('all', array('conditions' => array('user_id' => $this->auth->user_id())));
            Template::set('codes', $codes);
            Template::set('title', 'Receipts - EduFocal');
            Template::render();
        }
    }

    public function receipt_pdf($id = '')
    {
        if($id != '')
        {
            //download as pdf.
            $receipt = Receipt::find_by_id_and_user_id($id, $this->auth->user_id());
            if($receipt != null)
                $html = $this->load->view('receipt_pdf', array('receipt' => $receipt), TRUE);
            if(isset($html) && strlen($html) > 0)
            {
                $this->load->helper('dompdf');
                pdf_create($html, $this->clean_filename($receipt->card_holder .'_EduFocal_Receipt'. $receipt->id), TRUE);
            }
            return;
        }
    }

    private function clean_filename($filename)
    {
        $filename = str_replace(' ', '_', $filename);
        $filename = preg_replace('![^0-9A-Za-z_.-]!', '', $filename);
        $filename = strtolower($filename);
        return $filename;
    }
	
	private function add_notification($user_data) {
		$config = array(
			'apikey' => '6cb450b898a52643f1604b9cf8fea02d-us2',      // Insert your api key
			'secure' => FALSE   // Optional (defaults to FALSE)
		);
		$this->load->library('MCAPI', $config, 'mail_chimp');
		
		$merge_vars = array(
			'FNAME' => ucwords($user_data->first_name), 
			'LNAME' => ucwords($user_data->last_name),
			'PHONE' => $user_data->profile('phone'),
			'SCHOOL' => $user_data->school->name,
			'GENDER' => $user_data->profile('sex')
		);
		
		$this->mail_chimp->listSubscribe('698fab3a3d', $this->auth->email(), $merge_vars);
	}
	
    private function remove_notification($user_data)
    {
		$config = array(
			'apikey' => '6cb450b898a52643f1604b9cf8fea02d-us2',      // Insert your api key
			'secure' => FALSE   // Optional (defaults to FALSE)
		);
		$this->load->library('MCAPI', $config, 'mail_chimp');
		
		$this->mail_chimp->listUnsubscribe('698fab3a3d', $this->auth->email(), true, false, false);
	}
	
	public function payment ()
	{
        $rule_group = '';

        if(count($_POST) > 0)
        {
            $option = isset($_POST['code_option']) ? 'code' : 'card';
            $rule_group = $option == 'code' ? 'edufocal_code' : 'card_payment';
        }

        $configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

		if($this->form_validation->run($rule_group) === TRUE)
		{
            switch($option)
            {
                case 'code':
                    $code = Code::find_by_code($this->input->post('educode'));
                break;

                case 'card':

                //did we just confirm payment?
                //if not send confirm payment screen.

                //get_card_post_data
                $this->load->library('PnP');
                $plan = $this->input->post('plan');
                $card_number = $this->input->post('card_number');
                $card_name = $this->input->post('card_name');
                $card_cvc = $this->input->post('card_cvc');
                $card_expiry_month = $this->input->post('card_expiry_month');
                $card_expiry_year = $this->input->post('card_expiry_year');
                $currency = $this->input->post('currency');
                $address1 = $this->input->post('address1');
                $address2 = $this->input->post('address2');
                $city = $this->input->post('city');
                $state = $this->input->post('state');
                $country = $this->input->post('country');

                $code_charges = $this->config->item($configs[$currency]);
                $code_charge = $code_charges[strtolower($plan)];

                if(!$this->input->post('confirm'))
                {
                    $hidden = array(
                                'plan' => $plan,
                                'card_number' => $card_number,
                                'card_name' => $card_name,
                                'card_cvc' => $card_cvc,
                                'card_expiry_month' => $card_expiry_month,
                                'card_expiry_year' => $card_expiry_year,
                                'option' => $option,
                                'currency' => $currency,
                                'charge' => $code_charge['charge'],
                                'confirm' => 'true',
                                'address1' => $address1,
                                'address2' => $address2,
                                'city' => $city,
                                'state' => $state,
                                'country' => $country,
                              );

                    Template::set_view('confirm');
                    Assets::add_module_js('account', 'confirm');
                    Template::set('title', 'Confirm your purchase - EduFocal');
                    Template::set('hidden', $hidden);
                    Template::render();
                    return;
                }

                if($code_charge)
                {
                    $payment = array(
                                    'card-number' => $card_number,
                                    'card-name' => $card_name,
                                    'card-amount' => $code_charge['charge'],
                                    'card-cvv' => $card_cvc,
                                    'currency' => $currency,
                                    'ship-name' => $this->auth->name(),
                                    'card-exp' => $card_expiry_month . '/' . $card_expiry_year,
                                    'email' => $this->auth->email(),
                                );

                    $pnp = new PnP();
                    $response = $pnp->auth($payment);

                    switch(strtolower($response->FinalStatus))
                    {
                        case 'success':
                            $receipt = Receipt::create( array(
                                                        'payment_ref' => $response->orderID,
                                                        'user_id' => $this->auth->user_id(),
                                                        'amount' => $code_charge['charge'],
                                                        'currency' => $currency,
                                                        'card_holder' => $card_name,
                                                        'email' => $this->auth->email(),
                                                        'paymethod' => $response->paymethod,
                                                        'auth_code' => $response->auth_code,
                                                        'card_type' => $response->card_type,
                                                        'card' => str_repeat('X', strlen($card_number) - 5) . substr($card_number, strlen($card_number) - 5),
                                                        'address1' => $address1,
                                                        'address2' => $address2,
                                                        'city' => $city,
                                                        'state' => $state,
                                                        'country' => $country,
                                                       ));                        

                            $code = Code::Generate( array(
                                                    'type' => $plan,
                                                    'user_id' => $this->auth->user_id(),
                                                    'source_ref' => $receipt->id,
                                                    'value' => $code_charge['charge'],
                                                    'currency' => $currency,
                                                ));

                            //if($this->config->item('instant_receipt')) { 

                            $this->load->library('emailer/emailer');
                            $info = array(
                                            'to' => $this->auth->email(),
                                            'subject' => 'EduFocal.com: Payment Receipt #' . $receipt->id,
                                            'message' => $this->load->view('email', array('name' => $this->auth->name(), 'receipt' => $receipt), TRUE)
                                         );
                            
                            $result = $this->emailer->send($info);
                            if($result['success'])
                                $receipt->email_sent = 1;
                            $receipt->save();

                            //}

                        break; //case 'success'

                        case 'badcard':
                        case 'fraud':
                        case 'problem':
                            $this->form_validation->set_errors(array('card_number' => 'Sorry! Your payment did not go through. Please contact your financial institution or email us at help@edufocal.com.'));
                            log_message('error', $response->FinalStatus . ' ' . $response->MErrMsg);
                        break;

                        default:
                            log_message('error', $response->FinalStatus . ' ' . $response->MErrMsg);
                        break;
                    }
                }

                break;
            }

            if(isset($code))
            {
                $this->session->set_userdata('paid.member', FALSE); //invalidate cached permission

                $code->user_id = $this->auth->user_id();

                //if the user already has a valid code don't just overwrite it. just save this code under their name
                if($this->auth->is_expired())
                {
                    $code_charges = $this->config->item('educodes');
                    $code->accept($code_charges);
                    $user_data = User::find($this->auth->user_id());
                    $user_data->code_id = $code->id;
                    $user_data->save();
                }
                else
                {
                    $code->save();
                }
                redirect('account/payment');
            }
		}

        $this->load->helper('country_dropdown');

        $current_code = $this->auth->profile()->code;

        if($this->auth->is_expired())
        {
            if($current_code != null)
                Template::set_message('Your subscription expired on ' . date('F j, Y', $current_code->expiry_date) , 'error');
            else
                Template::set_message('You have to subscribe to access EduFocal. Use an option below.', 'error');
        }
        else
        {
            $unused = Code::all(array('conditions' => array('user_id' => $this->auth->user_id(), 'accepted_on' => 0)));
            $expiry_date = $current_code->expiry_date;
            if(count($unused) > 0)
            {
                $code_charges = $this->config->item('educodes');
                foreach($unused as $c)
                {
                    $code_charge = isset($code_charges[$c->type]) ? $code_charges[$c->type] : null;
                    if($code_charge != null)
                    {
                        if($c->expiry_date <= 0) {
                            $valid_for = $code_charge['valid_for'];
                            $expiry_date = strtotime('+ ' . $valid_for, $expiry_date);
                        }
                        else if($c->expiry_date > $expiry_date)
                            $expiry_date = $c->expiry_date;

                    }
                }
            }
            
            if(isset($code)) //a new code was just added, show message about it.
            {
                $msg = 'Thank you! You can view your current and past receipts on the <a href="' . site_url('account/receipts') . '">payments history page</a>';
                $status_type = 'success';
            }
            else
            {
                $msg = 'Your current subscription will expire on ' . date('F j, Y', $expiry_date);
                $status_type = 'info';
            }
            Template::set_message($msg, $status_type);
        }

        $currencies = array();
        foreach($configs as $curr => $key)
            $currencies[$curr] = $this->config->item($key);

        Assets::add_js($this->load->view('account/payment_script', array('currency' => $currencies),  true), 'inline');

        if(isset($option))
            Template::set('option', $option); 
        Template::set('currencies', $currencies);
		Template::set('title', 'Payment Options - EduFocal');
		Template::render();
	}

    private function setup_payment_view()
    {
        $this->load->helper('country_dropdown');

        $configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );

        $currencies = array();
        foreach($configs as $curr => $key)
            $currencies[$curr] = $this->config->item($key);

        Assets::add_js($this->load->view('account/payment_script', array('currency' => $currencies),  true), 'inline');
        Template::set('currencies', $currencies);
    }

    public function _valid_card($number)
    {
        return valid_card($number, $this);
    }

    public function _valid_efcode(&$input)
    {
       return valid_efcode($input, $this);
    }
} 
