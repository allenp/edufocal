<div class="codes clearfix">
<input type="hidden" id="hash" value="<?php print $this->security->get_csrf_hash() ?>" />
<input type="hidden" id="token_name" value="<?php print $this->security->get_csrf_token_name() ?>" />
<?php if (isset($codes) && count($codes) > 0) : ?>
<?php foreach ($codes as $code) : ?>
    <label class="code col-sm-2 <?php print $code->tagged == 1? 'tagged' : 'new'  ?>">
    <input type="checkbox" data-tag-id="<?php print $code->id ?>" value="<?php print $code->id ?>" <?php if($code->tagged == 1) { print 'checked=\"checked\"'; } ?>/>
        <?php print $code->code ?>
    </label>
<?php endforeach ?>
<?php else : ?>
    <p>No codes left :-(. Please contact EduFocal to request more.</p>
<?php endif ?>
</div>
<hr />
<em class="col-xs-12">Codes that are <span class="tagged">crossed-out</span> are those that you have marked as being issued to a student before (but have not yet been used by them). The codes that have been used will not show up in this list.<em>

