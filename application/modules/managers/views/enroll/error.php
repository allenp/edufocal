<hr />
<div id="classroom_panel_error" class="panel panel-danger">
    <div class="panel-heading">
   Errors 
    </div>
    <div class="panel-body">
<?php if(isset($messages) && count($messages) > 0) : ?>
<ul class="messages">
<?php foreach ($messages as $message) : ?>
<li><?php print $message ?></li>
<?php endforeach ?>
</ul>
<?php endif ?>
<hr />
<button class="btn btn-default" type="button" data-dismiss="panel" data-target="#classroom_panel_error">Close</button>
</div>
</div>
