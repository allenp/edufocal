<ul class="list-group fade in classrooms">
<?php foreach($classrooms as $classroom) : ?>
    <li class="list-group-item">
        <h3 class="list-group-item-heading"><?php print $classroom->name ?></h3> 
        <hr />
        <p class="list-group-item-text data-classroom-id-<?php print $classroom->id ?>">
            <div class="form-group clearfix">
            <?php if ($classroom->used < $classroom->total) :?>
            <a href="<?php print site_url(array('managers', 'codes', 'for_class', $classroom->id)) ?>" data-async=true data-target="#classroom_<?php print $classroom->id ?>" class="btn btn-primary btn-sm">Issue codes to students</a>
            <?php endif ?>
            <a href="<?php print site_url('managers/enroll/invite/'.$classroom->id) ?>" data-async=true data-target="#classroom_<?php print $classroom->id ?>" class="btn btn-default btn-sm">Invite students via email</a>
            </div>
        <table class="table table-bordered">
            <tr>
                <td>
                    <span class=""><?php print $classroom->registered ?> registered students</span>
                </td>
                <td>
                    <span class=""><?php print $classroom->pending ?> pending invitations</span>
                </td>
                <td>
                    <span class=""><?php print $classroom->used ?> of <?php print $classroom->total ?> codes used</span>
                </td>
            </tr>
        </table>
        <div class="clearfix" id="classroom_<?php print $classroom->id ?>">
        </div>
        </p>
    </li>
<?php endforeach; ?>
</ul>
