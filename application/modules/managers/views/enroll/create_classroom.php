<?php if(isset($classroom) && $classroom->id > 0) : ?>
    <?php echo form_open(current_url(), array('class' => 'form-vertical', 'data-async' => 'true', 'data-target' => '#dialog', 'role' => 'form')); ?>
<?php else: ?>
    <?php echo form_open(current_url(), array('class' => 'form-vertical', 'data-async' => 'true', 'data-target' => '#dialog', 'role' => 'form')); ?>
<?php endif; ?>
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title">Create Classroom</h4>
     </div>  
     <div class="modal-body clearfix">
    <?php if(validation_errors()) : ?>
        <?php print validation_errors() ?>
    <?php endif; ?>
         <div class="form-group">
            <input id="name" name="name" class="form-control" autocomplete="off" value="" placeholder="Enter classroom name here"/>
         </div>      
        <span class="inline-help"><em>Use something like: 2014 GSAT Students or Study Group.</em></span>
     </div>      
     <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php print form_submit('create', 'Create', array('class' => 'btn btn-primary btn-lg')); ?>
     </div>  
<?php print form_close() ?>
