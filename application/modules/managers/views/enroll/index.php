<?php
Assets::add_js(array(
    site_url('assets/js/jquery.form.js'),
    site_url('assets/js/editor.js'),
    site_url('assets/js/render.js'),
));
?>
<div class="panel panel-info">
    <div class="panel-heading">
        <h3>Your classrooms</h3>
    </div>
    <div class="panel-body">
        <?php if( !isset($classrooms) && count($classrooms) == 0) : ?>
            <p>Create your first classroom and start inviting your students to join!</p>
        <?php endif ?>
            <a href="<?php print site_url('managers/enroll/create_classroom') ?>" id="create_class" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#dialog">Create Classroom</a>
    </div>
</div>
<div id="classrooms">
<?php if(isset($classrooms) && count($classrooms) > 0) : ?>
    <?php $this->load->view('enroll/classrooms', array('classrooms' => $classrooms)) ?>
<?php endif; ?>
</div>
