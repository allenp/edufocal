<hr />
<div id="classroom_panel<?php print $classroom_id ?>" class="panel panel-default">
    <div class="panel-heading">
    Invitations sent
    </div>
    <div class="panel-body">
<?php if(isset($users) && count($users) > 0) : ?>
<?php foreach ($users as $user) : ?>
    <div class="col-sm-3">
<?php if (is_object($user) && !empty($user->avatar)) : ?>
<img src="<?php echo site_url("uploads/".$user->avatar); ?>" alt="<?php echo $user->first_name.' '.$user->last_name; ?>" width="40"              class="img" />
<?php else: ?>
<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" width="40" class="img" />
<?php endif ?>
<?php if (is_object($user)) : ?>
<h5><?php print $user->first_name . ' ' . $user->last_name ?></h5>
<?php else: ?>
<h5><?php print $user ?>*</h5>
<?php endif ?>
    </div>
<?php endforeach ?>
<?php endif ?>
<hr />
<button class="btn btn-success" type="button" data-dismiss="panel" data-target="#classroom_panel<?php print $classroom_id ?>">Close</button>
</div>
</div>
