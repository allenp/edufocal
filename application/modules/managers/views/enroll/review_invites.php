<hr />
<div id="classroom_panel<?php print $classroom_id ?>" class="panel panel-default">
    <div class="panel-heading">
    Confirm classroom invitations
    </div>
    <div class="panel-body">
<?php print form_open(site_url('managers/enroll/invite_confirm'), array('class' => 'form-vertical', 'data-async' => 'true', 'data-target' => '#classroom_'.$classroom_id)) ?>
<?php print form_hidden('classroom_id', $classroom_id) ?>
<?php if (isset($users) && count($users) > 0) : ?>
<?php foreach ($users as $user) : ?>
    <div class="col-sm-3">
    <input type="checkbox" name="user_ids[]" value="<?php print is_object($user) ? $user->id : $user ?>" checked="checked" />
<?php if (is_object($user) && !empty($user->avatar)) : ?>
<img src="<?php echo site_url("uploads/".$user->avatar); ?>" alt="<?php echo $user->first_name.' '.$user->last_name; ?>" width="40"              class="img" />
<?php else: ?>
<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" width="40" class="img" />
<?php endif ?>
<?php if (is_object($user)) : ?>
<h5><?php print $user->first_name . ' ' . $user->last_name ?></h5>
<?php else: ?>
<h5><?php print $user ?>*</h5>
<?php endif ?>
    </div>
<?php endforeach ?>
<?php endif ?>
        <div class="form-group col-xs-12">
        <hr />
            <button class="btn btn-default" type="button" data-dismiss="panel" data-target="#classroom_panel<?php print $classroom_id ?>">Cancel</button>
            <input type="submit" class="btn btn-primary btn-md" value="Confirm Invitation" />
        </div>
    </div>

<?php print form_close() ?>
    <div class="panel-footer">
    <p>
    <small>
    Invitations showing email address only will send out an email invitation for the user to register. They must register with the code provided in the email to be admitted to this classroom.*
    </small>
    </p>
    <p>
    <small>
    All other users will be prompted accept their invitations to join your classroom.
    </small>
    </p>
    </div>
</div>
