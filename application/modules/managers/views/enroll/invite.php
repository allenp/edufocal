<hr />
<?php print form_open(
    site_url('managers/enroll/invite_check_emails'),
    array(
        'id' => 'form_classroom'.$classroom_id,
        'class' => 'form-vertical fade in',
        'data-async' => 'true',
        'data-target' => '#classroom_'.$classroom_id
    ))
?>
<?php print form_hidden('classroom_id', $classroom_id) ?>
    <div class="form-group">
        <textarea class="form-control input-lg" name="emails" id="emails"  placeholder="Enter email addresses here separated by a space or new line." rows="8"></textarea> 
    </div>
    <div class="form-group">
        <button class="btn btn-default" type="button" data-dismiss="panel" data-target="#form_classroom<?php print $classroom_id ?>">Close</button>
        <input type="submit" class="btn btn-primary btn-md" value="Invite Students" />
    </div>
<?php print form_close() ?>
