<ul class="nav navbar-nav">
    <li<?php if($this->uri->segment(1) == 'managers' && $this->uri->segment(2) === false) print ' class="active"'; ?>><a href="<?php print site_url('managers'); ?>">Dashboard</a></li>
    <li<?php if($this->uri->segment(1) == 'managers' && $this->uri->segment(2) === 'enroll') print ' class="active"'; ?>><a href="<?php print site_url('managers/enroll'); ?>">Enrollment</a></li>
</ul>
