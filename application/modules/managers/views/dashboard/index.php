<div class="col-xs-12">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for student" />
        <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Choose class <span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
<div class="col-xs-12">
<?php if (isset($users) && count($users) > 0) : ?>
<?php foreach ($users as $user) : ?>
    <div class="col-xs-3 profile-card">
		<div class="section clearfix">
		<?php if ( !empty($user->avatar)) : ?>
		<img src="<?php echo site_url("uploads/".$user->avatar); ?>" alt="<?php echo $user->first_name.' '.$user->last_name; ?>" width="40"              class="img" />
		<?php else: ?>
		<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" width="40" class="img" />
		<?php endif ?>
		<h5><?php print $user->first_name . ' ' . $user->last_name ?></h5>
		<a href="<?php print site_url(array('student', 'reports', $user->id)) ?>" class="">View report</a>
		</div>
    </div>
<?php endforeach ?>
<?php else : ?>
<p>No students yet. Go to enrollment to start.</p>
<?php endif ?>
</div>
</div>
