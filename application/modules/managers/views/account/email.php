<p>Hello <?php print $name ?>,</p>
<p>This confirms that your payment was successful.</p>
<p>Thank you for your support. Here's the receipt for your order:</p>
<p>Order #: <?php print $receipt->payment_ref ?><br />
Receipt #: <?php print $receipt->id ?> <br />
Date: <?php print date('F j, Y',  $receipt->created_on); ?><br />
Payed by: <?php print $receipt->card_holder; ?><br />
Payment Method:<?php print $receipt->card_type . ' | ' .$receipt->card ?><br />
Authorization Code: <?php print $receipt->auth_code ?><br />
Transaction Type: Purchase</p>
<p>Billing Address: <br />
<?php print $receipt->address1 ?><br />
<?php print $receipt->address2 ?><br />
<?php print $receipt->city ?><br />
<?php print $receipt->state ?><br />
<?php print $receipt->country ?>
</p>

<?php foreach($receipt->codes as $code): ?>
<p> 1 <?php print $code->type ?> EduFocal subscription - <?php print strtoupper($code->currency) . ' $' . money_format('%(#6n', $code->value) ?></p>
<?php endforeach; ?>
<p>Total: <?php print strtoupper($receipt->currency) . ' $' . money_format('%(#6n', $receipt->amount) ?></p> 

<p>You can view your order here: <a href="<?php print site_url(array('account', 'receipts', $receipt->id)) ?>">{unwrap}<?php print site_url(array('account', 'receipts', $receipt->id)) ?>{/unwrap}</a>. We strongly recommend you keep a copy of your transaction record.</p>
<p>If you have any questions or concerns, please respond to this email.</p>
<p>Thanks! <br />
The EduFocal Team</p>
<p>EduFocal Limited<br />
Suite #8<br />
Technology Innovation Centre<br />
University of Technology<br />
237 Old Hope Road<br />
Kingston 6<br />
Jamaica W.I.<br />
<a href="<?php print site_url() ?>"><?php print site_url() ?></a></p>
