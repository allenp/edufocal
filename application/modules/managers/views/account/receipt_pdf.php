<!doctype html>
<head>
    <meta charset="utf-8" />
    <title>Receipt # <?php print $receipt != null ? $receipt->id : '' ?></title>
    <style type="text/css">
body, html {
    margin: 0;
    padding: 0;
    color: #333;
}

body {
    font-size: 100%;
    line-height: 1.4em;
}

body {
    font-family: Arial, Helvetica, sans-serif;
}

#logo
{
    background: transparent url(/assets/img/logo.png) no-repeat scroll 0 0;
    width: 122px;
    height: 50px;
    display: block;
    text-indent: -9999px;
}

.wrapper
{
    padding: 2em;
}

table#items
{
    border-top: solid 1px #666;
}

table#items td
{
    border-top: solid 1px #666;
}

table#items th
{
    text-align: left;
    background-color: #ccc;
}

#footer
{
    position: absolute;
    bottom: 0;
    text-align: center;
}
    </style>
</head>
<body>
<div id="main">
    <div class="wrapper">
        <div id="receipt">
        <table border="0" width="100%" cellpadding="8">
        <tr><td><h1 id="logo">&nbsp;</h1>
            <p>EduFocal Limited<br />
               Suite #8,<br />
               Technology Innovation Centre,<br />
               University of Technology,<br />
               237 Old Hope Road,<br />
               Kingston 6,<br />
               Jamaica W.I.<br />
               <a href="<?php print site_url() ?>" title="EduFocal"><?php print site_url() ?></a>
            </p>
            </td>
            <td><h1>RECEIPT #: <?php print $receipt->id ?></h1>
                <p>Order Date: <?php print date('F j, Y', $receipt->created_on) ?><br />
                Order #: <?php print $receipt->payment_ref ?><br />
                Payed by: <?php print $receipt->card_holder ?><br />
                Payment Method: <?print $receipt->card_type . ' | ' . $receipt->card ?><br />
                Authorization Code: <?print $receipt->auth_code ?><br />
                Transaction Type: Purchase</p>
                <p>Billing Address: <br />
                <?php print $receipt->address1 ?><br />
                <?php print $receipt->address2 ?><br />
                <?php print $receipt->city ?><br />
                <?php print $receipt->state ?><br />
                <?php print $receipt->country ?>
                </p>

            </td>
            </tr>
            <tr>
            <td colspan="2">
          <?php if(count($receipt->codes)) : ?>
          <table id="items" border="0" width="100%" cellpadding="8">
          <thead>
          <tr><th>Item</th><th>Cost (<?php print strtoupper($receipt->currency) ?>)</th></tr>
          </thead>
          <tbody>
          <?php foreach($receipt->codes as $code) : ?>
          <tr><td>1 <?php print $code->type ?> subscription</td><td><?php print money_format('%(#6n',$code->value) ?></td>
          <?php endforeach; ?>
          <tr><td>Total:</td><td><?php print money_format('%(#6n', $receipt->amount) ?></td></tr>
          </tbody>
          </table>
          <?php endif; ?>
          </td>
          </tr>
          </tbody>
          </table>
          <div id="footer"><p>Thank you for your purchase!</p></div>
        </div>
    </div>
</div>
</body>
</html>
