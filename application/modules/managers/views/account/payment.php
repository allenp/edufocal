<div class="settings">
<!--<div class="heading">
    <p>Use one of the options below to renew your subscription or add extra codes to your account.</p>
</div>
-->
    <?php echo Template::message(); ?>
    <?php if ( validation_errors()) : ?>
        <div class="alert alert-warning">
            <h4>Please check the following before continuing:</h4>
        <?php echo validation_errors(); ?>
        </div>
    <?php endif; ?>
    <?php $this->load->view('account/pay', array('currencies', $currencies)); ?>	
</div>
