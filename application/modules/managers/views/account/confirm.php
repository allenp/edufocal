<div class="container">
    <div class="settings">
    <?php echo Template::message(); ?>
    <?php if ( validation_errors()) : ?>
        <div class="alert alert-warning">
            <h4>Please check the following before continuing:</h4>
            <br clear="all" />
            <?php echo validation_errors(); ?>
        </div>
    <?php endif; ?>
    <?php echo form_open(current_url(), '', $hidden); ?>	
        <h3>Review and confirm your purchase</h3>
        <p>1 <?php print ucfirst($hidden['plan']) ?> EduFocal Subscription for <?php print $hidden['currency'] . ' $' . money_format('%(#6n', $hidden['charge']); ?></p>
        <p>Total: <?php print $hidden['currency'] . ' $' . money_format('%(#6n', $hidden['charge']); ?></p> 
        <p>To be payed by: <?php print $hidden['card_name'] ?></p>
        <p>Using card: <?php print $hidden['card_number'] ?></p>
        <p>Address:<br />
        <?php print $hidden['address1'] ?><br /><?php print $hidden['address2'] ?><br /><?php if(strlen($hidden['city']) > 0) print $hidden['city'] ?></br>
        <?php if(strlen($hidden['state']) > 0) print $hidden['state'] . '<br />' ?>
        <?php print $hidden['country'] ?>
        <p>&nbsp;</p>
        <?php print form_submit('submit', 'Process Payment', array('class' => 'btn btn-success')); ?> &nbsp;<a href="<?php print site_url('payment'); ?>" alt="Payment">Or cancel</a>
        <?php print form_close(); ?>
    </div>
</div>
