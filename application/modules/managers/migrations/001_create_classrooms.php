<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_create_classrooms extends Migration
{

    public function up() 
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'integer', 'unsigned' => true, 'auto_increment' => true),
            'name' => array('type' => 'varchar(255)', 'null' => false),
            'manager_id' => array('type' => 'integer', 'unsigned' => true),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('classrooms');

        $fields = array(
            'id' => array('type' => 'integer', 'unsigned' => true, 'auto_increment' => true),
            'classroom_id' => array('type' => 'integer', 'unsigned' => true),
            'user_id' => array('type' => 'integer', 'unsigned' => true),
            'event' => array('type' => 'varchar(60)'),
            'misc' => array('type' => 'varchar(255)', 'null' => true),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('classroom_event_logs');

        $fields = array(
            'id' => array('type' => 'integer', 'unsigned' => true, 'auto_increment' => true),
            'classroom_id' => array('type' => 'integer', 'unsigned' => true),
            'manager_id' => array('type' => 'integer', 'unsigned' => true),
            'user_id' => array('type' => 'integer', 'unsigned' => true),
            'manager_confirmed_dt' => array('type' => 'datetime', 'null' => true),
            'user_confirmed_dt' => array('type' => 'datetime', 'null' => true),
            'active' => array('type' =>  'tinyint', 'default' => 0),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('classroom_students');

        $this->db->query("ALTER TABLE  `classroom_students` ADD UNIQUE (
            `classroom_id` ,
            `manager_id` ,
            `user_id`
        );");

        $fields = array(
            'id' => array('type' => 'integer', 'unsigned' => true, 'auto_increment' => true),
            'classroom_id' => array('type' => 'integer', 'unsigned' => true),
            'code_id' => array('type' => 'integer', 'unsigned' => true),
            'tagged' => array('type' => 'tinyint', 'default' => 0),
            'tagged_for' => array('type' => 'varchar(255)', 'null' => true),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('classroom_codes');
    }

    //--------------------------------------------------------------------

    public function down() 
    {
        $prefix = $this->db->dbprefix;

        $this->dbforge->drop_table('classrooms');
        $this->dbforge->drop_table('classroom_students');
        $this->dbforge->drop_table('classroom_codes');
        $this->dbforge->drop_table('classroom_event_logs');
    }

    //--------------------------------------------------------------------

}
