<?php

/**
 * Author: Paul Allen
 * Description: Orchestrates tests with a magic wand.
 *
 **/

class TestMaster
{
    var $test;
    var $page_size;
    var $max;
    var $test_type = 'timed';
    var $format = 1; //1 - multiple; 2 - short answer

    public function __construct()
    {
    }

    public function init($test, $page_size, $max)
    {
        $this->test = $test;
        $this->page_size = $page_size;
        $this->max = $max;
    }

    /**
     * Returns blank answer objects or
     * filled out answers.
     **/
    public function get_answers($page=1)
    {
        if ($page < 0) {
            throw new Exception('Invalid page specified');
        }

        $weights = $this->test->get_weights();

        if ($page >= count($weights) && array_sum($weights) < $this->max) {
            $this->_get_new_questions();
        } elseif ($page >= $this->max) {
            return false;
        }

        return $this->get_page($page);
    }

    /**
     * Gets a page of answers
     * $page is 1 based. Page 1 has the first set of questions.
     **/
    private function get_page($page)
    {
        $weights = $this->test->get_weights();

        if ($page > count($weights)) {
            return false;
        }

        $offset = 0;
        for ($i = 0; $i < $page - 1; $i++) {
            $offset += $weights[$i];
        }

        if (count($this->test->answers) >= $offset + $weights[$i]) {
            return array_slice(
                $this->test->answers, $offset, $weights[$i]
            );
        } else {
            return false;
        }
    }

    /**
     * Finds new questions to pose on the
     * Test.
     **/
    private function _get_new_questions()
    {

        if ($this->test->finished > 0) {
            return false;
        }

        $result = array();
        $answers = $this->test->answers;

        $used = array();
        foreach ($answers as $answer) {
                $used[] = $answer->question_id;
        }

        $params = array();

        $query = "SELECT * FROM(SELECT *
            FROM `questions` WHERE topic_id = ?
            AND type = ? AND status = ? AND question_id = 0 ";

        $params[] = $this->test->topic_id;
        $params[] = $this->format;
        $params[] = 'Accepted';

        if ($this->test->teacher_id > 0) {
            $query .= " AND user_id = ?";
            $params[] = $this->test->teacher_id;
        }

        if (count($used) > 0) {
            $query .= " AND id NOT IN (".implode(',', $used).")";
        }

        $query .= " ORDER BY RAND() LIMIT 5) as q ORDER BY WEIGHT DESC";

        $questions = Question::find_by_sql($query, $params);

        $weight = 0;

        foreach ($questions as $question) {
            $result[] = $question;
            $weight += $question->weight;
            if ($question->weight > 1) {
                $linked = Question::all(
                    array(
                        'conditions' => array(
                            'question_id = ?', $question->id
                        ),
                        'order' => 'id'
                    )
                );

                $result = array_merge($result, $linked);
            }

            if ($weight >= $this->page_size) {
                break;
            }
        }

        if ($weight > 0) {
            $this->test->add_weight($weight);
            $this->test->save();
        }

        $this->test = Test::find_by_id($this->test->id);

        //theoretically this is to cache the questions chosen
        //and the answers not yet given.
        //This could possibly be stored outside the database
        //Until the answers are provided.
        $this->create_answers($result);
        return $result;
    }

    public function create_answers($questions)
    {
        foreach ($questions as $question) {
            $this->create_answer($question);
        }
    }

    private function create_answer($question)
    {
        return Answer::create(
            array(
                'test_id' => $this->test->id,
                'question_id' => $question->id,
                'subject_id' => $question->topic->subject_id,
                'topic_id' => $question->topic_id,
                'test_type' => $this->test_type,
                'question_type' => $this->format,
                'completed' => 'no',
                'resolved' => 'no',
                'user_id' => $this->test->user_id,
                'teacher_id' => $question->user_id
            )
        );
    }

    public function accept_answers($answers)
    {
        //don't accept any submissions after test
        //marked as finished
        if (!empty($this->test->finished) && $this->test->finished > 0) {
            return;
        }

        foreach ($answers as $question_id => $ans) {
            //skip unanswered questions
            if (empty($ans)) {
                continue;
            }

            $a = Answer::first(
                array('conditions' => array(
                    'test_id = ? and question_id = ?',
                    $this->test->id, $question_id
                ))
            );

            $a->answer = is_array($ans) ? json_encode($ans) : $ans;
            $a->save();
        }
    }
}
