<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class StudentRankCommand
{
    public $ci = null;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    function execute()
    {
        $prefix = $CI->db->dbprefix;

        try
        {
            $last_rank = CronRank::find(
                array('order' => 'timestamp desc')
            );
        }
        catch(Exception $ex)
        {
            $last_rank = null;
        }

        $new_rank = CronRank::create(array());
        $time = is_null($last_rank) ?
            mktime(0, 0, 1, 1, 1, 1970) : $last_rank->timestamp;

        //subject score archiving
        $query = "update {$prefix}cron_rank_subjects c,
                  student_subject_scores s,
                  users u
                  set c.score = s.score,
                  c.level = s.level,
                  c.cron_rank_id = {$new_rank->id}
                  where 
                  s.track_id = c.track_id
                  and s.timestamp > {$time}
                  and c.user_id = s.user_id
                  and u.id = s.user_id
                  and u.ranked = 1
                  and c.subject_id = s.subject_id";

        $CI->db->query($query);

        $query = "INSERT into {$prefix}cron_rank_subjects
                  (cron_rank_id, user_id, subject_id, level,score,curr_rank, prev_rank, track_id)
                  SELECT {$new_rank->id}, s.user_id, s.subject_id, s.level, s.score, 0,0, s.track_id
                  from {$prefix}student_subject_scores s
                  inner join {$prefix}student_tracks t on
                  t.user_id = s.user_id and t.track_id = s.track_id
                  inner join users u on s.user_id = u.id
                  left outer join {$prefix}cron_rank_subjects c
                  on s.user_id = c.user_id and s.subject_id = c.subject_id
                  and s.track_id = c.track_id
                  where c.id is null and u.ranked = 1";

        $CI->db->query($query);

        $query = "update {$prefix}cron_rank_subjects c
            set prev_rank = ifnull(curr_rank,0)
            where prev_rank <> curr_rank";

        $CI->db->query($query);

        $query = "select track_id from
                  {$prefix}cron_rank_subjects
                  group by track_id";
        $cursor = $CI->db->query($query);

        foreach($cursor->result() as $value)
        {
            $query = "update cron_rank_subjects c,
                      (select x.id,  x.position
                       from(
                               select sub.id, sub.score,
                               @rownum := @rownum + 1 as position
                               from cron_rank_subjects sub
                               join (select @rownum := 0) r
                               where track_id = {$value->track_id}
                               order by sub.score desc
                           ) x
                      ) y

                      set c.curr_rank = y.position,
                      cron_rank_id = {$new_rank->id}
                      where c.id = y.id
                      and c.curr_rank <> y.position";
            $CI->db->query($query);
        }
 
        $query = "insert into {$prefix}cron_rank_subject_logs
            (cron_rank_id, user_id, subject_id, level, score, curr_rank,
            prev_rank, track_id)
            select cron_rank_id, user_id, subject_id, level, score,
            curr_rank, prev_rank, track_id
            from {$prefix}cron_rank_subjects
            where cron_rank_id = {$new_rank->id}";

        $CI->db->query($query);

        //student score archive
        $query = "update " . $prefix ."cron_rank_overalls o,
                  student_scores s,
                  users u
                  set o.score = s.score,
                  o.cron_rank_id = {$new_rank->id}
                  where 
                  s.timestamp > {$time} 
                  and o.user_id = s.user_id
                  and o.track_id = s.track_id
                  and u.id = s.user_id
                  and u.ranked = 1";

        $CI->db->query($query);

        $query = "INSERT into {$prefix}cron_rank_overalls
            (cron_rank_id, user_id, score,curr_rank, prev_rank, track_id)
            SELECT {$new_rank->id}, s.user_id, s.score, 0,0,
            s.track_id
                  from {$prefix}student_scores s
                  inner join users u on s.user_id = u.id and u.ranked = 1
                  left outer join {$prefix}cron_rank_overalls o
                  on s.user_id = o.user_id and o.track_id = s.track_id
                  where o.id is null";

        $CI->db->query($query);

        $query = "update {$prefix}cron_rank_overalls
                  set prev_rank = ifnull(curr_rank,0)";

        $CI->db->query($query);


        $query = "select track_id from
                  {$prefix}cron_rank_overalls
                  group by track_id";
        $cursor = $CI->db->query($query);

        foreach($cursor->result() as $value)
        {
            $query = "update cron_rank_overalls o,
                   (select x.id,  x.position
                      from(
                           select sub.id, sub.score,
                           @rownum := @rownum + 1 as position
                           from cron_rank_overalls sub
                           join (select @rownum := 0) r
                           where track_id = {$value->track_id}
                           order by sub.score desc
                       ) x
                   ) y

                  set o.curr_rank = y.position,
                  cron_rank_id = {$new_rank->id}
                  where o.id = y.id
                  and o.curr_rank <> y.position";

            $CI->db->query($query);
        }

        $query = "insert into {$prefix}cron_rank_overall_logs(
            cron_rank_id, user_id, score, curr_rank, prev_rank, track_id)
            select cron_rank_id, user_id, score, curr_rank, prev_rank,
            track_id
            from {$prefix}cron_rank_overalls
            where cron_rank_id = {$new_rank->id}";

        $CI->db->query($query);
    }
}
