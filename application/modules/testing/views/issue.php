<?php print form_open(site_url(['testing', 'question_issue', $question->id]), array(
			'id' => 'form_report_issue'.$question->id,
			'class' => 'form-vertical fade in',
			'data-async' => 'true',
			'data-target' => '#dialog'
			)) ?>
<div class="panel-danger">
	<div class="panel-heading">
		<h4>Report an issue with question #<?php print $question->id ?></h4>
	</div>
	<div class="modal-body clearfix">
		<p>Please choose the option that best describes the problem you are reporting:</p>
		<div class="form-group">
			<label style="font-weight: normal; padding-left: 10px">
				<input type="radio" value="Broken image in question" name="problem"/> The image is not showing for this question
			</label><br />
			<label style="font-weight: normal; padding-left: 10px">
				<input type="radio" value="My answer is right but it says I'm wrong" name="problem"/> My answer is right but it says I'm wrong
			</label><br />
			<label style="font-weight: normal; padding-left: 10px">
				<input type="radio" value="The correct answer is not one of the choices" name="problem"/> The correct answer is not one of the choices
			</label>
		</div>
			<div class="form-group"><label>
            <textarea class="form-control" rows=3 cols=80 name="other" placeholder="Other reasons? Enter them here..."></textarea>
	</div>
	<div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php print form_submit('submit', 'Report Issue', array('class' => 'btn btn-danger btn-lg')); ?>
     </div>
</div>
<?php print form_close() ?>
