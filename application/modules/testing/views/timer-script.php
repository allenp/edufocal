var austDay = new Date(<?php echo ($started + 1800) * 1000; ?>);

$('#countdown').countdown({
    until: austDay, 
    layout: '{mnn}{sep}{snn}',
    onExpiry: function() {
        // Redirect to Expiry Page
        window.location = "<?php echo site_url('testing/expired/'.$test_id); ?>";
    }
});

$(".hint").click(function() {
    $(this).toggleClass("closed");
});
