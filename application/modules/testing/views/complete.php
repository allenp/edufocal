<?php
Assets::add_js(
    $this->load->view('testing/complete-script', array('test_id' => $test_id), TRUE), 'inline');
?>
<?php
    if(isset($net_points))
    {

        $end = $is_new_topic_level ? round($total_topic_points / $max_level_topic_points * 98) : 98;

        $start = $is_new_topic_level ?
                   round( ($total_topic_points -  floor($net_points) ) / $max_level_topic_points * 98 )
                     : 0;

        if(($start < 0 || $end < 0) && $start < $end)
        {
            $end = $end - $start;
            $start = 0;
        }
        else if(($start < 0 || $end < 0) && $start > $end)
        {
            $start = $start - $end;
            $end = 0;
        }
    }
?>
<div class="row">
<div class="col-xs-12 col-md-3 col-md-offset-8 pull-left">
    <div class="timer">
    <h5>Test Completed!</h5>
    <div class="progress">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
            <span class="sr-only">100% Complete!</span>
        </div>
    </div>
    </div>
</div>

<div class="test container col-xs-12 col-md-8 pull-left">
    <img src="<?php print site_url('assets/testing/img/test_logo.png')?>" class="logo" alt="EduFocal" />
    <h1><?php print $subject->name; ?>: <?php print $topic->name; ?></h1>
    <?php if(isset($teacher)) : ?>
    <p class="teacher"><?php print $teacher->first_name . ' ' . $teacher->last_name; ?></p>
    <?php else: ?>
    <p class="teacher">Content from multiple teachers</p>
    <?php endif; ?>
    <div class="divider"></div>

    <div class="results">
    <div class="grade">
        <div class="row">
            <div class="review col-xs-12 col-sm-5 pull-left">
                <?php if($percentage != false || $percentage == 0) : ?>
                <div class="score">
                    <h6>Your Score</h6>
                    <span><?php echo $percentage; ?><sup>%</sup></span>
                </div>
                <?php else : ?>
                <div class="complete col-xs-12 col-sm-3 pull-left">
                    <img src="<?php echo site_url("assets/dbrd/img/test_complete.png"); ?>" alt="Test Complete!" />
                </div>
                <?php endif; ?>
                <?php if($percentage != false && $percentage < 100) : ?>
                <p><a href="<?php echo site_url('testing/review/'.$test_id); ?>" class="btn btn-info btn-lg btn-block">Review missed questions</a></p>
                <?php endif; ?>
            </div>
            <div class="copy col-xs-12 col-sm-7 pull-right">
                <?php if($percentage != false || $percentage == 0) : ?>
                    <?php if($percentage >= 90) { ?>
                        <h1>Wow! Awesome <?php echo $user->first_name; ?></h1>
                        <p>You’re a genius! Keep this up and you’ll be an expert student in no time! Try doing more timed tests to gain additional experience points.</p>
                    <?php } elseif($percentage >= 80) { ?>
                        <h1>Great job, <?php echo $user->first_name; ?></h1>
                        <p>You’ve worked hard! You seem to have an excellent grasp of this topic. Try challenging yourself by taking another practice test or by taking a timed test to gain experience points.</p>
                    <?php } elseif($percentage >= 70) { ?>
                        <h1>Well done, <?php echo $user->first_name; ?></h1>
                        <p>You’ve scored well on this test, which indicates that you have a good grasp of this topic. This was a great performance, but there’s still room for improvement. Continue working to improve your score.</p>
                    <?php } elseif($percentage >= 60) { ?>
                        <h1>Almost there...</h1>
                        <p>You’re doing well, <?php echo $user->first_name; ?>, but you could use some more work. Review your content and try again. Your score is bound to improve with a bit more practice.</p>
                    <?php } elseif($percentage >= 40) { ?>
                        <h1><?php echo $user->first_name; ?>, You’re not quite there yet...</h1>
                        <p>You have the ability to do much better. Work a bit harder and give it another shot. You’ll definitely do better next time.</p>
                    <?php } elseif($percentage >= 20) { ?>
                        <h1>Don’t give up <?php echo $user->first_name; ?></h1>
                        <p>Great things take time. Don’t let these grades get you down. Practice, Practice, Practice!</p>
                    <?php } else { ?>
                        <h1>Keep at it, <?php echo $user->first_name; ?>. It gets better...</h1>
                        <p>This is pretty bad <?php echo $user->first_name; ?>, but it will get better in time. You definitely need some more practice, but don’t give up. Study harder and try again. With a lot more practice, you’ll be improving in no time!</p>
                    <?php } ?>
                    <?php else : ?>
                    <h1>Test Complete!</h1>
                    <p>Some copy about how the test is in the queue to be graded, due to the fact that there were some answers that need to be evaluated by a human. This section should set a clear expectation on how long it will take to get a final score, and how the student will be notified (email, dashboard, etc.)</p>
                    <?php endif; ?>
                </div>
            </div><!--row-->

            <div class="row">
            <?php if(isset($net_points)): ?>
            <hr />
            <div class="experience col-xs-12 col-sm-5">
            <div id="star_five" class="summary">
                <?php print floor($net_points) >= 0 ? '+' : '-' ?><?php print abs(floor($net_points)) ?>
                <?php if($is_new_topic_level): ?>
                <h3>Level Up!</h3>
                <?php endif; ?>
            </div>
            </div>
            <div class="details col-xs-12 col-sm-7">
            <h6>You <?php print floor($net_points) >= 0 ? 'gained' : 'lost' ?> <?php print abs(floor($net_points)) ?> experience points for <span class="topic-summary"><?php print $topic->name ?></span> in <?php print $subject->name ?><?php if($is_new_topic_level) : ?> and advanced to Level <?php print $new_topic_level; ?><?php endif; ?>.</h6>
            <div class="progress_bar">
                <div id="progress" class="progress" style="width: <?php print !$is_new_topic_level ? round($total_topic_points / $max_level_topic_points * 100) : 100; ?>%">&nbsp;</div>
            </div>
            <?php if(!($is_new_topic_level)): ?>
            <p>You need just <?php print round($max_level_topic_points - $total_topic_points); ?> Experience Points to reach level <?php print $current_topic_level + 1; ?>.
            <?php else : ?>
            <p>You are now advanced to Level <?php print $new_topic_level; ?>!
            <?php endif; ?> Once you get to Level 65 you will unlock your Expert Student status. As an Expert Student, you can create content and <a href="#">earn points for cash!</a></p>
            </div>
            </div>
            <?php endif; ?>
            </div>
        <div class="rating clearfix">
            <?php if(isset($teacher) && !empty($teacher->user_picture)) : ?>
                <div class="thumb pull-left col-xs-3 col-sm-2">
                <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php echo $teacher->first_name.' '.$teacher->last_name; ?>" width="60" />
                </div>
            <?php endif; ?>
            <?php if(isset($teacher)) : ?>
            <div class="input pull-left col-xs-9">
                <h4>How would you rate <?php echo $teacher->first_name.' '.$teacher->last_name; ?>?</h4>
                <?php print form_open('', array('id' => 'rat')); ?>
                    <select name="rate">
                        <?php foreach ($options as $id => $title): ?>
                            <option value="<?php echo $id ?>"><?php echo $title ?></option>
                        <?php endforeach; ?>
                    </select>

                    <input type="submit" value="Rate it!" class="rate-hide"/>
                <?php print form_close(); ?>
                <p id="caption"></p>
            </div>
            <?php endif; ?>
        </div>
        </div>
    </div>
</div>
</div>

<div class="col-xs-12 col-md-8 controls clearfix">
<p align="right"><a href="<?php echo site_url('dashboard'); ?>">
    <img src="<?php echo site_url("assets/dbrd/img/return_dashboard.png"); ?>" alt="Return to Dashboard" />
</a></p>
</div>

