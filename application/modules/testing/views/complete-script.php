$(".rate-hide").hide();
$("#rat").stars({
    inputType: "select",
    cancelShow: false,
    captionEl: $("#caption"),
    callback: function(ui, type, value)
    {
        //ui.disable();

        $("#messages").text("Saving...").stop().css("opacity", 1).fadeIn(30);
        $.post("<?php echo site_url('dashboard/testing/save_rating'); ?>", { "rate": value, "test": "<?php echo $test_id; ?>", "csrf_test_name" : $("input[name='csrf_test_name']").val() }, function(db)
        {
                $("#messages").text("Thanks!").stop().css("opacity", 1).fadeIn(30);
                setTimeout(function(){
                    $("#messages").fadeOut(1000);
                }, 2000);
        });
    }
});

$("#rat").stars("selectID", -1);
$('<div id="messages"/>').appendTo("#rat");
