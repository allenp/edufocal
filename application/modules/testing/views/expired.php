<?php
Assets::add_js(
    $this->load->view('testing/complete-script', array('test_id' => $test_id), TRUE), 'inline');
?>
<div class="test container col-xs-12 col-md-8">
    <img src="<?php print site_url('assets/testing/img/test_logo.png')?>" class="logo" alt="EduFocal" />
    <h1><?php print $subject->name; ?>: <?php print $topic->name; ?></h1>
    <?php if (isset($teacher)) : ?>
    <p class="teacher"><?php print $teacher->first_name . ' ' . $teacher->last_name; ?></p>
    <?php endif; ?>
    <div class="divider"></div>

    <div class="results">
    <div class="grade">
        <div class="row">
            <div class="review col-xs-12 col-sm-5 pull-left">
                <?php if($percentage != false || $percentage == 0) : ?>
                <div class="score">
                    <h6>Your Score</h6>
                    <span><?php echo $percentage; ?><sup>%</sup></span>
                </div>
                <?php else : ?>
                <div class="complete col-xs-12 col-sm-3 pull-left">
                    <img src="<?php echo site_url("assets/dbrd/img/test_complete.png"); ?>" alt="Test Complete!" />
                </div>
                <?php endif; ?>
            </div>
            <div class="copy col-xs-12 col-sm-7 pull-right">
                <h1>Time Expired!</h1>
                <p>Sorry, <?php echo $user->first_name; ?>, your time expired.</p>
            </div>
        </div>
    </div>
    <div class="rating clearfix">
        <?php if(isset($teacher) && !empty($teacher->user_picture)) : ?>
            <div class="thumb pull-left col-xs-3 col-sm-2">
            <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php echo $teacher->first_name.' '.$teacher->last_name; ?>" width="60" />
            </div>
        <?php endif; ?>
        <div class="input pull-left col-xs-9">
            <h4>How would you rate <?php echo $teacher->first_name.' '.$teacher->last_name; ?>?</h4>
            <?php print form_open('', array('id' => 'rat')); ?>
                <select name="rate">
                    <?php foreach ($options as $id => $title): ?>
                        <option value="<?php echo $id ?>"><?php echo $title ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" value="Rate it!" class="rate-hide"/>
            <?php print form_close(); ?>
            <p id="caption"></p>
        </div>
    </div>
    </div>
</div>
<div class="col-xs-12 container col-md-8 controls clearfix">
<p align="right"><a href="<?php echo site_url('dashboard'); ?>">
    <img src="<?php echo site_url("assets/dbrd/img/return_dashboard.png"); ?>" alt="Return to Dashboard" />
</a></p>
</div>
