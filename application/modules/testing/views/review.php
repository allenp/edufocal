<?php Assets::add_js($this->load->view('testing/review-script',array(),  TRUE), 'inline'); ?>
<div class="row">
    <div class="col-xs-12 col-md-3 col-md-offset-8 pull-left">
        <div class="timer">
            <h5>Test Progress: <span>Complete!</span></h5>
            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                    <span class="sr-only">100% Complete!</span>
                </div>
            </div>
        </div>
    </div>

   	<div class="test container col-xs-12 col-md-8 pull-left">

        <img src="<?php print site_url('assets/testing/img/test_logo.png')?>" class="logo" alt="EduFocal" />
        <h1><span>Test Review</span> <br /><?php print $subject->name; ?>: <?php print $topic->name; ?></h1>
        <?php if(isset($teacher)) : ?>
            <p class="teacher"><?php print $teacher->first_name . ' ' . $teacher->last_name; ?></p>
        <?php else: ?>
            <p class="teacher">Content from multiple teachers</p>
        <?php endif; ?>
        <div class="divider"></div>

        <div class="stats">
            <ul class="list-unstyled clearfix">
                <li>
                    <h4>Date Taken</h4>
                    <p><?php echo $date; ?></p>
                </li>
                <li>
                    <h4>Time Taken</h4>
                    <p><?php echo $minutes.' Minute'.(($minutes != 1) ? 's ' : ' ');
                    echo $seconds.' Second'.(($seconds != 1) ? 's' : ''); ?></p>
                </li>
                <?php if($percentage != false) : ?>
                <li>
                    <h4>Final Score</h4>
                    <p><?php echo $percentage; ?>%</p>
                </li>
                <?php endif; ?>
            </ul>
        </div>

        <?php if($percentage != false) : ?>
        <!--<p>Some copy about how this test has been taken and these are the incorrect answers along with correct answers.</p>-->
        <h2>You answered <?php echo ($percentage == 5) ? 'this' : 'these'; ?> question<?php echo ($percentage != 5) ? 's' : ''; ?> incorrectly:</h2>
        <?php else : ?>
        <!--<p>Some copy about how the short answer practice tests are all self evaluated.</p>-->
        <?php endif; ?>
        <?php $counter = 0; ?>
        <?php foreach($questions as $q) : ?>
            <?php $counter++;
        ?>
        <div class="item type-<?php print $q->question->type ."-".$q->question->id; ?>">

        <?php if($q->question->type == 1) : ?>
        <?php if($q->answer != $q->question->accepted_answers) : ?>
        <div class="question <?php print $q->question->id; ?>">
            <span class="number"><?php echo $counter.'.';?></span>
            <?php echo $q->question->question; ?>
        </div>
        <?php if(!empty($q->question->explain)) : ?>
        <div class="explain closed <?php print $q->question->id; ?>">
            <header>
            <div class="img">
                <?php if(isset($teacher) && !empty($teacher->user_picture)) :?>
                <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php print $teacher->first_name. ' '.$teacher->last_name; ?>" width="25" height="25" />
                <?php endif; ?>
            </div>
                <h4>Explain</h4>
                </header>
                <section>
                    <?php echo $q->question->explain; ?>
                </section>
        </div>
        <?php endif;  ?>
        <ul class="question-answers list-unstyled">
            <?php $choices = json_decode($q->question->choices);
            foreach($choices as $key => $choice) : ?>
                <li<?php echo (($q->answer == $key) ? ' class="incorrect"' : '').(($q->question->accepted_answers == $key) ? ' class="correct"' : ''); ?>>
                <span class="number"><strong><?php echo strtoupper(base_convert($key + 9, 10, 36)); ?>.</strong></span>
                <label><div class="opt"><?php echo $choice; ?></div></label>
                <div style="clear:left;"></div>
                </li>
            <?php endforeach; ?>
        </ul>
<p class="report-issue-wrapper" style="text-align:right;"><a class="report-issue" data-toggle="modal" data-target="#dialog" href="/testing/question_issue/<?php print $q->question->id ?>" style="color: #a33;font-size: 12px">Report question</a>
</p>
    <?php endif; ?>
    <?php endif; ?>
    <?php if($q->question->type == 2) : ?>
            <h3><?php echo $counter.'. '.$q->question->question; ?></h3>
            <?php if(empty($q->question->choices)) : ?>
                <ul class="question-answers unstlyed">
                    <li class="correct"><p><strong>Correct Answer: </strong></p>
                        <?php echo $q->question->accepted_answers; ?>
                    </li>
                    <li class="incorrect"><p><strong>Your Answer: </strong></p>
                        <?php echo $q->answer; ?>
                    </li>
                </ul>
                else : ?>
                 <ul class="question-answers unstyled">
                    <?php $choices = (array) json_decode($q->question->choices);
                    $ans = (array) json_decode($q->answer);
                    $correct = (array) json_decode($q->question->accepted_answers);
                    foreach($choices as $value => $option) : ?>
                    <li><span class="number"><strong><?php echo $value; ?>.</strong></span>
                    <?php echo $option; ?></li>
                    <li class="correct"><p><strong>Correct Answer: </strong></p>
                        <?php echo $correct[$value]; ?>
                    </li>
                    <li class="incorrect"><p><strong>Your Answer: </strong></p>
                        <?php echo $ans[$value]; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
    <?php
        endif;
    endif; ?>
    </div>
    <?php endforeach; ?>
        </div>
</div>
<div class="col-xs-12 col-md-8 controls clearfix">
<p align="right"><a href="<?php echo site_url('dashboard'); ?>">
    <img src="<?php echo site_url("assets/dbrd/img/return_dashboard.png"); ?>" alt="Return to Dashboard" />
</a></p>
</div>
