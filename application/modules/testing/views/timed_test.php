<?php Assets::add_js(array(site_url('assets/dbrd/js/jquery.countdown.pack.js'))); ?>
<?php Assets::add_js($this->load->view('testing/timer-script', array('test_id' => $test->id, 'started' => $test->started), TRUE), 'inline'); ?>

<?php print form_open(); ?>
<div class="row">
<div class="col-xs-12 col-md-3 col-md-offset-8 pull-left">
    <div class="timer">
    <?php //print form_submit('quit', 'Quit Test', array('id' => 'quit', 'class' => 'btn btn-danger btn-xs pull-right')); ?>
    <h5>Test Progress <span id="countdown"></span></h5>
    <div class="progress">
        <?php $value_now =  min(100,array_sum(array_slice($weight, 0, $page - 1)) * 100 / $qc); ?>
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php print $value_now; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $value_now; ?>%;">
                <span class="sr-only"><?php print $value_now; ?>% Complete</span>
        </div>
    </div>
    </div>
</div>
<div class="test container col-xs-12 col-md-8 pull-left">
    <img src="<?php echo site_url(); ?>assets/testing/img/test_logo.png" class="logo" alt="EduFocal" />
    <h1><?php echo $subject->name; ?>: <?php echo $topic->name; ?></h1>
    <?php if(isset($teacher)) : ?>
    <p class="teacher"><?php echo $teacher->first_name.' '.$teacher->last_name; ?></p>
    <?php else: ?>
    <p class="teacher">Content from multiple teachers</p>
    <?php endif; ?>
    <div class="divider"></div>
    <?php $counter = $start_count;
    $offset = array_sum(array_slice( $weight, 0, $page));
    foreach($questions as $q) :
        $counter++;
        if(!empty($q->question->hint)) : ?>
        <div class="hint closed clearfix">
            <header class="clearfix">
                <div class="img">
                    <?php if(isset($teacher) && !empty($teacher->user_picture)) : ?>
                        <img src="<?php echo site_url("uploads/".$teacher->user_picture); ?>" alt="<?php echo $teacher->first_name.' '.$teacher->last_name; ?>" width="25" height="25" />
                    <?php endif; ?>
                </div>
                <h4>Hint</h4>
            </header>
            <section>
                <?php echo $q->question->hint; ?>
            </section>
        </div>
        <?php endif; ?>
        <?php if(!empty($q->question->instruction)) : ?>
        <div class="question">
            <p class="instruction">Referring to Questions <?php echo $counter.' - '.($counter + $q->question->weight - 1);?></p>
            <?php echo $q->question->instruction; ?>
        </div>
        <?php endif; ?>
        <div class="question">
            <?php if($this->auth->role_id() == ADMIN_ROLE || $this->auth->role_id() == REVIEWER_ROLE) : ?>
            <span class="number"><a href="<?php echo site_url(array('reviewer', 'question_queue', 'review', $q->question->question_id > 0 ? $q->question->question_id : $q->question->id )); ?>" target="_review"><?php print $counter . '.';?></a></span>
            <?php else : ?>
            <span class="number"><?php echo $counter.'.';?></span>
            <?php endif; ?>
            <?php echo $q->question->question; ?>
        </div>
        <?php if($q->question->type == 1) : ?>
        <ul class="list-unstyled choices">
            <?php $choices = (array) json_decode($q->question->choices);
            foreach($choices as $value => $option) : ?>
            <li><span class="number"><strong><?php echo strtoupper(base_convert($value + 9, 10, 36)); ?>.</strong></span>
                <label><input type="radio" name="Question[<?php echo $q->question->id; ?>]" value="<?php echo $value; ?>"<?php if($q->answer == $value) echo ' checked="checked"'; ?> /> <div class="opt"><?php echo $option; ?></div></label>
                <div style="clear:left;"></div></li>
            <?php endforeach; ?>
        </ul>
			<p class="report-issue-wrapper" style="text-align:right;"><a class="report-issue" data-toggle="modal" data-target="#dialog" href="/testing/question_issue/<?php print $q->question->id ?>" style="color: #a33;font-size: 12px">Report question</a>
</p>
        <input type="radio" name="Question[<?php echo $q->question->id; ?>]" value="0"<?php if($q->answer == 0) echo ' checked="checked"'; ?> class="hidden" />
        <?php endif;
        if($q->question->type == 2) :
            if(empty($q->question->choices)) : ?>
            <ul>
                <li>
                    <textarea name="Question[<?php echo $q->question->id; ?>]"<?php if($counter % 5 == 1) echo ' autofocus'; ?> cols="80" rows="3"><?php echo $q->answer; ?></textarea>
                </li>
            </ul>
        <?php
            else : ?>
             <ul>
                <?php $choices = (array) json_decode($q->question->choices);
                if(!empty($q->answer)) $ans = (array) json_decode($q->answer);
                foreach($choices as $value => $option) : ?>
                <li><span class="number"><strong><?php echo $value; ?>.</strong></span>
                    <?php echo $option; ?>
                    <textarea name="Question[<?php echo $q->question->id; ?>][<?php echo $value; ?>]" cols="80" rows="3"><?php if(isset($ans[$value])) echo $ans[$value]; ?></textarea></li>
                <?php endforeach; ?>
            </ul>
        <?php
            endif;
        endif;
    endforeach; ?>

    <input type="hidden" name="page" value="<?php echo $page; ?>" />
    <input type="hidden" name="start_count" value="<?php print $start_count; ?>" />
    <input type="hidden" name="test_id" value="<?php echo $test->id ?>" />
</div>
</div>
<div class="col-xs-12 col-md-8 controls clearfix">
<?php if($offset < $qc) : ?>
    <?php print form_submit('next', 'Next Page', array('id' => 'next', 'class' => 'btn btn-primary next btn-lg pull-right')); ?>
    <?php if($start_count != 0) : ?>
        <?php print form_submit('previous', 'Previous Page', array('id' => 'previous', 'class' => 'btn btn-primary btn-lg previous pull-left')); ?>
    <?php endif; ?>
<?php endif; ?>
<?php if($offset >= $qc) : ?>
    <?php print form_submit('next', 'Finish Test', array('class' => 'btn btn-danger finish btn-lg pull-right')); ?>
    <?php print form_submit('previous', 'Previous Page', array('class' => 'btn btn-primary btn-lg pull-left previous')); ?>
<?php endif; ?>
</div>
<?php print form_close(); ?>
