<div class="panel-success">
	<div class="panel-heading">
		<h4>Issue reported</h4>
	</div>
	<div class="modal-body">
	<p>Your issue was reported to the support team! You will hear from us soon.</p>
	</div>
<div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
</div>
