<?php
/**
 * Testing
 */

define('PAGE_SIZE', 5);

/*
 * this is used as a guideline, tests may actually have
 * more questions than that
 * if the linked questions exceed the amount.
 * */
define('MAX_QUESTIONS', 20);

class Testing extends EF_Dashboard_Controller
{
    public function __construct ()
    {
        parent::__construct();
        Template::set_theme('testing');
    }

    public function start ()
    {
        $teacher = $this->input->post('teacher');
        $topic = $this->input->post('topic');

        $teacher = Teacher::find_by_permalink($teacher);
        $topic = Topic::find_by_id($topic);

        if ($topic === NULL) {
            Template::set_message(
                'Oops! No topic selected to start your test. Try again.', 'error'
            );
            redirect('dashboard');
        }

        $params = array(
            'user_id' => $this->auth->user_id(),
            'topic_id' => $topic->id,
            'subject_id' => $topic->subject_id
        );

        $params['teacher_id'] = $teacher === null ? -1 : $teacher->user_id;
        $test = Test::create($params);

        if ($test !== NULL) {

            $type = $this->input->post('format') == 'multiple' ? 1 : 2;
            $max_q = $this->input->post('max_questions');
            $max_q = $max_q !== FALSE ?
                min($max_q, MAX_QUESTIONS) : MAX_QUESTIONS;

            $query = "SELECT IFNULL(SUM(weight), 0) count FROM questions ";
            $query .= " WHERE topic_id = ? and TYPE = ? and status = ?
                AND question_id = 0 ";

            //reusing params variable
            $params = array($topic->id, $type, 'Accepted');

            if ($teacher !== null) {
                $query .= " AND user_id = ? ";
                $params[] = $teacher->user_id;
            }

            $countQ = $this->db->query($query, $params);
            $countQ = $countQ->row()->count;
            $max_q = min($max_q, $countQ);

            $p_size = $this->input->post('page_size');
            $p_size = $p_size !== FALSE ? min($p_size, PAGE_SIZE) : PAGE_SIZE;
            $p_size = min($p_size, $max_q);

            $this->session->set_userdata('page_size', $p_size);
            $this->session->set_userdata('max_questions', $max_q);
            $this->session->set_userdata('format', $type);

            redirect("testing/{$test->id}");
        } else {
            redirect('testing'); //error
        }
    }

    public function index($id=0)
    {
        if ($this->input->post('quit')) {
            redirect('dashboard');
        }

        $test = Test::find_by_id_and_user_id(
            $id, $this->auth->user_id()
        );

        if ($test == NULL) {
            Template::set_message(
                "Oops! Something went wrong with loading your test. Try again. Test ID: #{$id}.", 'error'
            );
            redirect('dashboard');
        }

        $page_size = $this->session->userdata('page_size');
        $page_size = $page_size === FALSE ? PAGE_SIZE : $page_size;

        $max = $this->session->userdata('max_questions');
        $max = $max === FALSE ? MAX_QUESTIONS : $max;

        $curr_page = $this->input->post('page') === FALSE ? 1 :
            $this->input->post('page');

        $curr_page = isset($_POST['previous']) ? max(1, $curr_page - 1) : $curr_page;
        $curr_page = isset($_POST['next']) ? $curr_page + 1 : $curr_page;

        $this->load->library('TestMaster');
        $master = $this->testmaster;
        $master->init($test, $page_size, $max);

        //accept submitted questions if any
        if (isset($_POST['Question'])) {
            $master->accept_answers($this->input->post('Question'));
        }

        //will return false if test is finished
        $questions = $master->get_answers($curr_page);

        if ($questions === FALSE) {
            redirect("testing/complete/{$test->id}");
        } else {
            Template::set('questions', $questions);
        }

        Template::set('topic', Topic::find_by_id($test->topic_id));
        Template::set('subject', Subject::find_by_id($test->subject_id));
        Template::set('teacher', Teacher::find_by_user_id($test->teacher_id));
        Template::set('weight', $test->get_weights());
        Template::set('start_count', array_sum(array_slice( $test->get_weights(), 0, $curr_page - 1)));
        Template::set('qc', $this->session->userdata('max_questions'));
        Template::set('test', $test);
        Template::set('page', $curr_page);
        Template::set('title', 'Test - EduFocal');
        Template::set_view('testing/timed_test');
        Template::render();
    }

    /**
     * Test Complete
     *
     * @param integer $id Test ID
     */
    public function complete ( $id )
    {
        $test = Test::find_by_id_and_user_id($id, $this->auth->profile()->id);

        if (count($test) == 0) redirect('dashboard');

        $test->complete();
        $data['questions'] = $test->answers;

        $data['teacher'] = Teacher::find_by_user_id($test->teacher_id);
        $data['subject'] = Subject::find_by_id($test->subject_id);
        $data['topic'] = Topic::find_by_id($test->topic_id);
        $data['user'] = $this->auth->profile();

        $data['percentage'] = $test->mark();

        if (empty($test->finished) || $test->finished == 0)
        {
            $test->finished = time();
            $test->save();

            //check on raffles
            $this->load->library('raffles/raffle_runner');
            $winner = $this->raffle_runner->run($this->auth->profile()->id, $test->id);
            if ($winner !== FALSE)
                $data['gift'] = $winner->raffle_basket;
        }

        if ($data['questions'][0]->test_type == 'timed')
        {
            $result = $this->calculate_exp_points($data['questions']);
            $data = array_merge($data, $result);
        }

        $current_points = StudentExpPoint::find_by_user_id_and_topic_id( $this->auth->profile()->id, $test->topic_id );
        $data['current_level'] = $current_points != null ? $current_points->level : 1;

        $data['title'] = 'Results - EduFocal';
        $data['options'] = array(
            1 => 'Not so great',
            2 => 'Quite good',
            3 => 'Good',
            4 => 'Great!',
            5 => 'Excellent!'
        );
        $data['test_id'] = $id;
        foreach($data as $key => $value)
            Template::set($key, $value);
        Template::set_view('testing/complete');
        Template::render();
    }

    /**
     * Test Review
     *
     * @param integer $id Test ID
     */
    public function review ( $id ) {
        $test = Test::find_by_id_and_user_id($id, $this->auth->profile()->id);

        if (count($test) == 0) redirect('dashboard');

        $data['questions'] = Answer::all(array("conditions" => array("test_id = ?", $id), "order" => "id"));

        $data['teacher'] = Teacher::find_by_user_id($test->teacher_id);
        $data['subject'] = Subject::find_by_id($test->subject_id);
        $data['topic'] = Topic::find_by_id($test->topic_id);

        $data['date'] = date("n/j/Y", $test->started);
        $data['minutes'] = floor(($test->finished - $test->started) / 60);
        $data['seconds'] = ($test->finished - $test->started) - ($data['minutes'] * 60);

        $data['percentage'] = $test->percentage;

        $data['title'] = 'Test Review - EduFocal';
        foreach($data as $key => $value)
            Template::set($key, $value);
        Template::set_view('testing/review');
        Template::render();
    }

    /**
     * Expired Timed Test
     *
     * @param integer $test Test ID
     */
    public function expired ( $test )
    {
        $data['title'] = 'Time Expired - EduFocal';

        $test_data = Test::find_by_id($test);
        $data['test_id'] = $test;
        $data['teacher'] = Teacher::find_by_id($test_data->teacher_id);
        $data['subject'] = Subject::find_by_id($test_data->subject_id);
        $data['topic'] = Topic::find_by_id($test_data->topic_id);

        $answers = Answer::all(array("conditions" => array("test_id = ?", $test)));

        foreach($answers as $answer) {
            $answer->completed = 'yes';
            $answer->save();
        }

        $data['percentage'] = $test_data->mark();
        $data['user'] = $this->auth->profile();
        $data['options'] = array(
            1 => 'Not so great',
            2 => 'Quite good',
            3 => 'Good',
            4 => 'Great!',
            5 => 'Excellent!'
        );

        if (empty($test_data->finished)) {
            $test_data->finished = time();
            $test_data->save();
        }

        foreach($data as $key => $value)
            Template::set($key, $value);

        Template::set_view('testing/expired');
        Template::render();
    }

    /**
     * Save Teacher Rating
     */
    public function save_rating ()
    {
        $test = Test::find_by_id($this->input->post('test'));
        Rating::create(array(
            'rating' => $this->input->post('rate'),
            'teacher_id' => $test->teacher_id,
            'topic_id' => $test->topic_id,
            'student_id' => $test->user_id,
            'test_id' => $this->input->post('test')
        ));
        echo "success";
    }

    public function question_issue($id)
    {
        $question = Question::find_by_id($id);

        if($this->input->post('problem') || $this->input->post('other')) {
            $problem = $this->input->post('other', true);

            if($problem == '') {
                $problem = $this->input->post('problem');
            }

            $qid = $question->question_id > 0 ? $question->question_id : $question->id;

            $fd_domain = "https://edufocal.freshdesk.com";
            $token = "paul@edufocal.com";
            $password = "tuudhfho";
            $data = array(
                "helpdesk_ticket" => array(
                    "description_html" => $problem . " <a href='" . site_url(['reviewer', 'question_queue', 'review',$qid]) . "'>Review</a>",
                    "subject" => "Q#:" . $question->id . " " . $problem,
                    "email" => $this->auth->email(),
                    "priority" => 1,
                    "status" => 2
                )
            );

            $query = "INSERT INTO `question_issues` (`id`, `question_id`, `user_id`, `comment`, `created_at`) VALUES (NULL, ?, ?, ?, NOW());";
            $parameters = array(
                "question_Id" => $question->id,
                "user_Id" => $this->auth->user()->id,
                "comment" => $problem
            );
            $this->db->query($query, $parameters);

            $json_body = json_encode($data, JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
            $header[] = "Content-type: application/json";
            $connection = curl_init("$fd_domain/helpdesk/tickets.json");
            curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($connection, CURLOPT_HTTPHEADER, $header);
            curl_setopt($connection, CURLOPT_HEADER, false);
            curl_setopt($connection, CURLOPT_USERPWD, "$token:$password");
            curl_setopt($connection, CURLOPT_POST, true);
            curl_setopt($connection, CURLOPT_POSTFIELDS, $json_body);
            curl_setopt($connection, CURLOPT_VERBOSE, 1);
            $response = curl_exec($connection);
            $this->load->view('issue_success', ['question' => $question]);
        } else {
            $this->load->view('issue', ['question'=> $question]);
        }
    }

    /**
     * Assign Experience Points
     *
     * @param array $answers 10 - 20 Question/Answers
     * @return Total Experience Earned/Lost
     */
    private function calculate_exp_points( $answers, $recalculate = false )
    {
        $this->load->helper('student_score');
        return ef_calculate_exp_points($answers, $recalculate);
    }
}
