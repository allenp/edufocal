<?php

class Migration_add_columns_to_tests_answers extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'weight' => array('type' => 'varchar(255)', 'default' => null),
        );
        $this->dbforge->add_column($prefix.'tests', $fields);

        $fields = array(
            'correct' => array('type' => 'varchar(3)', 'default' => null)
        );
        $this->dbforge->add_column($prefix.'answers', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column($this->db->dbprefix.'tests', 'weight');
        $this->dbforge->drop_column($this->db->dbprefix.'answers', 'correct');
    }
}
