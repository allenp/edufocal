<?php defined('BASEPATH') OR exit('NO direct script access allowed');

class Migration_create_question_issues_table extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;
        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true),
            'question_id' => array('type' => 'int(11)'),
            'user_id' => array('type' => 'int(11)'),
            'comment' => array('type' => 'text'),
            'created_at' => array('type' => 'datetime', 'null' => true),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix.'question_issues');

    }

    public function down()
    {
        $prefix = $this->db->prefix;
        $this->dbforge->drop_table($prefix. 'question_issues');
    }
}
