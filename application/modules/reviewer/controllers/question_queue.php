<?php
/**
 * Question Queue
 *
 * @package EduFocal
 * @author Shane Shipston
 */
class Question_Queue extends EF_Reviewer_Controller
{
    private $rules = array();

	public function __construct ()
	{
		parent::__construct();
		
		$subjects = Subject::all();
		
		Template::set('subjects', $subjects);
	}
	
	/**
	 * General Subject Overview
	 */
	public function index ()
	{
		$this->session->unset_userdata('reviewer_type');
		$this->session->unset_userdata('reviewer_subject');
		
		$questions = Question::find_by_sql("SELECT COUNT(*) AS `count`, `topics`.`subject_id` AS `subject`, `topics`.`id` AS `topic` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `question_id` = 0 GROUP BY `questions`.`topic_id`");
		
		$counts = array();
		
		foreach($questions as $question) {
			if(array_key_exists($question->subject, $counts)) $counts[$question->subject] += $question->count;
			else $counts[$question->subject] = $question->count;
		}
		
		Template::set('counts', $counts);
		
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}
	
	public function type ($type)
	{
		switch($type) {
			case 'short_answer':
				$q_type = 2;
				break;
			case 'multiple_choice':
				$q_type = 1;
				break;
		}
		
		$this->session->unset_userdata('reviewer_subject');
		$this->session->set_userdata('reviewer_type', $q_type);
		
		$questions = Question::find_by_sql("SELECT COUNT(*) AS `count`, `topics`.`subject_id` AS `subject`, `topics`.`id` AS `topic` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `type` = ?  AND `question_id` = 0 GROUP BY `questions`.`topic_id`", array($q_type));
		
		$counts = array();
		
		foreach($questions as $question) {
			if(array_key_exists($question->subject, $counts)) $counts[$question->subject] += $question->count;
			else $counts[$question->subject] = $question->count;
		}
		
		Template::set_view('question_queue/index');
		Template::set('counts', $counts);
		
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}
	
	/**
	 * Pending questions based on subject
	 */
	public function subject ($level = '', $subject = '', $offset = 0)
	{
		$this->load->library("pagination");
		$this->load->helper('text');
		
		$details = Subject::find_by_permalink_and_level($subject, $level);
		$topics = array();
		foreach($details->topics as $topic) {
			$topics[$topic->id] = array('name' => $topic->name, 'permalink' => $topic->permalink);
		}
		
		$this->session->set_userdata('reviewer_subject', $details->id);
		
		$attr = array($details->id);
		$where = "`questions`.`status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0";
		
		if($this->session->userdata('reviewer_type')) {
			$attr[] = $this->session->userdata('reviewer_type');
			$where .= " AND `questions`.`type` = ?";
		}
		
		$count = count(Question::find_by_sql("SELECT * FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE ".$where, $attr));
		
		$config = array(
			'base_url' => site_url('reviewer/question_queue/subject/'.$level.'/'.$subject.'/'),
			'total_rows' => $count,
			'per_page' => 10,
			'uri_segment' => 6,
			'first_link' => false,
			'last_link' => false,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
			'full_tag_open' => '<ul>',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li>',
			'cur_tag_close' => '</li>'
		);
		
		$this->pagination->initialize($config);
		
		$questions = Question::find_by_sql("SELECT `questions`.*, `teachers`.`first_name`, `teachers`.`last_name`, users.first_name editor_first_name, users.last_name editor_last_name FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` LEFT JOIN `teachers` ON `questions`.`user_id` = `teachers`.`user_id` LEFT OUTER JOIN `users` ON users.id = questions.editor_id WHERE ".$where." LIMIT ".$offset.", 10", $attr);
		
		Template::set('subject', $subject);
		Template::set('offset', $offset);
		Template::set('count', $count);
		Template::set('questions', $questions);
		Template::set('topics', $topics);
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}

	/**
	 * Question Review
	 */
	public function review($id) {
		$data = Question::find_all_by_id_or_question_id($id, $id
		, array('order' => 'id'));
		$subject = Subject::find($data[0]->topic->subject_id);
		$teacher = Teacher::find_by_user_id($data[0]->user_id);
		
		if($this->session->userdata('reviewer_type'))
			$next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? AND `questions`.`type` = ? ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id, $this->session->userdata('reviewer_type')));
		else
			$next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id));
		
		if($this->session->userdata('reviewer_type'))
			$previous = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` < ? AND `questions`.`type` = ? ORDER BY `questions`.`id` DESC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id, $this->session->userdata('reviewer_type')));
		else
			$previous = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` < ? ORDER BY `questions`.`id` DESC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id));
		
		Template::set('title', 'Question Queue - EduFocal');
		if(count($next) > 0) Template::set('next', $next[0]->id);
		if(count($previous) > 0) Template::set('previous', $previous[0]->id);
		Template::set('topic_name', $data[0]->topic->name);
		Template::set('subject', $subject);
		Template::set('questions', $data);
		Template::set('teacher', $teacher);
		Template::render();
	}
	
	/**
	 * User Voted Yes
	 */
	public function vote_yes($id) {
		$check = QuestionStatus::find_by_question_id($id);
		
		$question = Question::find($id);
		$subject = Subject::find($question->topic->subject_id);
        $questions = Question::find_all_by_question_id($id);
			QuestionStatus::create(
				array(
					'user_id' => $this->auth->profile()->id,
					'question_id' => $id,
					'status' => 'Accepted'
				)
			);
			
			$question->status = 'Accepted';
			$question->save();

            foreach($questions as $q) {
                $q->status = 'Accepted';
                $q->save();
            }
			
			// Check Listings
			$teacher_topic = TeachersTopic::find_by_teacher_id_and_topic_id($question->user_id, $question->topic_id); 
			if($teacher_topic->active == 0) {
				$question_count = Question::find_by_sql("SELECT SUM(weight) as `total_weight` FROM `questions` WHERE `user_id` = ? AND `topic_id` = ? AND `status` = ? AND `type` = ?", array($question->user_id, $question->topic_id, 'Accepted', $question->type));
				if(count($question_count) > 0 && $question_count[0]->total_weight >= 10) {
					$teacher_topic->active = 1;
					$teacher_topic->save();
					
					// Activate Topic
					$topic = Topic::find($question->topic_id);
					$topic->update_attributes(array('active' => 1));
					
					// Activate Subject
					$subject->update_attributes(array('active' => 1));
				}
			}

        if($this->session->userdata('reviewer_type'))
            $next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? AND `questions`.`type` = ? ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id, $this->session->userdata('reviewer_type')));
		else
			$next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id));
			
		if(count($next) > 0)
			redirect('reviewer/question_queue/review/'.$next[0]->id);
		else 
			redirect('reviewer/question_queue');
	}
	
	/**
	 * User Voted No
	 */
	public function vote_no($id) {
		$check = QuestionStatus::find_by_question_id($id);
		$question = Question::find($id);
		$subject = Subject::find($question->topic->subject_id);
		
        QuestionStatus::create(
            array(
                'user_id' => $this->auth->profile()->id,
                'question_id' => $id,
                'status' => 'Rejected',
                'reason' => $this->input->post('comments')
            )
        );

        $question->status = 'Rejected';
        $question->save();
		
        if($this->session->userdata('reviewer_type'))
            $next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? AND `questions`.`type` = ? ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id, $this->session->userdata('reviewer_type')));
		else
			$next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `status` = 'Pending' AND `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id));
			
		if(count($next) > 0)
			redirect('reviewer/question_queue/review/'.$next[0]->id);
		else 
			redirect('reviewer/question_queue');
	}

	public function explain($explained = 'no', $level = '', $subject = '', $offset = 0)
	{
        if($explained == 'no')
        {
           $explain = "AND (`explain` IS NULL OR CHAR_LENGTH(`explain`) < 10)";
        }
        else
        {
            $explained = 'yes'; //sanitize the value since we only check for no
            $explain = "AND (`explain` IS NOT NULL AND CHAR_LENGTH(`explain`) > 10)";
        }
        if($level == '')
        {
            $type = 'multiple_choice';
            switch($type)
            {
                case 'short_answer':
                    $q_type = 2;
                    break;
                case 'multiple_choice':
                    $q_type = 1;
                    break;
            }

            $questions = Question::find_by_sql("SELECT COUNT(*) AS `count`, `topics`.`subject_id` AS `subject`, `topics`.`id` AS `topic` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `type` = ?  AND `status` <> 'Rejected' AND `question_id` = 0 ".$explain." GROUP BY `questions`.`topic_id`", array($q_type));
            
            $counts = array();
            
            foreach($questions as $question) {
                if(array_key_exists($question->subject, $counts)) $counts[$question->subject] += $question->count;
                else $counts[$question->subject] = $question->count;
            }
            
            Template::set_view('question_queue/explain');
            Template::set('counts', $counts);
            Template::set('explained', $explained);
            Template::set('title', 'Question Queue - EduFocal');
            Template::render();
        }
        else
        {
            $this->load->library("pagination");
            $this->load->helper('text');
            
            $details = Subject::find_by_permalink_and_level($subject, $level);
            $topics = array();
            foreach($details->topics as $topic) {
                $topics[$topic->id] = array('name' => $topic->name, 'permalink' => $topic->permalink);
            }
            
            $this->session->set_userdata('reviewer_subject', $details->id);
            
            $attr = array($details->id);
            $where = "`topics`.`subject_id` = ? AND `questions`.`status` <> 'Rejected' AND `question_id` = 0 " .$explain;
            
            $count = Question::count(array("conditions" => array($where,$attr), "joins" => "LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id`"));
            
            $config = array(
                'base_url' => site_url('reviewer/question_queue/explain/'.$explained.'/'.$level.'/'.$subject.'/'),
                'total_rows' => $count,
                'per_page' => 10,
                'uri_segment' => 7,
                'first_link' => false,
                'last_link' => false,
                'next_link' => 'Next',
                'prev_link' => 'Previous',
                'full_tag_open' => '<ul>',
                'full_tag_close' => '</ul>',
                'num_tag_open' => '<li>',
                'num_tag_close' => '</li>',
                'next_tag_open' => '<li>',
                'next_tag_close' => '</li>',
                'prev_tag_open' => '<li>',
                'prev_tag_close' => '</li>',
                'cur_tag_open' => '<li>',
                'cur_tag_close' => '</li>'
            );
            
            $this->pagination->initialize($config);
            
            $questions = Question::find_by_sql("SELECT `questions`.*, `teachers`.`first_name`, `teachers`.`last_name` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` LEFT JOIN `teachers` ON `questions`.`user_id` = `teachers`.`user_id` WHERE ".$where." LIMIT ".$offset.", 10", $attr);
            
            Template::set('subject', $subject);
            Template::set_view('question_queue/subject');
            Template::set('offset', $offset);
            Template::set('count', $count);
            Template::set('questions', $questions);
            Template::set('topics', $topics);
            Template::set('title', 'Question Queue - EduFocal');
            Template::render();
        }
	}

    public function edit($id=0)
    {
		if(isset($_POST['submit']))
        {
            $id = $this->input->post('id');

            if($this->save_multiple($id) === TRUE)
                redirect('reviewer/question_queue/edit/'.$id);
		}

        $questions = Question::find('all', array('conditions' => array('id = ? OR question_id = ?', $id, $id), 'order' => 'id asc'));

        if($questions == null)
            return;
        $subject = Subject::find_by_id($questions[0]->topic->subject_id);
		$topics = TeachersTopic::find_all_by_teacher_id($questions[0]->user_id, array('order' => 'subject_id desc')); 
		$topic_dropdown = array();
		foreach ( $topics as $topic )
			$topic_dropdown[$topic->subject->name][$topic->topic->id] = $topic->topic->name . ' - ' . $topic->subject->level;
		
		$teacher = Teacher::find_by_user_id($questions[0]->user_id);
		
		if($this->session->userdata('reviewer_type'))
			$next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? AND `questions`.`type` = ?  AND `questions`.`status` <> 'Rejected' ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id, $this->session->userdata('reviewer_type')));
		else
			$next = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` > ? AND `questions`.`status` <> 'Rejected' ORDER BY `questions`.`id` ASC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id));
		
		if($this->session->userdata('reviewer_type'))
			$previous = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` < ?  AND `questions`.`status` <> 'Rejected' AND `questions`.`type` = ? ORDER BY `questions`.`id` DESC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id, $this->session->userdata('reviewer_type')));
		else
			$previous = Question::find_by_sql("SELECT `questions`.`id` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` WHERE `topics`.`subject_id` = ? AND `question_id` = 0 AND `questions`.`id` < ? AND `questions`.`status` <> 'Rejected' ORDER BY `questions`.`id` DESC LIMIT 1", array($this->session->userdata('reviewer_subject'), $id));

		if(count($next) > 0) Template::set('next', $next[0]->id);
		if(count($previous) > 0) Template::set('previous', $previous[0]->id);

		Template::set('permalink', $teacher->permalink);
        Template::set('teacher', $teacher);
        Template::set('subject', $subject);
		Template::set('topics', $topic_dropdown);
        Template::set('topic_name', $questions[0]->topic->name);

        $qs = array();
        $correct = array();
        $hint = array();
        $explanation =  array();

        foreach($questions as $qkey => $que)
        {
            $qs[] = $que->question;
            $correct[] = $que->accepted_answers;
            $hint[] = $que->hint;
            $explanation[] = $que->explain;
            $choices[$qkey] = (array)json_decode($que->choices);
        }

        Template::set('questions', $qs);
        Template::set('question_id', $questions[0]->id);
        Template::set('instruction', $questions[0]->instruction);
        Template::set('choices', $choices);
        Template::set('correct', $correct);
        Template::set('hint', $hint);
        Template::set('explanation', $explanation);
        Template::set('topic_id', $questions[0]->topic_id);
        Template::set_view('question_queue/edit_multiple');
        Template::set('title', 'Edit Multple Choice - EduFocal');

		Template::render();
    }

    private function save_short_answer()
    {
		$type = $this->input->post('type');
		
		$this->rules[] = array(
			'field' => 'topic',
			'label' => 'Subject / Topic',
			'rules' => 'required'
		);
        $this->rules[] = array(
            'field' => 'explain',
            'label' => 'Explanation',
            'rules' => 'trim'
        );
		if($this->input->post('style') == 'multi')
        {
			$this->rules[] = array(
				'field' => 'question',
				'label' => 'Instruction',
				'rules' => 'trim|required'
			);
			$this->rules[] = array(
				'field' => 'choices',
				'label' => 'question',
				'rules' => 'required|callback__array_check'
			);
			$this->rules[] = array(
				'field' => 'answers',
				'label' => 'answer',
				'rules' => 'required|callback__array_check'
			);
		}
        else
        {
			$this->rules[] = array(
				'field' => 'question',
				'label' => 'Question',
				'rules' => 'trim|required'
			);
		}
		
		$this->form_validation->set_rules($this->rules);
		
		if ($this->form_validation->run() === TRUE)
        {
			$question = Question::find($this->input->post('id'));
			$question->topic_id = $this->input->post('topic');
			$question->question = $this->input->post('question');
			$question->type = $type;
			$question->difficulty = $this->input->post('difficulty');
            if($question->status == 'Rejected')
                $question->status = 'Pending';
            $question->explain = $this->input->post('explain'); 
			$question->hint = $this->input->post('hint');
            if($this->input->post('style') == 'multi') {
                $question->accepted_answers = json_encode($this->input->post('answers'));
                $question->choices = json_encode($this->input->post('choices'));
            } else {
                $question->accepted_answers = $this->input->post('answer');
            }
			
			$question->save();
			
			Template::set_message('Question saved successfully', 'success');
            return true;
		}
        return false;
    }

	/**
	 * Save Linked Multiples 
	 */
	public function save_multiple($id=0)
    {
		$this->rules[] = array(
			'field' => 'topic',
			'label' => 'Subject / Topic',
			'rules' => 'required'
		);
		$this->rules[] = array(
			'field' => 'instruction',
			'label' => 'Instruction / Data',
			'rules' => 'trim' /*required for linked multiples*/
		);
		$this->rules[] = array(
			'field' => 'question',
			'label' => 'question',
			'rules' => 'callback__array_check'
		);
		$this->rules[] = array(
			'field' => 'choices',
			'label' => 'answer option',
			'rules' => 'callback__array_check'
		);
		$this->rules[] = array(
			'field' => 'correct',
			'label' => 'correct answer',
			'rules' => 'required|callback__correct_answers'
		);
		
		$this->form_validation->set_rules($this->rules);
		
		$question_id = 0;
		
		if ($this->form_validation->run() === TRUE)
        {
            if($id !== 0)
                $qObs = Question::find('all', array('conditions' => array('id = ? OR question_id = ?', $id, $id),
                                    'order' => 'id asc'));
            $index = 0;
			foreach($this->input->post('question') as $key => $question_body)
            {
                if(isset($qObs) && isset($qObs[$index])) 
                {
                    $question = $qObs[$index];
                }
                else
                {
                    $question = new Question();
                    $question->user_id = $this->input->post('teacher_id');
                }

                $index += 1;
				$question->topic_id = $this->input->post('topic');
				$question->question = $question_body;
				$question->type = 1;
				$question->difficulty = $this->input->post('difficulty');
				//$question->status = 'Pending';
				$question->accepted_answers = $_POST['correct'][$key];
				$question->choices = json_encode($_POST['choices'][$key]);
				$question->hint = $_POST['hint'][$key];
				$question->question_id = $question_id;
                $question->explain = $_POST['explain'][$key];
				if($key == 0)
                {
					$question->instruction = $this->input->post('instruction');
					$question->weight = count($this->input->post('question'));
				}

				$question->save();
				if($key == 0) $question_id = $question->id;
			}
            Template::set_message('Question saved successfully');
            return TRUE;
		}
        else
        {
			return false;
		}
	}


    public function editor_summary()
    {
        return $this->summary(TRUE);
    }

    public function teacher_summary()
    {
        return $this->summary(FALSE);
    }

    private function summary($editor = FALSE)
    {
        if($editor) {
            $join = 'questions.editor_id = users.id';
            Template::set('title', 'Editor upload summary - EduFocal');
        }
        else {
            $join = 'questions.user_id = users.id';
            Template::set('title', 'Teacher upload summary - EduFocal');
        }

        if(isset($_POST['submit'])) {
            $month = $this->input->post('month');
            $year = $this->input->post('year');
        } else {
            $month = date('n');
           $year = date('Y'); 
        }

        $summary = Question::find_by_sql(
            "SELECT count(questions.id) count, users.first_name, users.last_name, questions.status, DATE_FORMAT(questions.created_at, '%Y / %M') month
FROM questions
LEFT OUTER JOIN users on " . $join . " 
WHERE (? = '0' OR YEAR(questions.created_at) = ?) AND (? = '0' OR MONTH(questions.created_at) = ?)
GROUP BY users.first_name, users.last_name, questions.status, MONTH(questions.created_at)", array($year, $year, $month, $month));

        $y = Question::find_by_sql("SELECT DISTINCT YEAR(questions.created_at) year FROM questions");
        $years = array('0' => 'All');
        foreach($y as $item)
            if(trim($item->year) != '')
                $years[$item->year] = $item->year;
        Template::set('years', $years);
        Template::set('year', $year);
        Template::set('month', $month);
        Template::set('summary', $summary);
        Template::set_view('question_queue/summary');
        Template:: render();
    }

	/**
	 * Validate Input Arrays
	 *
	 * @param array $options
	 * @return boolean Success
	 */
	public function _array_check ( $options )
	{
		$return = true;
		
		foreach ($options as $question)
		{
			if(is_array($question))
			{
				if(!$this->_array_check($question)) $return = false;
			} 
			else
			{
				$question = trim($question);
				if(empty($question)) $return = false;
			}
		}
		
		if(!$return)
		{
			$this->form_validation->set_message(
			'_array_check',
			'One or more %s is missing.');
		}
		
		return $return;
	}

	/**
	 * Validate Correct Answers
	 *
	 * @param array $options
	 * @return boolean Success
	 */
	public function _correct_answers ( $options )
	{
		$return = true;
		
		foreach ($this->input->post('question') as $key => $question)
		{
			if(!array_key_exists($key, $options)) $return = false;
		}
		
		if(!$return)
		{
			$this->form_validation->set_message(
			'_correct_answers',
			'One or more %s is missing.');
		}
		return $return;
	}
}
