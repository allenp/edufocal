<?php

class Profile extends EF_Dashboard_Controller
{
	public function __construct ()
	{
		parent::__construct();
	}
	
	public function index ()
	{

        $fields = $this->auth->rules;

        unset($fields['birth_day']);
        unset($fields['birth_month']);
        unset($fields['birth_year']);
		
		Template::set('fields', $fields);
		Template::set('title', 'My Profile - EduFocal');
		Template::set_view('profile/view');
		Template::render();
	}
	
	public function edit ()
	{
		foreach ( $this->auth->rules as $info )
		{
			$this->form_validation->set_rules($info['field'], $info['label'], $info['rules']);
		}
		
		if ( $this->form_validation->run() === TRUE )
		{
			$data = array();
			foreach ( $this->auth->rules as $info )
			{
				$data[$info['field']] = $this->input->post($info['field']);	
			}
			
			$this->db->update('users', $data, array('id' => $this->auth->user_id()));
			Template::set_message('Your profile has been updated successfully!', 'success');
			redirect('dashboard/profile');
		}

        $fields = $this->auth->rules;

        //remove these fields becase they cant be set automated (see view) 
        unset($fields['birth_day']);
        unset($fields['birth_month']);
        unset($fields['birth_year']);
        unset($fields['sex']);
		
		Template::set('fields', $fields);
		Template::set('title', 'Edit Profile - EduFocal');
		Template::render();
	}
}

