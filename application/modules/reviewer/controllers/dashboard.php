<?php
/**
 * Reviewer Dashboard
 */
class Dashboard extends EF_Reviewer_Controller
{
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * General overview of Teachers Account + Coursework
     */
    public function index ()
    {
        $this->load->helper('text');

        $unread = count(MessageUser::find_all_by_user_id_and_read_and_deleted($this->auth->user_id(), 'no', 'no'));

        $mUser = MessageUser::find_all_by_user_id($this->auth->user_id());
        $message_ids = array();

        foreach ( $mUser as $mU ) {
            $message_ids[] = $mU->message_id;
        }

        $messages = array();

        if ( count($message_ids ) > 0 ) {
            $messages = Message::find_all_by_id($message_ids, array('limit' => 7));
        }

        $last_login = new DateTime($this->auth->profile()->last_login);

        Template::set('unread', $unread);
        Template::set('messages', $messages);
        Template::set('last_login', $last_login->format("d / m / Y"));
        Template::set_view('reviewer/index');
        Template::set('title', 'Dashboard - EduFocal');
        Template::render();
    }
}
