    <?php Assets::add_module_js('reviewer', 'question_queue_review'); ?>
	<div id="main">
    	<div class="heading padded">
        	<div class="left">
            	<h3><?php echo $subject->name; ?>: <?php echo $topic_name; ?> <small>by <?php echo $teacher->first_name.' '.$teacher->last_name; ?></small></h3>
            </div>
            <div class="right">
            	<?php if(isset($previous)) { ?>
            	<a href="<?php echo site_url('reviewer/question_queue/review/'.$previous); ?>"><img src="<?php echo site_url('assets/dbrd/img/previous_question.png'); ?>" alt="Previous Question" /></a>	
            	<?php }
				if(isset($next)) { ?>
            	<a href="<?php echo site_url('reviewer/question_queue/review/'.$next); ?>"><img src="<?php echo site_url('assets/dbrd/img/next_question.png'); ?>" alt="Next Question" /></a>
            	<?php } ?>
            </div>
        </div>
    	<div class="wrapper">
        	<?php foreach($questions as $question) : ?>
                <?php if($question->weight > 1 && $question->question_id == 0) : ?>
                    <div class="selection instruction">
                        <div class="question">
                            <?php echo $question->instruction; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <div class="selection">
                    <h3>Q:</h3>
                    <div class="question">
                    <?php if($question->question_id == 0) : ?>
                    <div style="float:right;margin:-1em 1em;font-size:small;text-transform:none;"><a href="<?php print site_url('reviewer/question_queue/edit/'.$question->id); ?>">Edit Question</a></div>
                    <?php endif; ?>
                        <?php echo $question->question; ?>

							<?php if(!empty($question->choices)) {
                            $choices = json_decode($question->choices);
                            foreach($choices as $num => $choice) { ?>
                            <div class="clearfix"></div>
                            <h3><?php echo (is_numeric($num) ? strtoupper(base_convert($num + 9, 10, 36)) : $num); ?>.</h3>
                            <div class="question">
                                <?php echo $choice; ?>
                            </div>
                            <?php } 
                        } ?>
                        <div class="clearfix">&nbsp;</div>
                        <?php if(strlen(trim($question->hint)) > 0): ?>
                        <div class="selection">
                        <h4>Hint</h4>
                        <?php print $question->hint; ?>
                        </div>
                        <?php endif; ?>
                        <?php if(strlen(trim($question->explain)) > 0): ?>
                        <div class="selection">
                        <h4>Explanation</h4>
                        <?php print $question->explain; ?>
                        </div>
                        <?php endif; ?>

                    </div>
                    <div class="clearfix"></div>
                </div>
                
                <?php if(!empty($question->hint)) : ?>
                    <div class="answer">
                        <div class="submitted" style="border-radius: 6px;">
                            <h3>Hint:</h3>
                            <div class="question">
                                <?php echo $question->hint; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                <?php endif; ?>
                
                <?php if($question->weight > 1 || $question->question_id != 0) : ?>
                    <div class="answer selection">
                        <h3>A:</h3>
                        <div class="question">
                            <?php if($question->type == 2 && substr($question->accepted_answers, 0, 1) == '{') {
                                $answers = json_decode($question->accepted_answers);
                                foreach($answers as $num => $choice) { ?>
                                <div class="clearfix"></div>
                                <h3><?php echo (is_numeric($num) ? strtoupper(base_convert($num + 9, 10, 36)) : $num); ?>.</h3>
                                <p><?php echo $choice; ?></p>
                            <?php } 
                            } else { ?>
                                <p><?php echo (is_numeric($question->accepted_answers) ? strtoupper(base_convert($question->accepted_answers + 9, 10, 36)) : $question->accepted_answers); ?></p>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php else: ?>
                    <div class="answer">
                        <div class="submitted">
                            <h3>A:</h3>
                            <div class="question">
                                <?php if($question->type == 2 && substr($question->accepted_answers, 0, 1) == '{') {
                                    $answers = json_decode($question->accepted_answers);
                                    foreach($answers as $num => $choice) { ?>
                                    <div class="clearfix"></div>
                                    <h3><?php echo (is_numeric($num) ? strtoupper(base_convert($num + 9, 10, 36)) : $num); ?>.</h3>
                                    <p><?php echo $choice; ?></p>
                                <?php } 
                                } else { ?>
                                    <p><?php echo (is_numeric($question->accepted_answers) ? strtoupper(base_convert($question->accepted_answers + 9, 10, 36)) : $question->accepted_answers); ?></p>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="vote">
                            <div id="vote_selection">
                                <p>Is <?php echo $teacher->first_name; ?>'s question acceptable?</p>
                                <a href="<?php echo site_url('reviewer/question_queue/vote_yes/'.$question->id); ?>"><img src="<?php echo site_url('assets/dbrd/img/vote_yes.png'); ?>" alt="Yes" /></a>
                                <a href="#" id="vote_no"><img src="<?php echo site_url('assets/dbrd/img/vote_no.png'); ?>" alt="No" /></a>
                            </div>
                            <?php print form_open(site_url('reviewer/question_queue/vote_no/'.$question->id), array('id' => 'voting_no', 'class' => 'hidden')); ?>
                                <p>You are voting NO to 's question. Please provide a reason you feel it is unacceptable.</p>
                                <table width="100%" cellspacing="0" cellpadding="4">
                                    <tr>
                                        <td><label>Comments</label></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><textarea name="comments" cols="50" rows="6"></textarea></td>
                                        <td valign="top"><p><button type="submit" class="blue">Save</button> or <a href="#" id="cancel_vote">cancel</a></p></td>
                                    </tr>
                                </table>
                                <?php print form_close(); ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            
            <?php if(count($questions) > 1) : ?>
            	<div class="answer">
                    <div class="vote">
                        <div id="vote_selection">
                                <p>Is <?php echo $teacher->first_name; ?>'s question acceptable?</p>
                                <a href="<?php echo site_url('reviewer/question_queue/vote_yes/'.$questions[0]->id); ?>"><img src="<?php echo site_url('assets/dbrd/img/vote_yes.png'); ?>" alt="Yes" /></a>
                                <a href="#" id="vote_no"><img src="<?php echo site_url('assets/dbrd/img/vote_no.png'); ?>" alt="No" /></a>
                            </div>
                            <?php print form_open(site_url('reviewer/question_queue/vote_no/'.$questions[0]->id), array('id' => 'voting_no', 'class' => 'hidden')); ?>
                                <p>You are voting NO to <?php echo $teacher->first_name; ?>'s question. Please provide a reason you feel it is unacceptable.</p>
                                <table width="100%" cellspacing="0" cellpadding="4">
                                    <tr>
                                        <td><label>Comments</label></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><textarea name="comments" cols="50" rows="6"></textarea></td>
                                        <td valign="top"><p><button type="submit" class="blue">Save</button> or <a href="#" id="cancel_vote">cancel</a></p></td>
                                    </tr>
                                </table>
                                <?php print form_close(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
