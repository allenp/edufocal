<div id="main">
    <div class="wrapper">
        <?php print form_open(); ?>
       <table cellpadding="10">
       <tr>
<td>
<?php $date[0] = 'All'; ?>
<?php for($i = 1; $i <= 12; $i++)
        $date[$i] = date("F", mktime(0, 0, 0, $i, 10)); ?>
<?php echo form_dropdown('month', $date, $month); ?> 
</td>
<td>
<?php echo form_dropdown('year', $years, $year); ?>
</td>
<td><input type="submit" name="submit" value="View month" /></td></tr>
      </table> 
        <?php print form_close(); ?>
        <table width="100%" cellspacing="0" cellpadding="12" class="styled">
            <tr class="heading">
                <td>Editor/Teacher</td>
                <td>Status</td>
                <td>Count</td>
                <td>Month</td>
            </tr>
<?php if(isset($summary) && count($summary) == 0) : ?>
<tr><td colspan="4">No summary to show.</td></tr>
<?php else: ?>
<?php foreach($summary as $summary) : ?>
            <tr>
                <td><?php print $summary->first_name . ' ' . $summary->last_name ?></td>
                <td><?php print ucfirst($summary->status); ?></td>
                <td><?php print $summary->count; ?></td>
                <td><?php print $summary->month; ?></td>
            <tr>
<?php endforeach; ?>
<?php endif; ?>
</table>
</div>
</div>
