<div id="main">
    	<div class="wrapper">
<?php print form_open($this->uri->uri_string()) ?>
        <?php if(count($teachers)) : ?>
            <div class="box">
            <select name="teacher_id">
            <?php foreach($teachers as $teacher) : ?>
                <option <?php if($teacher->user_id == $teacher_id) print "selected" ?> value="<?php print $teacher->user_id ?>"><?php print $teacher->first_name . ' ' . $teacher->last_name ?></option>
            <?php endforeach; ?>
            </select>
            </div>
        <?php endif; ?>
        <button name="change_owner">
            <span>Update Ownership</span>
        </button>
            
            <table width="100%" cellspacing="0" cellpadding="12" class="styled">
                <tr class="heading">
                    <td width="20%">Fully owned by EduFocal</td>
                    <td width="50%">Question Excerpt</td>
                    <td width="20%">Type</td>
                </tr>
                <?php if(count($questions) == 0) echo '<tr><td colspan="7">No Questions in the Queue</td></tr>';
                foreach($questions as $question) : ?>
                <tr>
                    <td><input type="checkbox" name="Questions[]" value="<?php print $question->id ?>" <?php if($question->owned > 0) print "checked" ?>/></td>
                    <!--<td><?php //echo word_limiter(strip_tags($question->question), 140); ?></td>-->
                    <td><strong><?php echo $topics[$question->topic_id]['name']; ?></strong><?php echo $question->question ?></td>
                    <td><?php echo $question->type == 1 ? ($question->weight > 1 ? 'Linked Multiple Choice' : 'Multiple Choice') : 'Short Answer'; ?></td>
                </tr>
                <?php endforeach; ?>
                <tr class="heading">
                	<td colspan="1"><strong>Showing <?php echo $offset + 1; ?> - <?php echo $offset + 10; ?> of <?php echo $count; ?> match<?php echo $count != 1 ? "es": ""; ?></strong></td>
                    <td colspan="2" align="right">
                        <?php echo $this->pagination->create_links(); ?>
                    </td>
                </tr>
            </table>
<?php print form_close(); ?>
        </div>
</div>
<div id="debug"></div>
