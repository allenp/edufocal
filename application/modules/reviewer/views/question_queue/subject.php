    <div id="main">
    	<div class="wrapper">
            <table width="100%" cellspacing="0" cellpadding="12" class="styled">
                <tr class="heading">
                    <td>Topic</td>
                    <td>Teacher</td>
                    <td>Editor</td>
                    <td width="30%">Question Excerpt</td>
                    <td>Type</td>
                    <td>Date</td>
                    <td></td>
                </tr>
                <?php if(count($questions) == 0) echo '<tr><td colspan="7">No Questions in the Queue</td></tr>';
                foreach($questions as $question) : ?>
                <tr>
                    <td><?php echo $topics[$question->topic_id]['name']; ?></td>
                    <td><?php echo $question->first_name.' '.$question->last_name; ?></td>
<td>
<?php
                    try
                    {
                        $fullname = $question->editor_first_name . ' ' . $question->editor_last_name;
                    }catch(Exception $e)
                    {
                        $fullname = '';
                    }
                    echo $fullname;
?>
</td>
                    <td><?php echo word_limiter(strip_tags($question->question), 15); ?></td>
                    <td><?php echo $question->type == 1 ? ($question->weight > 1 ? 'LMC' : 'MC') : 'SA'; ?></td>
                    <td><?php echo $question->created_at != null ?  date('Y-M-d', strtotime($question->created_at)) : '' ?></td>
                    <td><a href="<?php echo site_url('reviewer/question_queue/review/'.$question->id); ?>">Review</a> or <a href="<?php print site_url('/reviewer/question_queue/edit/'.$question->id); ?>">Edit</a></td>
                </tr>
                <?php endforeach; ?>
                <tr class="heading">
                	<td colspan="4"><strong>Showing <?php echo $count ? $offset + 1 : 0; ?> - <?php echo min($count, $offset + 10); ?> of <?php echo $count; ?> match<?php echo $count != 1 ? "es": ""; ?></strong></td>
                    <td colspan="3" align="right">
                        <?php echo $this->pagination->create_links(); ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
