    <div id="main">
        <div class="single_column">
        	<?php if(array_sum($counts) == 0) : ?>
            <table width="100%" cellspacing="0" cellpadding="15" class="styled border">
            	<tr>
                	<td>There are currently no questions in the queue.</td>
                </tr>
            </table>
            <?php else: ?>
            <table width="100%" cellspacing="0" cellpadding="15" class="styled border">
            	<tr class="heading2">
                	<td>Available Subjects:</td>
                    <td>Level:</td>
                </tr>
                <?php foreach($subjects as $subject) : 
					if(array_key_exists($subject->id, $counts)) : ?>
                <tr>
                	<td><h3><a href="<?php echo site_url('reviewer/question_queue/explain/'.$explained.'/'.$subject->level.'/'.$subject->permalink); ?>"><?php echo $subject->name; ?></a> - <?php echo $counts[$subject->id]; ?> Question<?php if($counts[$subject->id] != 1) echo 's'; ?></h3></td>
                    <td><h3><?php echo $subject->level; ?></h3></td>
                </tr>
				<?php endif;
				endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
    </div>
