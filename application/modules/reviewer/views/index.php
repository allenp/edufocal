	<div id="main">
		<div class="wrapper">
        	<?php if ( isset( $message ) && strlen($message) > 0 ) : ?>
    		<div class="alert">
        		<h4><?php echo $message; ?></h4>
        		<p class="close"><a href="#"><img src="<?php echo site_url(); ?>assets/dbrd/img/cross.png" alt="Close Message" /></a></p>
            	<div class="clearfix"></div>
       		</div>
      		<?php endif; ?> 
            <div id="column_1">
            	<div class="summary">
                	<div class="left">
                    	<h3>Hello, <?php echo $this->auth->profile()->first_name; ?></h3>
                    </div>
                    <div class="right">
                    	<a href="<?php echo site_url('reviewer/question_queue'); ?>"><img src="<?php echo site_url(); ?>assets/dbrd/img/question_queue.png" alt="My Question Queue" /></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div id="column_2">
            	<div class="overview">
                	<?php $avatar = $this->auth->profile()->avatar;
					if(empty($avatar)) : ?>
						<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" width="111" height="111" />
					<?php else : ?>
						<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" width="111" height="111" />
					<?php endif; ?>
                    <p>Last login: <?php echo $last_login; ?><br />
                    Current Earnings: N/A<br />
                    <br /><br />
                    <a href="<?php echo site_url("account"); ?>">Change profile picture</a><br />
                    <a href="<?php echo site_url("account"); ?>">Change user name</a></p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
