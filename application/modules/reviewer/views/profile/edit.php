    <div id="main">
		<div class="wrapper">
        	<div class="single_column">
            	<h2>My Profile</h2>
                <h3>Personal Information</h3>
                <div class="bordered">
                    <?php print form_open(site_url('dashboard/profile/edit')); ?>
                        <div class="data">
                            <div class="photo">
                                <img src="<?php echo site_url(); ?>assets/dbrd/img/gordon_big.png" alt="Gordon Swaby" />
                                <p><a href="#">Edit Photo</a> | <a href="#">Delete Photo</a></p>
                            </div>
                            <table width="60%" cellpadding="2">
                            	<?php foreach ( $fields as $info ) : ?>
                                <tr>
                                    <td><strong><?php echo $info['label']; ?></strong></td>
                                    <td><?php echo form_input($info['field'], $this->auth->profile->{$info['field']}); ?></td>
                                </tr>
                                <?php endforeach; ?>
                                
                                
                                
                                <!-- 
                                <tr>
                                    <td><strong>Date of Birth</strong></td>
                                    <td><input type="text" name="date_of_birth" value="10/3/1997" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Grade</strong></td>
                                    <td><input type="text" name="grade" value="8" size="2" /></td>
                                </tr>
                                <tr>
                                    <td><strong>School</strong></td>
                                    <td><input type="text" name="school" value="Allman Town Secondary School" /></td>
                                </tr>
                                <tr>
                                    <td><strong>Country</strong></td>
                                    <td><select name="country" />
                                    	<option selected>Jamaica</option>
                                    </select></td>
                                </tr> -->
                            </table>
                            <div class="clearfix"></div>
                        </div>
                        <div class="action">
                            <p align="right"><button type="submit" class="blue">Save</button> or <a href="<?php echo site_url('dashboard/profile'); ?>">cancel</a></p>
                        </div>
                    <?php print form_close(); ?>
                </div>
            </div>
            <div class="single_column">
            	<h3>Your Overall Experience Level</h3>
                <div class="bordered">
                	<div class="data exp">
                    	<table width="70%">
                        	<tr>
                            	<td width="38%">125,450 Points</td>
                                <td>Level 65</td>
                                <td><div class="bar_graph"><div class="bar_yellow" style="width: 100px;"></div></div></td>
                                <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                            </tr>
                        </table>
                    </div>
                    <div class="action">
                    	<h4>Looking to level up?</h4>
                        <p>Earn additional experience points by correcting answers in the <a href="questionqueue1.html">Question Queue!</a></p>
                    </div>
                </div>
            </div>
            <div class="single_column">
            	<h3>Gordon's Experience Levels by Subject</h3>
                <table width="100%" cellspacing="0" cellpadding="8" border="0" class="styled">
                	<tr class="heading">
                    	<td><a href="#">Subject</a></td>
                        <td><a href="#">Experience</a></td>
                        <td colspan="2"><a href="#">Level</a></td>
                        <td></td>
                        <td></td>
                        <td>Next Level</td>
                        <td>Question Queue</td>
                    </tr>
                    <tr>
                    	<td>English</td>
                        <td>25,000</td>
                        <td>65</td>
                        <td><div class="bar_graph"><div class="bar_yellow" style="width: 131px;"></div></div></td>
                        <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                        <td align="center"><a href="#">Submit Content</a></td>
                        <td align="right">3,000</td>
                        <td align="right"><a href="#">12</a></td>
                    </tr>
                    <tr>
                    	<td>History</td>
                        <td>21,000</td>
                        <td>60</td>
                        <td><div class="bar_graph"><div class="bar_yellow" style="width: 120px;"></div></div></td>
                        <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                        <td align="center"><a href="#">Submit Content</a></td>
                        <td align="right">3,500</td>
                        <td align="right"><a href="#">7</a></td>
                    </tr>
                    <tr>
                    	<td>Math</td>
                        <td>15,000</td>
                        <td>60</td>
                        <td><div class="bar_graph"><div class="bar_yellow" style="width: 120px;"></div></div></td>
                        <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                        <td align="center"><a href="#">Submit Content</a></td>
                        <td align="right">2,500</td>
                        <td align="right"><a href="#">5</a></td>
                    </tr>
                    <tr>
                    	<td>Social Studies</td>
                        <td>12,000</td>
                        <td>45</td>
                        <td><div class="bar_graph"><div class="bar_green" style="width: 100px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">1,800</td>
                        <td align="right"><a href="#">16</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>9,000</td>
                        <td>34</td>
                        <td><div class="bar_graph"><div class="bar_green" style="width: 90px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">200</td>
                        <td align="right"><a href="#">8</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>5,000</td>
                        <td>30</td>
                        <td><div class="bar_graph"><div class="bar_blue" style="width: 82px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">400</td>
                        <td align="right"><a href="#">22</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>4,500</td>
                        <td>25</td>
                        <td><div class="bar_graph"><div class="bar_blue" style="width: 75px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">1,500</td>
                        <td align="right"><a href="#">3</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>2,000</td>
                        <td>15</td>
                        <td><div class="bar_graph"><div class="bar_blue" style="width: 60px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">100</td>
                        <td align="right"><a href="#">4</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>950</td>
                        <td>9</td>
                        <td><div class="bar_graph"><div class="bar_grey" style="width: 35px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">200</td>
                        <td align="right"><a href="#">7</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>500</td>
                        <td>2</td>
                        <td><div class="bar_graph"><div class="bar_grey" style="width: 20px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">500</td>
                        <td align="right"><a href="#">2</a></td>
                    </tr>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
