    <?php Assets::add_module_js('reviewer', 'profile_view'); ?>
    <div id="main">
		<div class="wrapper">
        	<div class="single_column">
            	<h2>My Profile</h2>
                <h3>Personal Information <a href="<?php echo site_url('dashboard/profile/edit'); ?>">Edit</a></h3>
                <div class="bordered">

<div class="wrapper data">
<?php print form_open(); ?>
<ul class="form">
	<li>
		<label>Full Name</label>
		<span>Jamie Chung</span>
	</li>
	
	<li>
		<label>Full Name</label>
		<div>
			<input type="text" value="Jamie Chung" />
			<button>Save</button>
			<button>Cancel</button>
		</div>
	</li>
</ul>
<?php print form_close(); ?>
</div>
<style type="text/css">
ul.form
{
	list-style:none;
	margin:15px 0;
	padding:0;
	display:block;
}
ul.form li
{
	display:block;
	margin:0 0 5px 0;
	clear:both;
	padding:10px 15px;
}
ul.form li.active
{
	background:#EBEFFA;
}
ul.form li:hover
{
	background:#EBEFFA;
	cursor:pointer;
}
ul.form li label
{
	width:80px;
	text-align:right;
	display:inline-block;
	font-weight:bold;
	font-size:14px;
	margin-right:15px;
}
ul.form li span
{
	display:inline-block;
}
ul.form li div
{
	display:inline-block;
}
</style>


                	<div class="data">
                        <div class="photo">
                            <img src="<?php echo site_url(); ?>assets/dbrd/img/gordon_big.png" alt="Gordon Swaby" />
                            <p><a href="#">Edit Photo</a> | <a href="#">Delete Photo</a></p>
                        </div>

							
                        <table width="60%" cellpadding="8">
                        <?php foreach ( $fields as $rule ) : ?>
                        	
                            <tr>
                                <td><strong><?php echo $rule['label']; ?></strong></td>
                                <td><?php echo $this->auth->profile{$rule['field']}; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="single_column">
            	<h3>Your Overall Experience Level</h3>
                <div class="bordered">
                	<div class="data exp">
                    	<table width="70%">
                        	<tr>
                            	<td width="38%">125,450 Points</td>
                                <td>Level 65</td>
                                <td><div class="bar_graph"><div class="bar_yellow" style="width: 100px;"></div></div></td>
                                <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                            </tr>
                        </table>
                    </div>
                    <div class="action">
                    	<h4>Looking to level up?</h4>
                        <p>Earn additional experience points by correcting answers in the <a href="<?php echo site_url('dashboard/question_queue'); ?>">Question Queue!</a></p>
                    </div>
                </div>
            </div>
            <div class="single_column">
            	<h3>Gordon's Experience Levels by Subject</h3>
                <table width="100%" cellspacing="0" cellpadding="8" border="0" class="styled">
                	<tr class="heading">
                    	<td><a href="#">Subject</a></td>
                        <td><a href="#">Experience</a></td>
                        <td colspan="2"><a href="#">Level</a></td>
                        <td></td>
                        <td></td>
                        <td>Next Level</td>
                        <td>Question Queue</td>
                    </tr>
                    <tr>
                    	<td>English</td>
                        <td>25,000</td>
                        <td>65</td>
                        <td><div class="bar_graph"><div class="bar_yellow" style="width: 131px;"></div></div></td>
                        <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                        <td align="center"><a href="#">Submit Content</a></td>
                        <td align="right">3,000</td>
                        <td align="right"><a href="#">12</a></td>
                    </tr>
                    <tr>
                    	<td>History</td>
                        <td>21,000</td>
                        <td>60</td>
                        <td><div class="bar_graph"><div class="bar_yellow" style="width: 120px;"></div></div></td>
                        <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                        <td align="center"><a href="#">Submit Content</a></td>
                        <td align="right">3,500</td>
                        <td align="right"><a href="#">7</a></td>
                    </tr>
                    <tr>
                    	<td>Math</td>
                        <td>15,000</td>
                        <td>60</td>
                        <td><div class="bar_graph"><div class="bar_yellow" style="width: 120px;"></div></div></td>
                        <td class="tutor"><img src="<?php echo site_url(); ?>assets/dbrd/img/star.png" alt="Tutor" /><br />Tutor</td>
                        <td align="center"><a href="#">Submit Content</a></td>
                        <td align="right">2,500</td>
                        <td align="right"><a href="#">5</a></td>
                    </tr>
                    <tr>
                    	<td>Social Studies</td>
                        <td>12,000</td>
                        <td>45</td>
                        <td><div class="bar_graph"><div class="bar_green" style="width: 100px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">1,800</td>
                        <td align="right"><a href="#">16</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>9,000</td>
                        <td>34</td>
                        <td><div class="bar_graph"><div class="bar_green" style="width: 90px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">200</td>
                        <td align="right"><a href="#">8</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>5,000</td>
                        <td>30</td>
                        <td><div class="bar_graph"><div class="bar_blue" style="width: 82px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">400</td>
                        <td align="right"><a href="#">22</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>4,500</td>
                        <td>25</td>
                        <td><div class="bar_graph"><div class="bar_blue" style="width: 75px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">1,500</td>
                        <td align="right"><a href="#">3</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>2,000</td>
                        <td>15</td>
                        <td><div class="bar_graph"><div class="bar_blue" style="width: 60px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">100</td>
                        <td align="right"><a href="#">4</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>950</td>
                        <td>9</td>
                        <td><div class="bar_graph"><div class="bar_grey" style="width: 35px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">200</td>
                        <td align="right"><a href="#">7</a></td>
                    </tr>
                    <tr>
                    	<td>Subject</td>
                        <td>500</td>
                        <td>2</td>
                        <td><div class="bar_graph"><div class="bar_grey" style="width: 20px;"></div></div></td>
                        <td class="tutor"></td>
                        <td></td>
                        <td align="right">500</td>
                        <td align="right"><a href="#">2</a></td>
                    </tr>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
