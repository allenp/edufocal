<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_status extends Migration {
	
	public function up() 
	{
		$fields = array(
			'status' => array('type' => 'varchar(255)', 'default' => 'Pending')
		);
		$this->dbforge->add_column('questions', $fields);
	}
	
	public function down() 
	{
		$this->dbforge->drop_column('questions', 'status');
	}
	
}