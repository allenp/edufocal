head.ready(function(){
	$('.form li').click(function(){
		
		if ( $(this).hasClass('active') )
		{
			$('.form li').removeClass('active').fadeTo(0.0, 1.0);
			return;
		}
		
		$('.form li').removeClass('active').fadeTo(0.0, 0.1);
		$(this).fadeTo(0.0, 1.0);
		$(this).addClass('active');
	})
});
