<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Leaderboards
 *
 * @package EduFocal
 */
class Rankings extends EF_Authenticated_Controller
{
    public function __construct()
    {
        parent::__construct();

		$this->load->library("pagination");
        Template::set_theme('dashboard');
    }

    public function index($offset = 0)
    {
        $join = "INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1
            INNER JOIN schools on users.school_id = schools.id
            INNER JOIN (
                select user_id, SUM(score) score
                from student_scores
                group by user_id
            ) scores_total
            on scores_total.user_id = student_scores.user_id";

        $count_join = "INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1
            LEFT JOIN schools on users.school_id = schools.id";

        $condition = array(
            "student_scores.track_id = ? AND users.exam_level = ?",
            $this->auth->track_id(), $this->auth->profile()->testing_level()
        );

        $ranks = StudentScore::find('all',
            array(
                'select' => 'users.id user_id, student_scores.score, scores_total.score total,
                             users.first_name, users.last_name,
                             users.avatar, schools.name school_name, schools.id school_id',
                'limit' => 10,
                'offset' => $offset,
                'order' => 'score desc',
                'joins' => $join, 'conditions' => $condition
            )
        );

        $ranks_all = StudentScore::count(
            array('joins' => $count_join, 'conditions' => $condition)
        );

		$config = array(
			'base_url' => site_url('rankings/index/'),
			'total_rows' => $ranks_all,
			'per_page' => 10,
			'uri_segment' => 3,
			'first_link' => false,
			'last_link' => false,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
			'full_tag_open' => '<ul class="list-unstyled pager">',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li>',
			'cur_tag_close' => '</li>'
		);

		$this->pagination->initialize($config);

        $track = Track::find_by_id($this->auth->track_id());

        if ($track == null || $track->active !== 1) {
            $count = Track::count(
                    array(
                        'conditions' => array(
                            '(`exam_type` = ? or `exam_type` = ?) and `active` = 1',
                            $this->auth->profile()->testing_level(), 'ALL'
                            )
                        ));

            if ($count > 0) {
                $link = site_url('rankings/change');
                Template::set_message(
                        "Your current leaderboard has been retired. <a href=\"{$link}\" class=\"alert-link\">Click here to choose from our newer leaderboards for a chance to win prizes.</a>", "error");
            }
        }

		Template::set('total_ranks', $ranks_all);
		Template::set('offset', $offset);
		Template::set('ranks', $ranks);
        Template::set('title', 'Rankings - EduFocal');
        Template::render();
    }

    public function awards($offset = 0)
    {
        $level = $this->auth->profile()->testing_level();
        $track = $this->auth->track_id();
        $sql = <<<EOT
SELECT s.user_id, first_name, last_name, C.score, schools.name school_name, users.avatar, schools.id school_id
FROM
(
  SELECT user_id, SUM((1/curr_rank ) * count) score
  FROM
  (
    SELECT user_id, curr_rank, count(*) count
    FROM cron_rank_overall_logs, cron_ranks
    where cron_ranks.id = cron_rank_overall_logs.cron_rank_id
    and cron_rank_overall_logs.track_id = '$track'
    group by user_id, curr_rank
    order by curr_rank
  ) B
  GROUP BY user_id
) C, schools, users, (select @rownum := 0) r, student_scores s
WHERE C.user_id = users.id AND users.school_id = schools.id
AND s.user_id = users.id
AND users.ranked = 1
AND users.exam_level = '$level'
AND s.track_id = '$track'
ORDER BY C.score DESC
LIMIT $offset, 10
EOT;

        $ranks = StudentScore::find_by_sql($sql);

        $join = "INNER JOIN users on users.id = student_scores.user_id AND users.ranked = 1
                 INNER JOIN schools on users.school_id = schools.id";

        $condition = array(
            "student_scores.track_id = ? and users.exam_level = ?",
            $track, $level
        );

        $ranks_all = StudentScore::count(
            array('joins' => $join, 'conditions' => $condition)
        );

        $config = array(
			'base_url' => site_url('rankings/awards/'),
			'total_rows' => $ranks_all,
			'per_page' => 10,
			'uri_segment' => 3,
			'first_link' => false,
			'last_link' => false,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
			'full_tag_open' => '<ul class="list-unstyled pager">',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li>',
			'cur_tag_close' => '</li>'
		);

		$this->pagination->initialize($config);

		Template::set('total_ranks', $ranks_all);
		Template::set('offset', $offset);
		Template::set('ranks', $ranks);
        Template::set('show_points', FALSE);
        Template::set_view('rankings/index');
        Template::set('title', 'All Time Rankings - EduFocal');
        Template::render();
    }

    public function change()
    {
        $tracks = Track::find('all',
            array(
                'conditions' => array(
                    'exam_type = ? and active = 1 and id <> ?',
                    $this->auth->profile()->testing_level(),
                    $this->auth->track_id()
                ),
                'order' => 'created_at desc'
            )
        );

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('track', 'Choose a leader board', 'required|is_natural|callback_valid_track');
            if($this->form_validation->run() === TRUE) {
                $track_id = $this->input->post('track');
                $student_track = StudentTrack::find_by_user_id($this->auth->user_id());

                if($student_track == NULL || $track_id != $student_track->track_id)
                {
                    if($student_track == NULL)
                        $student_track = StudentTrack::create(
                            array('user_id' => $this->auth->user_id(),
                                  'track_id' => $track_id));
                    else
                    {
                        $student_track->track_id = $track_id;
                        $student_track->save();
                    }

                    foreach($tracks as $track)
                        if($track->id == $track_id)
                            $track_name = $track->title;

                    Template::set_message("Your leader board has been changed to: {$track_name}", "info");
                    redirect(site_url('rankings'));
                }
                else
                {
                    Template::set_message("You're already set to this leader board.");
                }
            }
        }

        $current_track = Track::find_by_id($this->auth->track_id());
        Template::set('tracks', $tracks);
        Template::set('current_track', $current_track);
        Template::set('title', 'Change Your Leaderboard - EduFocal');
        Template::render();
    }

    public function valid_track($id)
    {
        $track = NULL;

        try
        {
            $track = Track::find($id);
        }
        catch(Exception $ex)
        {
            return FALSE;
        }

        if($track == NULL)
            $this->form_validation->set_message('track', 'Invalid competition selected.');
        return ($track !== NULL);
    }
}
