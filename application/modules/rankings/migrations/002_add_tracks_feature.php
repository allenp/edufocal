<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_tracks_feature extends Migration {
	
    static $tables = array(
        'cron_rank_overalls',
        'cron_rank_overall_logs',
        'cron_rank_subjects',
        'cron_rank_subject_logs',
        'student_exp_points',
        'student_exp_test_points',
        'student_scores',
        'student_subject_scores',
    );

	public function up() 
	{
        $prefix = $this->db->dbprefix;
		$fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true,
                          'unsigned' => true),
            'title' => array('type' => 'varchar(140)', 'null' => true),
            'exam_type' => array('type' => 'varchar(10)', 'null' => true),
            'description' => array('type' => 'varchar(256)', 'null' => true),
            'default_track' => array('type' => 'TINYINT', 'default' => '0'),
            'active' => array('type' => 'TINYINT', 'default' => '0'),
            'created_at' => array('type' => 'DATETIME', 'null' => true),
		);

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table("{$prefix}tracks", $fields);

        $fields = array(
            'id' => array('type' => 'int(11)', 'auto_increment' => true,
                          'unsigned' => true),
            'user_id' => array('type' => 'int(11)', 'unsigned' => true),
            'track_id' => array('type' => 'int(11)', 'unsigned' => true),
            'created_at' => array('type' => 'DATETIME', 'null' => true),
            'modified_at' => array('type' => 'DATETIME', 'null' => true),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("{$prefix}student_tracks", $fields);

        unset($fields['modified_at']);
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("{$prefix}student_track_logs", $fields);

        $this->db->query("INSERT INTO {$prefix}tracks(title, description, created_at, default_track, exam_type, active) VALUES(?, ?, CURRENT_TIMESTAMP(), 0, 'ALL',1)", array("2012 to 2013 Academic Year", "Our very first year of EduFocal."));

        $this->db->query("INSERT INTO {$prefix}tracks(title, description, created_at, default_track, exam_type, active) VALUES(?, ?, CURRENT_TIMESTAMP(), 1, 'GSAT',1)", array("2013 to 2014 Academic Year - GSAT", "GSAT Students for 2013 to 2014"));

        $this->db->query("INSERT INTO {$prefix}tracks(title, description, created_at, default_track, exam_type, active) VALUES(?, ?, CURRENT_TIMESTAMP(), 1, 'CXC',1)", array("2013 to 2014 Academic Year - CSEC", "CSEC Students for 2013 to 2014"));

        /*$this->db->query("INSERT INTO {$prefix}student_tracks(user_id, track_id, created_at, modified_at)
            SELECT users.id, tracks.id, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM {$prefix}users,
    {$prefix}schools, {$prefix}tracks
    WHERE {$prefix}users.school_id = {$prefix}schools.id and {$prefix}schools.level = {$prefix}tracks.exam_type
    and {$prefix}tracks.default_track = 1 and {$prefix}tracks.active = 1;");*/
        $this->db->query("INSERT INTO {$prefix}student_tracks(user_id, track_id, created_at, modified_at)
            SELECT users.id, 1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM {$prefix}users;");


        $fields = array(
            'track_id' => array('type' => 'int(11)', 'unsigned' => true),
        );

        foreach(self::$tables as $table)
            $this->dbforge->add_column($prefix . $table, $fields);

        foreach(self::$tables as $table)
            $this->db->query("UPDATE {$prefix}{$table} SET track_id = 1");
	}
	
	public function down() 
	{
        $prefix = $this->db->dbprefix;
        foreach(self::$tables as $table)
            $this->dbforge->drop_column("{$prefix}{$table}", 'track_id');
        $this->dbforge->drop_table("{$prefix}student_tracks");
        $this->dbforge->drop_table("{$prefix}student_track_logs");
        $this->dbforge->drop_table("{$prefix}tracks");
	}
}
