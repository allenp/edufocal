<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_ranked_column extends Migration {
	
	public function up() 
	{
        $prefix = $this->db->dbprefix;
		$fields = array(
			'ranked' => array('type' => 'tinyint', 'null' => true, 'default' => 1)
		);
		$this->dbforge->add_column("{$prefix}users", $fields);

	}
	
	public function down() 
	{
        $prefix = $this->db->dbprefix;
		$this->dbforge->drop_column("{$prefix}users", 'ranked');
	}
	
}
