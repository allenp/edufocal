<?php echo Template::message(); ?>
<table class="full-rankings table table-bordered table-bordered-only">
    <thead>
    <tr><th>Rank</th><th colspan="2">Student</th><?php if(!isset($show_points) || $show_points === TRUE) :?><th>Experience</th> <?php endif; ?></tr>
    </thead>
    <tbody>
    <?php $counter = $offset;
    foreach($ranks as $rank) :
        $counter++; ?>
    <tr>
        <td align="center"><span class="rank">#<?php echo $counter; ?></span></td>
        <td>
<?php $avatar = $rank->avatar; if(empty($avatar)) : ?>
            <img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $rank->first_name.' '.$rank->last_name; ?>" width="40" class="img" />
<?php else : ?>
            <img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $rank->first_name.' '.$rank->last_name; ?>" width="40" class="img" />
<?php endif; ?></td>
        <td><strong><a href="<?php echo site_url('dashboard/profile/view/'.$rank->user_id); ?>"><?php echo $rank->first_name.' '.$rank->last_name; ?></a></strong><br /> <?php if($rank->school_name && $rank->school_id != 1) echo $rank->school_name; ?>
        </td>
<?php if(!isset($show_points) || $show_points === TRUE) :?>
        <td><?php echo floor($rank->score); ?> Points <?php if(abs($rank->score - $rank->total) > 1):?>
        (<strong><?php print floor($rank->total); ?></strong> of all time)<?php endif; ?>
        </td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
    <tfoot>
    <tr>
        <td colspan="2">Showing <?php echo $total_ranks ? $offset + 1 : 0; ?> - <?php echo min($total_ranks, $offset + 10); ?> of <?php echo $total_ranks; ?> match<?php echo $total_ranks != 1 ? "es": ""; ?></td>
        <td colspan="2" align="right">
            <?php echo $this->pagination->create_links(); ?>
        </td>
    </tr>
    </tfoot>
    </tbody>
</table>
