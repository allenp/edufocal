<?php print Template::message(); ?>
<?php if ( validation_errors()) : ?>
    <div>
        <div class="alert warning">
            <h4>Please check the following before continuing:</h4>
            <br clear="all" />
            <?php echo validation_errors(); ?>
        </div>
    </div>
<?php endif; ?>

<h3 class="flush-top">Change Leaderboard</h3>
<?php if($current_track == null || $current_track->active !== 1): ?>
<p>Your current leaderboard <strong><?php print $current_track != null ? $current_track->title : ''; ?></strong> has been retired. You can choose from one of our competitions below. Your score will be reset to zero if this is your first time switching over.</p>
<?php endif; ?>
<?php print form_open(); ?>
<table class="table table-bordered">
<thead>
    <tr>
        <th></th>
        <th>Competition</th>
    </tr>
</thead>
<tbody>
    <?php foreach($tracks as $track) : ?>
    <tr>
        <td><input type="radio" name="track" value="<?php print $track->id; ?>" id="<?php print "track{$track->id}"; ?>" /></td>
        <td><h3><label for="<?php print "track{$track->id}" ?>"><?php print $track->title; ?></label></h3></td>
    </tr> 
    <?php endforeach; ?>
</tbody>
</table>
<?php print form_submit('submit', 'Change Leaderboard'); ?>
<?php print form_close(); ?>
