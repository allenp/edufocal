<nav class="category">
    <ul class="nav navbar-nav">
        <li class="<?php if($this->uri->segment(2) === FALSE || $this->uri->segment(2) == 'index') echo 'active'; ?>"><a href="<?php echo site_url('rankings'); ?>">Current Top Rankings</a></li>
        <li class="<?php if($this->uri->segment(2) == 'awards') echo 'active'; ?>"><a href="<?php echo site_url('rankings/awards'); ?>">EduFocal Awards</a></li>
    </ul>
</nav>

