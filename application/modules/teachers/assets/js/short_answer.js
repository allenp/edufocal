$(function() {
			var counter = $("#duration").val();
			var opened = false;
			
			$("#multipart").click(function() {
				$("input[name=style]").val('multi');
				$(".parts").removeClass('hidden');
				$(".accepted").hide();
			});
			
			$("#add").click(function() {
				counter++;
				$(".choices").append('<div class="float_left"><label>'+String.fromCharCode(96 + counter).toUpperCase()+'. Question:</label><textarea name="choices['+String.fromCharCode(96 + counter)+']" id="editor_'+String.fromCharCode(96 + counter)+'" ></textarea></div><div class="float_right"><label>Answer:</label><textarea name="answers['+String.fromCharCode(96 + counter)+']" id="answer_'+String.fromCharCode(96 + counter)+'" ></textarea></div><div class="clearfix"></div>');
				
				tinyMCE.execCommand('mceAddControl', false, 'editor_'+String.fromCharCode(96 + counter));
				tinyMCE.execCommand('mceAddControl', false, 'answer_'+String.fromCharCode(96 + counter));
				
				return false;
			});
			
			$("#difficulty").slider({
				value: 5,
				min: 1,
				max: 9,
				step: 1,
				slide: function(event, ui) {
					$("#level").val(ui.value);
				}
			});
			
			$(".hint").click(function() {
				$("#hint").slideToggle();
				if(opened == false) {
					$(this).animate({ height: '48px' });
					opened = true;
				} else {
					$(this).animate({ height: '31px' });
					opened = false;
				}
			});
});
