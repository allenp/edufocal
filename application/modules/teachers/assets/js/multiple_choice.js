$(function(){
    var counter = $("#duration").val();

    $("#add").click(function() {
        counter++;
        $(".options ol").append('<li>' +
            '<div class="option">' +
                '<span class="correct"><label><input type="radio" name="correct" value="'+counter+'"> Correct Answer</label></span>' +
                '<textarea name="choices['+counter+']" class="editor" id="editor_'+counter+'"></textarea>' +
            '</div>' +
            '<div class="clearfix"></div>' +
        '</li>');
        
        tinyMCE.execCommand('mceAddControl', false, 'editor_'+counter);
        return false;
    });

    $("#difficulty").slider({
        value: 5,
        min: 1,
        max: 9,
        step: 1,
        slide: function(event, ui) {
            $("#level").val(ui.value);
        }
    });

    /*
    var hint_opened = false;
    $(".hint").click(function() {
        $("#hint").slideToggle();
        if(hint_opened == false) {
            $(this).animate({ height: '48px' });
            hint_opened = true;
        } else {
            $(this).animate({ height: '31px' });
            hint_opened = false;
        }
    });
    */

    $(document).on('click', '.hint', function(e) {
        var current = $(this).parent().find(".hint_box");
        var hidden = current.attr("data-hidden") || "true";
        
        current.slideToggle();
        
        if(hidden == "true") {
            $(this).animate({ height: '48px' });
            current.attr("data-hidden", "false");
        } else {
            $(this).animate({ height: '31px' });
            current.attr("data-hidden", "true");
        }
    });

    $(document).on('click', '.explain', function(e) {

        var current = $(this).parent().find(".explanation");
        var hidden = current.css("display") == 'none';

        current.slideToggle();

        if(hidden) {
            $(this).animate({ height: '48px' });
        } else {
            $(this).animate({ height: '31px' });
        }
    });

    $(".explain").each(function(index, element) {
        var current = $(this).parent().find(".explanation");
        var hidden = current.css("display") == 'none';

        if(hidden) {
            $(this).animate({ height: '31px' });
        } else {
            $(this).animate({ height: '48px' });
        }
    });

});
