$(function() {
        var counter = $("#duration").val();
        
        /*
        $("#multipart").click(function() {
            $("input[name=style]").val('multi');
            $(".parts").removeClass('hidden');
            $(".accepted").hide();
        });
        */
        
        $("#addquestion").click(function(e) {
            e.preventDefault();
            counter++;
            $(".choices").append('<div class="left">' +
                    '<label>Question '+counter+':</label>' +
                    '<textarea name="question['+counter+']" class="editor" id="question_'+counter+'"></textarea>' +
                '</div>' +
                '<div class="right">' +
                    '<div class="hint"><span class="help"><div>Hints are provided to students during practice tests to help them learn the material.</div></span></div>' +
                    '<textarea name="hint['+counter+']" class="hint_box hidden"></textarea>' +
                '</div>' +
                '<div class="clearfix"></div>' +
                '<div class="options" title="choices_'+counter+'">' +
                    '<label>Answer Options: <span class="help"><div>The more options the harder the question</div></span></label>' +
                    '<div class="clearfix"></div>' +
                    '<ol>' +
                        '<li>' +
                            '<div class="option">' +
                                '<span class="correct"><label><input type="radio" name="correct['+counter+']" value="1"> Correct Answer</label></span>' +
                                '<textarea name="choices['+counter+'][1]" class="editor" id="choice_'+counter+'_1"></textarea>' +
                            '</div>' +
                            '<div class="clearfix"></div>' +
                        '</li>' +
                        '<li>' +
                            '<div class="option">' +
                                '<span class="correct"><label><input type="radio" name="correct['+counter+']" value="2"> Correct Answer</label></span>' +
                                '<textarea name="choices['+counter+'][2]" class="editor" id="choice_'+counter+'_2"></textarea>' +
                            '</div>' +
                            '<div class="clearfix"></div>' +
                        '</li>' +
                    '</ol>' +
                    '<p><a href="#" class="another" title="choices_'+counter+'">Add More Options</a></p>' +
                '</div>' +
                '<div class="clearfix"></div>');
            
            tinyMCE.execCommand('mceAddControl', false, 'question_'+counter);
            tinyMCE.execCommand('mceAddControl', false, 'choice_'+counter+'_1');
            tinyMCE.execCommand('mceAddControl', false, 'choice_'+counter+'_2');
            
            if(counter >= 12) $(this).hide();
            
            return false;
        });

        $(document).on('click', '.another', function(e) {
            e.preventDefault();
            var title = $(this).prop('title');
            var options = $('.options[title='+title+']').find('ol');
            var count = options.find('li').length + 1;
            var parent_count = title.split("_");
            
            options.append('<li>' +
                '<div class="option">' +
                    '<span class="correct"><label><input type="radio" name="correct['+parent_count[1]+']" value="'+count+'"> Correct Answer</label></span>' +
                    '<textarea name="choices['+parent_count[1]+']['+count+']" class="editor" id="choice_'+parent_count[1]+'_'+count+'"></textarea>' +
                '</div>' +
                '<div class="clearfix"></div>' +
            '</li>');
            
            tinyMCE.execCommand('mceAddControl', false, 'choice_'+parent_count[1]+'_'+count);
            return false;
        });
        
        $("#difficulty").slider({
            value: 5,
            min: 1,
            max: 9,
            step: 1,
            slide: function(event, ui) {
                $("#level").val(ui.value);
            }
        });
        
        $(document).on('click', '.hint', function(e) {
            var current = $(this).parent().find(".hint_box");
            var hidden = current.attr("data-hidden") || "true";
            
            current.slideToggle();
            
            if(hidden == "true") {
                $(this).animate({ height: '48px' });
                current.attr("data-hidden", "false");
            } else {
                $(this).animate({ height: '31px' });
                current.attr("data-hidden", "true");
            }
        });

        $(document).on('click', '.explain', function() {

            var current = $(this).parent().find(".explanation");
            var hidden = current.css("display") == 'none';

            current.slideToggle();

            if(hidden) {
                $(this).animate({ height: '48px' });
            } else {
                $(this).animate({ height: '31px' });
            }
        });

        $(".explain").each(function(index, element) {
            var current = $(this).parent().find(".explanation");
            var hidden = current.css("display") == 'none';

            if(hidden) {
                $(this).animate({ height: '31px' });
            } else {
                $(this).animate({ height: '48px' });
            }
        });
});
