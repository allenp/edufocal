<?php

/**
 * Question Queue
 *
 * @package EduFocal
 * @author Shane Shipston
 */
class Question_Queue extends EF_Teacher_Controller
{

    private $teacher = null;
    private $editor = null;

	public function __construct ()
	{
		parent::__construct();
		$subjects = Subject::all();
		Template::set('subjects', $subjects);

        $this->teacher = $this->auth->role_id() == EDITOR_ROLE ? $this->session->userdata('teacher') : $this->auth->user_id();
        $this->editor = $this->auth->user_id();
	}
	
	/**
	 * Question Status
	 */
	public function index ($offset = 0)
	{
		$this->load->helper('text');
		
        if($this->editor != $this->teacher)
            $count = Question::count_by_editor_id_and_question_id($this->editor, 0);
        else
            $count = Question::count_by_user_id_and_question_id($this->teacher, 0);

		$this->paging($count, site_url('teachers/question_queue/index/'), 4);

        if($this->editor != $this->teacher)
            $condition = " questions.editor_id = ? ";
        else
            $condition = " questions.user_id = ? ";
		
		$questions = Question::find_by_sql("SELECT `questions`.*, `topics`.`name` AS `topic_name`, `subjects`.`name` AS `subject_name`, `subjects`.`level` AS `subject_level` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` LEFT JOIN `subjects` ON `subjects`.`id` = `topics`.`subject_id` WHERE " . $condition . " AND `questions`.`question_id` = 0 ORDER BY `id` DESC LIMIT ".$offset.", 10", array(($this->editor != $this->teacher ? $this->editor : $this->teacher)));
		
		Template::set('offset', $offset);
		Template::set('count', $count);
		Template::set('questions', $questions);
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}
	
	/**
	 * Question Status
	 */
	public function status ($status, $offset = 0)
	{
		$this->load->helper('text');
		
        if($this->editor != $this->teacher)
            $count = Question::count_by_editor_id_and_status_and_question_id($this->editor, ucfirst($status), 0);
        else
            $count = Question::count_by_user_id_and_status_and_question_id($this->teacher, ucfirst($status), 0);

        if($this->editor != $this->teacher)
            $condition = " questions.editor_id = ? ";
        else
            $condition = " questions.user_id = ? ";
	
		$this->paging($count, site_url('teachers/question_queue/status/'.$status.'/'), 5);
		
		$questions = Question::find_by_sql("SELECT `questions`.*, `topics`.`name` AS `topic_name`, `subjects`.`name` AS `subject_name`, `subjects`.`level` AS `subject_level` FROM `questions` LEFT JOIN `topics` ON `questions`.`topic_id` = `topics`.`id` LEFT JOIN `subjects` ON `subjects`.`id` = `topics`.`subject_id` WHERE " . $condition . " AND `questions`.`status` = ? AND `questions`.`question_id` = 0 ORDER BY `id` DESC LIMIT ".$offset.", 10", array(($this->editor != $this->teacher ? $this->editor : $this->teacher), ucwords($status)));
		
		Template::set_view('question_queue/index');
		Template::set('offset', $offset);
		Template::set('count', $count);
		Template::set('questions', $questions);
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}
	
	/**
	 * Easier Pagination Initialize
	 */
	private function paging($count, $url, $segment) {
		$this->load->library("pagination");
		
		$config = array(
			'base_url' => $url,
			'total_rows' => $count,
			'per_page' => 10,
			'uri_segment' => $segment,
			'first_link' => false,
			'last_link' => false,
			'next_link' => 'Next',
			'prev_link' => 'Previous',
			'full_tag_open' => '<ul>',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'cur_tag_open' => '<li>',
			'cur_tag_close' => '</li>'
		);
		
		$this->pagination->initialize($config);
	}

	/**
	 * Question Review
	 */
	public function review($id) {
		$data = Question::find_all_by_id_or_question_id_and_user_id($id, $id, $this->auth->profile()->id
		, array('order' => 'id'));
		
		if(count($data) > 0)
		{
			$subject = Subject::find($data[0]->topic->subject_id);
			$next = Question::find(array('conditions' => array('id > ? AND user_id = ? AND question_id = 0', $id, $this->auth->profile()->id), 'order' => 'id asc'));
			$previous = Question::find(array('conditions' => array('id < ? AND user_id = ? AND question_id = 0', $id, $this->auth->profile()->id), 'order' => 'id desc'));
			$history = QuestionStatus::find('all', array('conditions' => array('question_id = ?', $id), 'order' => 'id desc'));
			
			if(count($next) > 0) Template::set('next', $next->id);
			if(count($previous) > 0) Template::set('previous', $previous->id);
			if(count($history) > 0) Template::set('history', $history);
			
			Template::set('topic_name', $data[0]->topic->name);
			Template::set('question_status', $data[0]->status);
			Template::set('subject', $subject);
			Template::set('questions', $data);
		}
		else
		{
			Template::set_view('question_queue/invalid');
		}
		
		Template::set('title', 'Question Queue - EduFocal');
		Template::render();
	}

    public function summary()
    {

        $cond = array('user_id = ?', $this->auth->profile()->id);

        $data = Question::find('all', array(
            'conditions' => $cond,
            'order' => 'id desc'
        ));

        Template::set('title', 'Question Summary - EduFocal');
        Template::render();
    }
}
