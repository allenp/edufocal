<?php

class Dashboard extends EF_Teacher_Controller
{
	public function __construct ()
	{
		parent::__construct();
	}

	public function index ()
	{
        if(isset($_POST['submit']))
        {
            $this->session->set_userdata('teacher', $this->input->post('teacher'));
            Template::set_message('Teacher updated successfully!', 'info');
        }

		$this->load->helper('text');
        $unread = MessageUser::count(
            array('conditions' => array(
                'user_id = ? and `read` = ? and `deleted` = ?',
                $this->auth->user_id(),'no', 'no')
            )
        );

		$mUser = MessageUser::find_all_by_user_id_and_deleted($this->auth->user_id(), 'no');
		$message_ids = array();
		$status = array();

		foreach($mUser as $m)
		{
			$status[$m->message_id] = $m->read;
			$message_ids[] = $m->message_id;
		}

		$messages = array();

		if(count($message_ids ) > 0)
            $messages = Message::find_all_by_id(
                $message_ids,
                array('limit' => 5, 'order' => 'last_stamp desc')
            );

		$last_login = new DateTime($this->auth->profile()->last_login);

        if($this->auth->role_id() == EDITOR_ROLE)
        {
            $query = Teacher::find_by_sql(
                "SELECT t.user_id, t.first_name, t.last_name FROM teachers t, teachers_topics top
                 WHERE t.user_id = top.teacher_id
                 GROUP BY t.user_id, t.first_name, t.last_name");

            $teachers = array('0' => '  ');
            foreach($query as $item)
                $teachers[$item->user_id] = $item->first_name . ' ' . $item->last_name;

            Template::set('teachers', $teachers);
            Template::set('teacher', $this->session->userdata('teacher'));
        }

        Template::set('status', $status);
		Template::set('unread', $unread);
		Template::set('messages', $messages);

		Template::set('last_login', $last_login->format("d / m / Y"));

		Template::set_view('teachers/index');
		Template::set('title', 'Dashboard - EduFocal');
		Template::render();
	}
}


