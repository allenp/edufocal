<?php

class Videos extends EF_Teacher_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$ts = Topic::all(['conditions' => ['subject_id = 1']]);
		$topics = [];

		$videos = Video::all(['order' => 'sort_order asc']);

		foreach ($ts as $t) {
			$topics[$t->id] = $t->name;
		}

		Template::set('videos', $videos);
		Template::set('topics', $topics);
		Template::set('title', 'Add new video');
		Template::render();
	}

	public function create()
	{
		if ($this->input->post('submit')) {

			$video = Video::create([
				'title' => $this->input->post('title'),
				'description' => $this->input->post('description'),
				'topic_id' => $this->input->post('topic_id'),
				'duration' => 0,
				'sort_order' => $this->input->post('sort_order'),
				'free' => $this->input->post('free') ? 1 : 0,
			]);

			$video_source = VideoSource::create([
				'video_id' => $video->id,
				'format' => 'mp4',
				'w' => 640,
				'h' => 360,
				'path' => $this->input->post('path'),
				]);
			Template::set_message('Video added!', 'info');
			redirect(site_url(['teachers', 'videos']));
		}

		$ts = Topic::all(['conditions' => ['subject_id = 1']]);
		$topics = [];

		foreach ($ts as $t) {
			$topics[$t->id] = $t->name;
		}

		$this->load->view('videos/create', compact('topics'));
	}

	public function edit($id)
	{
		$video = Video::find($id);

		if ($this->input->post('submit')) {
			$video->title = $this->input->post('title');
			$video->description = $this->input->post('description');
			$video->topic_id = $this->input->post('topic_id');
			$video->sort_order = $this->input->post('sort_order');
			$video->free = $this->input->post('free') ? 1 : 0;
			$video->save();
			Template::set_message('Video updated!', 'info');
			redirect(site_url(['teachers', 'videos']));
		}

		$ts = Topic::all(['conditions' => ['subject_id = 1']]);
		$topics = [];

		foreach ($ts as $t) {
			$topics[$t->id] = $t->name;
		}

		$this->load->view('videos/edit', compact('topics', 'video'));
	}

	public function view($id)
	{
		$video = Video::find($id);

		$videos = Video::all(
			['conditions' => ['topic_id = ?', $video->topic_id],
			 'order' => 'sort_order asc',
			]);

		$v = [];
		foreach ($videos as $vid) {
			$v[$vid->permalink] = $vid;
		}

		Template::set('selected', $video->permalink);
		Template::set('videos', $v);
		Template::set('title', 'Viewing ' . $video->title);
		Template::set('video', $video);
		Template::render();
	}

	public function get_delete($id)
	{
		$video = Video::find($id);
		$data['title'] = $video->title;
		$data['id'] = $video->id;
		$data['origin'] = 'video';
		$this->load->view('videos/delete', $data);
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$origin = $this->input->post('origin');
		if ($origin == 'video') {
			$video = Video::find($id);
			$video->delete();
		} else {
			$source = VideoSource::find($id);
			$source->delete();
		}
		redirect(site_url(['teachers', 'videos']));
	}

	public function get_delete_source($id)
	{
		$source = VideoSource::find($id);
		$data['title'] = $source->path;
		$data['id'] = $source->id;
		$data['origin'] = 'source';
		$this->load->view('videos/delete', $data);
	}

	public function add_source($id)
	{
		if ($this->input->post('submit')) {
			VideoSource::create([
				'path' => $this->input->post('path'),
				'video_id' => $this->input->post('id'),
				'w' => 0,
				'h' => 0,
				'format' => 'mp4 or webm'
				]);
			redirect(site_url(['teachers', 'videos']));
		}

		$video = Video::find($id);
		$this->load->view('videos/create_source', ['video' => $video]);
	}
}
