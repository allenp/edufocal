<?php

class Profile extends EF_Teacher_Controller
{
	public function __construct ()
	{
		parent::__construct();
        if($this->auth->role_id() == EDITOR_ROLE)
            redirect('account');
	}
	
	/**
	 * Overall Profile Details
	 */
	public function index ()
	{
        $teacher = Teacher::find_by_user_id($this->auth->user_id());
		
		Template::set('teacher', $teacher);
		Template::set('title', 'My Profile - EduFocal');
		Template::set_view('profile/view');
		Template::render();
	}
	
	public function edit ()
	{
		$teacher = Teacher::find_by_user_id($this->auth->user_id());
		
		if(isset($_POST['save'])) {
			$teacher->first_name = $this->input->post('first_name');
			$teacher->last_name = $this->input->post('last_name');
			$teacher->title = $this->input->post('title');
			$teacher->short_bio = $this->input->post('short_bio');
			$teacher->biography = $this->input->post('biography');
			$teacher->save();
		}

		$fields = $this->auth->rules;

        //remove these fields becase they cant be set automated (see view) 
        unset($fields['birth_day']);
        unset($fields['birth_month']);
        unset($fields['birth_year']);
        unset($fields['sex']);

		Template::set('teacher', $teacher);
		Template::set('fields', $fields);
		Template::set('title', 'Edit Profile - EduFocal');
		Template::render();
	}
}

