<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        Assets::add_js($this->load->view('content/js', null, true), 'inline');
        Template::set_block('sub_nav', 'content/_sub_nav');
    }

    /**
     * function index
     *
     * list form data
     */
    public function index()
    {
        $teachers = Teacher::find('all', array('order' => 'last_name'));

        Template::set_view("content/index");
        Template::set("teachers", $teachers);
        Template::set("toolbar_title", "Manage Teachers");
        Template::render();
    }

    //--------------------------------------------------------------------

    public function create()
    {
        if ($this->input->post('submit')) {
            if ($this->save_teacher()) {
                Template::set_message('Teacher Added Successfully', 'success');
                Template::redirect(SITE_AREA .'/content/teachers');
            } else {
                Template::set_message('An Error Occurred', 'error');
            }
        }

        $subjects = Subject::find('all');

        $topics = array(); // Teacher ID is actually User ID

        $t = array();
        foreach($topics as $topic) {
            $t[] = $topic->topic_id;
        }

        Template::set('toolbar_title', 'New Teacher');
        Template::set('subjects', $subjects);
        Template::set('topics', $t);
        Template::set_view('content/edit');
        Template::render();
    }
    //--------------------------------------------------------------------


    public function edit()
    {
        $id = (int)$this->uri->segment(5);

        if (empty($id)) {
            redirect(SITE_AREA .'/content/teachers');
        }

        if ($this->input->post('submit')) {
            if ($this->save_teacher('update', $id)) {
                Template::set_message('Teacher Updated Successfully', 'success');
            } else {
                Template::set_message('An Error Occurred', 'error');
            }
        }

        $subjects = Subject::find('all');

        $teacher = Teacher::find($id);

        $user = User::find($teacher->user_id);

        $topics = TeachersTopic::find_all_by_teacher_id($teacher->user_id); // Teacher ID is actually User ID

        $t = array();
        foreach($topics as $topic) {
            $t[] = $topic->topic_id;
        }

        Template::set('subjects', $subjects);
        Template::set('teacher', $teacher);
        Template::set('user', $user);
        Template::set('topics', $t);
        Template::set_view('content/edit');
        Template::set("toolbar_title", "Edit Teacher");
        Template::render();
    }

    public function delete()
    {
        $id = $this->uri->segment(5);

        if (!empty($id))
        {
            if ($this->drop($id))
            {
                Template::set_message('Teacher Deleted Successfully', 'success');
            } else
            {
                Template::set_message('An Error Occurred', 'error');
            }
        }

        redirect(SITE_AREA .'/content/teachers');
    }

    private function drop($id)
    {
        // Delete Teacher
        $teacher = Teacher::find($id);
        $teacher->delete();

        // Delete Login
        $user = User::find($teacher->user_id);
        $user->delete();

        // Delete Topic Assignment
        TeachersTopic::table()->delete(array('teacher_id' => $teacher->user_id));

        return true;
    }

    public function save_teacher($type='insert', $id=0)
    {
        if(!function_exists('url_title')) {
            $this->load->helper('url');
        }

        $this->form_validation->set_rules('first_name','First Name','required');
        $this->form_validation->set_rules('last_name','Last Name','required');
        $this->form_validation->set_rules('email','Email','required');
        if ($type == 'insert') {
            $this->form_validation->set_rules('password','Password','required');
        }

        if ($this->form_validation->run() === false) {
            return false;
        }

        $additional = array(
            'gender' => $this->input->post('sex'),
            'phone' => $this->input->post('phone')
        );

        $new = array(
            'email'		=> $this->input->post('email'),
            'username'	=> isset($_POST['username']) ? $_POST['username'] : '',
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'profile' => json_encode($additional),
        );

        $attributes = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'title' => $this->input->post('title'),
            'credentials' => $this->input->post('credentials'),
            'short_bio' => $this->input->post('short_bio'),
            'biography' => $this->input->post('biography'),
            'level' => $this->input->post('level'),
            'permalink' => url_title($this->input->post('first_name').' '.$this->input->post('last_name'), 'underscore', TRUE)
        );

        try {

            if ($type == 'insert') {
                list($password, $salt) = $this->hash_password($this->input->post('password'));
                $this->load->helper('url');
                $new['referral_code'] = url_title(strtolower(substr($attributes['first_name'], 0, 1) . $attributes['last_name']));/*$this->get_unique_referral_code();*/
                $new['role_id'] = '7';
                $new['code_id'] = '-1';
                $new['password_hash'] = $password;
                $new['salt'] = $salt;

                $user_id = User::create($new);

                if (is_numeric($user_id->id)) {
                    $attributes['user_id'] = $user_id->id;

                    Teacher::create($attributes);

                    if(!empty($_POST['topics'])) {
                        foreach($this->input->post('topics') as $entry) {
                            list($subject, $topic) = explode("_", $entry);

                            TeachersTopic::create(array('teacher_id' => $user_id->id, 'subject_id' => $subject, 'topic_id' => $topic));
                        }
                    }

                    $return = true;
                } else {
                    $return = false;
                }
            } else if ($type == 'update') {
                $teacher = Teacher::find($id);
                $teacher->update_attributes($attributes);

                if(isset($_POST['password']) && !empty($_POST['password'])) {
                    list($password, $salt) = $this->hash_password($this->input->post('password'));

                    $new['password_hash'] = $password;
                    $new['salt'] = $salt;
                }

                $user = User::find($teacher->user_id);
                $user->update_attributes($new);

                $topics = TeachersTopic::find_all_by_teacher_id($teacher->user_id);

                $t = array();
                foreach($topics as $topic) {
                    $t[$topic->topic_id] = $topic->active;
                }

                TeachersTopic::table()->delete(array('teacher_id' => $teacher->user_id));

                foreach($this->input->post('topics') as $entry) {
                    list($subject, $topic) = explode("_", $entry);

                    if(array_key_exists($topic, $t))
                        TeachersTopic::create(array('teacher_id' => $teacher->user_id, 'subject_id' => $subject, 'topic_id' => $topic, 'active' => $t[$topic]));
                    else
                        TeachersTopic::create(array('teacher_id' => $teacher->user_id, 'subject_id' => $subject, 'topic_id' => $topic));
                }

                $return = true;
            }
        } catch( Exception $e) {
            $this->form_validation->set_errors(array('first_name', $e->getMessage()));
        }

        return $return;
    }

    private function get_unique_referral_code ()
    {
        $code = random_string('alnum', 10);

        if ( User::exists(array('referral_code', $code)) ) {
            return $this->get_unique_referral_code();
        }

        return $code;
    }

    private function generate_salt()
    {
        if (!function_exists('random_string')) {
            $this->load->helper('string');
        }

        return random_string('alnum', 7);
    }

    public function hash_password($old='')
    {
        if (!function_exists('do_hash')) {
            $this->load->helper('security');
        }

        $salt = $this->generate_salt();
        $pass = do_hash($salt . $old);

        return array($pass, $salt);
    }
}
