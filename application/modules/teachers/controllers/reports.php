<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Admin_Controller {
               
	function __construct()
	{
 		parent::__construct();
        Template::set_block('sub_nav', 'reports/_sub_nav');
	}

    /*
     * Active Teachers 
     */
	public function index()
	{
		Assets::add_js($this->load->view('reports/js', null, true), 'inline');

		$teachers = TeachersTopic::find_by_sql("SELECT * FROM `teachers_topics` AS `link` INNER JOIN `teachers` ON `teachers`.`user_id` = `link`.`teacher_id` WHERE `link`.`active` = 1 GROUP BY `link`.`teacher_id`");
		
		Template::set_view("reports/index");
		Template::set("teachers", $teachers);
		Template::set("toolbar_title", "Active Teachers");
		Template::render();
	}

    /*
     * View a teacher report
     */
	public function view($user_id) 
	{
		Assets::add_js($this->load->view('reports/js', null, true), 'inline');

        $topics = Question::find_by_sql("SELECT tops.id, tops.name, COUNT(questions.id) as total
                                            FROM 
                                            (SELECT `topics`.`id`, `topics`.`name`
                                            from teachers_topics
                                            INNER JOIN topics on topics.id = teachers_topics.topic_id
                                            AND teachers_topics.teacher_id= ? ) tops
                                            LEFT OUTER JOIN questions
                                            ON questions.user_id = ? 
                                            AND questions.topic_id = tops.id AND questions.Status = 'Accepted'
                                            AND questions.question_id = 0
                                            GROUP BY tops.id
                                            ORDER BY COUNT(questions.id) DESC", array($user_id, $user_id));
		
		$totals = array();
		

        $temp = Question::find_by_sql("SELECT tops.id, tops.name, COUNT(questions.id) as total
                                        FROM 
                                        (SELECT `topics`.`id`, `topics`.`name`
                                        from teachers_topics
                                        INNER JOIN topics on topics.id = teachers_topics.topic_id
                                        AND teachers_topics.teacher_id= ?) tops
                                        LEFT OUTER JOIN questions
                                        ON questions.user_id = ?
                                        AND questions.topic_id = tops.id
                                        AND questions.question_id = 0
                                        GROUP BY tops.id", array($user_id, $user_id));
		foreach($temp as $t) {
			$totals[$t->id] = $t->total;
		}
		
		Template::set('toolbar_title', 'View Teacher');
		Template::set('topics', $topics);
		Template::set('totals', $totals);
		Template::set_view('reports/view');
		Template::render();
	}

    /*
     * Payouts list view
     */
    public function payouts()
    {
		Assets::add_js($this->load->view('reports/payoutjs', null, true), 'inline');

        $periods = PayoutPeriod::all(array('order' => 'startdate desc'));
        Template::set("periods", $periods);
        Template::set("toolbar_title", "Teacher Payouts");
        Template::render();
    }

    public function payout($id)
    {
        $payout = PayoutPeriod::find($id);
        $breakdown = TeacherPayout::find_by_sql("SELECT first_name, last_name, name as subject, t.teacher_id, payout
                                                 FROM teacher_payouts t
                                                 INNER JOIN teachers on t.teacher_id = teachers.id
                                                 INNER JOIN subjects on t.subject_id = subjects.id
                                                 WHERE t.period_id = ?
                                                 ORDER BY t.teacher_id, payout desc", array($payout->id));
        Template::set('payout', $payout);
        Template::set('breakdowns', $breakdown);
		Assets::add_js($this->load->view('reports/payoutjs', null, true), 'inline');
        Template::render();
    }
    
}
