<?php
/**
 * Content Creation
 */
class Coursework extends EF_Teacher_Controller
{
	private $rules = array();

    private $teacher = null;
    private $selected_topic = null;

	public function __construct ()
	{
		parent::__construct();

        $this->teacher = $this->auth->role_id() == EDITOR_ROLE ? $this->session->userdata('teacher') : $this->auth->user_id();
        $this->selected_topic = $this->session->userdata('topic_id');
	}

	public function index ()
	{
		Template::set('body_id', 'referral');
		Template::set('title', 'Coursework - EduFocal');
		Template::render();
	}

	/**
	 * Create a Multiple Choice Question
	 */
	public function multiple ($op='insert', $id=0)
    {
        if((!is_numeric($this->teacher) || $this->teacher <= 0) && $op != 'edit')
            Template::set_message('No teacher selected. please choose a teacher on your dashboard', 'error');

		$this->load->helper('form');
		if(isset($_POST['submit']) || isset($_POST['another']))
        {
            if($op == 'insert')
            {
                $this->save_multiple();
            }
            else if($op == 'edit')
            {
                $id = $this->input->post('id');
                if($id !== 0)
                {
                    if($this->save_multiple($id) == TRUE)
                        redirect('teachers/coursework/multiple/edit/' . $id);
                }
            }
		}

        if($op == 'insert')
        {
            Template::set('title', 'New Multiple Choice - EduFocal');
            Template::set_view('coursework/multiple');
        }
        else if ($op == 'edit')
        {
            $qObs = Question::find('all', array('conditions' => array('id = ? OR question_id = ?', $id, $id),
                                                    'order' => 'id asc'));
            if($qObs == null) return;

            $questions = array();
            $correct = array();
            $hint = array();
            $topic_id = $qObs[0]->topic_id;

            foreach($qObs as $qkey => $que)
            {
                $questions[] = $que->question;
                $correct[] = $que->accepted_answers;
                $hint[] = $que->hint;
                $choices[$qkey] = (array)json_decode($que->choices);
            }

            $this->teacher = $qObs[0]->user_id;

            Template::set('question_id', $qObs[0]->id);
            Template::set('choices', $choices);
            Template::set('questions', $questions);
            Template::set('instruction', $qObs[0]->instruction);
            Template::set('correct', $correct);
            Template::set('hint', $hint);
            Template::set('topic_id', $qObs[0]->topic_id);
            Template::set('title', 'Edit Multiple Choice - EduFocal');
            Template::set_view('coursework/edit_multiple');
        }

        $teacher = Teacher::find_by_user_id($this->teacher);

        if($teacher != null)
        {
            $topics = TeachersTopic::find_all_by_teacher_id($teacher->user_id, array('order' => 'subject_id desc'));
            $topic_dropdown = array();
            foreach ( $topics as $topic )
                $topic_dropdown[$topic->subject->name][$topic->topic->id] = $topic->topic->name . ' - ' . $topic->subject->level;

            Template::set('permalink', $teacher->permalink);
            Template::set('teacher', $teacher);
            Template::set('topics', $topic_dropdown);
            if($this->selected_topic != null)
                Template::set('topic_id', $this->selected_topic);
        }

		Template::render();
	}

	/**
	 * Create a Short Answer Question
	 */
	public function short_answer ()
    {
        if(!is_numeric($this->teacher) || $this->teacher <= 0)
            Template::set_message('No teacher selected. please choose a teacher on your dashboard', 'error');

		$this->load->helper('form');

		if(isset($_POST['submit']) || isset($_POST['another'])) {
			$this->save_short_answer();
		}

		$topics = TeachersTopic::find_all_by_teacher_id($this->teacher, array('order' => 'subject_id desc'));
		$topic_dropdown = array();
		foreach ( $topics as $topic )
		{
			$topic_dropdown[$topic->subject->name][$topic->topic->id] = $topic->topic->name;
		}

		$teacher = Teacher::find_by_user_id($this->teacher);

        Template::set('teacher', $teacher);
		Template::set('permalink', $teacher->permalink);
		Template::set('topics', $topic_dropdown);
        Template::set('title', 'New Short Answer - EduFocal');
		Template::render();
	}

	/**
	 * Save Multiples
	 */
	public function save_multiple( $id=0 )
    {
		$this->rules[] = array(
			'field' => 'topic',
			'label' => 'Subject / Topic',
			'rules' => 'required'
		);
		$this->rules[] = array(
			'field' => 'instruction',
			'label' => 'Instruction / Data',
            'rules' => 'trim' /* should be required when multiple */
		);

		$this->rules[] = array(
			'field' => 'question',
			'label' => 'question',
			'rules' => 'callback__array_check'
		);
		$this->rules[] = array(
			'field' => 'choices',
			'label' => 'answer option',
			'rules' => 'callback__array_check'
		);
		$this->rules[] = array(
			'field' => 'correct',
			'label' => 'correct answer',
			'rules' => 'required|callback__correct_answers'
		);

		$this->form_validation->set_rules($this->rules);

		$question_id = 0;

		if ($this->form_validation->run() === TRUE)
        {
            if($id !== 0)
                $qObs = Question::find('all', array('conditions' => array('id = ? OR question_id = ?', $id, $id),
                                    'order' => 'id asc'));
            $index = 0;
			foreach($this->input->post('question') as $key => $question_body)
            {
                if(isset($qObs) && isset($qObs[$index]))
                {
                    $question = $qObs[$index];
                }
                else
                {
                    $question = new Question();
                    $question->user_id = $this->teacher;
                    $question->editor_id = $this->auth->user_id();
                }

                $index += 1;
				$question->topic_id = $this->input->post('topic');
				$question->question = $question_body;
				$question->type = 1;
				$question->difficulty = $this->input->post('difficulty');
				$question->status = 'Pending';
				$question->accepted_answers = $_POST['correct'][$key];
				$question->choices = json_encode($_POST['choices'][$key]);
				$question->hint = $_POST['hint'][$key];
				$question->question_id = $question_id;
				if($key == 0)
                {
					$question->instruction = $this->input->post('instruction');
					$question->weight = count($this->input->post('question'));
				}
				$question->save();

				if($key == 0) $question_id = $question->id;
			}


            if(isset($qObs))
            {
                Template::set_message('Question saved successfully');
                return TRUE;
            }
            else
            {
                Template::set_message('Question Added Successfully');
                if(isset($_POST['another']))
                {
                    $this->session->set_userdata('topic_id', $question->topic_id);
                    redirect("teachers/coursework/multiple");
                }
                else
                {
                    redirect("teachers/coursework");
                }
            }
		}
        else
        {
			return false;
		}
	}

	/**
	 * Validate Correct Answers
	 *
	 * @param array $options
	 * @return boolean Success
	 */
	public function _correct_answers ( $options )
	{
		$return = true;

		foreach ($this->input->post('question') as $key => $question)
		{
			if(!array_key_exists($key, $options)) $return = false;
		}

		if(!$return)
		{
			$this->form_validation->set_message(
			'_correct_answers',
			'One or more %s is missing.');
		}
		return $return;
	}

	/**
	 * Validate Input Arrays
	 *
	 * @param array $options
	 * @return boolean Success
	 */
	public function _array_check ( $options )
	{
		$return = true;

		foreach ($options as $question)
		{
			if(is_array($question))
			{
				if(!$this->_array_check($question)) $return = false;
			}
			else
			{
				$question = trim($question);
				if(empty($question)) $return = false;
			}
		}

		if(!$return)
		{
			$this->form_validation->set_message(
			'_array_check',
			'One or more %s is missing.');
		}

		return $return;
	}
}
