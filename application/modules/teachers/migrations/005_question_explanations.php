<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_question_explanations extends Migration {
	
	public function up() 
	{
        $prefix = $this->db->dbprefix;
		$fields = array(
			'explain' => array('type' => 'text', 'null' => true),
		);
		
		$this->dbforge->add_column($prefix.'questions', $fields);
	}
	
	public function down() 
	{
        $prefix = $this->db->dbprefix;
		$this->dbforge->drop_column($prefix.'questions', 'explain');
	}
}
