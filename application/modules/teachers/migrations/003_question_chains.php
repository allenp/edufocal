<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_question_chains extends Migration {
	
	public function up() 
	{
		$fields = array(
			'weight' => array('type' => 'int(11)', 'default' => '1'),
			'question_id' => array('type' => 'int(11)', 'default' => '0')
		);
		
		$this->dbforge->add_column('questions', $fields);
	}
	
	public function down() 
	{
		$this->dbforge->drop_column('questions', 'weight');
		$this->dbforge->drop_column('questions', 'question_id');
	}
}
