<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_update_questions_again extends Migration {
	
	public function up() 
	{
		$fields = array(
			'estimate_in_sec' => array('type' => 'int(11)'),
			'difficulty' => array('type' => 'float')
		);
		
		$mods = array(
						'difficulty' => array(
											 'name' => 'level',
											 'type' => 'int(11)',
												),
		);
		
		$this->dbforge->modify_column('questions', $mods);
		$this->dbforge->add_column('questions', $fields);
	}
	
	public function down() 
	{
		$this->dbforge->drop_column('questions', 'difficulty');
		$this->dbforge->drop_column('questions', 'estimate_in_sec');
		
		$mods = array(
				'level' => array(
									 'name' => 'difficulty',
									 'type' => 'int(11)',
										),
		);
		
		$this->dbforge->modify_column('questions', $mods);
	}
	
}
