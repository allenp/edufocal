<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_update_questions extends Migration {
	
	public function up() 
	{
		$fields = array(
			'difficulty' => array('type' => 'int(11)')
		);
		$this->dbforge->add_column('questions', $fields);

	}
	
	public function down() 
	{
		$this->dbforge->drop_column('questions', 'difficulty');
	}
	
}