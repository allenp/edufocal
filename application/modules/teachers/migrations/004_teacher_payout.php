<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Teacher_Payout extends Migration
{
    function up()
    {
        $dbprefix = $this->db->dbprefix;

        $fields = array(
                    'id' => array('type' => 'int(11)', 'unsigned' => true, 'auto_increment' => true),
                    'startdate' => array('type' => 'int(11)'),
                    'enddate' => array('type' => 'int(11)'),
                    'main_budget' => array('type' => 'decimal(10,2)'),
                    'top_budget' => array('type' => 'decimal(10,2)'),
                    'status' => array('type' => 'tinyint', 'unsigned' => true, 'default' => 0),
                    'user_id' => array('type' => 'int(11)', 'unsigned' => true),
                  );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'payout_periods');

        $fields = array(
                    'id' => array('type' => 'int(11)', 'unsigned' => true, 'auto_increment' => true),
                    'topic_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'period_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'teachers' => array('type' => 'int(11)', 'unsigned' => true),
                    'used' => array('type' => 'int(11)', 'unsigned' => true),
                    'teacher_quotient' => array('type' => 'float'),
                    'usage_quotient' => array('type' => 'float'),
                    'payout' => array('type' => 'decimal(10,2)'),
                 );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'topic_costs');

        $fields = array(
                    'id' => array('type' => 'int(11)', 'unsigned' => true, 'auto_increment' => true),
                    'period_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'teacher_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'topic_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'subject_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'rating' => array('type' => 'float'),
                    'used' => array('type' => 'int(11)', 'unsigned' => true),
                    'score' => array('type' => 'float'),
                    'rank' => array('type' => 'int(11)', 'unsigned' => true),
                    'payout' => array('type' => 'decimal(10,2)'),
                    'process_id' => array('type' => 'int(11)', 'unsigned' => true),
                  );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'teacher_topic_payouts');

        $fields = array(
                    'id' => array('type' => 'int(11)', 'unsigned' => true, 'auto_increment' => true),
                    'period_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'teacher_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'subject_id' => array('type' => 'int(11)', 'unsigned' => true),
                    'payout' => array('type' => 'decimal(10,2)'),
                    'process_id' => array('type' => 'int(11)', 'unsigned' => true, 'null' => true),
                 );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'teacher_payouts');

        $fields = array(
                    'owned' => array('type' => 'tinyint', 'default' => 0),
                    );

        $this->dbforge->add_column($dbprefix . 'questions', $fields);

        //get rid of old table
        $this->dbforge->drop_table($dbprefix . 'teacher_ratings_scores');
    }

    function down()
    {
        $dbprefix = $this->db->dbprefix;
        $this->dbforge->drop_table($dbprefix . 'payout_periods');
        $this->dbforge->drop_table($dbprefix . 'teacher_topic_payouts');
        $this->dbforge->drop_table($dbprefix . 'teacher_payouts');
        $this->dbforge->drop_table($dbprefix . 'topic_costs');
        $this->dbforge->drop_column($dbprefix . 'questions', 'owned');

        //put back old table to make migration repeatable without errors
        $fields = array(
                    'id' => array('type' => 'int(11)', 'unsigned' => true),
                  );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($dbprefix . 'teacher_ratings_scores');
    }
}
