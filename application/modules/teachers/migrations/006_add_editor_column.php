<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_editor_column extends Migration {
	
	public function up() 
	{
        $prefix = $this->db->dbprefix;
		$fields = array(
			'editor_id' => array('type' => 'INT(11)', 'null' => true),
		);
		
		$this->dbforge->add_column($prefix.'questions', $fields);
	}
	
	public function down() 
	{
        $prefix = $this->db->dbprefix;
		$this->dbforge->drop_column($prefix.'questions', 'editor_id');
	}
}
