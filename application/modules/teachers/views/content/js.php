$.subscribe('list-view/list-item/click', function(id) {
	$('#content').load('<?php echo site_url(SITE_AREA .'/content/teachers/edit') ?>/'+ id);
});

$(function() {
    $(".slideToggle .toggle").click(function() {
        $(this).parent().next().toggle();
    });
    
    $(".select-all").click(function() {
        var update = $(this).val();
        
        if($(this).is(':checked')) {
            $("#"+update+" input").attr("checked", true);
        } else {
            $("#"+update+" input").attr("checked", false);
        }
    });
});
