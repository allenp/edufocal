<ul class="nav nav-pills">
    <li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
        <?php echo anchor(SITE_AREA.'/content/teachers', 'All Teachers') ?>
    </li>
    <li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
        <?php echo anchor(SITE_AREA.'/content/teachers/create', 'Create A Teacher'); ?>
    </li>
</ul>
