<?php if (isset($teachers) && count($teachers)) : ?>
<h2>Teachers</h2>
<table class="table table-striped">
    <thead>
        <tr>
            <th>USER ID</th>
            <th>Name</th>
            <th>Level</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($teachers as $teacher) : ?>
        <tr>
            <td><?php print $teacher->user_id ?></td>
            <td><?php echo $teacher->first_name.' '.$teacher->last_name;?></td>
            <td><?php print $teacher->level ?></td>
            <td><?php echo anchor(SITE_AREA .'/content/teachers/edit/'. $teacher->id, 'Edit', 'class=""') ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

