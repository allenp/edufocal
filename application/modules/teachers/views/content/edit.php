<?php if (validation_errors()) : ?>
<div class="alert alert-info fade in">
    <a data-dismiss="alert" class="close">&times;</a>
	<?php echo validation_errors(); ?>
</div>
<?php endif; ?>
<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
<fieldset>
    <h2>Teacher's Account Details</h2>
</fieldset>
<?php if(isset($teacher)) : ?>
<div class="well shallow-well">
    <div class="text-right">
        <a class="btn btn-danger" id="delete-me" href="<?php echo site_url(SITE_AREA .'/content/teachers/delete/'. $teacher->id); ?>" onclick="return confirm('<?php echo "Are you sure you want to delete this teacher?"; ?>')">Delete this Teacher</a>
    </div>
</div>
<?php endif; ?>
<div class="row">
    <div class="span6">
        <div class="control-group">
            <?php echo form_label('First Name *', 'first_name', array('class' => 'control-label')); ?>
            <div class="controls">
                    <input type="text" name="first_name" maxlength="255" value="<?php echo set_value('first_name', isset($teacher->first_name) ? $teacher->first_name : ''); ?>" />
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Last Name *', 'last_name', array('class' => 'control-label')); ?>
            <div class="controls">
                <input type="text" name="last_name" maxlength="255" value="<?php echo set_value('last_name', isset($teacher->last_name) ? $teacher->last_name : ''); ?>" />
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Professional Title', 'title', array('class' => 'control-label')); ?>
            <div class="controls">
                <input type="text" name="title" maxlength="255" value="<?php echo set_value('title', isset($teacher->title) ? $teacher->title : ''); ?>" />
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Gender', 'sex', array('class' => 'control-label')); ?>
            <div class="controls">
                <select name="sex">
                    <option value="m"<?php if((isset($user) && $user->sex == 'm') || $this->input->post('sex') == 'm') echo ' selected="selected"'; ?>>Male</option>
                    <option value="f"<?php if((isset($user) && $user->sex == 'm') || $this->input->post('sex') == 'f') echo ' selected="selected"'; ?>>Female</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Email *', 'email', array('class' => 'control-label')); ?>
            <div class="controls">
                <input type="text" name="email" maxlength="255" value="<?php echo set_value('email', isset($user) ? $user->email : ''); ?>" />
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Password', 'password', array('class' => 'control-label')); ?>
            <div class="controls">
                <input type="password" name="password" maxlength="255" />
            </div>
        </div>
    </div> <!-- //.span6 -->

    <div class="span6">
        <div class="control-group">
            <?php echo form_label('Level', 'level', array('class' => 'control-label')); ?>
            <?php
            $options = array(
                'CXC' => 'CXC',
                'GSAT' =>  'GSAT'
            ); ?>
            <div class="controls">
                <select name="level">
                <?php foreach($options as $key => $value) : ?>
                <option value="<?php echo $key ?>" <?php if(isset($teacher) && $key == $teacher->level) echo ' selected="selected"'?>><?php echo $value ?></option>
                <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Phone Number', 'phone', array('class' => 'control-label')); ?>
            <div class="controls">
                <input type="text" name="phone" maxlength="255" value="<?php echo set_value('phone', isset($user) ? $user->phone: $this->input->post('phone')); ?>" />
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label('Credentials', 'credentials', array('class' => 'control-label')); ?>
            <div class="controls">
                <textarea name="credentials"><?php echo set_value('credentials', isset($teacher->credentials) ? $teacher->credentials : ''); ?></textarea>
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Short Biography', 'short_bio', array('class' => 'control-label')); ?>
            <div class="controls">
                <textarea name="short_bio"><?php echo set_value('short_bio', isset($teacher->short_bio) ? $teacher->short_bio : ''); ?></textarea>
            </div>
        </div>
        <div class="control-group">
            <?php echo form_label('Biography', 'biography', array('class' => 'control-label')); ?>
            <div class="controls">
                <textarea name="biography"><?php echo set_value('biography', isset($teacher->biography) ? $teacher->biography : ''); ?></textarea>
            </div>
        </div>
    </div> <!--//.span6 -->
</div>
<h2>Subjects taught</h2>
<?php foreach($subjects as $subject) : ?>
<div class="row control-group">
    <p class="slideToggle">
        <input id="all_<?php echo $subject->id ?>" type="checkbox" value="<?php echo $subject->id; ?>" class="select-all" />
        <label style="display:inline" for="all_<?php echo $subject->id ?>">
            <strong>
            <?php echo $subject->name.' ('.$subject->level.')'; ?>
            </strong>
        </label>
        <a class="toggle">(Show all)</a>
    </p>
    <div id="<?php echo $subject->id; ?>" style="display: none;" class="subjects">
    <?php foreach($subject->topics as $topic) : ?>
        <div class="span4">
        <label>
           <input type="checkbox" name="topics[]" value="<?php echo $subject->id.'_'.$topic->id; ?>"<?php if(in_array($topic->id, $topics)) echo ' checked="checked"'; ?>  />
            <?php echo $topic->name; ?>
        </label>
        </div>
    <?php endforeach; ?>
    </div>
</div>
<?php endforeach; ?>
</div>

<div class="form-actions">
	<input class="btn btn-primary btn-large" type="submit" name="submit" value="Save teacher" /> or <?php echo anchor(SITE_AREA .'/content/teachers', 'Cancel'); ?>
</div>


<?php echo form_close(); ?>
<style type="text/css">
	.slideToggle {
		cursor: pointer;
	}
</style>
