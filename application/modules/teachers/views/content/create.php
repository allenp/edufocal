<?php if (validation_errors()) : ?>
<div class="notification error">
	<?php echo validation_errors(); ?>
</div>
<?php endif; 

echo form_open($this->uri->uri_string(), 'class="constrained ajax-form"'); ?>
<div>
    <?php echo form_label('First Name', 'first_name'); ?> <span class="required">*</span>
    <input type="text" name="first_name" maxlength="255" />
</div>
<div>
    <?php echo form_label('Last Name', 'last_name'); ?> <span class="required">*</span>
    <input type="text" name="last_name" maxlength="255" />
</div>
<div>
    <?php echo form_label('Professional Title', 'title'); ?>
    <input type="text" name="title" maxlength="255" />
</div>
<div>
    <?php echo form_label('Gender', 'sex'); ?>
    <select name="sex">
    	<option value="m">Male</option>
    	<option value="f">Female</option>
    </select>
</div>
<div>
    <?php echo form_label('Level', 'level'); ?>
    <select name="level">
    	<option>CXC</option>
    	<option>GSAT</option>
        <option>SAT</option>
        <option>LSAT</option>
        <option>SSAT</option>
        <option>GRE</option>
    </select>
</div>
<div>
    <?php echo form_label('Phone Number', 'phone'); ?>
    <input type="text" name="phone" maxlength="255" />
</div>
<div>
    <?php echo form_label('Email', 'email'); ?> <span class="required">*</span>
    <input type="text" name="email" maxlength="255" />
</div>
<div>
    <?php echo form_label('Username', 'username'); ?>
    <input type="text" name="username" maxlength="255" />
</div>
<div>
    <?php echo form_label('Password', 'password'); ?> <span class="required">*</span>
    <input type="password" name="password" maxlength="255" />
</div>
<div>
    <?php echo form_label('Credentials', 'credentials'); ?>
    <textarea name="credentials"></textarea>
</div>
<div>
    <?php echo form_label('Short Biography', 'short_bio'); ?>
    <textarea name="short_bio"></textarea>
</div>
<div>
    <?php echo form_label('Biography', 'biography'); ?>
    <textarea name="biography"></textarea>
</div>
<div>
	<h4>Subjects / Topics</h4>
    <?php foreach($subjects as $subject) : ?>
    	<p class="slideToggle"><strong><?php echo $subject->name.' ('.$subject->level.')'; ?></strong> 
    		<input type="checkbox" value="<?php echo $subject->id; ?>" class="select-all" /> Select All</p>
    	<div id="<?php echo $subject->id; ?>" style="display: none;">
    	<?php foreach($subject->topics as $topic) : ?>
    		<div>
    		<label> <?php echo $topic->name; ?>
    		</label> <input type="checkbox" name="topics[]" value="<?php echo $subject->id.'_'.$topic->id; ?>" />
    		</div>
    	<?php endforeach; ?>
    	</div>
    <?php endforeach; ?>
</div>

<div class="text-right">
	<input type="submit" name="submit" value="Insert Teacher" /> or <?php echo anchor(SITE_AREA .'/content/teachers', 'Cancel'); ?>
</div>
<?php echo form_close(); ?>
<style type="text/css">
	.slideToggle {
		cursor: pointer;
	}
</style>
<script type="text/javascript">
	$(function() {
		$(".slideToggle strong").click(function() {
			$(this).parent().next().toggle();
		});
		
		$(".select-all").click(function() {
			var update = $(this).val();
			
			if($(this).is(':checked')) {
				$("#"+update+" input").attr("checked", true);
			} else {
				$("#"+update+" input").attr("checked", false);
			}
		});
	});
</script>
