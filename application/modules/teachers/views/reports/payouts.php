<div class="view split-view">
	<div class="view">
    <h3>Payout History</h3>	
	<?php if (isset($periods) && count($periods)) : ?>
		<div class="scrollable">
			<div class="list-view" id="role-list">
				<?php foreach ($periods as $period) : ?>
					<div class="list-item" data-id="<?php echo $period->id; ?>">
						<p>
							<b><?php echo date('j-M-y',$period->startdate).' to '.date('j-M-y', $period->enddate) ?></b>
						</p>
					</div>
				<?php endforeach; ?>
			</div>	<!-- /list-view -->
		</div>
	
	<?php endif; ?>
	</div>
	<!-- Role Editor -->
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
	
        <h3>Create new payout report</h3>
        <div class="box create rounded">
        <?php echo form_open(site_url(SITE_AREA . '/reports/teachers/generate_payout', 'class="constrained ajax-form"'));; ?>
            <?php echo form_label('Payout Budget', 'main_budget'); ?><span class="required">*</span>
            <input id="main_budget" type="text" name="main_budget" value="<?php echo set_value("main_budget", isset($main_budget) ? $main_budget : 0); ?>" />
            <div class="text-right">
                <input type="submit" name="submit" value="New Payout Report" />
            </div>
        </div>
			<?php if (isset($periods) && count($periods)) : ?>
				
					<h2>Payout History</h2>
	<table>
		<thead>
		<th>Period</th>
		<th>Payout</th>
        <th>Action</th>
		</thead>
		<tbody>
<?php
foreach ($periods as $period) : ?>
			<tr>
				<td><?php echo date('j-M-y',$period->startdate).' to '.date('j-M-y', $period->enddate); ?></td>
                <td><?php print number_format($period->main_budget, 2,'.', ',') ?></td>
				<td><?php echo anchor(SITE_AREA .'/reports/teachers/payout/'. $period->id, 'View', 'class="ajaxify"') ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
				<?php endif; ?>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
