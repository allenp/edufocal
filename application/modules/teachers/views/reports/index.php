<div class="view split-view">
	
	<!-- Role List -->
	<div class="view">
	
	<?php if (isset($teachers) && count($teachers)) : ?>
		<div class="scrollable">
			<div class="list-view" id="role-list">
				<?php foreach ($teachers as $teacher) : ?>
					<div class="list-item" data-id="<?php echo $teacher->user_id; ?>">
						<p>
							<b><?php echo $teacher->first_name.' '.$teacher->last_name; ?></b>
						</p>
					</div>
				<?php endforeach; ?>
			</div>	<!-- /list-view -->
		</div>
	
	<?php endif; ?>
	</div>
	<!-- Role Editor -->
	<div id="content" class="view">
		<div class="scrollable" id="ajax-content">
				<?php if (isset($teachers) && count($teachers)) : ?>
				
					<h2>Teachers</h2>
	<table>
		<thead>
		<th>Name</th>
		<th>Actions</th>
		</thead>
		<tbody>
<?php
foreach ($teachers as $teacher) : ?>
			<tr>
				<td><?php echo $teacher->first_name.' '.$teacher->last_name;?></td>
				<td><?php echo anchor(SITE_AREA .'/reports/teachers/view/'. $teacher->user_id, 'View', 'class="ajaxify"') ?></td>
			</tr>
<?php endforeach; ?>
		</tbody>
	</table>
				<?php endif; ?>
				
		</div>	<!-- /ajax-content -->
	</div>	<!-- /content -->
</div>
