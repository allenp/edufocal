<?php if(validation_errors()) : ?>
<div class="notification error">
    <?php echo validation_errors(); ?>
</div>
<?php endif; ?>
<?php if(isset($payout)) : ?>
    <h3><?php print date('M j, Y', $payout->startdate) ?> to <?php print date('M j, Y', $payout->enddate) ?> - $<?php print number_format($payout->main_budget, 2, '.', ',') ?></h3>
<div class="box create rounded">
<?php
    echo form_open(site_url(SITE_AREA . 'reports/teachers/generate_payout', 'class="constrained ajax-form"')); ?>
    <div>
    <input type="hidden" name="period" value="<?php print $payout->id ?>" />
    <?php echo form_label('Payout Budget', 'main_budget'); ?><span class="required">*</span>
    <input id="main_budget" type="text" name="main_budget" value="<?php echo set_value("main_budget", $payout->main_budget); ?>" />
    </div>
    <div class="text-right">
        <input type="submit" name="submit" value="Update Payout Report" />
    </div>

    <?php echo form_close(); ?>
    </div>
    <!-- include payout amount and box to edit this amount and regenrate everything -->
    <table>
        <thead>
            <th>Teacher</th>
            <th>Subject</th>
            <th>Payout</th>
        </th>
        <tbody>
        <?php $teacher_total = 0;
              $ct = 0; //current teacher
              $total_print = 0;
              $total = 0;
              $prevt = 0;//prev teacher

        if(isset($breakdowns) && count($breakdowns)) : //main if wraps everything below
            $ct = $breakdowns[0]->teacher_id;

            foreach($breakdowns as $breakdown) :
                $total += $breakdown->payout;

                if($ct == $breakdown->teacher_id) 
                    $teacher_total += $breakdown->payout;
                else {
                    $ct = $breakdown->teacher_id;
                    $total_print = $teacher_total;
                    $teacher_total = $breakdown->payout; 
                }

            if($total_print > 0) : ?>

        <tr height="30px">
            <td colspan="2"></td>
            <td style="text-align:right"><strong><?php print number_format($total_print, 2,'.', ',') ?></strong></td>
        </tr>

            <?php $total_print = 0; ?>

            <?php endif; ?>

        <tr>
            <?php if($prevt != $ct) : ?> 
            <td data-teacher-id="<?php print $breakdown->teacher_id ?>"><?php print $breakdown->first_name . ', ' . $breakdown->last_name ?></td>
            <?php else : ?>
            <td></td>
            <?php endif; ?>
            <td><?php print $breakdown->subject ?></td>
            <td style="text-align:right"><?php print number_format($breakdown->payout, 2, '.', ',') ?></td>
        </tr>

           <?php  
                    $prevt = $ct;
           ?>
           <?  endforeach; ?>

     </tbody>
    <tfoot>
        <tr>
            <td colspan="2">Grand Total</td>
            <td style="text-align:right"><strong><?php print number_format($total, 2, '.', ',')?></strong></td>
        </tr>
    </tfoot>
          <?php endif; ?>
    </table>
<?php endif; ?>
