<table>
	<thead>
    	<th>Topic</th>
        <th>Accepted</th>
        <th>Rate</th>
        <th>Status</th>
    </thead>
    <tbody>
		<?php $total_accepted = 0;
		$total_overall = 0;
		foreach($topics as $topic) :
			$total_accepted += $topic->total;
			$total_overall += $totals[$topic->id]; ?>
            <tr>
                <td><?php echo $topic->name; ?></td>
                <td><?php echo $topic->total; ?> of <?php echo $totals[$topic->id]; ?></td>
                <td><?php echo $totals[$topic->id] > 0 ? round($topic->total / $totals[$topic->id] * 100, 2) : 0; ?>%</td>
                <td><?php echo $topic->total >= 10 ? '<strong>Active</strong>' : 'Inactive'; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tr>
    	<td><strong>Grand Total</strong></td>
        <td><strong><?php echo $total_accepted; ?></strong> of <strong><?php echo $total_overall; ?></strong></td>
        <td><strong><?php echo round($total_accepted / $total_overall * 100, 2); ?>%</strong></td>
    </tr>
</table>
