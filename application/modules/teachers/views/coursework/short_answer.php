    <?
        Assets::add_js(array(
            'jquery-ui-1.8.16.custom.min.js',
                ));
        Assets::add_module_js('teachers', 'short_answer');
    ?>
    <div id="main">
		<div class="heading padded">
        	<p class="back_button"><a href="<?php echo site_url('teachers/coursework'); ?>"><img src="<?php echo site_url("assets/dbrd/img/back_btn.png"); ?>" alt="Back" width="53" height="24" /></a></p>
        	<ul class="breadcrumb">
            	<li><a href="<?php echo site_url('teachers/coursework'); ?>">Add Content</a></li>
                <?php if(isset($teacher)): ?><li><?php echo $teacher->first_name . ' ' . $teacher->last_name ?></li><?php endif; ?>
                <li class="last">Short Answer</li>
            </ul>
        </div>
        <div id="multiple" class="wrapper">
        	<?php if ( Template::message() ) : ?>
            <div class="alert">
                <h4><?php echo Template::message(); ?></h4>
                <div class="clearfix"></div>
            </div>
            <?php endif; ?> 
            
        	<?php if ( validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
            
            <?php if(isset($_POST['style'])) $style = $this->input->post('style');
			else $style = 'single';
			echo form_open('teachers/coursework/short_answer', '', array('type' => '2', 'style' => $style)); ?>
        	<div class="difficulty">
                <label>Slide to adjust difficulty level <span class="help"><div>Harder questions are reserved for higher level students.</div></span></label>
                <div id="difficult">
                    <div id="difficulty"></div>
                </div>
            </div>
            <h1>New Short Answer Question</h1>
            <label class="topic">Subject / Topic:</label>
            <?php if(isset($topics)) : ?>
            <?php echo form_dropdown('topic', $topics, $this->input->post('topic')); ?>
            <?php endif; ?>
            <div class="clearfix"></div>
            <div class="left">
            	<label>Question / Instruction: <span class="help"><div>Creating a new question</div></span></label>
                <?php echo form_textarea(array('name' => 'question', 'class' => 'editor', 'value' => set_value('question'))); ?>
            </div>
            <div class="right">
            	<div class="hint"><span class="help"><div>Hints are provided to students during practice tests to help them learn the material.</div></span></div>
                <?php echo form_textarea(array('name' => 'hint', 'id' => 'hint', 'class' => 'hidden', 'value' => $this->input->post('hint'))); ?>
            </div>
            <div class="clearfix"></div>
            <div class="answers">
            	<div class="accepted left<?php if(isset($_POST['style']) && $this->input->post('style') == 'multi') echo ' hidden'; ?>">
                    <label>Answer:</label>
                    <div class="multi">
                        <?php echo form_button(array('id' => 'multipart', 'content' => 'Multiple Part Question')); ?>
                    </div>
                    <textarea name="answer" class="editor"><?php if(isset($_POST['answer'])) echo $_POST['answer']; ?></textarea>
            	</div>
                <div class="parts<?php if(!isset($_POST['style']) || $this->input->post('style') == 'single') echo ' hidden'; ?>">
	        		<label class="title">Multiple Parts</label>
                    <div class="clearfix"></div>
                    <div class="choices">
	        		<?php if(isset($_POST['choices'])) $duration = count($_POST['choices']);
					else $duration = 2;
					for($i = 1; $i <= $duration; $i++) : ?>
                    <div class="float_left">
                        <label><?php echo strtoupper(base_convert($i + 9, 10, 36)); ?>. Question:</label>
                        <textarea name="choices[<?php echo base_convert($i + 9, 10, 36); ?>]" class="editor"><?php if(isset($_POST['choices'][base_convert($i + 9, 10, 36)])) echo $_POST['choices'][base_convert($i + 9, 10, 36)]; ?></textarea>
                    </div>
                    <div class="float_right">
                        <label>Answer:</label>
                        <textarea name="answers[<?php echo base_convert($i + 9, 10, 36); ?>]" class="editor"><?php if(isset($_POST['answers'][base_convert($i + 9, 10, 36)])) echo $_POST['answers'][base_convert($i + 9, 10, 36)]; ?></textarea>
                    </div>
                    <div class="clearfix"></div>
					<?php endfor; ?>
        			</div>
                    <input type="hidden" id="duration" value="<?php echo $duration; ?>" />
                    <p><a href="#" class="another" id="add">Add More Options</a></p>
        		</div>
            </div>
            <div class="clearfix"></div>
            <?php if(isset($permalink)) : ?>
            <input type="hidden" id="permalink" value="<?php echo $permalink; ?>" />
            <?php endif; ?>
            <input type="hidden" name="difficulty" value="1" id="level">
            <button type="submit" name="submit">Save and Publish</button>
            <button type="submit" name="another">Save &amp; Add Another</button>
            <?php echo form_close(); ?>
        </div>
    </div>
