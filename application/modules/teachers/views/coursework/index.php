    	<?php if ( Template::message() ) : ?>
        <div class="alert">
            <h4><?php echo Template::message(); ?></h4>
            <div class="clearfix"></div>
        </div>
        <?php endif; ?>
		<h1>Add Content</h1>
        <div id="example" class="clearfix">
        	<div class="multiple col-xs-12 col-md-6">
            	<h1>Multiple Choice Questions</h1>
                <h4>Example:</h4>
                <ol>
                	<li>Lorem ipsum dolor sit amet consecutur?
                    	<ul>
                        	<li><input type="radio" name="sample"> Lorem ipsum dolor sit amet, Lorem ipsum dolor sit am</li>
                            <li><input type="radio" name="sample"> Lorem ipsum dolor sit amet, Lorem ipsum dolor sit am</li>
                            <li><input type="radio" name="sample"> Lorem ipsum dolor sit amet, Lorem ipsum dolor sit am</li>
                            <li><input type="radio" name="sample"> Lorem ipsum dolor sit amet, Lorem ipsum dolor sit am</li>
                            <li><input type="radio" name="sample"> Lorem ipsum dolor sit amet, Lorem ipsum dolor sit am</li>
                        </ul>
                    </li>
                </ol>
                <a href="<?php echo site_url("teachers/coursework/multiple"); ?>">Add Multiple Choice Questions</a>
            </div>
            <div class="short col-xs-12 col-md-6">
            	<h1>Short Answer Questions</h1>
                <h4>Example:</h4>
                <ol>
                	<li>Lorem ipsum dolor sit amet consecutur?
                    	<ul>
                        	<li>Your answer:<br />
                            <textarea class="form-control"></textarea></li>
                        </ul>
                    </li>
                </ol>
                <a href="<?php echo site_url("teachers/coursework/short_answer"); ?>">Add Short Answer Questions</a>
            </div>
        </div>
