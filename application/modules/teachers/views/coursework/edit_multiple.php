<?php
        Assets::add_js(array(
                    'jquery-ui-1.8.16.custom.min.js',
                ));
        Assets::add_module_js('teachers', 'multiple');
?>
    <div id="main">
		<div class="heading padded">
        	<p class="back_button"><a href="<?php echo site_url('teachers/coursework'); ?>"><img src="<?php echo site_url("assets/dbrd/img/back_btn.png"); ?>" alt="Back" width="53" height="24" /></a></p>
        	<ul class="breadcrumb">
            	<li><a href="<?php echo site_url('teachers/coursework'); ?>">Add Content</a></li>
                <?php if(isset($teacher)): ?><li><?php echo $teacher->first_name . ' ' . $teacher->last_name ?></li><?php endif; ?>
                <li class="last">Multiple Choice</li>
            </ul>
        </div>
        <div id="multiple" class="wrapper">
        	<?php if ( Template::message() ) : ?>
            <div class="alert">
                <h4><?php echo Template::message(); ?></h4>
                <div class="clearfix"></div>
            </div>
            <?php endif; ?> 
            
        	<?php if ( validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
            
            <?php echo form_open('teachers/coursework/multiple/edit/' . $question_id); ?>
            
        	<h1>Edit Multiple Choice</h1>
            <label class="topic">Subject / Topic:</label>
            <?php echo form_dropdown('topic', $topics, $this->input->post('topic') ? $this->input->post('topic') : $topic_id); ?>
            <div class="clearfix"></div>
            <div class="left">
            	<label>Instruction / Data: <span class="help"><div>General information pertaining to the questions you will be providing</div></span></label>
                <?php $instruction = isset($_POST['instruction']) ? $_POST['instruction'] : $instruction; ?>
                <textarea name="instruction" class="editor"><?php print $instruction ?></textarea>
            </div>
            <div class="difficulty">
                <label>Slide to adjust difficulty level <span class="help"><div>Harder questions are reserved for higher level students.</div></span></label>
                <div id="difficult">
                    <div id="difficulty"></div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="answers">
                <div class="parts">
                    <div class="choices">
                    <?php $questions = isset($_POST['question']) ? $_POST['question'] : $questions;
                            $hint = isset($_POST['hint']) ? $_POST['hint'] : $hint;
                            $duration = count($questions) ? count($questions) : 2;

					for($i = 0; $i < $duration; $i++) : ?>
                        <div class="left">
                            <label>Question <?php echo $i+1; ?>:</label>
                            <textarea name="question[<?php echo $i; ?>]" class="editor"><?php if(isset($questions[$i])) echo $questions[$i]; ?></textarea>
                        </div>
                        <div class="right">
                            <div class="hint"><span class="help"><div>Hints are provided to students during practice tests to help them learn the material.</div></span></div>
                            <textarea name="hint[<?php echo $i; ?>]" class="hint_box hidden"><?php print isset($hint[$i]) ? $hint[$i] : ''; ?></textarea>
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div class="options" title="choices_<?php echo $i; ?>">
                            <label>Answer Options: <span class="help"><div>The more options the harder the question</div></span></label>
                            <div class="clearfix"></div>
                            <ol>
                            <?php $qchoices = isset($_POST['choices'][$i]) ? $_POST['choices'][$i] : $choices[$i];
                                  $qchoices = array_values($qchoices); ?>
                            <?php $correct = isset($_POST['correct']) ? $_POST['correct'] : $correct; ?>
                                <?php $duration2 = count($qchoices) ? count($qchoices) : 2;
                                for($j = 1; $j <= $duration2; $j++) : ?>
                                <li>
                                    <div class="option">
                                        <span class="correct"><label><input type="radio" name="correct[<?php echo $i; ?>]" value="<?php echo $j; ?>"<?php if(isset($correct[$i]) && $correct[$i] == $j) echo ' checked="checked"'; ?>> Correct Answer</label></span>
                                        <textarea name="choices[<?php echo $i; ?>][<?php echo $j; ?>]" class="editor"><?php if(isset($qchoices[$j - 1])) echo $qchoices[$j - 1]; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <?php endfor; ?>
                            </ol>
                            <p><a href="javascript;" class="another" title="choices_<?php echo $i; ?>">Add More Options</a></p>
                        </div>
                        
                        <div class="clearfix"></div>
					<?php endfor; ?>
        			</div>
                    <input type="hidden" id="duration" value="<?php echo $duration; ?>" />
                    <p><a href="javascript;" class="another" id="addquestion">Add Another Question</a></p>
        		</div>
            </div>
            <div class="clearfix"></div>
            <input type="hidden" id="permalink" value="<?php echo $permalink; ?>" />
            <input type="hidden" name="difficulty" value="1" id="level">
            <input type="hidden" name="id" value="<?php print $question_id; ?>" />
            <button type="submit" name="submit">Save and Publish</button>
            <button type="submit" name="another">Save &amp; Add Another</button>
            <?php echo form_close(); ?>
        </div>
    </div>
