<?php Assets::add_module_js('teachers', 'multiple'); ?>
		<div class="chooser">
		<div class="heading padded">
        	<p class="back_button"><a href="<?php echo site_url('teachers/coursework'); ?>"><img src="<?php echo site_url("assets/dbrd/img/back_btn.png"); ?>" alt="Back" width="53" height="24" /></a></p>
        	<ul class="breadcrumb">
            	<li><a href="<?php echo site_url('teachers/coursework'); ?>">Add Content</a></li>
                <?php if(isset($teacher)): ?><li><?php echo $teacher->first_name . ' ' . $teacher->last_name ?></li><?php endif; ?>
                <li class="last">Multiple Choice</li>
            </ul>
        </div>
		</div>
        <div id="multiple">
        	<?php if ( Template::message() ) : ?>
            <div class="alert">
                <h4><?php echo Template::message(); ?></h4>
                <div class="clearfix"></div>
            </div>
            <?php endif; ?>

        	<?php if ( validation_errors()) : ?>
			<div class="alert">
				<h4>Please check the following before continuing:</h4>
				<br clear="all" />
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>

            <?php echo form_open('teachers/coursework/multiple'); ?>

        	<h1>New Multiple Choice / Linked Question</h1>
            <label class="topic">Subject / Topic:</label>
            <?php if(isset($topics)): ?>
            <?php if($this->input->post('topic')){
                    $topic_id = $this->input->post('topic');
            } ?>
            <?php echo form_dropdown('topic', $topics, isset($topic_id) ? $topic_id : null); ?>
            <?php endif; ?>
            <div class="clearfix"></div>
            <div class="left">
            	<label>Instruction / Data: <span class="help"><div>General information pertaining to the questions you will be providing</div></span></label>
                <textarea name="instruction" class="editor"><?php if(isset($_POST['instruction'])) echo $_POST['instruction']; ?></textarea>
            </div>
            <div class="difficulty">
                <label>Slide to adjust difficulty level <span class="help"><div>Harder questions are reserved for higher level students.</div></span></label>
                <div id="difficult">
                    <div id="difficulty"></div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="answers">
                <div class="parts">
                    <div class="choices">
	        		<?php if(isset($_POST['question'])) $duration = count($_POST['question']);
					else $duration = 1;
					for($i = 0; $i < $duration; $i++) : ?>
                        <div class="left">
                            <label>Question <?php echo $i+1; ?>:</label>
                            <textarea name="question[<?php echo $i; ?>]" class="editor"><?php if(isset($_POST['question'][$i])) echo $_POST['question'][$i]; ?></textarea>
                        </div>
                        <div class="right">
                            <div class="hint"><span class="help"><div>Hints are provided to students during practice tests to help them learn the material.</div></span></div>
                            <textarea name="hint[<?php echo $i; ?>]" class="hint_box hidden"><?php if(isset($_POST['hint'][$i])) echo $_POST['hint'][$i]; ?></textarea>
                        </div>

                        <div class="clearfix"></div>

                        <div class="options" title="choices_<?php echo $i; ?>">
                            <label>Answer Options: <span class="help"><div>The more options the harder the question</div></span></label>
                            <div class="clearfix"></div>
                            <ol>
                                <?php if(isset($_POST['choices'][$i])) $duration2 = count($_POST['choices'][$i]);
                                else $duration2 = 2;
                                for($j = 1; $j <= $duration2; $j++) : ?>
                                <li>
                                    <div class="option">
                                        <span class="correct"><label><input type="radio" name="correct[<?php echo $i; ?>]" value="<?php echo $j; ?>"<?php if(isset($_POST['correct'][$i]) && $_POST['correct'][$i] == $j) echo ' checked="checked"'; ?>> Correct Answer</label></span>
                                        <textarea name="choices[<?php echo $i; ?>][<?php echo $j; ?>]" class="editor"><?php if(isset($_POST['choices'][$i][$j])) echo $_POST['choices'][$i][$j]; ?></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <?php endfor; ?>
                            </ol>
                            <p><a href="javascript;" class="another" title="choices_<?php echo $i; ?>">Add More Options</a></p>
                        </div>

                        <div class="clearfix"></div>
					<?php endfor; ?>
        			</div>
                    <input type="hidden" id="duration" value="<?php echo $duration; ?>" />
                    <p><a href="javascript;" class="another" id="addquestion">Add Another Question</a></p>
        		</div>
            </div>
            <div class="clearfix"></div>
            <?php if(isset($permalink)) : ?>
            <input type="hidden" id="permalink" value="<?php echo $permalink; ?>" />
            <?php endif; ?>
            <input type="hidden" name="difficulty" value="1" id="level">
            <button type="submit" name="submit">Save and Publish</button>
            <button type="submit" name="another">Save &amp; Add Another</button>
            <?php echo form_close(); ?>
        </div>
