<ul class="nav navbar-nav">

<li<?php if($this->uri->segment(1) == 'teachers' && $this->uri->segment(2) === false) echo ' class="active"'; ?>><a href="<?php echo site_url('teachers'); ?>">Dashboard</a></li>
<li<?php if($this->uri->segment(2) == 'coursework') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/coursework'); ?>">Coursework</a></li>

<li<?php if($this->uri->segment(2) == 'question_queue') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/question_queue'); ?>">Question Queue</a></li>
<li<?php if($this->uri->segment(2) == 'videos') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/videos'); ?>">Videos</a></li>

</ul>
