<?php if($this->uri->segment(2) == 'coursework') : ?>
			<nav class="category">
            	<ul class="nav navbar-nav">
                	<li<?php if($this->uri->segment(3) == 'multiple') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/coursework/multiple'); ?>">Multiple Choice</a></li>
                    <li<?php if($this->uri->segment(3) == 'short_answer') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/coursework/short_answer'); ?>">Short Answer</a></li>
                    <!--<li<?php if($this->uri->segment(3) == 'linked_multiple') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/coursework/linked_multiple'); ?>">Linked Multiple Choice</a></li>-->
                </ul>
            </nav>
            <?php endif; ?>
            <?php if($this->uri->segment(2) == 'question_queue') : ?>
			<nav class="category">
            	<ul class="nav navbar-nav">
                	<li<?php if($this->uri->segment(4) == 'pending') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/question_queue/status/pending'); ?>">Pending</a></li>
                    <li<?php if($this->uri->segment(4) == 'accepted') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/question_queue/status/accepted'); ?>">Accepted</a></li>
                    <li<?php if($this->uri->segment(4) == 'rejected') echo ' class="active"'; ?>><a href="<?php echo site_url('teachers/question_queue/status/rejected'); ?>">Rejected</a></li>
                </ul>
            </nav>
            <?php endif; ?>
