<?php if ( Template::message() ) : ?>
<div class="alert">
	<h4><?php echo Template::message(); ?></h4>
	<div class="clearfix"></div>
</div>
<?php endif; ?>
<div class="col-xs-12 col-sm-8">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-7 greeting">
			<h2>Getting Started</h2>
			<p><a href="https://docs.google.com/document/d/1h0JL_jwpJX87xVrjRnfMdb3PEJue6fFYTR0vonG4kwU/edit">Review the EduFocal Teacher manual</a></p>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-5 callout">
			<a href="<?php echo site_url("teachers/coursework"); ?>"><img src="<?php echo site_url("assets/teachers/img/add_content.png"); ?>" alt="Add Course Content" /></a>
			<a href="#"><img src="<?php echo site_url("assets/teachers/img/payment_history.png"); ?>" alt="View Payment History" /></a>
		</div>
	</div>
</div>
<div class="col-xs-4">
	<div class="overview">
		<?php $avatar = $this->auth->profile()->avatar;
		if(empty($avatar)) : ?>
			<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" width="111" />
		<?php else : ?>
			<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" width="111"/>
		<?php endif; ?>
		<p>Last login: <?php echo $last_login; ?><br />
		<a href="<?php echo site_url("account"); ?>">Change profile picture</a><br />
	</div>

	<?php if(isset($teachers)) : ?>
	<?php echo form_open('', 'class="form-horizontal"'); ?>
	<?php echo form_dropdown('teacher', $teachers, isset($teacher) ? $teacher : '', 'Which teacher are you adding questions for?'); ?>
	<input type="submit" name="submit" value="Set teacher now" class="btn-primary btn-large"/>
	<?php echo form_close(); ?>
	<?php endif; ?>

</div>
<div class="row">
<div class="col-xs-12 col-md-8">
		<h3><?php echo $unread; ?> New Message<?php if($unread != 1) echo 's'; ?></h3>
		<ul class="list-unstyled messages">
		<?php foreach($messages as $message) :
			$lastReply = $message->replies[0]; ?>
		<li<?php /* class="starred" */ ?><?php if ( $status[$message->id] == 'no' ) echo ' class="unread"'; ?>><a href="<?php echo $message->permalink(); ?>"><?php echo $lastReply->user->name(); ?>: <?php echo word_limiter($lastReply->body, 5); ?></a></li>
		<?php endforeach; ?>
		</ul>
		<p align="right"><a href="<?php echo site_url('messages'); ?>">View all messages &raquo;</a></p>
</div>
</div>
                        <div class="clearfix"></div>
