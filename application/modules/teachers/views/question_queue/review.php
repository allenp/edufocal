	<div id="main">
    	<div class="heading padded">
        	<div class="left">
            	<h3><?php echo $subject->name; ?>: <?php echo $topic_name; ?></h3>
            </div>
            <div class="right">
            	<?php if(isset($previous)) : ?>
            	<a href="<?php echo site_url('teachers/question_queue/review/'.$previous); ?>"><img src="<?php echo site_url('assets/dbrd/img/previous_question.png'); ?>" alt="Previous Question" /></a>	
            	<?php endif;
				if(isset($next)) : ?>
            	<a href="<?php echo site_url('teachers/question_queue/review/'.$next); ?>"><img src="<?php echo site_url('assets/dbrd/img/next_question.png'); ?>" alt="Next Question" /></a>
            	<?php endif; ?>
            </div>
        </div>
    	<div class="wrapper">
        	<?php foreach($questions as $question) : ?>
                
                <?php if($question->weight > 1 && $question->question_id == 0) : ?>
                    <div class="selection instruction">
                        <div class="question">
                            <?php echo $question->instruction; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                
                <div class="selection">
                    <h3>Q:</h3>
                    <div class="question">
                        <?php echo $question->question; ?>
                            <?php if(!empty($question->choices)) {
                            $choices = json_decode($question->choices);
                            foreach($choices as $num => $choice) { ?>
                            <div class="clearfix"></div>
                            <h3><?php echo (is_numeric($num) ? strtoupper(base_convert($num + 9, 10, 36)) : $num); ?>.</h3>
                            <div class="question">
                                <?php echo $choice; ?>
                            </div>
                            <?php } 
                        } ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                
                <?php if($question->weight > 1 || $question->question_id != 0) : ?>
                    <div class="answer selection">
                        <h3>A:</h3>
                        <div class="question">
                            <?php if($question->type == 2 && substr($question->accepted_answers, 0, 1) == '{') {
                                $answers = json_decode($question->accepted_answers);
                                foreach($answers as $num => $choice) { ?>
                                <div class="clearfix"></div>
                                <h3><?php echo (is_numeric($num) ? strtoupper(base_convert($num + 9, 10, 36)) : $num); ?>.</h3>
                                <p><?php echo $choice; ?></p>
                            <?php } 
                            } else { ?>
                                <p><?php echo (is_numeric($question->accepted_answers) ? strtoupper(base_convert($question->accepted_answers + 9, 10, 36)) : $question->accepted_answers); ?></p>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php else: ?>
                    <div class="answer">
                        <div class="submitted">
                            <h3>A:</h3>
                            <div class="question">
                                <?php if($question->type == 2 && substr($question->accepted_answers, 0, 1) == '{') {
                                    $answers = json_decode($question->accepted_answers);
                                    foreach($answers as $num => $choice) { ?>
                                    <div class="clearfix"></div>
                                    <h3><?php echo (is_numeric($num) ? strtoupper(base_convert($num + 9, 10, 36)) : $num); ?>.</h3>
                                    <p><?php echo $choice; ?></p>
                                <?php } 
                                } else { ?>
                                    <p><?php echo (is_numeric($question->accepted_answers) ? strtoupper(base_convert($question->accepted_answers + 9, 10, 36)) : $question->accepted_answers); ?></p>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            
            <?php if(isset($history)) : ?>
            	<div class="answer">
                         <div class="vote">
                            <div id="vote_selection">
                            <?php
                                      $htotal = count($history);
                                      $hcount = 1;?>
                                    <?php if($htotal > 0): ?>
                                        <?php foreach($history as $rejected) : ?>
                                        <p><strong><?php print $rejected->status ?></strong>
                                        <?php echo $rejected->status == 'Rejected' && !empty($rejected->reason) ? ": " . $rejected->reason : ''; ?></p>
                                        <?php if($hcount++ < $htotal): ?>
                                        <hr />
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </div>
                        </div>
                </div>
            <?php endif; ?>
    </div>
