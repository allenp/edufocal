<table class="table table-bordered">
	<tr class="heading">
		<td>#</td>
		<td>Level - Subject/Topic</td>
		<td width="30%">Question Excerpt</td>
		<td>Type</td>
		<td>Status</td>
		<td>Date</td>
		<td></td>
	</tr>
	<?php if(count($questions) == 0) echo '<tr><td colspan="7">No Questions in the Queue</td></tr>';
	foreach($questions as $question) : ?>
		<tr>
		<td><?php echo $question->id ?></td>
		<td><?php echo $question->subject_level; ?> | <?php echo $question->subject_name . ' | ' . $question->topic_name; ?></td>
		<td><?php echo word_limiter(strip_tags($question->question), 15); ?></td>
		<td><?php echo $question->type == 1 ? ($question->weight > 1 ? 'LMC' : 'MC') : 'SA'; ?></td>
		<td  class="<?php echo strtolower($question->status) ?> status"><?php echo $question->status == 'rejected' ? '<span class="red">'.$question->status.'</span>' : $question->status; ?></td>
		<td><?php echo $question->created_at != null ?  date('Y-M-d', strtotime($question->created_at)) : '' ?></td>
		<td><a href="<?php echo site_url('teachers/question_queue/review/'.$question->id); ?>">Review</a> or
		<?php $url = $question->type == 1 ? 'multiple' : 'short_answer'; ?>
			<a href="<?php print site_url('teachers/coursework/'. $url .'/edit/'.$question->id); ?>">Edit</a></td>
	</tr>
	<?php endforeach; ?>
	<tr class="heading">
		<td colspan="3"><strong>Showing <?php echo $offset + 1; ?> - <?php echo $count > $offset + 10 ? $offset + 10 : $count; ?> of <?php echo $count; ?> match<?php echo $count != 1 ? "es": ""; ?></strong></td>
		<td colspan="4" align="right">
			<?php echo $this->pagination->create_links(); ?>
		</td>
	</tr>
</table>
<p>LMC - Linked Multiple Choice | MC - Multiple Choice | SA - Short Answer</p>
