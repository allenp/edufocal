<table class="table table-bordered">
	<tr class="heading">
		<td>Level</td>
		<td>Subject</td>
		<td>Topic</td>
		<td width="20%">Question Excerpt</td>
		<td>Type</td>
		<td>Status</td>
		<td></td>
	</tr>
	<?php if(count($questions) == 0) echo '<tr><td colspan="7">No Questions in the Queue</td></tr>';
	foreach($questions as $question) : ?>
	<tr>
		<td><?php echo $question->subject_level; ?></td>
		<td><?php echo $question->subject_name; ?></td>
		<td><?php echo $question->topic_name; ?></td>
		<td><?php echo word_limiter(strip_tags($question->question), 15); ?></td>
		<td><?php echo $question->type == 1 ? ($question->weight > 1 ? 'Linked Multiple Choice' : 'Multiple Choice') : 'Short Answer'; ?></td>
		<td><?php echo $question->status == 'rejected' ? '<span class="red">'.$question->status.'</span>' : $question->status; ?></td>
		<td><a href="<?php echo site_url('teachers/question_queue/review/'.$question->id); ?>">Review</a> or
			<a href="<?php print site_url('teachers/coursework/multiple/edit/'.$question->id); ?>">Edit</a></td>
	</tr>
	<?php endforeach; ?>
	<tr class="heading">
		<td colspan="3"><strong>Showing <?php echo $offset + 1; ?> - <?php echo $offset + 10; ?> of <?php echo $count; ?> match<?php echo $count != 1 ? "es": ""; ?></strong></td>
		<td colspan="4" align="right">
			<?php echo $this->pagination->create_links(); ?>
		</td>
	</tr>
</table>
