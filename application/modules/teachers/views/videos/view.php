<div class="chooser row">
	<div class="heading">
		<div class="back_button hidden-xs">
		<a href="<?php print site_url(['teachers', 'videos']) ?>">
				<img src="http://ef.paulallen.org/assets/dbrd/img/back_btn.png" alt="Back" width="53" height="24">
			</a>
		</div>
		<ul class="breadcrumb list-unstyled">
			<li><a href="<?php print site_url(['teachers', 'videos']) ?>">Videos</a></li>
			<li><?php print $video->title ?></li>
		</ul>
	</div>
	<div class="container">
		<div class="row">
		<div class="video-wrap col-md-8 col-xs-12 pull-right">
			<video id="player" preload="none" width="640" height="360" style="width: 100
%; height: 100%">
			<?php if(count($video->sources()) > 0): ?>
			<?php foreach($video->sources() as $source) : ?>
<source src="<?php print $source->url() ?>" media="all"/>
			<?php endforeach ?>
			<?php endif ?>
				<object width="640px" height="360px" type="application/x-shockwave-flash" data="<?php print site_url() ?>/public/assets/mediaelement/flashmediaelement.swf">
					<param name="movie" value="flashmediaelement.swf" />
					<param name="flashvars" value="controls=true&file=<?php //print $videos[$selected]->video_sources[0]->url ?>" />
					<!-- Image as a last resort -->
					<!--<img src="myvideo.jpg" width="640px" height="360px" title="No video playback capabilities" />-->
				</object>
			</video>
			<h2><?php print $video->title ?></h2>
			<p>
			<?php print $video->description ?>
			</p>
		</div>

		<div class="col-xs-12 col-md-4 pull-left">
			<ul class="menu list-unstyled listing clearfix video-more row">
			<?php if(count($videos) > 0) : ?>
			<li><h4 class="video-more-title"><?php print $video->topic->name ?></h4></li>
				<?php foreach($videos as $key => $video): ?>
				<li class="clearfix <?php print $key == $selected ? 'active' : ''?>">
					<a class="" href="<?php print site_url(['teachers', 'videos', 'view', $video->id])?>">
					<?php print $video->title ?>
					</a>
				</li>
				<?php endforeach ?>
			<?php endif; ?>
			</ul>
		</div>
		</div>
	</div>

</div>

