<?php print form_open(site_url(['teachers','videos', 'create']), ['class' => 'form-vertical']) ?>
 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	 <h4 class="modal-title">Create video</h4>
 </div>
 <div class="modal-body clearfix">
	<?php if(validation_errors()) : ?>
		<?php print validation_errors() ?>
	<?php endif; ?>
	<?php print form_dropdown('topic_id', $topics, '', 'Topic', ['class' => 'form-control']) ?>
	<div class="form-group">
		<input class="form-control" placeholder="title" name="title" />
	</div>
	<div class="form-group">
		<textarea class="form-control" name="description" placeholder="Description"></textarea>
	</div>
	<div class="form-group">
		<input class="form-control" placeholder="File path: http://full-url-external.mp4 or myVideo.mp4" name="path" />
	</div>
	<div class="form-group row">
		<div class="col-xs-6">
		<input name="sort_order" class="form-control" placeholder="Enter number for sort order" />
		</div>
		<div class="col-xs-6">
			<div class="checkbox">
				<label for="free">
					<input type="checkbox" name="free" /> Is video free?
				</label>
			</div>
		</div>
	</div>
</div>
 <div class="modal-footer">
	 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php print form_submit('submit', 'Save video', array('class' => 'btn btn-primary btn-lg')); ?>
 </div>
<?php print form_close() ?>
