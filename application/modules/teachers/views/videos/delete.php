<?php print form_open(site_url(['teachers','videos', 'delete']), ['class' => 'form-vertical']) ?>
 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	 <h4 class="modal-title">Confirm delete</h4>
 </div>
 <div class="modal-body clearfix">
<?php print form_hidden('id', $id) ?>
<?php print form_hidden('origin', $origin) ?>
Delete  <strong><?php print $title ?>?</strong>
</div>
 <div class="modal-footer">
	 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php print form_submit('submit', 'Delete', array('class' => 'btn btn-primary btn-lg')); ?>
 </div>
<?php print form_close() ?>
