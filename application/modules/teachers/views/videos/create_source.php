<?php print form_open(site_url(['teachers','videos', 'add_source', $video->id]), ['class' => 'form-vertical']) ?>
 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	 <h4 class="modal-title">Add source</h4>
 </div>
 <div class="modal-body clearfix">
<?php print form_hidden('id', $video->id) ?>
<div class="form-group">
	<input class="form-control" name="path" placeholder="external url or filename" />
</div>
</div>
 <div class="modal-footer">
	 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php print form_submit('submit', 'Save', array('class' => 'btn btn-primary btn-lg')); ?>
 </div>
<?php print form_close() ?>
