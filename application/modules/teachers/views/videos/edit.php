<?php print form_open(site_url(['teachers','videos', 'edit', $video->id]), ['class' => 'form-vertical']) ?>
 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	 <h4 class="modal-title">Edit video</h4>
 </div>
 <div class="modal-body clearfix">
	<?php if(validation_errors()) : ?>
		<?php print validation_errors() ?>
	<?php endif; ?>
	<?php print form_dropdown('topic_id', $topics, $video->topic_id, 'Topic', ['class' => 'form-control']) ?>
	<div class="form-group">
	<input class="form-control" placeholder="title" name="title" value="<?php print $video->title ?>"/>
	</div>
	<div class="form-group">
	<textarea class="form-control" name="description" placeholder="Description"><?php print $video->description ?></textarea>
	</div>
	<div class="form-group row">
		<div class="col-xs-6">
		<input name="sort_order" class="form-control" placeholder="Enter number for sort order" value="<?php print $video->sort_order ?>"/>
		</div>
		<div class="col-xs-6">
			<div class="checkbox">
				<label for="free">
				<input type="checkbox" name="free" <?php if($video->free == 1) print 'checked="checked"' ?>/> Is video free?
				</label>
			</div>
		</div>
	</div>
</div>
 <div class="modal-footer">
	 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php print form_submit('submit', 'Save video', array('class' => 'btn btn-primary btn-lg')); ?>
 </div>
<?php print form_close() ?>
