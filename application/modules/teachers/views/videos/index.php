<?php
  Assets::add_js(array(
      site_url('assets/js/jquery.form.js'),
      site_url('assets/js/editor.js'),
      site_url('assets/js/render.js'),
  ));
?>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3>Videos</h3>
	</div>
	<div class="panel-body">
		<a class="btn btn-primary" href="<?php print site_url(['teachers', 'videos', 'create']) ?>" data-toggle="modal" data-target="#dialog">Add new video</a>
	</div>
</div>
<table class="table table-bordered table-hover">
	<thead>
	<tr>
		<th class="col-md-7">Title</th><th>Sources</th><th>Order</th>
	</tr>
	<tbody>
	<?php if(count($videos) > 0) : ?>
	<?php foreach($videos as $video) : ?>
		<tr>
		<td>
			<h4><a href="<?php print site_url(['teachers','videos', 'view', $video->id]) ?>">
<?php print $video->title ?>
			</a>
<span class="pull-right">
<a data-toggle="modal" data-target="#dialog" href="<?php print site_url(['teachers', 'videos', 'edit', $video->id]) ?>">
<span class="glyphicon glyphicon-pencil">
</a>
<a data-toggle="modal" data-target="#dialog" href="<?php print site_url(['teachers', 'videos', 'get_delete', $video->id]) ?>">
<span class="glyphicon glyphicon-trash">
</span>
</a>
</span></h4>
<?php if($video->free) : ?>
<span class="label label-info">Free</span>
<?php endif ?>
			<p><?php print $video->description ?></p>
</td>
			<td>
			<div class="form-group clearfix">
			<span class="pull-right">
				<a class="btn btn-default btn-xs" data-toggle="modal" data-target="#dialog" href="<?php print site_url(['teachers', 'videos', 'add_source', $video->id]) ?>"><span class="glyphicon glyphicon-plus-sign"></span> Add source</a>
			</span>
			</div>
			<ul class="list-unstyled">
			<?php if(count($video->video_sources) > 0) : ?>
			<?php foreach($video->video_sources as $source): ?>
			<li><!--<a href="<?php //print site_url(['teachers', 'videos', 'source', $source->id]) ?>">--><?php print $source->path ?><!--</a>-->
			<span class="pull-right">
				<a data-toggle="modal" data-target="#dialog" href="<?php print site_url(['teachers', 'videos', 'get_delete_source', $source->id]) ?>"><span class="glyphicon glyphicon-trash"></span></a>
			</span>
</li>
			<?php endforeach ?>
			<?php endif ?>
			</td>
			<td><?php print $video->sort_order ?></td>
		</tr>
	<?php endforeach ?>
	<?php endif ?>
	</tbody>
</table>
