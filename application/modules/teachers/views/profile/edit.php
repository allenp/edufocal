    <div id="main">
		<div class="wrapper">
        	<?php if ( validation_errors() ) : ?>
        	<div class="alert">
                <?php echo validation_errors(); ?>
                <div class="clearfix"></div>
            </div>
            <?php endif; ?>
        	<div class="single_column">
            	<h2>My Profile</h2>
                <h3>Personal Information</h3>
                <div class="bordered">
                	<?php echo form_open_multipart('teachers/profile/edit'); ?>
                        <div class="data">
                            <div class="photo">
                                <?php $avatar = $this->auth->profile()->avatar;
								if(empty($avatar)) : ?>
									<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" />
								<?php else : ?>
									<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" />
								<?php endif; ?>
                            </div>
                            <table width="60%" cellpadding="2">
                                <tr>
                                    <td>First Name</td>
                                    <td><?php echo form_input('first_name', $teacher->first_name); ?></td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td><?php echo form_input('last_name', $teacher->last_name); ?></td>
                                </tr>
                                <tr>
                                    <td>Professional Title</td>
                                    <td><?php echo form_input('title', $teacher->title); ?></td>
                                </tr>
                                <tr>
                                    <td>Short Bio</td>
                                    <td><?php echo form_textarea('short_bio', $teacher->short_bio); ?></td>
                                </tr>
                                <tr>
                                    <td>Full Bio</td>
                                    <td><?php echo form_textarea('biography', strip_tags($teacher->biography)); ?></td>
                                </tr>
                            </table>
                            <div class="clearfix"></div>
                        </div>
                        <div class="action">
                            <p align="right"><button type="submit" class="blue" name="save">Save</button> or <a href="<?php echo site_url('teachers/profile'); ?>">cancel</a></p>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
