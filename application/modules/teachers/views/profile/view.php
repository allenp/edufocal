    <div id="main">
		<div class="wrapper">
        	<div class="single_column">
            	<h3>Personal Information (<a href="<?php echo site_url("teachers/profile/edit"); ?>">Edit</a>)</h3>
                <p>The following is seen by students. So please only provide information you wish for students to know.</p>
                <div class="bordered">
					<div class="data">
                        <div class="photo">
                            <?php $avatar = $this->auth->profile()->avatar;
							if(empty($avatar)) : ?>
                            	<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $this->auth->name(); ?>" />
                            <?php else : ?>
                            	<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $this->auth->name(); ?>" />
                            <?php endif; ?>
                        </div>
                        <table width="60%" cellpadding="10">
							<tr>
                            	<td>First Name</td>
                                <td><?php echo $teacher->first_name; ?></td>
                            </tr>
                            <tr>
                            	<td>Last Name</td>
                                <td><?php echo $teacher->last_name; ?></td>
                            </tr>
                            <tr>
                            	<td>Professional Title</td>
                                <td><?php echo $teacher->title; ?></td>
                            </tr>
                            <tr>
                            	<td>Short Bio</td>
                                <td><?php echo $teacher->short_bio; ?></td>
                            </tr>
                            <tr>
                            	<td>Full Bio</td>
                                <td><?php echo $teacher->biography; ?></td>
                            </tr>
                        </table>
                        <div class="clearfix"></div>
					</div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
