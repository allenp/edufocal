<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'name'			=> 'Teachers',
	'description'	=> '',
	'menus'	=> array(
		'reports'	=> 'teachers/reports/menu'
	),
	'author'		=> 'EduFocal Team'
);
