<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_Column_To_Codes extends Migration
{
    public function up()
    {
        $fields = array(
            'api' => array('type' => 'int(1)', 'default' => '0')
        );
        $this->dbforge->add_column('codes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('codes', 'api');
    }

}
