<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Code_Sponsor extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
                    'sponsor_id' => array('type' => 'int(11)', 'null' => true),
                   );

        $this->dbforge->add_column($prefix . 'codes', $fields);

        $fields = array(
                    'id' => array('type' => 'int(11)', 'auto_increment' => true),
                    'name' => array('type' => 'nvarchar(255)', 'null' => false),
                    'image' => array('type' => 'nvarchar(255)', 'null' => true),
                    'created_on' => array('type' => 'timestamp'),
                  );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($prefix . 'sponsors');
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'codes', 'sponsor_id');
        $this->dbforge->drop_table($prefix . 'sponsors');

    }
}

