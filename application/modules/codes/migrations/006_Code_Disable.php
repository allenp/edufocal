<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Code_Disable extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
                    'active' => array('type' => 'tinyint', 'null' => true, 'default' => 1),
                   );

        $this->dbforge->add_column($prefix . 'codes', $fields);
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'codes', 'active');
    }
}

