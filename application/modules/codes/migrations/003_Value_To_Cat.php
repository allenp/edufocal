<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Value_To_Cat extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
            'type' => array('type' => 'varchar(20)', 'null' => true),
            'receipt_id' => array('type' => 'int(11)', 'unsigned' => true, 'null' => true)
        );

        $this->dbforge->add_column($prefix . 'codes', $fields);

        $this->db->query('update `codes` set type = case value 
                            when 2000.00 then \'yearly\'
                            when 200.00 then \'monthly\'
                            when 24.00 then \'yearly\'
                            when 3.00 then \'monthly\'
                            else \'yearly\'
                            end');
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;

        $this->dbforge->drop_column($prefix . 'codes', 'type');
        $this->dbforge->drop_column($prefix . 'codes', 'receipt_id');
    }
}

