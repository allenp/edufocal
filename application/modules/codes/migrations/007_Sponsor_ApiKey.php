<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Sponsor_ApiKey extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;

        $fields = array(
                    'api_key' => array('type' => 'varchar(256)', 'null' => true),
                   );

        $this->dbforge->add_column($prefix . 'sponsors', $fields);
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;
        $this->dbforge->drop_column($prefix . 'sponsors', 'api_key');
    }
}

