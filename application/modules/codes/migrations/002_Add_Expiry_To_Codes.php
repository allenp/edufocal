<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_Expiry_To_Codes extends Migration
{
    public function up()
    {
        $fields = array(
            'expiry_date' => array('type' => 'int(11)', 'null' => true),
            'value' => array('type' => 'decimal(9,2)', 'default' => '0.00')
        );
        $this->dbforge->add_column('codes', $fields);
        $this->db->query('update `codes` set expiry_date = accepted_on + 60 * 60 * 24 * 365, value=2000.00 where value=0.00 and expiry_date is null');
    }

    public function down()
    {
        $this->dbforge->drop_column('codes', 'expiry_date');
        $this->dbforge->drop_column('codes', 'value');
    }

}
