<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_track_code_source extends Migration
{
    public function up()
    {
        $prefix = $this->db->dbprefix;

        /*
            Sources: 0 - Bought by customer
                     1 - Requested via API
                     2 - Ordered via Backend by Administrator
         */

         $fields = array(
                    'receipt_id' => array('name' => 'source_ref', 'type' => 'int(11)', 'null' => true),
                    'api' => array('name' => 'source', 'type' => 'int(11)', 'default' => 0)
                   );

        $this->dbforge->modify_column($prefix . 'codes', $fields);
    }

    public function down()
    {
        $prefix = $this->db->dbprefix;

         $fields = array(
                    'source_ref' => array('name' => 'receipt_id'),
                    'source' => array('name' => 'api')
                   );

        $this->dbforge->modify_column($prefix . 'codes', $fields);

    }
}

