<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_Quarterly_Codes extends Migration
{
    public function up()
    {
		$this->db->query(
			"ALTER TABLE  `codes` CHANGE  `type`  `type` ENUM(  'yearly',  'monthly',  'weekly',  'quarterly' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL"
		);
    }

    public function down()
    {
		$this->db->query(
			"ALTER TABLE  `codes` CHANGE  `type`  `type` ENUM(  'yearly',  'monthly',  'weekly' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL"
		);
    }
}

