<?php
Assets::add_js(
    array(
        site_url(array('assets', 'js', 'jquery.form.js')),
        site_url(array('assets', 'js', 'editor.js')),
    )
);
?>
<div class="row-fluid">
    <div class="span5">
        <div class="well">
            <div class="control-group">
                <a class="btn btn-primary" data-async="true" data-target="#codes" href="<?php print site_url(array(SITE_AREA, 'content', 'codes', 'generate', 'sales')) ?>">New sales codes</a>
                <a class="btn btn-default" data-async="true" data-target="#codes" href="<?php print site_url(array(SITE_AREA, 'content', 'codes', 'generate', 'classroom')) ?>">New classroom codes</a>
            </div>
            <?php if (isset($orders) && count($orders) > 0) : ?>
            <table class="table table-hover">
            <thead>
                <tr><th>Used</th><th>For</th><th></th></tr>
            </thead>
            <?php foreach ($orders as $order) : ?>
            <tr>
                <td><span class="badge badge-info"><?php print $order->used . ' of ' . $order->unused ?></span></td>
                <td><?php print str_replace('AS SALES AGENT', '', $order->card_holder) ?><span> - #<?php print $order->id ?></span></td>
                <td><a class="btn btn-small" data-async="true" data-target="#codes" href="<?php print site_url(array(SITE_AREA, 'content', 'codes', 'view_codes', $order->id, $order->user_id)) ?>">View codes</a></td>
            </tr>
            <?php endforeach ?>
            </table>
            <?php endif ?>
        </div>
    </div>
    <div class="span7" id="codes">
    <p style="text-align: center; margin-top: 100px;">Click on stuff to your left to see them here :)</p>
    </div>
</div>
