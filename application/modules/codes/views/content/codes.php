            <?php if(isset($codes) && count($codes) > 0) : ?>
            <h2>Receipt #<?php print $codes[0]->source_ref ?></h2>
            <div class="control-group">
            <a class="btn btn-small btn-default" href="<?php print site_url(array(SITE_AREA,'content','codes', 'sales_codes_pdf', $codes[0]->source_ref, $generator_id)) ?>">Download PDF</a>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                    <th>Code</th><th>Type</th><th>Used</th><th>Currency</th>
                </thead>
                <tbody>
                    <?php foreach ( $codes as $code ) : ?>
                    <tr>
                        <td><?php print $code->code ?></td>
                        <td><?php print $code->type ?></td>
                        <td><?php print $code->user_id > 0 ? 'yes' : 'no' ?></td>
                        <td><?php print $code->currency ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
