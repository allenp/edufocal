<div class="span6">
<h4>Codes generated successfully</h4>
<p>Refresh the page to see them at the top of the list to your left</p>
<p>Yea, I know I could refresh it for you but I'm a bit chuffed, mate.</p>
<a href="<?php print site_url(array(SITE_AREA, 'content', 'codes')) ?>" class="btn btn-default">Refresh Page</a>
</div>
