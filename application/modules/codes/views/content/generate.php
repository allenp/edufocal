<?php print form_open(site_url(SITE_AREA . "/content/codes/sales_order"),
    array('class' => 'form-horizontal', 'data-async' => 'true', 'data-target' => '#codes'));?>
<?php print form_hidden('for', $for) ?>
<div class="modal-header">
<h3>Generate new <?php print $for ?> codes</h3>
</div>
<div class="modal-body">
	<div class="control-group">
		<label for="plan">Plan</label>
		<div class="controls">
		<select name="plan" class="input-lg">
		<?php foreach($plan as $pl) : ?>
			<option value="<?php print $pl ?>"><?php print ucfirst($pl) ?></option>
		<?php endforeach; ?>
		</select>
		</div>
	</div>

	<div class="control-group">
		<label for="amount">Amount</label>
		<div class="controls">
			<input name="amount" type="text" value="10" />
		</div>
	</div>
	<div class="control-group">
		<label for="currency">Currency</label>
		<div class="controls">
		<select name="currency">
		<?php foreach($currency as $curr) : ?>
			<option value="<?php print $curr ?>"><?php print $curr ?></option>
		<?php endforeach; ?>
		</select>
		</div>
	</div>
	<div class="control-group">
		<?php if(isset($users)) : ?>
		<label for="user">For</label>
		<div class="controls">
			<select name="user">
			<?php foreach($users as $user): ?>
				<option value="<?php print $user->id ?>" <?php if($user->id == $user_id)  print 'selected' ; ?>>
<?php print $user->first_name . ' ' . $user->last_name ?>
				</option>
			<?php endforeach; ?>
			</select>
		</div>
		<?php endif; ?>
	</div>
</div>
<div class="modal-footer">
    <input class="btn btn-large btn-primary" type="submit" value="Generate" />
</div>
</div>
<?php print form_close() ?>
