<?php if(isset($codes)) :?>
<!doctype html>
<head>
    <meta charset="utf-8" />
    <title>Voucher # <?php print $receipt != null ? $receipt->id : '' ?></title>
    <style type="text/css">
html {
    margin: 0;
    padding: 0;
}

body{
    margin: 0;
    padding:30px 20px;
    color: #333;
}

body {
    font-size: 100%;
    line-height: 1.4em;
}

body {
    font-family: Arial, Helvetica, sans-serif;
}

.vouchers .educode
{
    width: 320px;
    text-align: center;
    font-family: Courier, "Courier New", "Consolas", monospace;
    display: inline-block;
    border: solid 1px #eee;
    padding: 15px;
    font-size: 12px;
    margin:0 15px;
}

.break
{
    page-break-after: always;
}

.vouchers .tiny-educode
{
    font-size: x-small;
}

.vouchers .code
{
    font-family: monospace;
    font-weight: bold;
    font-size: large;
}

.vouchers .instructions
{
    text-align: left;
}

.vouchers h2
{
    font-family: monospace;
    font-size: small;
    font-weight: bold;
}

#logo
{
    background: transparent url(/assets/img/logo_term.png) no-repeat scroll 0 0;
    display: block;
    text-indent: -999px;
    margin: 0 auto;
    width: 122px;
    height: 50px;
}

</style>
</head>
<body class="vouchers">
<?php $break = 0; $count = count($codes); ?>
    <?php foreach($codes as $code) : ?>
        <?php $break += 1 ?>
        <div class="educode">
<p>
<h1 id="logo">&nbsp;</h1>
</p>
<p>USER: <?php print $receipt->user_id ?></p>
<p>TIME: <?php print date('H:i:s', $receipt->created_on) ?></p>
        ----------------------------
        <div class="tiny-educode">EduFocal Code</div>
        <div class="code"><?php print join(' ', str_split($code->code, 4)); ?></div>
        ----------------------------
        <p>Value: $<?php print $code->value ?></p>
        <p>How to use your EduFocal Code</p>
        <h2>Not registered?</h2>
        <p class="instructions">1. Go to www.EduFocal.com/signup to register, and enter
        your EduFocal Code printed above when prompted</p>
        <h2>Already registered?</h2>
        <p class="instructions">1. Log into your EduFocal account at www.EduFocal.com</p>
        <p class="instructions">2. Click 'Account Settings' on the top right hand corner of your screen</p>
        <p class="instructions">3. Scroll down to 'Renew Subscription'</p>
        <p class="instructions">4. Select 'EduFocal Codes' and enter your Edufocal Code printed above</p>
        <p class="instructions">5. Follow on-screen prompts to complete your account top-up</p>
        <p>For Customer Service assistance, to report issues or other queries
        send an email to help@edufocal.com for immediate assistance</p>
        <h2>Find us online!</h2>
        <p>website: www.EduFocal.com</p>
        <p>Twitter: @EduFocal</p>
        <p>Facebook: www.facebook.com/EduFocal</p>
        <p>Serial Number: SLE<?php print $code->id ?></p>
        </div>
        <?php if($break % 2 == 0 && $break < $count) : ?>
        <div class="break">&nbsp;</div>
        <?php endif; ?>
    <?php endforeach; ?>
</body>
</html>
<?php endif; ?>
