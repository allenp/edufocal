<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->auth->restrict("Codes.Content.View");
	}

	public function index ( $id = NULL , $user = NULL)
	{
		if (null == $user && ! has_permission('Codes.Content.Admin')) {
			$user = $this->auth->user_id();
		}

		$this->db->select('receipts.id, receipts.card_holder,
			receipts.currency, receipts.user_id,
			receipts.created_on, SUM(codes.user_id > 0) used, SUM(codes.user_id = 0) unused')
			->from('receipts')
			->join('codes', 'receipts.id = codes.source_ref')
			//->where('receipts.user_id', $user)
			->where('payment_ref is null')
			->order_by('created_on desc')
			->group_by('source_ref');

		if ($user != null) {
			$this->db->where('receipts.user_id', $user);
		}

		$query = $this->db->get();
		$data = array();
		$data['orders'] = $query->result();

		if ($id !== NULL && count($data['orders']) > 0) {
			$data['codes'] = $this->load->view(
				'content/codes',
				array('codes' => $this->get_receipt_codes($id, $user)),
				true
			);
		}

		$data['user_id'] = $user;

		$this->load->helper('ui/ui');

		foreach($data as $key => $value) {
			Template::set($key, $value);
		}

		Template::set("toolbar_title", "EduFocal Codes");
		Template::set_view("content/index");
		Template::render();
	}

	public function generate($for='sales')
	{
		if (!isset($user)) {
			$user = $this->auth->user_id();
		}

		if ('sales' == $for) {
			$roles = $this->db->query(
				"SELECT role_id from role_permissions where permission_id
				IN
				(SELECT permissions.permission_id from permissions
				where permissions.name = 'Codes.Content.View')"
			);

			$users = User::find_all_by_role_id(array_values($roles->result_array()));

		} else {
			$users = $this->db->from('users')
				->join('classrooms', 'classrooms.manager_id = users.id')
				->select('classrooms.id, users.first_name, users.last_name, classrooms.name')
				->get()->result();

			//append classroom name for display
			foreach ($users as $user) {
				$user->last_name = $user->last_name . ' - ' . $user->name;
			}
		}

		$this->load->view(
			'content/generate',
			array(
				'plan' => array('monthly', 'quarterly', 'weekly', 'yearly'),
				'currency' => array('JMD', 'USD'),
				'users' => $users,
				'value' => 'yearly',
				'user_id' => $user,
				'for' => $for,
			)
		);
	}

	private function get_receipt_codes( $id = NULL , $user = NULL)
	{
		$user = $user  && has_permission('Codes.Content.Admin') ? $user : $this->auth->user_id();

		if($id != NULL)
		{
			$this->db->select('codes.*')
				->from('codes')
				->join('receipts', 'codes.source_ref = receipts.id')
				->where('codes.source_ref', $id)
				->where('codes.source', 2)
				->where('receipts.user_id', $user)
				->where('receipts.payment_ref is null');

			$query = $this->db->get();
			return  $query->result();
		}
		return NULL;
	}

	public function view_codes( $id = NULL, $user = NULL )
	{
		$user = $user && has_permission('Codes.Content.Admin') ? $user : $this->auth->user_id();
		$codes = $this->get_receipt_codes($id, $user);
		$this->load->view('content/codes', array('codes' => $codes, 'generator_id' => $user));
	}

	public function view_code( $id = NULL )
	{
		$code = Code::find($id);
		Template::set('code', $code);
		Template::set_view('content/code');
		Template::render();
	}

	public function sales_codes_pdf( $id = NULL, $user = NULL )
	{
		$user = $user ? $user : $this->auth->user_id();

		if($data = $this->get_receipt_codes($id, $user)) {
			$receipt = Receipt::find_by_id_and_user_id($id, $user);
			$html = $this->load->view('content/sales_pdf', array('codes' => $data, 'receipt' => $receipt), TRUE);

			if(strlen($html) > 0) {
				$this->load->helper('dompdf');
				pdf_create($html, 'Code_Vouchers_' . $receipt->id, TRUE);
			}
		}
	}

	public function sales_order()
	{
		$this->auth->restrict("Codes.Content.Order");

		$plan = $this->input->post('plan');
		$amount = $this->input->post('amount');
		$currency = $this->input->post('currency');
		$for = $this->input->post('for');

		$source = 2;
		if ('sales' == $for) {
			$user = $this->input->post('user') && has_permission('Codes.Content.Admin') ?  $this->input->post('user') : $this->auth->user_id();
			$agent = User::find($user);
			$name = $agent->first_name . ' ' . $agent->last_name;
		} else {
			$user = $this->input->post('user');
			$classroom = Classroom::find($user);
			$agent = User::find($classroom->manager_id);
			$name = $classroom->name;
		}

		$configs = array( 'JMD' => 'educodes', 'USD' => 'educodes_usd' );
		$code_charges = $this->config->item($configs[$currency]);
		$code_charge = $code_charges[strtolower($plan)];

		if(null == $code_charge) {
			throw new \Exception('No code charge found');
		}

		$receipt = Receipt::create(
			array(
				'user_id' => $user,
				'amount' => $code_charge['charge'] * $amount,
				'currency' => $currency,
				'card_holder' => $name,
				'email' => $agent->email,
				'paymethod' => 'credit'
			)
		);

		foreach (range(1, $amount) as $index) {
			$code = Code::Generate(
				array(
					'type' => $plan,
					'source_ref' => $receipt->id,
					'value' => $code_charge['charge'],
					'currency' => $currency,
					'source' => $source,
				)
			);
		}

		if ('sales' != $for) {
			$this->db->query(
				"INSERT INTO classroom_codes (code_id, classroom_id, created_at)
				SELECT codes.id, $user as classroom_id, NOW()
				from codes where codes.source = 2 and codes.source_ref = ?", array($receipt->id)
			);
		}

		$this->load->view('content/generate_success');

	}
}
