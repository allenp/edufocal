<div class="chooser row">
    <div class="menu message_options col-xs-12 col-sm-3">
        <h3 class="heading">Messages</h3>
        <ul class="list-unstyled">
            <li<?php if ( $this->uri->total_segments() == 1 || $this->uri->segment(2) == 'view') echo ' class="active"'; ?>><?php echo anchor('messages', 'Inbox'); ?></li>
            <li<?php if ( $this->uri->segment(2) == 'compose') echo ' class="active"'; ?>><?php echo anchor('messages/compose', 'Compose'); ?></li>
        </ul>
    </div>
<div class="message_body col-xs-12 col-sm-9">
    <h3 class="heading" style="width:auto;"><?php echo $header; ?></h3>
    <div id="ajax-content">

    <?php if ( Template::message() || validation_errors() ) : ?>
        <div class="alert alert-danger">
            <h4><?php echo Template::message(); ?></h4>
            <?php echo validation_errors(); ?>
       </div>
    <?php endif; ?>

<?php echo form_open('messages/compose'); ?>

<?php print form_input('user_id', '', 'To', array('id' => 'message-compose-token')); ?>
<div class="form-group">
    <label for="compose_body form-control">Message</label>
    <textarea class="form-control" rows="5" name="body" id="compose_body"><?php echo set_value('body'); ?></textarea>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary btn-lg">Send Message</button>
    or
    <?php echo anchor('messages', 'Cancel'); ?>
</div>

<?php echo form_close(); ?>

<?php

$cdata = array();
if(isset($prepopulate))
    $cdata['prepopulate'] = $prepopulate;
Assets::add_js($this->load->view('messages/compose-script', $cdata, TRUE), 'inline');

?>
        </div>
    </div>
</div>
