	<div class="chooser row">
			<div class="menu message_options col-xs-12 col-sm-3">
				<h3 class="heading">Messages</h3>
				<ul class="list-unstyled">
					<li<?php if ( $this->uri->total_segments() == 1 || $this->uri->segment(2) == 'view') echo ' class="active"'; ?>><?php echo anchor('messages', 'Inbox'); ?></li>
					<li<?php if ( $this->uri->segment(2) == 'compose') echo ' class="active"'; ?>><?php echo anchor('messages/compose', 'Compose'); ?></li>
				</ul>
			</div>
			<div class="message_body col-xs-12 col-sm-9">
				<h3 class="heading" style="width:auto;"><?php echo $header; ?></h3>
				<div id="ajax-content">

				<?php if ( Template::message() || validation_errors() ) : ?>
					<div class="alert">
		            	<h4><?php echo Template::message(); ?></h4>
		            	<div class="clearfix"></div>
		            	<?php echo validation_errors(); ?>
		                <div class="clearfix"></div>
		           </div>
				<?php endif; ?>

				<div id="message_scroll" class="general_header">
					<ul class="message-thread">
						<?php foreach ( $replies as $reply ) : ?>
						<li class="clearfix" id="reply_<?php echo $reply->id; ?>">
						<a href="<?php echo site_url("dashboard/profile/view/".$reply->user->id); ?>">
						<?php $avatar = $reply->user->avatar;
						if(empty($avatar)) : ?>
							<img src="<?php echo site_url("assets/img/default_user_icon.png"); ?>" alt="<?php echo $reply->user->name(); ?>" width="32px" height="32px" class="img" />
						<?php else : ?>
							<img src="<?php echo site_url("uploads/".$avatar); ?>" alt="<?php echo $reply->user->name(); ?>" width="32px" height="32px" class="img" />
						<?php endif; ?>
						</a>
						<div class="msg-envelope col-sm-11">
							<a class="msg-sender" href="<?php echo site_url("dashboard/profile/view/".$reply->user->id); ?>">
								<?php echo $reply->user->name(); ?>
							</a>
							<span class="msg-timestamp">
							<?php echo timespan($reply->stamp); ?> ago
							</span>
							<div class="msg-content">
							<?php echo auto_typography($reply->body); ?>
							</div>
						</div>
						</li>
						<?php endforeach ?>
					</ul>
				</div>

				<?php echo form_open($single_message->permalink()); ?>
                    <h4>Send Reply</h4>
                    <div class="form-group">
                    <label for="compose_body" class="label-control">Message</label>
                    <textarea rows="5" name="body" class="form-control" id="compose_body"><?php echo set_value('body'); ?></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg">Send Message</button>
                    </div>

                    <?php echo form_close(); ?>

				</div>
			</div>
		<br clear="all" />
		<br clear="all" />
	</div>
<?php Assets::add_js($this->load->view('messages/view-script', array(), TRUE), 'inline'); ?>
