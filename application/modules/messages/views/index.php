<?php Assets::add_module_js('messages', 'index'); ?>
<div class="chooser row">
<div class="menu message_options col-xs-12 col-sm-3">
    <h3 class="heading">Menu</h3>
    <ul class="list-unstyled">
        <li<?php if ( $this->uri->total_segments() == 1 || $this->uri->segment(2) == 'view') echo ' class="active"'; ?>><?php echo anchor('messages', 'Inbox'); ?></li>
        <li<?php if ( $this->uri->segment(2) == 'compose') echo ' class="active"'; ?>><?php echo anchor('messages/compose', 'Compose'); ?></li>
    </ul>
</div>
    <div class="message_body col-xs-12 col-sm-9">
        <h3 class="heading"><?php echo $header; ?></h3>
        <div id="ajax-content">

        <?php if ( Template::message() || validation_errors() ) : ?>
            <div class="alert">
                <h4><?php echo Template::message(); ?></h4>
                <div class="clearfix"></div>
                <?php echo validation_errors(); ?>
                <div class="clearfix"></div>
           </div>
        <?php endif; ?>

        <table class="table bordered">
            <tbody>
                <?php foreach ( $all_messages as $m ) : $lastReply = $m->replies[0];
                    $users = array();
                    foreach ( $m->users as $user )
                    {
                        if ( $user->id == $this->auth->user_id() )
                            continue;

                        $users[] = $user->name();
                    }
                ?>
                <tr<?php if ( $status[$m->id] == 'no' ) echo ' class="td-unread"'; ?>>
                    <td class="msg" style="with: 50%;">
                        <a href="<?php echo $m->permalink(); ?>"><?php echo implode(', ', $users); ?></a>
                        <p><?php echo word_limiter($lastReply->body, 10); ?></p>
                    </td>
                    <td align="right">
                        <a href="<?php echo site_url('messages/delete/'.$m->id); ?>" class="delete">Delete</a>
                        <p><?php echo timespan($m->last_stamp); ?> ago...</p>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div>
