$(function() {
    $("#message-compose-token").tokenInput("<?php echo site_url('messages/search_users'); ?>", {
        theme: "edufocal",
        preventDuplicates: true
        <?php if(isset($prepopulate)) : 
        echo ', prePopulate: '.$prepopulate;
        endif; ?>
    });
});
