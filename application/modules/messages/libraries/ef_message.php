<?php

class EF_Message {

    public function __construct()
    {
        self::init();
    }

    public static function init()
    {
    }

    function send($from, $to_user_ids, $firstReply)
    {		
        if(!is_array($to_user_ids))
            $to_user_ids = array($to_user_ids);

        $to_user_ids = array_merge ($to_user_ids, array($from));
        $to_user_ids = array_unique($to_user_ids);
        sort($to_user_ids);
        $user_hash = md5(json_encode($to_user_ids));

        return Message::transaction(function()
        use($firstReply, $to_user_ids, $user_hash, $from)
        {
            $message = Message::find_or_create_by_hash($user_hash);
            if($message == NULL)
                return FALSE;

            $firstReply = array(
                'message_id' => $message->id,
                'body' => $firstReply,
                'user_id' => $from,
            );
            
            $message->last_stamp = time();
            MessageReply::create($firstReply);
            $message->save();
            
            foreach ( $to_user_ids as $uid )
            {
                $use = MessageUser::find_or_create_by_user_id_and_message_id(
                    $uid, $message->id);
                $use->deleted = 'no';
                $use->save();
            }
        });
    }
}
