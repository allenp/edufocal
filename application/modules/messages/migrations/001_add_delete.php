<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_delete extends Migration {
	
	public function up() 
	{
		$fields = array(
			'deleted' => array('type' => "enum('yes','no')", 'default' => 'no')
		);
		$this->dbforge->add_column('message_users', $fields);

	}
	
	public function down() 
	{
		$this->dbforge->drop_column('message_users', 'deleted');
	}
	
}