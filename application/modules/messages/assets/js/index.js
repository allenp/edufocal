$(function(){
    $('tr').hover(function(){
        $(this).find('td').addClass('td-hover');
    }, function(){
        $(this).find('td').removeClass('td-hover');
    });

    $('.msg').click(function() {
        var href = $(this).find("a").attr("href");
        if(href) {
            window.location = href;
        }
    });
});
