<?php

use EduFocal\Support\Listener;
use Slack\Client;
use Slack\Notifier;

/**
 * Handles the messages between users on EduFocal
 *
 * @package EduFocal
 */
class Messages extends EF_Authenticated_Controller
{
	/**
	 * Sets up the messages module and theme.
	 */
	public function __construct ()
	{
		parent::__construct();

		$this->load->helper('text');
		$this->load->helper('date');

		switch ( $this->auth->role_id() )
		{
		case TEACHER_ROLE:
			Template::set_theme('teachers');
			break;
		case REVIEWER_ROLE:
			Template::set_theme('reviewer');
			break;
		case STUDENT_ROLE:
		default:
			Template::set_theme('dashboard');
			break;
		}
		Assets::add_module_js('messages', site_url('assets/js/jquery-tokeninput/src/jquery.tokeninput.js'));
		Assets::add_module_css('messages',
			array(
				'jquery-tokeninput/styles/token-input.css',
				'jquery-tokeninput/styles/token-input-edufocal.css',
			)
		);
	}

	/**
	 * Displays all the user's latest message threads.
	 */
	public function index ()
	{
		$mUser = MessageUser::find_all_by_user_id_and_deleted($this->auth->user_id(), 'no');
		$message_ids = array();
		$status = array();

		foreach ( $mUser as $m )
		{
			$status[$m->message_id] = $m->read;
			$message_ids[] = $m->message_id;
		}

		$messages = array();

		if ( count($message_ids ) > 0 ) {
			$messages = Message::find_all_by_id($message_ids, array('order' => 'last_stamp desc'));
		}

		Template::set('header', 'All Messages');
		Template::set('title', 'Messages - EduFocal');

		Template::set('status', $status);
		Template::set('all_messages', $messages);
		Template::render();
	}

	/**
	 * Compose a message to at least one user.
	 */
	public function compose ( $user_id = 0 )
	{
		//TODO: Make sure that students can only message students
		//TODO: Make sure students can't message themselves

		$students = $this->db
			->order_by('first_name', 'asc')
			->order_by('last_name', 'asc')
			->get('users')
			->result();

		// Prepare the dropdown with user information
		$users = array();
		foreach ( $students as $user )
		{
			$users[$user->id] = $user->first_name .' '.$user->last_name;
		}

		// Let's run the validation to show some errors
		if ( $this->form_validation->run() === TRUE )
		{
			$user_ids = array_merge ( explode(',', $this->input->post('user_id')), array($this->auth->user_id()));
			$user_ids = array_unique($user_ids);
			sort($user_ids);

			// Encode all the users sorted by ids
			$user_hash = md5(json_encode($user_ids));

			$message = Message::find_or_create_by_hash($user_hash);

			if ($message) {
				$firstReply = array(
					'message_id' => $message->id,
					'body' => $this->input->post('body'),
					'user_id' => $this->auth->user_id()
				);

				$firstReply = MessageReply::create($firstReply);

				$message->last_stamp = time();
				$message->save();

				foreach ( $user_ids as $uid ) {
					$use = MessageUser::find_or_create_by_user_id_and_message_id($uid, $message->id);
					$use->deleted = 'no';
					$use->save();
				}

				$e = Listener::get();
				$e->emit("message.created", [$firstReply]);

				Template::set_message('Message successfully created!', 'success');
				redirect($message->permalink());
			}
		}

		if($user_id != 0) {
			$user = User::find($user_id);

			Template::set('prepopulate', '[{id: '.$user_id.', name: "'.$user->name().'"}]');
		}

		Template::set('title','Compose Message - EduFocal');
		Template::set('header', 'Compose a Message');
		Template::set('users', $users);
		Template::render();
	}

	public function delete ( $id )
	{
		$message = MessageUser::find_by_user_id_and_message_id($this->auth->user_id(), $id);

		if( $message )
		{
			$message->deleted = 'yes';
			$message->save();
		}

		Template::set_message('Message deleted!', 'success');
		redirect('messages');
	}

	/**
	 * View a single message thread containing one or more users.
	 */
	public function view ( $id )
	{
		$this->load->helper('typography');

		try
		{
			$message = Message::find($id);
			$mUser = MessageUser::find_by_user_id_and_message_id($this->auth->user_id(), $id);

		} catch ( Exception $e ) {
			Template::set_message('You are looking for a message that does not seem to exist.', 'error');
			redirect('messages');
		}

		if ( $this->form_validation->run('messages/reply') == TRUE ) {
			$message->last_stamp = time();
			$message->save();

			$mID = $message->id;
			foreach ( MessageUser::find_all_by_message_id($mID) as $m ) {
				$m->read = 'no';
				$m->deleted = 'no';
				$m->save();
			}

			$reply = MessageReply::create(array('user_id' => $this->auth->user_id(), 'message_id' => $message->id, 'body' => $this->input->post('body')));

			$e = Listener::get();
			$e->emit("message.created", [$reply]);

			/*
			$client = new Slack\Client('edufocal', 'GKFFi8J3IlZ4L8oMxEKGP1IE');
				$slack = new Slack\Notifier($client);

				$message = new Slack\Message\Message($reply->body);
				$message->setChannel('#class-monitor')
					->setIconEmoji(':baby_chick:')
					->setUsername('chatter-chick');

				$slack->notify($message, true);
			 */
			Template::set_message('Your reply has been added!');
			redirect($reply->permalink());
		}

		// Block error message
		if(count($mUser) == 0) redirect('messages');

		// Mark this message as read for the current user
		$mUser->read = 'yes';
		$mUser->save();

		// Generate users that conversation is going through.
		$users = array();
		foreach ( $message->users as $user )
		{
			if ( $this->auth->user_id() == $user->id )
				continue;

			$users[] = $user->name();
		}

		Template::set('users', $users);
		Template::set('header', 'Conversation with '.implode(', ', $users));
		Template::set('title', Template::get('header'));
		Template::set('single_message', $message);
		Template::set('replies', $message->replies);
		Template::render();
	}

	/**
	 * Ajax call to search for user names from the database.
	 */
	public function search_users ()
	{
		$users = array();
		$q = '%'.urldecode($this->input->get('q')).'%';

		foreach ( User::find('all', array('limit' => 25,'order' => 'first_name asc, last_name asc', 'conditions' => array('first_name LIKE ? OR last_name LIKE ?', $q, $q))) as $user )
		{
			$users[] = array('id' => $user->id, 'name' => $user->name(), 'avatar' => $user->avatar);
		};

		echo json_encode($users);
		exit;
	}
}
