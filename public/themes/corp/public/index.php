<?php
Assets::add_css(
    array(
        // 'bootstrap3/css/bootstrap.css',
        site_url('assets/css/edufocal.min.css'),
    ),
    'all'
);
Assets::add_js(
    array(
        site_url('assets/bootstrap3/js/bootstrap.js')
    )
);

$theme = site_url() . 'themes/corp/public/';
?>
<!doctype html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>EduFocal - Home</title>
  <meta name="author" content="EduFocal" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/favicon.ico" />
  <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/apple-touch-icon.png" />
<?php echo Assets::css(null, 'all', true); ?>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="<?php echo site_url();?>/assets/js/html5shiv.js"></script>
  <script src="<?php echo site_url();?>/assets/js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="notification-bar"></div>
<header>
  <div id="corporate" class="navbar navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
            <button data-toggle="collapse" class="navbar-toggle" data-target=".nav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a id="logo" href="/" class="navbar-brand"><img src="/assets/images/logo.png" class="logo" /></a>
        </div>
        <nav class="navbar-collapse collapse pull-left" role="navigation">
            <ul class="nav navbar-nav">
            <li class="active"><a href="/">Overview</a></li>
            <li><a href="http://blog.edufocal.com/">EduFocal Blog</a></li>
            </ul>
        </nav>
        <div class="nav-right pull-right col-sm-3">
        <a class="btn btn-info btn-small login-active" id="login-button">Member Sign in</a>
        <?php echo form_open(site_url('sessions/login'), array('id' => 'login-form', 'method' => 'post')); ?>
            <?php echo form_input('email', set_value('email'), 'Email Address', array('class' => 'form-control', 'placeholder' => ''));?>
            <?php echo form_password('password', set_value('email'), 'Password', array('class' => 'form-control', 'placeholder' => ''));?>
            <div class="form-group">
              <div class="checkbox">
                  <label>
                      <input type="checkbox" name="remember"> Remember me
                  </label>
              </div>
            </div>
            <hr />
            <button type="submit" value="submit" class="btn btn-primary btn-sm">Sign In</button>
            <a href="<?php echo site_url('forgot_password')?>">Forgot Password?</a>
        </form>
        </div>
    </div>
</header>

<div class="container">
  <?php echo Template::message(); ?>
</div>

<?php echo Template::content(); ?>

<footer id="corporate-footer">
  <div class="container">
      <div class="copyright col-sm-2">
        <p>&copy; <?php echo date('Y'); ?> EduFocal Ltd. </p>
      </div>
      <div class="col-sm-2">
      <ul>
        <li><a href="/">Overview</a></li>
        <li><a href="/signup/">Sign Up</a></li>
      </ul>
      </div>
      <div class="col-sm-2">
      <ul>
        <li><a href="http://blog.edufocal.com">EduFocal Blog</a></li>
        <li><a href="<?php echo site_url('terms'); ?>">Terms of Service</a> </li>
        <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a> </li>
        <li><a href="<?php echo site_url('privacy'); ?>">Privacy Policy</a></li>
      </ul>
      </div>
      <div class="col-sm-6">
      <ul class="social">
        <li><a href="http://blog.edufocal.com" id="social-blog">EduFocal Blog</a> </li>
        <li><a href="http://blog.edufocal.com/feed/" id="social-rss">RSS</a></li>
        <li><a href="https://www.facebook.com/EduFocal" id="social-facebook">Facebook</a></li>
        <li><a href="https://twitter.com/EduFocal" id="social-twitter">Twitter</a></li>
      </ul>
      </div>
  </div>
</footer>

<?php if(is_production()): ?>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php else: ?>
  <script src="/assets/js/jquery-1.10.2.min.js"></script>
<?php endif; ?>
<script src="/assets/bootstrap3/js/bootstrap.min.js"></script>
<script src="/assets/js/edufocal.min.js"></script>
  <div class="hide">
  </div>
</body>
