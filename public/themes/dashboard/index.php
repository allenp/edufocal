<?php
Assets::add_css(
    array(
        'bootstrap3/css/bootstrap.css',
        site_url('/assets/dashboard/css/ef.css?v=4.css'),
        'daterangepicker-bs3.css',
		'mediaelement/mediaelementplayer.min.css',
    ),
    'all'
);
Assets::add_js(
    array(
        site_url('assets/bootstrap3/js/bootstrap.js'),
		site_url('assets/mediaelement/mediaelement-and-player.min.js'),
    )
);

$this->load->helper('menu');
$menus = get_menu();
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo $title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="EduFocal">
  <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/apple-touch-icon.png">
  <?php echo Assets::css(null, 'all', true); ?>
<!--<script type="text/javascript" src="//cdn.sublimevideo.net/js/jpqqobww.js"></script>-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="<?php print site_url();?>/assets/js/html5shiv.js"></script>
  <script src="<?php print site_url();?>/assets/js/respond.min.js"></script>
<![endif]-->
</head>
<?php
    $id = isset($body_id) ? $body_id : NULL;
    if(isset($body_classes) && count($body_classes))
        $classes = implode((array)$body_classes);
    else
        $classes = NULL;
?>
<body <?php if($id !== NULL) print "id=\"{$id}\""; ?> class="<?php print $classes?> theme-dashboard">
    <header>
        <div class="container">
            <div class="navbar-header">
                <a id="logo" href="<?php print site_url('dashboard'); ?>" title="EduFocal Dashboard">EduFocal</a>
            </div>

            <div class="navbar navbar-inverse navbar-fixed-top hidden-sm hidden-md hidden-lg">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".nav-mobile">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="logo" class="navbar-brand"  href="<?php print site_url('dashboard'); ?>" title="EduFocal Dashboard">EduFocal</a>
                </div>
                <nav class="nav-mobile navbar-collapse collapse " role="navigation">
                    <ul class="nav navbar-nav">
<?php
foreach($menus as $key => $menu): ?>
<?php foreach($menu as $j => $nav): ?>
                        <li><a href="<?php print site_url($nav['url']); ?>" title="<?php print $nav['title']; ?>"><?php print $nav['title']; ?></a></li>
<?php endforeach; ?>
<?php endforeach; ?>
                    </ul>
                </nav>
            </div>

            <nav class="personal pull-right ef-navbar-collapse hidden-xs" role="navigation">
                <?php Template::block('personal_header_links', 'partials/personal_header_links'); ?>
            </nav>
            <div class="row clearfix hidden-xs">
            <div class="user col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right">
                <?php Template::block('global_user_info', 'partials/global_user_info'); ?>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
            <nav class="ef-navbar-main ef-navbar-collapse" role="navigation">
                <?php Template::block('main_navigation', 'partials/main_navigation'); ?>
            </nav>
            </div>
                <?php Template::block('dashboard_subnav', 'partials/dashboard_subnav'); ?>
            </div>
        </div>
    </header>
    <main id="content">
        <div class="container">
			<div class="section">
				<?php echo Template::content(); ?>
			</div>
        </div>
    </main>
	<footer>
        <div class="container hidden-xs" role="contentinfo">
        <img src="/assets/dashboard/img/observer.png" alt="Jamaica Observer" class="observer" />
        </div>
    </footer>
    <div id="dialog" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
            <p style="text-align: center; margin-top: 2em;">
            <img src="<?php print site_url() . '/assets/img/loading.gif' ?>" alt="Please wait. Loading..." />
                <br />
                Please wait a moment. Loading...
            </p>
            </div>
        </div>
    </div>

    <div id="debug"></div>
  <!--[if lt IE 7 ]>
    <script src="<?php echo base_url(); ?>assets/js/libs/dd_belatedpng.js"></script>
    <script> DD_belatedPNG.fix('img, .png_bg'); </script>
  <![endif]-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/jquery-1.10.2.min.js"><\/script>')</script>

<?php echo Assets::external_js(null); ?>
<?php echo Assets::module_js(); ?>
<?php echo Assets::inline_js(); ?>
<?php if(is_production()) : ?>
  <script src="//static.getclicky.com/js" type="text/javascript"></script>
	<script type="text/javascript">try{ clicky.init(66373717); }catch(e){}</script>
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66373717ns.gif" /></p></noscript>

   <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
<script type="text/javascript">
    FreshWidget.init("", {"queryString": "&widgetType=popup", "widgetType": "popup", "buttonType": "text", "buttonText": "Support", "buttonColor": "white", "buttonBg": "#B7009E", "alignment": "4", "offset": "235px", "formHeight": "500px", "url": "https://edufocal.freshdesk.com"} );
</script>
<?php endif; ?>
</body>
</html>
