<?php
Assets::add_css(array('reset.css',
            'global.css',
            'dbrd/css/style.css',
            'teachers/css/style.css',
            'js/jquery-tokeninput/styles/token-input.css',
            'js/jquery-tokeninput/styles/token-input-edufocal.css'
            )
        );
Assets::add_js(array(
            site_url('assets/js/jquery-1.10.2.min.js'),
            site_url('assets/js/jquery-ui-1.10.4.min.js'),
            site_url('assets/js/modernizr-2.5.3.js'),
            site_url('assets/js/jquery-tokeninput/src/jquery.tokeninput.js'),
            site_url('assets/tiny_mce/jquery.tinymce.js'),
            site_url('assets/teachers/js/script.js'),
            )
        );
?><!doctype html>  

<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php echo $title; ?></title>
  <meta name="description" content="">
  <meta name="author" content="EduFocal">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/apple-touch-icon.png">

  <?php echo Assets::css(null, 'all', true); ?>
</head>

<?php

/**
 * Print out relative body ID and classes. 
 *
 */
echo '<body';
if ( isset($body_id) ) echo ' id="'.$body_id.'"';
if ( isset($body_classes) && count($body_classes) > 0 )
{
	echo ' class="'.implode((array) $body_classes, ' ').'"';
}
echo '>';
?>
    <header>
        <div class="wrapper">
            <a href="<?php echo site_url('reviewer'); ?>"><img src="<?php echo site_url(); ?>assets/dbrd/img/logo.png" alt="EduFocal" class="logo" /></a>
            <nav class="personal">
            	<ul>
                	<?php $this->load->view('partials/reviewer_header_links'); ?>
                </ul>
            </nav>
            <div class="user">
            	<?php echo $this->load->view('partials/global_user_info'); ?>
			</div>
            <nav class="main">
            	<ul>
                	<li<?php if($this->uri->segment(1) == 'reviewer' && $this->uri->segment(2) === false) echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer'); ?>">Dashboard</a></li>
                    <li<?php if($this->uri->segment(2) == 'question_queue') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue'); ?>">Question Queue</a></li>
                </ul>
            </nav>
			<?php if($this->uri->segment(2) == 'question_queue') : ?>
			<nav class="category">
            	<ul>
                	<li<?php if($this->uri->segment(4) == 'multiple_choice') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue/type/multiple_choice'); ?>">Multiple Choice</a></li>
                    <li<?php if($this->uri->segment(4) == 'short_answer') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue/type/short_answer'); ?>">Short Answer</a></li>
                    <li<?php if($this->uri->segment(4) == 'no') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue/explain/no'); ?>">Require Explanations</a></li>
                    <li<?php if($this->uri->segment(4) == 'yes') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue/explain/yes'); ?>">Already Explained</a></li>
                    <li<?php if($this->uri->segment(3) == 'editor_summary') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue/editor_summary'); ?>">Editor summary</a></li>
                    <li<?php if($this->uri->segment(3) == 'teacher_summary') echo ' class="active"'; ?>><a href="<?php echo site_url('reviewer/question_queue/teacher_summary'); ?>">Teacher summary</a></li>
                </ul>
            </nav>
            <?php endif; ?>
        </div>
    </header>
    
	<?php echo Template::content(); ?>

	<footer>
    </footer>
	
  <!--[if lt IE 7 ]>
    <script src="<?php echo base_url(); ?>assets/js/libs/dd_belatedpng.js"></script>
    <script> DD_belatedPNG.fix('img, .png_bg'); </script>
  <![endif]-->
<?php echo Assets::external_js(null); ?>
<?php echo Assets::module_js(); ?>
<?php echo Assets::inline_js(); ?>

<?php if(is_production()) : ?>
  <script src="//static.getclicky.com/js" type="text/javascript"></script> 
	<script type="text/javascript">try{ clicky.init(66373717); }catch(e){}</script> 
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66373717ns.gif" /></p></noscript>
<?php endif; ?>
  
</body>
</html>
