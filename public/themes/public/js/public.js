document.ready(function() {

$('#top_student a').click(function() {
    var tab = $(this).attr("href");
    $('#top_student .tab li').removeClass('active');
    $(this).parent().addClass('active');
    $('#top_student .tab_body').hide();
    $(tab).show();
    return false;
});

$('#top_teacher a').click(function() {
    var tab = $(this).attr("href");
    $('#top_teacher .tab li').removeClass('active');
    $(this).parent().addClass('active');
    $('#top_teacher .tab_body').hide();
    $(tab).show();
    return false;
});

$('.login_btn').click(function() {
    if($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('#login').hide();
    } else {
        $(this).addClass('active');
        $('#login').show();
        $('#login input:first').focus();
    }
    return false;
});

$('#global_login').submit(function(){
    $('#global_login_ball').show();
    $('#global_login_error').hide();
    $.post('<?php echo site_url(); ?>sessions/login', $('#global_login').serialize(), function(data){

        if ( data.code != 1 )
        {
            $('#global_login_error').show();
            $('#global_login_ball').hide();
        }
        else
        {
            window.location = '<?php echo site_url('dashboard'); ?>';
        }
    });
    return false;
    });

    $('.close a').click(function() {
        $("#warning").fadeOut();
        return false;
    });

});
