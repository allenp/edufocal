<?php
    Assets::add_js( array(
        site_url() . 'assets/js/modernizr-2.5.3.js',
        site_url(). 'assets/js/jquery-1.7.2.min.js',
        site_url() . 'assets/js/checkbox.js',
        ), 'external');
?>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="description" content="" />
  <meta name="author" content="EduFocal" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/favicon.ico" />
  <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/apple-touch-icon.png" />
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/bootstrap.css" />
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/style.css" />
  <title><?php echo config_item('site.title'); ?></title>
</head>
<?php
$class = $this->uri->total_segments() == 0 ? "home" : "public";
?>
<body class="<?php print $class; ?>">
    <header>
        <?php 
        // For when we are on the home page...
        if ( $this->uri->total_segments() == 0 ) : ?>
        <div id="right"></div>
        <div id="left"></div>
        <div id="overlay"></div>
        <?php endif; ?>
        <div id="wrapper">
            <a href="<?php echo site_url(); ?>">
            	<img src="<?php echo site_url(); ?>assets/img/logo.png" alt="EduFocal" class="logo" />
            </a>
            <nav>
                <ul>
                    <li<?php if ( $this->uri->total_segments() == 0 ) echo ' class="current"'; ?>><a href="<?php echo site_url(); ?>">Overview</a></li>
                    <!--<li<?php if ( $this->uri->segment(1) == 'browse') echo ' class="current"'; ?>><a href="<?php echo site_url('browse'); ?>">Browse Subjects</a></li>-->
                    <li<?php if ( $this->uri->segment(1) == 'signup') echo ' class="current"'; ?>><a href="<?php echo site_url('signup'); ?>">Pricing &amp; Signup</a></li>
                    <li><a href="http://blog.edufocal.com">EduFocal Blog</a></li>
                </ul>
            </nav>
            <div id="signin">
            	<ul class="social">
                    <li><a href="http://twitter.com/EduFocal" class="twitter">Twitter</a></li>
                    <li><a href="https://www.facebook.com/EduFocal" class="facebook">Facebook</a></li>
                    <li><a href="http://blog.edufocal.com/feed/" class="rss">RSS</a></li>
                </ul>
                <div class="clear"></div>
                <?php if ( !$this->auth->is_logged_in() ) :?>
                <p><a href="#" class="login_btn">Member Sign In</a></p>
                <div id="login">
                    <?php echo form_open('sessions/login', array('id' => 'global_login')); ?>
					<label for="login_email">Email Address</label>
					<input type="text" name="email" id="login_email" />
					<label for="login_password">Password</label>
					<input type="password" name="password" id="login_password" />
					<div class="clearfix"></div>
					<div id="global_login_error">Sorry, please try again.</div> 
					<div class="remember">
						<label for="remember">Remember Me</label>
						<input type="checkbox" name="remember" class="styled" style="display: none;" />
					</div>
					<div id="global_login_ball">
						<img src="<?php echo site_url(); ?>assets/img/ajax-ball.gif" alt="Loading" />
					</div>
					<button type="submit" name="submit">Sign In</button>
					<div class="clearfix"></div>
					<!-- <p align="left"><a href="#">Forgot Username?</a></p> -->
					<p><a href="<?php echo site_url('forgot_password'); ?>">Forgot Password?</a></p> 
					<div class="clearfix"></div>
                    </form>
                </div>
                <?php else: ?>
                	<p>Welcome <?php echo $this->auth->profile()->first_name; ?></p>
                <?php endif; ?>
            </div>
            <?php // For when we are on the homepage
            if ( $this->uri->total_segments() == 0 ) : ?>
            <div class="slideshow">
            	<div class="left">
                    <h1>The future of learning is here</h1>
                    <p class="announce">Edufocal is the innovative Social Learning Platform that combines study with play. It's the first tool of its kind created for students at the primary and secondary level in the Caribbean.</p>
                    <a href="<?php echo site_url('signup'); ?>" class="pricing">See Pricing &amp; Signup</a><!--<p> Or <a href="#">Browse our subjects</a></p>-->
                </div>
                <div class="right">
                	<img src="<?php echo site_url(); ?>assets/img/slide/slide1.png" alt="EduFocal" />
                    <div class="overlay png_bg"></div>
                </div>
            </div>
            <p class="extra">EduFocal is the smart, new study tool for <a>CXC</a> &amp; <a>GSAT</a> Students It is a friendly online community where students have fun while learning. EduFocal is interactive, user-friendly, and students can earn rewards.</p>
            <?php endif; ?>
            
        </div>
        <div class="clearfix"></div>
    </header>
    
			<?php //echo Template::message(); ?>
			<?php echo Template::yield(); ?>
    
    <footer>
    	<div class="container">
        	<section class="copyright">
                <p>Copyright <?php echo date('Y'); ?> EduFocal</p>
                <?php if(is_production()) : ?>
                <!-- webbot bot="HTMLMarkup" startspan -->
                <!-- GeoTrust True Site [tm] Smart Icon tag. Do not edit. -->
                <script type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
                <!-- end GeoTrust Smart Icon tag -->
                <!-- webbot bot="HTMLMarkup" endspan -->
                <?php endif; ?>
            </section>
            <nav>
            	<ul>
                	<li><a href="<?php echo site_url(); ?>">Overview</a></li>
                    <!--<li><a href="<?php echo site_url('browse'); ?>">Browse Subjects</a></li>-->
                    <li><a href="<?php echo site_url('signup'); ?>">Pricing &amp; Signup</a></li>
                </ul>
                <ul>
                	<li><a href="http://blog.edufocal.com">EduFocal Blog</a></li>
                    <?php $pages = Page::find_all_by_status('publish');
					foreach($pages as $page) : ?>
                    <li><a href="<?php echo site_url('page/'.$page->permalink); ?>"><?php echo $page->title; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </nav>
            <ul class="social">
                <li><a href="http://blog.edufocal.com" class="blog">EduFocal Blog</a></li>
                <li><a href="http://blog.edufocal.com/feed/" class="rss">RSS</a></li>
                <li><a href="https://www.facebook.com/EduFocal" class="facebook">Facebook</a></li>
                <li><a href="http://twitter.com/EduFocal" class="twitter">Twitter</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </footer>

  <!--[if lt IE 7 ]>
    <script src="assets/js/libs/dd_belatedpng.js"></script>
    <script> DD_belatedPNG.fix('img, .png_bg'); </script>
  <![endif]-->

    <?php if(is_development() || is_testing()) : ?>
	<div id="debug"></div>
    <?php endif; ?>
	
    <?php echo Assets::external_js(null) ?>
    <?php echo Assets::module_js() ?>

    <script type="text/javascript">
        $(function() {

        $('#top_student a').click(function() {
            var tab = $(this).attr("href");
            $('#top_student .tab li').removeClass('active');
            $(this).parent().addClass('active');
            $('#top_student .tab_body').hide();
            $(tab).show();
            return false;
        });

        $('#top_teacher a').click(function() {
            var tab = $(this).attr("href");
            $('#top_teacher .tab li').removeClass('active');
            $(this).parent().addClass('active');
            $('#top_teacher .tab_body').hide();
            $(tab).show();
            return false;
        });

        $('.login_btn').click(function() {
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#login').hide();
            } else {
                $(this).addClass('active');
                $('#login').show();
                $('#login input:first').focus();
            }
            return false;
        });

        $('#global_login').submit(function(){
            $('#global_login_ball').show();
            $('#global_login_error').hide();
            $.post('<?php echo site_url(); ?>sessions/login', $('#global_login').serialize(), function(data){

                if ( data.code != 1 )
                {
                    $('#global_login_error').show();
                    $('#global_login_ball').hide();
                }
                else
                {
                    window.location = '<?php echo site_url('dashboard'); ?>';
                }
            });
            return false;
            });

            $('.close a').click(function() {
                $("#warning").fadeOut();
                return false;
            });

        });
    </script>

	<?php echo Assets::inline_js(); ?>

    <?php if(is_production()) : ?>
    
    <script src="//static.getclicky.com/js" type="text/javascript"></script> 
	<script type="text/javascript">try{ clicky.init(66373717); }catch(e){}</script> 
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66373717ns.gif" /></p></noscript>
    
    <script type="text/javascript">
	setTimeout(function(){var a=document.createElement("script");
	var b=document.getElementsByTagName('script')[0];
	a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0012/6023.js?"+Math.floor(new Date().getTime()/3600000);
	a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
	</script>

    <?php endif; ?>
	  
</body>
</html>
