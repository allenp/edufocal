<?php
    Assets::add_css(array('flow.css'));
?>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="description" content="EduFocal and Flow Partnership" >
  <meta name="author" content="EduFocal" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="<?php echo site_url(); ?>assets/favicon.ico" />
  <link rel="apple-touch-icon" href="<?php echo site_url(); ?>assets/apple-touch-icon.png" />
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
  <?php print Assets::css(null, 'all', true); ?>
  <title>EduFocal and Flow Partnership</title>
</head>
<body class="flow">
    <div class="container">
        <div class="section">
            <h1 class="title">Flow and EduFocal Partnership</h1>
            <header>
            <h2>Hello Flow Customer!</h2>
            <p>EduFocal is an online social learning service for <span class="level">GSAT</span> and <span class="level">CSEC</span> students. We borrow elements from video games to make learning fun.</p>
            </header>
            <img src="/assets/img/csec-students-group-shot.jpg" alt="CSEC Students" />
            <div class="testimonials">

                <div class="testimony cf">
                    <img src = "/assets/img/testimonials/ronald.jpg" alt="Ronald Thwaites" />
                    <div class="envelope">
                        <blockquote>Persons in the education industry are tired of the same conventional ineffective methods of getting information across to students. EduFocal presents a refreshing method of incorporating learning with play."
                        </blockquote>
                        <div class="name">Hon. Rev. Ronald Thwaites</div>
                        <div class="from">Minister of Education, Jamaica</div>
                    </div>
                </div>

                <div class="testimony cf">
                    <img src = "/assets/img/testimonials/warren.jpg" alt="Warren Cassell Jr." />
                    <div class="envelope">
                        <blockquote>Unlike most learning platforms, EduFocal makes revising a non-monotonous and enjoyable task. Not only has preparing for exams with EduFocal's practice questions boosted my confidence but also my overall performance."</blockquote>
                        <div class="name">Warren Cassell Jr.</div>
                        <div class="from">Secondary student</div>
                    </div>
                </div>

                <div class="testimony cf">
                    <img src = "/assets/img/shantol.jpg" alt="Shantol Barton" />
                    <div class="envelope">
                        <blockquote>EduFocal has helped me to prepare myself for the CSEC exam in one of the most outstanding ways. I will be sitting four subjects this year in fourth form and I am way ahead in my studies. I've excelled in all four subject areas as a result of EduFocal."</blockquote>
                        <div class="name">Shantol Barton</div>
                        <div class="from">Secondary student</div>
                    </div>
                </div>

            </div>
            <div class="call-to-action">
                <a href="/signup"><img src="/assets/img/edufocal-flow-signup.png" alt="Sign up for EduFocal" /></a>
                <p>Please click the button above to sign up to EduFocal with the code that you received from Flow.</p>
            </div>
        </div>
    </div>

	<?php if(!is_production()) : ?>
    <script src="/assets/js/jquery-1.10.2.min.js"></script>
    <?php endif; ?>

    <?php echo Assets::external_js(null) ?>
    <?php echo Assets::module_js() ?>
    <?php print Assets::inline_js() ?>

    <?php if(is_production()) : ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//static.getclicky.com/js" type="text/javascript"></script> 
	<script type="text/javascript">try{ clicky.init(66373717); }catch(e){}</script> 
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/66373717ns.gif" /></p></noscript>
    
    <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
    <script type="text/javascript">
    FreshWidget.init("", {"queryString": "&widgetType=popup", "widgetType": "popup", "buttonType": "text", "buttonText": "Support", "buttonColor": "white", "buttonBg": "#B7009E", "alignment": "4", "offset": "235px", "formHeight": "500px", "url": "https://edufocal.freshdesk.com"} );
    </script>  
    <?php endif; ?>
	  
</body>
</html>
