FILES=*.mp4
for FILENAME in $FILES
do
#FILENAME=$1
MP4=${FILENAME%.*}640x360.mp4
WEBM=${FILENAME%.*}640x360.webm

ffmpeg -y -i ${FILENAME} \
       -filter:v scale=640:360,setsar=1/1 -pix_fmt yuv420p \
       -c:v libx264 -preset:v slow -profile:v baseline \
       -x264opts level=3.0:ref=1 -b:v 700k -r:v 29/1 -force_fps \
       -movflags +faststart -c:a libfdk_aac -b:a 80k -pass 1 ${MP4}

#ffmpeg -y -i ${FILENAME} \
#    -filter:v scale=640:360,setsar=1/1 -pix_fmt yuv420p \
#    -vpre libvpx-720p -b:v 500k -r:v 29/1 -force_fps \
#    -c:a libvorbis -b:a 80k -pass 1 ${WEBM}

ffmpeg -i ${FILENAME} \
     -filter:v scale=640:360,setsar=1/1 -pix_fmt yuv420p \
    -c:v libvpx -minrate 500k \
    -maxrate 700k -b:v 500k \
    -c:a libvorbis ${WEBM}

done
