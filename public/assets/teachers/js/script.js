function filebrowser(field_name, url, type, win) {
	
	var dir = 'editor';
	if($("#permalink").length > 0) dir = $("#permalink").val();
	
	fileBrowserURL = "/assets/pdw_file_browser/index.php?editor=tinymce&filter=" + type + '&directory=' + dir;
	
	tinyMCE.activeEditor.windowManager.open({
		file : fileBrowserURL,
		title : 'File Browser',
		width : 950,  // Your dimensions may differ - toy around with them!
		height : 650,
		resizable : "no",
		inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
		close_previous : "no"
	}, {
		window : win,
		input : field_name
	});
}

$(function() {
	$('#top_student a').click(function() {
		var tab = $(this).attr("href");
		$('#top_student .tab li').removeClass('active');
		$(this).parent().addClass('active');
		$('#top_student .tab_body').hide();
		$(tab).show();
		return false;
	});
	
	$('.editor').tinymce({
		script_url : '/assets/tiny_mce/tiny_mce.js',

		theme : "advanced",
		skin : "default",
		skin_variant : "default",
		plugins : "wordcount,style,layer,table,advimage,advlink,inlinepopups,preview,media,searchreplace,paste,directionality,fullscreen,noneditable,xhtmlxtras,advlist,equation,spellchecker",

		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,equation,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,spellchecker,fullscreen,pastetext",
		theme_advanced_buttons2 : "tablecontrols,|,image",
		theme_advanced_buttons3 : "pasteword",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resize_horizontal : false,
		width : 445,
		theme_advanced_resizing : true,
		spellchecker_languages : "+English=en",
		file_browser_callback : "filebrowser",
		relative_urls : false,
		remove_script_host : false,
        paste_auto_cleanup_on_paste : true,
	});
});
