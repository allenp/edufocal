jQuery(function($) {

    events = {
         '#classrooms'        : 'classrooms.create'
    };

    $.each(events, function(target, event_list) {
        $(document).on(event_list, function(e, response) {
            $(target).html(response.data.html);
            $dialog = $('#dialog');
            if ($dialog.hasClass('in')) {
                $dialog.modal('hide');
            }
        });
    });

});
