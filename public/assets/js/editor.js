﻿jQuery(function($) {

    var loading ='<p style="text-align: center; margin-top: 2em;"><img src="/assets/img/loading.gif" alt="loading" /> <br /> Please wait. Doing stuff...</p>';

    function getLoading() {
        return loading;
    }

    $(document).on('click', '[data-dismiss="panel"]', function(event) {
        var $button = $(this);
        var target = $button.attr('data-target');
        $(target).remove();
    });

    $(document).on('click', '[data-hide="panel"]', function(event) {
        var $button = $(this);
        var target = $button.attr('data-target');
        $(target).hide();
    });

    $(document).on('click', 'a[data-async]', function(event) {
        event.preventDefault();
        var $link = $(this);
        var target = $link.attr('data-target');
        $(target).html(getLoading());
        $(target).load($link.attr('href'));
    });

    $(document).on('change', 'label.code > input[type="checkbox"]', function(event) {
        var self = $(this);
        var hash = $('#hash').val();
        var token_name = $('#token_name').val();

        data = { id : $(this).data('tag-id'), tag: this.checked ? 1 : 0 };
        data[token_name] = hash;

        $.ajax({
            type: "POST",
            url: "/managers/codes/tag",
            data: data
        }).done(function() {
            $(self).parent().toggleClass("tagged", self.checked);
        });
    });

    $(document).on('submit', 'form[data-async]', function (event) {
        event.preventDefault();

        var $form = $(this);

        var target = $form.attr('data-target');

        returned = false;

        var subOpts = {
            url: $form.attr('action') + '?_=' + new Date().getTime(),
            type: $form.attr('method') || 'POST',
            cache: false,
            dataTye: 'xml',
            target: null,
            success: function (responseXml) {
                returned = true;
                if (typeof responseXml === 'object') {
                    $(document).trigger(responseXml.broadcast, {data: responseXml});
                } else {
                    $(target).html(responseXml);
                }
            },
            error: function () {
                $(target).html('<p class="error">An error has occured. Refresh the page and try again or contact your support.</p>');
            },
            beforeSubmit: function () {
                window.setTimeout(function () {
                    if (returned == false) {
                        $(target).html(getLoading());
                    }
                }, 200);
            }
        };

        $form.ajaxSubmit(subOpts);

    });

    $(document).on('hidden.bs.modal', '#dialog', function (event) {
        $(this).removeData('bs.modal');
        $(this).find('.modal-content').html(getLoading());
    });
});
