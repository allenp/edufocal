jQuery(function($) {

        var loading ='<p style="text-align: center; margin-top: 2em;"><img src="/assets/img/loading.gif" alt="loading" /> <br /> Please wait. Processing...</p>';

        function getLoading() {
            return loading;
        }

        $(document).on('submit', 'form[data-async]', function (event) {

            event.preventDefault();
            var $form = $(this);
            var target = $form.attr('data-target');
            returned = false;

            var subOpts = {
                url: $form.attr('action') + '?_=' + new Date().getTime(),
                type: $form.attr('method') || 'POST',
                cache: false,
                dataTye: 'xml',
                target: null,
                success: function (responseXml) {
                    returned = true;
                    if (typeof responseXml === 'object') {
                        $(document).trigger(responseXml.broadcast, {data: responseXml});
                    } else {
                        if (target == '#dialog') {
                            $(target).find('.modal-content').html(responseXml);
                        } else {
                            $(target).html(responseXml);
                        }
                    }
                },
                error: function () {
                   $(target).find('.modal-content').html('<p class="error">An error has occured. Try again or contact your administrator.</p>');
                },
                beforeSubmit: function () {
                  window.setTimeout(function () {
                      if (returned == false) {
                          if (target == '#dialog') {
                              $(target).find('.modal-content').html(getLoading());
                          } else {
                              $(target).html(getLoading());
                          }
                      }
                  }, 200);
              }
            };

        $form.ajaxSubmit(subOpts);
    });

    $(document).on('hidden.bs.modal', '#dialog', function (event) {
        $(this).removeData('bs.modal');
        $(this).find('.modal-content').html(getLoading());
    });
});
