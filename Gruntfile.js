module.exports = function (grunt) {

    // Project Configuration
    grunt.initConfig({
         pkg: grunt.file.readJSON("package.json")

         // Uglify JS
        ,uglify: {
             options: { }
            ,dist: {
                files: {
                    "public/assets/js/edufocal.min.js": ["public/assets/js/main.js"]
                     // "public/js/stream.min.js" : ["lib/Stream.js", "lib/Stream.Github.js", "lib/Stream.Twitter.js", "lib/Stream.Tumblr.js"]
                }
            }
        }

        // Compile LESS
        ,less: {
            development: {
                options: {
                    compress: true
                }
                ,files: {
                    "public/assets/css/edufocal.min.css": "public/assets/less/edufocal.less"
                }
            }
        }

        // Watch Directories / Files
        ,watch: {
            files: [
                "public/assets/js/*.js"
                , "!public/assets/js/*.min.js"
                , "public/assets/less/*.less"
                , "public/assets/css/*.css"
                , "!public/assets/css/*.min.css"
            ],
            tasks: ["default"]
        }
    });

    // Load the plugin that provides the "uglify" task
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-watch");

    // Default tasks
    grunt.registerTask("default", ["uglify", "less"]);
};